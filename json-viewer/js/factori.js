
function mkCircle(v , id, success, sym, maxValue) {
    var color;
    var symbol ;
    var maxV;
    if (maxValue == undefined) { maxV = 100 } else { maxV = maxValue }
    if (sym == undefined) { symbol = "%" } else { symbol = sym };
    if (success) { color = "darkgreen" } else { color= "#b71616"} ;
    var myCircle = Circles.create({
        id:                  id,
        radius:              40,
        value:               v,
        maxValue:            maxV,
        width:               10,
        text:                function(value){ return value + "" + symbol;},
        colors:              ['#D3B6C6', color],
        duration:            400,
        wrpClass:            'circles-wrp',
        textClass:           'factori-stats-circle-text-success',
        valueStrokeClass:    'circles-valueStroke',
        maxValueStrokeClass: 'circles-maxValueStroke',
        styleWrapper:        true,
        styleText:           true
    });
}

function failed_only(id, col) {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(id);
    table = document.getElementById("result-table");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[col];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (!input.checked || txtValue.toUpperCase().includes("FAILED") ) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function search() {
    // Declare variables
    var td ;
    var txtValue;
    var i;
    const input = document.getElementById("myInput") ;
    const filter = input.value.toUpperCase();
    const table = document.getElementById("result-table") ;
    const tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1 ) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function mkLink(kt1, txt, log) {
    return '<a href="logs/'+kt1+'_'+log+'.html">'+txt+'</a>';
}

function mkLogs(cell, import_exit_code, exit_code, status, kt1, log) {
    var txt ;
    if (exit_code == 0) {
        txt = '<span class="badge badge-pill badge-success">'+status+': '+exit_code+'</span>'
    } else if( import_exit_code == 143) {
        txt = '<span class="badge badge-pill badge-danger">'+status+': timeout</span>'
    } else {
        txt = '<span class="badge badge-pill badge-danger">'+status+': '+exit_code+'</span>'
    }
    if (exit_code == 0 || import_exit_code == 143) {
        cell.innerHTML = txt;
    } else {
        cell.innerHTML = txt + ' ' + mkLink(kt1, '<i class="bi bi-file-earmark-check"></i>', log);
    }
}

function mkRow(result_json,mainnet_kt1_json) {
    var table = document.getElementById("result-table") ;
    fetch(result_json)
        .then(res => res.json()) // the .json() method parses the JSON response into a JS object literal
        .then(function (data) {
            var success_import = 0;
            var failure_import = 0;
            var success_ocaml_compilation = 0;
            var failure_ocaml_compilation = 0;
            var success_ts_compilation = 0 ;
            var failure_ts_compilation = 0 ;
            var success_ocaml_test = 0 ;
            var failure_ocaml_test = 0 ;
            var success_ts_test = 0 ;
            var failure_ts_test = 0 ;
            data.forEach(function (e) {
                if (e.import_exit_code == 0) { success_import++ } else { failure_import++};
                if (e.ocaml_compilation_exit_code == 0) { success_ocaml_compilation++ } else { failure_ocaml_compilation++};
                if (e.typescript_compilation_exit_code == 0) { success_ts_compilation++ } else { failure_ts_compilation++};
                if (e.test_ocaml_exit_code == 0) { success_ocaml_test++ } else { failure_ocaml_test++};
                if (e.test_ts_exit_code == 0) { success_ts_test++ } else { failure_ts_test++};
                var h = document.getElementById('nb-results');
                h.innerHTML= data.length;
                var row = table.insertRow();
                if (e.import_exit_code != 0) { row.classList.add('table-danger') } ;
                var cell = row.insertCell();
                cell.innerHTML ='<span title="'+e.hash+'">#'+e.hash.substring(0,10)+'</span>';
                cell = row.insertCell();
                cell.innerHTML = e.occurence;
                cell = row.insertCell();
                cell.innerHTML =      '<a href="https://tzkt.io/'+e.kt1+'/code">'+e.kt1+'</a>';
                cell = row.insertCell();
                mkLogs(cell, e.import_exit_code, e.import_exit_code, e.import_status, e.kt1, "import");

                cell = row.insertCell();
                mkLogs(cell, e.import_exit_code, e.ocaml_compilation_exit_code, e.ocaml_compilation_status, e.kt1, "ocaml_compilation") ;

                cell = row.insertCell();
                mkLogs(cell, e.import_exit_code, e.test_ocaml_exit_code, e.test_ocaml_status, e.kt1, "ocaml_test") ;

                cell = row.insertCell();
                mkLogs(cell, e.import_exit_code, e.typescript_compilation_exit_code, e.typescript_compilation_status, e.kt1, "typescript_compilation");

                cell = row.insertCell();
                mkLogs(cell, e.import_exit_code, e.test_ts_exit_code, e.test_ts_status, e.kt1, "typescript_test");

                cell = row.insertCell();
                cell.innerHTML = e.network;
                if (e.import_exit_code == 0 &&
                    (e.ocaml_compilation_exit_code != 0//  ||
                     // e.typescript_compilation_exit_code != 0
                    )) {
                    row.classList.add('table-warning') } ;
                if ((e.import_exit_code == 0 &&
                     (e.ocaml_compilation_exit_code == 0 &&
                      e.typescript_compilation_exit_code == 0) &&
                     (e.test_ocaml_exit_code != 0 // ||
                      // e.test_ts_exit_code !=0
                     ))) {
                    row.classList.add('table-info') } ;
            })
            fetch(mainnet_kt1_json)
                .then(res => res.json()) // the .json() method parses the JSON response into a JS object literal
                .then(function (res) {
                    // var nb_total = document.getElementById('nb-total') ;
                    // nb_total.innerH(success_import / data.length * 100).toFixed(2) + '%'ltal');
                    mkCircle(data.length, 'nb-processed', true, "", res.length);
                    mkCircle(res.length, 'nb-total', true, "", res.length);
                });

            mkCircle((success_import / data.length * 100).toFixed(2), 'nb-success-import', true);
            mkCircle((failure_import / data.length * 100).toFixed(2), 'nb-failure-import', false);
            mkCircle((success_ocaml_compilation / data.length * 100).toFixed(2), 'nb-success-ocaml-compilation', true);
            mkCircle((failure_ocaml_compilation / data.length * 100).toFixed(2), 'nb-failure-ocaml-compilation', false);
            mkCircle((success_ts_compilation / data.length * 100).toFixed(2), 'nb-success-ts-compilation', true);
            mkCircle((failure_ts_compilation / data.length * 100).toFixed(2), 'nb-failure-ts-compilation', false);
            mkCircle((success_ocaml_test / data.length * 100).toFixed(2), 'nb-success-ocaml-test', true);
            mkCircle((failure_ocaml_test / data.length * 100).toFixed(2), 'nb-failure-ocaml-test', false);
            mkCircle((success_ts_test / data.length * 100).toFixed(2), 'nb-success-ts-test', true);
            mkCircle((failure_ts_test / data.length * 100).toFixed(2), 'nb-failure-ts-test', false);
        });
}

function getVersion(version_json) {
    fetch(version_json)
        .then(res => res.json()) // the .json() method parses the JSON response into a JS object literal
        .then(function (data) {
            var v = document.getElementById('version');
            v.innerHTML = data.version;
        })
}

function sortTable() {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("result-table");
    switching = true;
    /*Make a loop that will continue until
      no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
          first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
              one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[1];
            y = rows[i + 1].getElementsByTagName("TD")[1];
            //check if the two rows should switch place:
            if (Number(x.innerHTML) < Number(y.innerHTML)) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
              and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function main () {
    mkRow('result.json', 'mainnet-kt1.json');
    getVersion('version.json');
}

main ();
