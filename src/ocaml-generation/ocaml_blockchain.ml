let blockchain_ml =
  {|(* For now this file is a copy of "new_scenario_toolkit" from
   spice_tezos. This is for development purposes; eventually ginger
   should be directly and automatically used with the generated
   code. *)

open Tzfunc
open Proto
open Crypto

(* let script_of_contract ~filename =
 *   let bytes =
 *     let ic = Unix.open_process_in cmd in
 *     let s = read ic in
 *     close_in ic;
 *     s in
 *   Tzfunc.Proto.(Bytes (Crypto__.H.mk bytes)) *)

type identity = {sk : string; pk : A.pk; pkh : A.pkh}

let str_of_identity id =
  Format.sprintf "{\nsk : %s;\npk : %s;\npkh : %s\n}" id.sk id.pk id.pkh

(* Traditional bootstrap1, bootstrap2 and bootstrap3 of the Tezos sandbox *)

let bootstrap1 : identity =
  {
    pkh = "tz1id1nnbxUwK1dgJwUra5NEYixK37k6hRri";
    pk = "edpkv2Gafe23nz4x4hJ1SaUM3H5hBAp5LDwsvGVjPsbMyZHNomxxQA";
    sk = "edsk4LhLg3212HzL7eCXfCWWvyWFDfwUS7doL4JUSmkgTe4qwZbVYm";
  }

let bootstrap2 : identity =
  {
    pkh = "tz1Tny451rJY5vqUs7JHoYpJHdMH5znNxuxw";
    pk = "edpkuW2GZePnF1rt3XrM533PYaRiXPwaHzCpHQSbsNzAds1SVXbhkm";
    sk = "edsk4DDEDf3VrBYYuhVgyjfP3VC1RVX7ZJHAsCMKV511U1a7nNxJdH";
  }

let bootstrap3 : identity =
  {
    pkh = "tz1Zebr6mNxTPTsbfkvNjAJTmaxGMFbqmhc9";
    pk = "edpku8ehu8waYZKfjvLWwJ6Pv7UuPvdKgszknNnUVj7MTyf4R9tyZA";
    sk = "edsk2z8cbf8LGkp1LsD8k7EBDYEMjFvWFv7EY3edr3cyGxzu4fyfqk";
  }

(* Flextesa agents: Alice and Bob *)

let bob_flextesa : identity =
  {
    pkh = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6";
    pk = "edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4";
    sk = "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt";
  }

let alice_flextesa : identity =
  {
    pkh = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb";
    pk = "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn";
    sk = "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq";
  }

let canonical_burn_address = "tz1burnburnburnburnburnburnburjAYjjX"

let null_address = "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"

let get_identity (seed : string) : identity =
  let better_seed = String.init 32 (fun i -> String.get seed (i mod String.length seed)) in
  let sk = Tzfunc.Crypto.Sk.T.mk @@ Raw.mk better_seed in
  let pk = Crypto.Sk.to_public_key sk in
  let curve = `ed25519 in
  let pkh = Pkh.b58enc ~curve @@ Pk.hash pk in
  let sk = Sk.b58enc ~curve sk in
  let pk = Pk.b58enc ~curve pk in
  {pkh; sk; pk}

let generate_identity () =
  let curve = `ed25519 in
  let pk, sk = Ed25519.keypair () in
  let pkh = Pkh.b58enc ~curve @@ Pk.hash pk in
  {sk = Sk.b58enc ~curve sk; pk = Pk.b58enc ~curve pk; pkh}

(* code and storage can be either:
   Bytes (H.mk "00a5ee...")
   Micheline (Mprim { prim="Pair"; annots=[]; args=[...] }).
   Mprim ... can be retrieved from a json file using
   EzEncoding.destruct micheline_enc.Encoding.json json_string *)

let sandbox_node = "http://127.0.0.1:18731"

let flextesa_node = "http://localhost:20000"

let ithaca_node = "https://ithacanet.tezos.marigold.dev"

let ithacanet_node = ithaca_node

let ghostnet_node = "https://ghostnet.tezos.marigold.dev"

let jakarta_node = "https://jakartanet.ecadinfra.com"

let jakartanet_node = jakarta_node

let mainnet_node = "https://tz.functori.com"

let default_node = flextesa_node

let get_node network = match network with
  | "sandbox" -> sandbox_node
  | "flextesa" -> flextesa_node
  | "ithaca" -> ithaca_node
  | "ghostnet" -> ghostnet_node
  | "jakarta" -> jakarta_node
  | "mainnet" -> mainnet_node
  | _ -> default_node

let get_default_identity = function
  | "ithacanet" | "ghostnet" | "flextesa" -> alice_flextesa
  | "sandbox" -> bootstrap1
  | n -> failwith (Format.sprintf "No default identity for %s" n)

let default_base = EzAPI.BASE default_node

let flextesa_base = EzAPI.BASE flextesa_node

let ithaca_base = EzAPI.BASE ithaca_node

let ghostnet_base = EzAPI.BASE ghostnet_node

let jakarta_base = EzAPI.BASE jakarta_node

let main_base = EzAPI.BASE mainnet_node

let inject_no_preapply ?base ~sign bytes =
  let open Tzfunc.Rp in
  let>? signature_bytes = sign bytes in
  match Binary.Writer.concat [Ok bytes; Ok signature_bytes] with
  | Error e -> Lwt.return_error e
  | Ok s -> Tzfunc.Node.silent_inject ?base s

let assert_static_success ?(prefix="") ?(msg = "") v =
  let n = String.length (prefix^msg) in
  let spaces = String.make (max 0 (80 - n)) ' ' in
  Format.eprintf "@.%s[%s]%s*****TEST SUCCESS*****@." prefix msg spaces;
  v

let assert_static_failure ?(prefix="") ?(msg = "") v =
  let n = String.length (prefix^msg) in
  let spaces = String.make (max 0 (80 - n)) ' ' in
  Format.eprintf "@.%s[%s]%s*****TEST FAILURE*****@." prefix msg spaces;
  v

let assert_success ?(prefix="") ?(msg = "") f x =
  let open Tzfunc.Rp in
  let> res = f x in
  match res with
  | Ok v -> Lwt.return_ok @@ assert_static_success ~prefix ~msg (Some v)
  | _ -> Lwt.return_ok @@ assert_static_failure ~prefix ~msg None

let assert_failure ?(prefix="") ?(msg = "") f x =
  let open Tzfunc.Rp in
  let> res = f x in
  match res with
  | Error _ -> Lwt.return_ok @@ assert_static_success ~prefix ~msg (Some ())
  | _ -> Lwt.return_ok @@ assert_static_failure ~prefix ~msg None

let assert_failwith_str ?(prefix="") ~msg ~expected f x =
  Lwt.bind (f x) @@ function
  | Ok (_, _, _, _) -> let msg = Format.sprintf "[failure expected, but operation was successful] %s" msg in Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
  | Error (`node_error l as _e) ->
    let open Json_encoding in
    let rec aux = function
      | [] ->
        Format.eprintf
          "%s[assert_failwith] decoding failed on all errors: %s@."
          prefix
          (Tzfunc.Rp.string_of_error _e) ;
        Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
      | e :: l -> (
        try
          let actual_fail =
            Json_encoding.destruct
              (EzEncoding.ignore_enc
                 (obj1
                    (req
                       "with"
                       (EzEncoding.ignore_enc (obj1 (req "string" string))))))
              e.err_info in
          if actual_fail <> expected then (
            Format.eprintf
              "%s[assert_failwith] expected %s, got %s%!"
              prefix
              expected
              actual_fail ;
            Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
          ) else (
            Format.eprintf "%s[assert_failwith] got %s as expected%!" prefix actual_fail ;
            Lwt.return_ok @@ assert_static_success ~prefix ~msg ()
          )
        with _ -> aux l) in
    aux l
  | Error _e ->
    Format.eprintf
      "%s[assert_failwith] Not a node error: %s@."
      prefix
      (Tzfunc.Rp.string_of_error _e) ;
    Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()

let assert_failwith_str_list ?(prefix="") ~msg ~expected f x =
  Lwt.bind (f x) @@ function
  | Ok (_, _, _, _) -> Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
  | Error (`node_error l as _e) ->
    let open Json_encoding in
    let rec aux = function
      | [] ->
        Format.eprintf
          "%s[assert_failwith] decoding failed on all errors: %s@."
          prefix (Tzfunc.Rp.string_of_error _e) ;
        Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
      | e :: l -> (
        try
          let actual_fail =
            Json_encoding.destruct
              (EzEncoding.ignore_enc
                 (obj1
                    (req
                       "with"
                       (EzEncoding.ignore_enc (obj1 (req "string" string))))))
              e.err_info in
          if not (List.mem actual_fail expected) then (
            Format.eprintf "%s[assert_failwith] expected " prefix ;
            Format.pp_print_list
              ~pp_sep:(fun format () -> Format.fprintf format " or ")
              (fun format elt -> Format.fprintf format "@[%s@]" elt)
              Format.err_formatter
              expected ;
            Format.eprintf " ,but got %s%!" actual_fail ;
            Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
          ) else (
            Format.eprintf "%s[assert_failwith] got %s as expected%!" prefix actual_fail ;
            Lwt.return_ok @@ assert_static_success ~prefix ~msg ()
          )
        with _ -> aux l) in
    aux l
  | Error _e ->
    Format.eprintf
      "[assert_failwith] Not a node error: %s@."
      (Tzfunc.Rp.string_of_error _e) ;
    Lwt.return_ok @@ assert_static_failure ~msg ()


let assert_failwith_generic ?(prefix="") ~enc ~msg ~expected f x =
  Lwt.bind (f x) @@ function
  | Ok (_, _, _, _) -> let msg = Format.sprintf "[failure expected, but operation was successful] %s" msg in Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
  | Error (`node_error l as _e) ->
    let open Json_encoding in
    let rec aux = function
      | [] ->
        Format.eprintf
          "%s[assert_failwith] decoding failed on all errors: %s@."
          prefix
          (Tzfunc.Rp.string_of_error _e) ;
        Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
      | e :: l -> (
        try
          let actual_fail =
            Json_encoding.destruct
              (EzEncoding.ignore_enc
                 (obj1
                    (req
                       "with"
                       enc)))
              e.err_info in
          if actual_fail <> expected then (
            Format.eprintf
              "%s[assert_failwith] expected %s, got %s%!"
              prefix
              (EzEncoding.construct enc expected)
              (EzEncoding.construct enc actual_fail) ;
            Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()
          ) else (
            Format.eprintf "%s[assert_failwith] got %s as expected%!" prefix (EzEncoding.construct enc actual_fail) ;
            Lwt.return_ok @@ assert_static_success ~prefix ~msg ()
          )
        with _ -> aux l) in
    aux l
  | Error _e ->
    Format.eprintf
      "%s[assert_failwith] Not a node error: %s@."
      prefix
      (Tzfunc.Rp.string_of_error _e) ;
    Lwt.return_ok @@ assert_static_failure ~prefix ~msg ()

let get_balance ?(base = default_base) ~addr () =
  let open Tzfunc.Rp in
  let>? acc = Tzfunc__Node.get_account_info ~base addr in
  let bal = acc.ac_balance in
  Lwt.return_ok bal

let get_storage ?(base = default_base) ?(debug = false) kt1 decode =
  let open Tzfunc.Rp in
  let>? storage = Tzfunc.Node.get_storage ~base kt1 in
  if debug then
    Format.eprintf
      "Storage: %s\n%!"
      (EzEncoding.construct
         Tzfunc.Proto.script_expr_enc.json
         (Micheline storage)) ;
  Lwt.return_ok @@ decode storage
(*   match storage with
 * | Micheline m -> Lwt.return_ok @@ decode m
 * | _ -> failwith "storage is not micheline" *)

let make_transfer ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ?(counter = Z.zero) ?(node = sandbox_node)
    ~from ~dst () =
  let open Tzfunc.Rp in
  let op =
    {
      man_info =
        {
          source = from.pkh;
          kind = Transaction {amount; destination = dst; parameters = None};
        };
      man_numbers = {fee; gas_limit; storage_limit; counter};
      man_metadata = None;
    }
  in

  let sign b = Node.sign ~edsk:from.sk b in
  let base = EzAPI.BASE node in
  let>? _ = Node.set_constants ~base () in
  let _ = Node.set_node node in
  let get_pk () = Lwt.return_ok from.pk in
  let>? bytes, protocol, branch, ops =
    Node.forge_manager_operations ~forge_method:`local ~base ~get_pk [op]
  in
  let>? hash = Node.inject ~base ~sign ~bytes ~branch ~protocol ops in
  let>? () = Utils.wait_next_block ~base () in
  let>? () = Utils.wait_next_block ~base () in
  Lwt.return_ok hash

let do_empty_account ~node ~src ~dst =
  let open Tzfunc.Rp in
  let base = EzAPI.BASE node in
  let sign b = Node.sign ~edsk:src.sk b in
  let get_pk () = Lwt.return_ok src.pk in
  let>? bytes, ops, branch, protocol  =
    Node.forge_empty_account ~get_pk ~base ~src:src.pkh dst in
  let>? hash = Node.inject ~base ~sign ~bytes ~branch ~protocol ops in
  let>? () = Utils.wait_next_block ~base () in
  let>? () = Utils.wait_next_block ~base () in
  Lwt.return_ok hash

let do_empty_account_alt ~node ~src ~dst =
  let open Tzfunc.Rp in
  let>? bal_before = get_balance ~base:(EzAPI.BASE node) ~addr:src.pkh () in
  if bal_before <= Int64.of_int 500 then Lwt.return_ok "balance too low"
  else
    (
      let fee = Int64.of_int 500 in
      let amount = Int64.sub bal_before fee in
      Format.eprintf "Transferring %s from %s@." (Int64.to_string amount) src.pkh;
      let>? _transfer = make_transfer ~fee ~node ~amount ~from:src ~dst:dst () in
      let>? bal = get_balance ~base:(EzAPI.BASE node) ~addr:src.pkh () in
      Format.eprintf "New balance: %s@." (Int64.to_string bal);
      Lwt.return_ok (_transfer)
)


let make_mass_transfer_ops ?(fee = -1L)
    ?(gas_limit = Z.minus_one) ?(storage_limit = Z.minus_one)
    ?(counter = Z.zero) ~from (dsts : (string * int64) list) =
  List.mapi
    (fun i (dst,amount) ->
      {
        man_info =
          {
            source = from.pkh;
            kind = Transaction {amount; destination = dst; parameters = None};
          };
        man_numbers =
          {fee; gas_limit; storage_limit; counter = Z.(counter + of_int i)};
        man_metadata = None;
      })
    dsts

let make_mass_transfer ?(node = sandbox_node) ~from dsts =
  let open Tzfunc.Rp in
  let ops = make_mass_transfer_ops ~from dsts in
  let base = EzAPI.BASE node in
  let sign b = Node.sign ~edsk:from.sk b in
  let>? _ = Node.set_constants ~base () in
  let _ = Node.set_node node in
  let get_pk () = Lwt.return_ok from.pk in
  let>? bytes, protocol, branch, ops =
    Node.forge_manager_operations ~forge_method:`local ~base ~get_pk ops in
  let>? hash = Node.inject ~base ~sign ~bytes ~branch ~protocol ops in
  let>? () = Utils.wait_next_block ~base () in
  let>? () = Utils.wait_next_block ~base () in
  Lwt.return_ok hash

(* Builds a transaction which empties the input account *)
let empty_account_transaction ?(node = sandbox_node) id =
  let open Tzfunc.Rp in
  let>? bal = get_balance ~base:(EzAPI.BASE node) ~addr:id.pkh () in
  if bal = Int64.zero then Lwt.return_ok [] else
    Lwt.return_ok [id,bal]

let empty_accounts ?(node = sandbox_node) ids =
    let open Tzfunc.Rp in
    let> accounts_to_empty =
      Lwt_list.fold_left_s (fun accu id ->
          let> empty_account_tr = empty_account_transaction ~node id in
          match empty_account_tr with
          | Error e -> failwith (Format.sprintf "[empty_accounts] %s" (Tzfunc.Rp.string_of_error e))
          | Ok tr ->
          Lwt.return (tr@accu)) [] ids in
    Lwt.return accounts_to_empty

let do_empty_accounts ?(node = sandbox_node) ?(sink=alice_flextesa.pkh) ids =
  let open Tzfunc.Rp in
  let> l = empty_accounts ~node ids in
  let catch (type a) (f : unit -> (a, error) result Lwt.t) =
    let> res = f () in
    match res with
    | Ok _res -> Lwt.return ()
    | Error e ->
      (Format.eprintf "[do_empty_accounts] error: %s@." (Tzfunc.Rp.string_of_error e)); Lwt.return ()
  in
  let> _transfers = Lwt.join
      (List.map (fun (id,_amount) ->
           catch @@ fun () -> do_empty_account_alt ~node ~src:id ~dst:sink
         ) l) in
  Lwt.return ()


let inject_existing_transferops ?(debug = false) ?(node = sandbox_node) ~from ops =
  let open Tzfunc.Rp in
  if debug then Format.eprintf "entering inject_existing_transferops\n%!" ;
  let base = EzAPI.BASE node in
  let sign b = Node.sign ~edsk:from.sk b in
  let>? _ = Node.set_constants ~base () in
  let _ = Node.set_node node in
  let get_pk () = Lwt.return_ok from.pk in
  Lwt.bind
    (Node.forge_manager_operations ~forge_method:`local ~base ~get_pk ops)
  @@ function
  | Error e ->
      Format.eprintf
        "[Error in inject_existing_transferops]: %s\nThe operation was %s%!\n"
        (Tzfunc.Rp.string_of_error e)
        (EzEncoding.construct
           (Json_encoding.list (manager_operation_enc script_expr_enc).json)
           ops) ;
      Lwt.return_error e
  | Ok (bytes, protocol, branch, ops) -> (
      if debug then Format.eprintf "Successful forge of transaction\n%!" ;
      Lwt.bind (Node.inject ~base ~sign ~bytes ~branch ~protocol ops)
      @@ function
      | Error e ->
          Format.eprintf
            "[Error at injection of transaction]: %s%!\n"
            (Tzfunc.Rp.string_of_error e) ;
          Lwt.return_error e
      | Ok hash ->
          let>? () = Utils.wait_next_block ~base () in
          let>? () = Utils.wait_next_block ~base () in
          Lwt.return_ok @@ hash)

(*   let>? (bytes, protocol, branch, ops) =
 *   Node.forge_manager_operations ~local_forge:false ~base ~get_pk ops
 * in
 * let>? hash = Node.inject ~base ~sign ~bytes ~branch ~protocol ops in
 * Utils.wait_operation hash @@ function _ ->
 * Lwt.return_ok hash *)

let make_call_op ?(amount = 0L) ?(fee = -1L)
    ?(gas_limit = Z.minus_one) ?(storage_limit = Z.minus_one)
    ?(counter = Z.zero) ~from ~dst param=
    {
      man_info =
        {
          source = from.pkh;
          kind =
            Transaction {amount; destination = dst; parameters = Some param};
        };
      man_numbers = {fee; gas_limit; storage_limit; counter};
      man_metadata = None;
    }


let forge_call_entrypoint ?(debug = true) ?(amount = 0L) ?(fee = -1L)
    ?(gas_limit = Z.minus_one) ?(storage_limit = Z.minus_one)
    ?(counter = Z.zero) ?(node = sandbox_node) ~from ~dst param =
  let open Tzfunc.Rp in
  let get_pk () = Lwt.return_ok from.pk in
  let op = make_call_op ~amount ~fee ~gas_limit ~storage_limit ~counter ~from ~dst param in
  let base = EzAPI.BASE node in
  let>? _ = Node.set_constants ~base () in
  let _ = Node.set_node node in

  if debug then
    Format.eprintf
      "Sending param: %s\n%!"
      (EzEncoding.construct script_expr_enc.json param.value) ;
  (match param.entrypoint with
  | EPnamed _entrypoint ->
      ()
  | _ ->
      ()) ;
    Node.forge_manager_operations ~forge_method:`both ~base ~get_pk [op]


let call_entrypoint ?(debug = true) ?(amount = 0L) ?(fee = -1L)
    ?(gas_limit = Z.minus_one) ?(storage_limit = Z.minus_one)
    ?(counter = Z.zero) ?(node = sandbox_node) ~from ~dst param =
  let open Tzfunc.Rp in
  let op = make_call_op ~amount ~fee ~gas_limit ~storage_limit ~counter ~from ~dst param in
  let base = EzAPI.BASE node in
  let sign b = Node.sign ~edsk:from.sk b in
  Lwt.bind
    (forge_call_entrypoint ~debug ~amount ~fee ~gas_limit ~storage_limit ~counter ~node ~from ~dst param)
  @@ function
  | Error e ->
      Format.eprintf
        "[Error in forge_manager_operations (call_entrypoint)]: %s\n\
         The operation was %s%!\n"
        (Tzfunc.Rp.string_of_error e)
        (EzEncoding.construct (manager_operation_enc script_expr_enc).json op) ;
      Lwt.return_error e
  | Ok (bytes, protocol, branch, ops) -> (
      if debug then Format.eprintf "Successful forge of transaction\n%!" ;
      Lwt.bind (Node.inject ~base ~sign ~bytes ~branch ~protocol ops)
      @@ function
      | Error e ->
          Format.eprintf
            "[Error at injection of transaction]: %s%!\n"
            (Tzfunc.Rp.string_of_error e) ;
          Lwt.return_error e
      | Ok hash ->
          let>? () = Utils.wait_next_block ~base () in
          let>? () = Utils.wait_next_block ~base () in
          Lwt.return_ok @@ hash)

let create_deploy_op ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ?(counter = Z.zero) ~from ~code storage =
  {
    man_info =
      {
        source = from.pkh;
        kind = Origination {balance = amount; script = {code; storage}};
      };
    man_numbers = {fee; gas_limit; storage_limit; counter};
    man_metadata = None;
  }

let deploy_existing_op ?(debug = false) ?(node = sandbox_node) ~from op =
  let open Tzfunc.Rp in
  let get_pk () = Lwt.return_ok from.pk in
  let base = EzAPI.BASE node in
  (* let get_pk () = Lwt.return_ok from.pk in *)
  let sign b = Node.sign ~edsk:from.sk b in
  (* Format.eprintf "Sending code: %s
     and storage %s
     %!" (EzEncoding.construct script_expr_enc.json code) (EzEncoding.construct script_expr_enc.json storage); *)
  let>? _ = Node.set_constants ~base () in
  let _ = Node.set_node node in
  Lwt.bind
    (Node.forge_manager_operations ~forge_method:`local ~base ~get_pk [op])
  @@ function
  | Error e ->
      Format.eprintf
        "[Error in forge_manager_operations (deploy)]: %s\n\
         The operation was %s\n\
         %!"
        (Tzfunc.Rp.string_of_error e)
        (EzEncoding.construct (manager_operation_enc script_expr_enc).json op) ;
      Lwt.return_error e
  | Ok (bytes, protocol, branch, ops) -> (
      if debug then Format.eprintf "Successful forge\n%!" ;
      Lwt.bind (Node.inject ~base ~sign ~bytes ~branch ~protocol ops)
      @@ function
      | Error e ->
          Format.eprintf
            "[Error at injection]: %s%!\n"
            (Tzfunc.Rp.string_of_error e) ;
          Lwt.return_error e
      | Ok hash ->
          let>? () = Utils.wait_next_block ~base () in
          let>? () = Utils.wait_next_block ~base () in
          (* Format.eprintf "Ok hash %s
             %!" hash; *)
          Lwt.return_ok @@ op_to_KT1 hash)
(* let>? bytes,protocol,branch,ops = Node.forge_manager_operations ~local_forge:false ~base ~get_pk [op] in
 * let>? hash = Node.inject ~base ~sign ~bytes ~branch ~protocol ops in
 * Utils.wait_operation hash @@ function _ ->
 * Lwt.return_ok @@ op_to_KT1 hash *)

let deploy ?(debug = false) ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ?(counter = Z.zero) ?(node = sandbox_node)
    ?(name = "No provided name") ~from ~code storage =
  let open Tzfunc.Rp in
  let get_pk () = Lwt.return_ok from.pk in
  let op =
    create_deploy_op
      ~amount
      ~fee
      ~gas_limit
      ~storage_limit
      ~counter
      ~from
      ~code
      storage
  in
  let base = EzAPI.BASE node in
  (* let get_pk () = Lwt.return_ok from.pk in *)
  let sign b = Node.sign ~edsk:from.sk b in
  (* Format.eprintf "Sending code: %s
     and storage %s
     %!" (EzEncoding.construct script_expr_enc.json code) (EzEncoding.construct script_expr_enc.json storage); *)
  let>? _ = Node.set_constants ~base () in
  let _ = Node.set_node node in
  Lwt.bind
    (Node.forge_manager_operations ~forge_method:`local ~base ~get_pk [op])
  @@ function
  | Error e ->
      Format.eprintf
        "[Error in forge_manager_operations (deploy %s)]: %s\n\
         The initial storage was %s\n\
         %!"
        name
        (Tzfunc.Rp.string_of_error e)
        (EzEncoding.construct script_expr_enc.json storage)
      (* (EzEncoding.construct manager_operation_enc.json op) *) ;
      Lwt.return_error e
  | Ok (bytes, protocol, branch, ops) -> (
      if debug then Format.eprintf "Successful forge\n%!" ;
      Lwt.bind (Node.inject ~base ~sign ~bytes ~branch ~protocol ops)
      @@ function
      | Error e ->
          Format.eprintf
            "[Error at injection]: %s%!\n"
            (Tzfunc.Rp.string_of_error e) ;
          Lwt.return_error e
      | Ok hash ->
          if debug then Format.eprintf "Ok hash %s\n%!" hash ;
          let kt1 = op_to_KT1 hash in
          if debug then Format.eprintf "KT1: %s\n%!" kt1 ;
          let>? () = Utils.wait_next_block ~base:(EzAPI.BASE node) () in
          let>? () = Utils.wait_next_block ~base:(EzAPI.BASE node) () in
          Lwt.return_ok @@ (op_to_KT1 hash, hash))
let parallel_calls (f : 'a -> unit) (l : (unit -> ('a, 'b) result Lwt.t) list) =
  let open Tzfunc.Rp in
  let operations = Lwt_list.map_p (fun f -> f ()) l in
  let> operations_hashes = operations in
  List.iter (Result.iter f) operations_hashes;
  Lwt.return (Ok ())

(* let get_storage kt1 = Node.get_storage kt1 *)

(* let check ?(node=default_node) ~contract f =
 *   let base = EzAPI.BASE node in
 *   Lwt.bind (Node.get_account_info ~base contract) @@ function
 *   | Error e -> Lwt.return_error e
 *   | Ok a -> f a *)|}
