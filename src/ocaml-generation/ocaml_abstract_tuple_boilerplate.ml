open Factori_utils

let abstract_tuple ppf i =
  let open Format in
  fprintf ppf "  type (%a) tuple%d = (%a) id_or_concrete"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "'x%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " * ")
       (fun ppf i -> fprintf ppf "'x%d" i))
    (list_integers i)

let lift_abstract_tuple ppf i =
  let open Format in
  fprintf ppf "  let lift_tuple%d %a (%a : %a) = Concrete (%a)" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "lift%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "x%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "*")
       (fun ppf i -> fprintf ppf "'a%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "lift%d x%d" i i))
    (list_integers i)

let abstract_tuple_value_of ppf i =
  let open Format in
  let rec aux ppf = function
    | [] -> failwith "[abstract_tuple_value_of] never happens: empty list"
    | [i] -> fprintf ppf "Single(f%d x%d)" i i
    | i :: l -> fprintf ppf "Multi(f%d x%d,%a)" i i aux l in

  if i = 1 then
    fprintf ppf
      "  let value_of_tuple1 f =\n\
       bind_abstract_value\n\
       (fun a -> VTuple (Single (f a)))"
  else
    fprintf ppf
      "  let value_of_tuple%d %a =\n\
       bind_abstract_value\n\
       (fun (%a) -> VTuple (%a))" i
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf " ")
         (fun ppf i -> fprintf ppf "f%d" i))
      (list_integers i)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf i -> fprintf ppf "x%d" i))
      (list_integers i) aux (list_integers i)

let abstract_tuple_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:1 in
  Format.asprintf "\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> abstract_tuple ppf i))
    (list_integers n)

let abstract_tuple_values_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:1 in
  Format.asprintf "\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> abstract_tuple_value_of ppf i))
    (list_integers n)

let abstract_tuple_lifts_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:1 in
  Format.asprintf "\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> lift_abstract_tuple ppf i))
    (list_integers n)
