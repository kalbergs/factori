open Format
open Factori_errors
(* This file is intended to help read, write and update dune files for
   a factori project *)

type filename = string [@@deriving encoding]

type library = {
  name : string;
  modules : string list;
  libraries : string list;
  implements : string list;
  preprocess : string list;
  preprocessor_deps : string list;
}
[@@deriving encoding]

type executable = {
  name : string;
  modules : string list;
  libraries : string list;
}
[@@deriving encoding]

type rule = {
  alias : string option;
  deps : string list;
  targets : string list;
  action : string list;
}
[@@deriving encoding]

type dune_stanza =
  | Include of (filename[@wrap "include"])
  | Env of (string[@wrap "env"])
  | Library of library [@kind]
  | Executable of executable [@kind]
  | Rule of rule
  | Comment of string list
[@@deriving encoding]

type dune_file = dune_stanza list [@@deriving encoding]

let get_name_of_stanza = function
  | Include _ -> None
  | Env _ -> Some "env"
  | Library l -> Some l.name
  | Executable e -> Some e.name
  | Rule _ -> None
  | Comment _ -> None

let is_library = function
  | Library _ -> true
  | _ -> false

let is_executable = function
  | Executable _ -> true
  | _ -> false

let has_library ~dependencies_matter ~dune_file libname =
  List.exists
    (fun x ->
      match x with
      | Library l -> l.name = libname
      | Executable e ->
        if dependencies_matter then
          List.mem libname e.libraries
        else
          false
      | _ -> false)
    dune_file

let has_executable ~dune_file libname =
  List.exists
    (fun x ->
      match x with
      | Executable e -> e.name = libname
      | _ -> false)
    dune_file

let find_and_replace_library ?(fail_if_doesnt_exist = true) ~dune_file name f =
  if
    fail_if_doesnt_exist
    && not (has_library ~dependencies_matter:false ~dune_file name)
  then
    failwith @@ sprintf "Library %s was not found in dune file" name
  else
    List.map
      (fun x ->
        match x with
        | Library l when l.name = name -> Library (f l)
        | y -> y)
      dune_file

let find_and_replace_executable ?(fail_if_doesnt_exist = true) ~dune_file name f
    =
  if fail_if_doesnt_exist && not (has_executable ~dune_file name) then
    failwith @@ sprintf "Library %s was not found in dune file" name
  else
    List.map
      (fun x ->
        match x with
        | Executable e when e.name = name -> Executable (f e)
        | y -> y)
      dune_file

(* returns "" if list is empty, or (prefix x1 x2 ... xn) *)
let print_list_if_non_empty ?(prefix = "") ?(suffix = "") ppf = function
  | [] -> fprintf ppf ""
  | l ->
    fprintf ppf "(%s %a%s)" prefix
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf " ")
         (fun ppf x -> fprintf ppf "%s" x))
      l suffix

let print_rule ppf = function
  | { alias; deps; targets; action } ->
    fprintf ppf "(rule\n%s%a\n%a\n%a)"
      (match alias with
      | None -> ""
      | Some alias -> Format.sprintf "(alias %s)\n" alias)
      (print_list_if_non_empty ~prefix:"deps" ~suffix:"")
      deps
      (print_list_if_non_empty ~prefix:"targets" ~suffix:"")
      targets
      (print_list_if_non_empty ~prefix:"action (progn" ~suffix:")")
      action

let print_comment ppf = function
  | [] -> fprintf ppf ""
  | l ->
    pp_print_list
      ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
      (fun ppf x -> fprintf ppf ";%s" x)
      ppf l

let print_library ppf = function
  | ({ name; modules; libraries; implements; preprocess; preprocessor_deps } :
      library) ->
    fprintf ppf "(library\n(name %s)\n%a\n%a\n%a\n%a\n%a)" name
      (print_list_if_non_empty ~suffix:"" ~prefix:"modules")
      modules
      (print_list_if_non_empty ~suffix:"" ~prefix:"libraries")
      libraries
      (print_list_if_non_empty ~suffix:"" ~prefix:"implements")
      implements
      (print_list_if_non_empty ~suffix:"" ~prefix:"preprocess")
      preprocess
      (print_list_if_non_empty ~suffix:"" ~prefix:"preprocessor_deps")
      preprocessor_deps

let print_executable ppf = function
  | ({ name; modules; libraries } : executable) ->
    fprintf ppf "(executable\n(name %s)\n%a\n%a)" name
      (print_list_if_non_empty ~suffix:"" ~prefix:"modules")
      modules
      (print_list_if_non_empty ~suffix:"" ~prefix:"libraries")
      libraries

let print_dune_stanza ppf = function
  | Library l -> print_library ppf l
  | Executable l -> print_executable ppf l
  | Rule r -> print_rule ppf r
  | Include f -> fprintf ppf "(include %s)" f
  | Comment cs -> print_comment ppf cs
  | Env e -> fprintf ppf "(env %s)" e

let print_dune_file ppf (df : dune_file) =
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (fun ppf x -> print_dune_stanza ppf x)
    ppf df

let has_stanza_with_name ~dune_file name =
  List.exists
    (fun x ->
      let name_x = get_name_of_stanza x in
      if Option.is_some name_x then
        name_x = Some name
      else
        false)
    dune_file

let has_stanza_with_same_name ~dune_file stanza =
  match get_name_of_stanza stanza with
  | None -> false
  | Some name -> has_stanza_with_name ~dune_file name

(** Default function for adding a stanza: preferably after env and
   include, and then in the first slot available *)
let add_stanza ~dune_file stanza =
  if
    has_stanza_with_same_name ~dune_file stanza
    (* List.exists (fun x -> let name_x = get_name_of_stanza x in
       *                       if Option.is_some name_x then name_x  = get_name_of_stanza stanza else false) dune_file *)
  then (
    eprintf "Trying to add already existing stanza %a" print_dune_stanza stanza ;
    dune_file
  ) else
    let rec aux = function
      | [] -> [stanza]
      | Env e :: xs -> Env e :: aux xs
      | Include i :: xs -> Include i :: aux xs
      | xs -> stanza :: xs in
    aux dune_file

let remove_stanza_by_name ~dune_file stanza_name =
  List.filter
    (function
      | Library l -> l.name <> stanza_name
      | Executable e -> e.name <> stanza_name
      | _ -> true)
    dune_file

let remove_library_by_name ~dune_file stanza_name =
  List.filter_map
    (function
      | Library l ->
        if l.name <> stanza_name then
          Some
            (Library
               {
                 l with
                 libraries = List.filter (fun x -> x <> stanza_name) l.libraries;
               })
        else
          None
      | Executable e ->
        Some
          (Executable
             {
               e with
               libraries = List.filter (fun x -> x <> stanza_name) e.libraries;
             })
      | x -> Some x)
    dune_file

let remove_executable_by_name ~dune_file stanza_name =
  List.filter
    (function
      | Executable e -> e.name <> stanza_name
      | _ -> true)
    dune_file

let add_stanza_at_end ~dune_file stanza = dune_file @ stanza

let get_backup_file_name ~filename =
  Format.sprintf "%s.json" (Filename.remove_extension filename)

let read_backup_file ?(check_if_already_there = true) ~filename () =
  let backup_file = get_backup_file_name ~filename in
  if not (Sys.file_exists backup_file) then
    if check_if_already_there then
      let dir = Filename.dirname backup_file in
      let msg =
        sprintf "[decode_dune_file] trying to decode  backup file %s"
          backup_file in
      if not (Factori_utils.is_dir dir) then
        raise (Factori_errors.NotADirectory (dir, msg))
      else
        raise (NotAFile (backup_file, msg))
    else
      []
  else
    try Factori_utils.deserialize_file ~filename:backup_file ~enc:dune_file_enc
    with CouldNotDestruct _ | CouldNotReadAsJson _ ->
      Factori_utils.output_verbose
        "Could not deserialize file, will behave as if it were not present" ;
      []

(** Take the filename of the dune file, and write the backup file by
   its side *)
let write_backup_file ~dune_file ~filename =
  let backup_filename = get_backup_file_name ~filename in
  Factori_utils.serialize_file ~filename:backup_filename ~enc:dune_file_enc
    dune_file

(** Write a dune file <path/dune> and, by its side, write
   <path/dune.json> which contains a json encoded version of the dune
   file, making it easy to read and update it. If with_readable_backup
   is set to false, then no json file is written *)
let write_dune_file ?(with_readable_backup = true) ~dune_file ~path () =
  let open Factori_utils in
  write_file path (asprintf "%a" print_dune_file dune_file) ;
  if with_readable_backup then write_backup_file ~dune_file ~filename:path

let is_already_there ~dune_file dune_stanza = List.mem dune_stanza dune_file

let add_stanza_to_dune_file ?(file_must_exist = true)
    ?(stanza_must_not_exist = true) ~path stanza =
  let dune_file =
    read_backup_file ~check_if_already_there:file_must_exist ~filename:path ()
  in
  if stanza_must_not_exist && has_stanza_with_same_name ~dune_file stanza then
    ()
  else
    let new_dune_file = add_stanza ~dune_file stanza in
    write_dune_file ~dune_file:new_dune_file ~with_readable_backup:true ~path ()

let add_to_dune_file ?(file_must_exist = true) ?(stanza_must_not_exist = true)
    ~path stanzas =
  List.iter
    (add_stanza_to_dune_file ~file_must_exist ~stanza_must_not_exist ~path)
    stanzas

(** If `stanza_must_exist` is set to false, then this function will do
   nothing *)
let add_library_to_stanza_in_dune_file ?(file_must_exist = true)
    ?(stanza_must_exist = true) ~path ~stanza_name library_name =
  let dune_file =
    read_backup_file ~check_if_already_there:file_must_exist ~filename:path ()
  in
  let rec aux res = function
    | [] ->
      if stanza_must_exist then
        failwith @@ sprintf "No stanza found with name %s" stanza_name
      else
        dune_file
    | Executable e :: xs when e.name = stanza_name ->
      if List.mem library_name e.libraries then (
        Factori_utils.output_verbose "Library was already present" ;
        dune_file
      ) else
        List.rev res
        @ [Executable { e with libraries = library_name :: e.libraries }]
        @ xs
    | Library l :: xs when l.name = stanza_name ->
      if List.mem library_name l.libraries then (
        Factori_utils.output_verbose "Library was already present" ;
        dune_file
      ) else
        List.rev res
        @ [Library { l with libraries = library_name :: l.libraries }]
        @ xs
    | x :: xs -> aux (x :: res) xs in
  let new_dune_file = aux [] dune_file in
  write_dune_file ~dune_file:new_dune_file ~path ()

let remove_library_by_name ?(fail_if_doesnt_exist = false) ~path library_name =
  let dune_file =
    read_backup_file ~check_if_already_there:fail_if_doesnt_exist ~filename:path
      () in
  if
    fail_if_doesnt_exist
    && not (has_library ~dependencies_matter:true ~dune_file library_name)
  then
    failwith @@ sprintf "Couldn't remove non-existing library %s" library_name
  else
    let new_dune_file = remove_library_by_name ~dune_file library_name in
    write_dune_file ~dune_file:new_dune_file ~path ()

let remove_executable_by_name ?(fail_if_doesnt_exist = false) ~path
    executable_name =
  let dune_file =
    read_backup_file ~check_if_already_there:fail_if_doesnt_exist ~filename:path
      () in
  if not (has_executable ~dune_file executable_name) then
    if fail_if_doesnt_exist then
      failwith
      @@ sprintf "Couldn't remove non-existing executable %s" executable_name
    else
      Factori_utils.output_verbose
      @@ sprintf "Couldn't remove executable %s because it doesn't exist"
           executable_name
  else
    let new_dune_file = remove_executable_by_name ~dune_file executable_name in
    write_dune_file ~dune_file:new_dune_file ~path ()
