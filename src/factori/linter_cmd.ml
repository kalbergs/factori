(*open Factori_command
  open Factori_options
  open Factori_errors

  let lint_kt1 =
    subcommand ~name:"lint kt1"
      ~args:
        [
          dir_anonymous_option;
          kt1_anonymous;
          contract_name_option;
          ocaml_language_option;
          typescript_language_option;
          python_language_option;
          csharp_language_option;
          field_prefixes_option;
          network_option;
          library_option;
          crawlori_option;
          crawlori_db_name_option;
          web_option;
          dipdup_option;
          project_name_option;
        ]
      ~doc:
        "Lint an on chain contract using its KT1. You may specify from which \
         network it should be pulled." (fun () ->
        language_shenanigans () ;
        try
          (* First initialize directory structure if it is not already there
              *)
          if (not (File_structure.is_initialized_dir ())) || !overwrite then
            Init_cmd.init_fun () ;
          let network = !Factori_options.network in
          let db_name = db_name () in
          Import_kt1.import_kt1 ~library:!library_mode ~crawlori:!crawlori
            ~dipdup:!dipdup ~network ~db_name () ;
          Format.eprintf "Successfully imported KT1.\n%!"
        with e -> handle_exception e)*)
