open Factori_command
open Factori_options
open Factori_errors
open Factori_linter

let lint_michelson =
  subcommand ~name:"lint michelson" ~args:[michelson_file_anonymous0]
    ~doc:
      "Lint a contract using its Michelson compiled form. It can be in either \
       a Json or normal Michelson format." (fun () ->
      try
        (* First initialize directory structure if it is not already there
            *)
        Factori_utils.try_overflow ~msg:"lint_michelson" (fun () ->
            match !michelson_file with
            | None -> raise NoMichelsonFileProvided
            | Some michelson_file ->
              let se =
                let res =
                  Parse_michelson.parse_michelson_micheline ~file:michelson_file
                in
                res in
              Linter.lint se)
      with e -> handle_exception e)
