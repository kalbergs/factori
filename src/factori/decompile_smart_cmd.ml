open Factori_command
open Factori_options
open Factori_errors
open Factori_analyse

let decompile_michelson =
  subcommand ~name:"decompile michelson" ~args:[michelson_file_anonymous0]
    ~doc:
      "Lint a contract using its Michelson compiled form. It can be in either \
       a Json or normal Michelson format." (fun () ->
      try
        (* First initialize directory structure if it is not already there
            *)
        Factori_utils.try_overflow ~msg:"decompile_michelson" (fun () ->
            match !michelson_file with
            | None -> raise NoMichelsonFileProvided
            | Some michelson_file -> (
              let se =
                let res =
                  Parse_michelson.parse_michelson_file ~file:michelson_file
                in
                res in
              match Micheline_transmo.decompile_smart se with
              | Error e -> raise e
              | Ok res -> Analyze.test res))
      with e -> handle_exception e)
