open Factori_command

let sandbox_stop =
  subcommand ~name:"sandbox stop" ~args:[]
    ~doc:"Stop the Flextesa sandbox started with Factori." (fun () ->
      let cmd =
        Factori_utils.command_if_exists
          ~error_msg:"Please install docker first." ~cmd_name:"docker"
          "docker kill factori-sandbox" in
      let _ = Sys.command cmd in
      ())
