(* open Ezcmd.V2 *)
open Factori_command
open Factori_options
open Factori_errors

let ( // ) = concat

let init_fun () =
  language_shenanigans () ;
  try
    let name = !project_name in
    let db_name = db_name () in
    File_structure.create ~library:!library_mode ~crawlori:!crawlori
      ~dipdup:!dipdup ~verbose:!verbosity ~overwrite:!overwrite ~name ~db_name
      ()
    (* Format.eprintf "Successfully created empty project.\n%!" *)
  with e -> handle_exception e

let cmd_init =
  subcommand ~name:"empty project"
    ~args:
      [
        dir_anonymous_option;
        library_option;
        ocaml_language_option;
        typescript_language_option;
        python_language_option;
        csharp_language_option;
        crawlori_option;
        crawlori_db_name_option;
        web_option;
      ]
    ~doc:
      "Create an empty Factori project. In most cases you don't need to do \
       this, you can just start importing your contracts."
    init_fun
