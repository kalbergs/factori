open Factori_command

let deploy_clean_command =
  subcommand ~name:"deploy clean" ~args:[]
    ~doc:"cleanup leftover scenario of the `factori deploy` command" (fun () ->
      let dir = Factori_config.get_dir () in
      let deploy_path = Factori_config.get_ocaml_deploy_dir ~dir in
      let files_to_remove = Sys.readdir deploy_path in
      Array.iter
        (fun f ->
          let f = Filename.concat deploy_path f in
          Format.printf "Removing [%s]@." f ;
          Sys.remove f)
        files_to_remove)
