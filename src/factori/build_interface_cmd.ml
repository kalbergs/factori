open Factori_command
open Factori_options
open Factori_errors

let ( // ) = Filename.concat

(* let build_interface =
 *   subcommand ~name:"build interface" ~args:[contract_name_option]
 *     ~doc:
 *       "Build (or re-build) a SDK for an existing contract (either created \
 *        inside the project or already imported)." (fun () ->
 *       try
 *         let contract_name =
 *           match !contract_name with
 *           | None ->
 *             raise
 *               (GenericError
 *                  ( "[build_interface]",
 *                    " No name provided for the contract whose interface you \
 *                     want to build" ))
 *           | Some contract_name -> contract_name in
 *         match Factori_config.get_contract_opt contract_name with
 *         | None ->
 *           Format.eprintf
 *             "The contract specified does not belong to the project or has not \
 *              been correctly added to the local configuration file (%s)"
 *             (Factori_config.get_local_config_name
 *                ~dir:(Factori_config.get_dir ()))
 *         | Some { contract_name; original_format; original_kt1 } ->
 *           let filename =
 *             File_structure.get_contract_path ~contract_name
 *               ~format:original_format in
 *           michelson_file := Some filename ;
 *           Factori_utils.output_verbose
 *             (Format.sprintf "Building interface from file: %s\n" filename) ;
 *           Factori_utils.try_overflow ~msg:"import_from_michelson"
 *             (Import_kt1.import_from_michelson ~original_kt1
 *                ~ctrct_name:contract_name)
 *       with e -> handle_exception e) *)
