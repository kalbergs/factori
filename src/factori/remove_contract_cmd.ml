open Factori_command
open Factori_options
open Factori_errors
open Factori_utils

let ( // ) = concat

let remove_contract =
  subcommand ~name:"remove contract"
    ~args:[dir_anonymous_option; purge_option; contract_name_option]
    ~doc:
      "Remove an existing contract from the project. If the --purge option is \
       activated, it will remove the file, otherwise it will only remove it \
       logically from the build infrastructure." (fun () ->
      try
        let dir = Factori_config.get_dir () in
        match !contract_name with
        | None ->
          Format.eprintf
            "Please provide a contract name at the end of the remove_contract \
             command\n\
             %!"
        | Some contract_name ->
          let contract_name = sanitize_basename contract_name in
          (match Factori_config.get_contract_opt contract_name with
          | None ->
            Format.eprintf "No contract found with this name: %s\n%!"
              (show_sanitized_name contract_name)
          | Some _c ->
            (* let contract_path =
             *   File_structure.get_contract_path ~contract_name
             *     ~format:c.original_format in *)
            if !Factori_options.purge then (
              (* Sys.remove contract_path ; *)
              let ocaml_abstract_interface_file =
                Factori_config.get_ocaml_abstract_interface_path ~dir
                  ~contract_name () in
              let ocaml_interface_file =
                Factori_config.get_ocaml_interface_path ~dir ~contract_name ()
              in
              let ocaml_interface_file_mli =
                Factori_config.get_ocaml_interface_mli_path ~dir ~contract_name
                  () in
              let ocaml_code_file =
                Factori_config.get_ocaml_code_path ~dir ~contract_name () in
              let ocaml_code_mli_file =
                Factori_config.get_ocaml_code_mli_path ~dir ~contract_name ()
              in
              let py_interface_file =
                Factori_config.get_python_interface_path ~dir ~contract_name ()
              in
              let py_code_file =
                Factori_config.get_python_code_path ~dir ~contract_name () in

              let ts_interface_file =
                Factori_config.get_typescript_interface_path ~dir ~contract_name
                  () in
              let csharp_interface_file =
                Factori_config.get_csharp_interface_path ~dir ~contract_name ()
              in
              let csharp_code_file =
                Factori_config.get_csharp_code_path ~dir ~contract_name () in
              let ts_code_file =
                Factori_config.get_typescript_code_path ~dir ~contract_name ()
              in
              output_verbose
              @@ Format.sprintf "Removing OCaml abstract interface file %s\n%!"
                   ocaml_abstract_interface_file ;
              remove_file ocaml_abstract_interface_file ;

              output_verbose
              @@ Format.sprintf "Removing OCaml interface file %s\n%!"
                   ocaml_interface_file ;
              remove_file ocaml_interface_file ;

              output_verbose
              @@ Format.sprintf "Removing OCaml interface mli file %s\n%!"
                   ocaml_interface_file ;
              remove_file ocaml_interface_file_mli ;

              output_verbose
              @@ Format.sprintf "Removing OCaml code file %s\n%!"
                   ocaml_code_file ;
              remove_file ocaml_code_file ;

              output_verbose
              @@ Format.sprintf "Removing OCaml code mli file %s\n%!"
                   ocaml_code_file ;
              remove_file ocaml_code_mli_file ;

              output_verbose
              @@ Format.sprintf "Removing Python code file %s\n%!" py_code_file ;
              remove_file py_code_file ;
              output_verbose
              @@ Format.sprintf "Removing Python interface file %s\n%!"
                   py_interface_file ;
              remove_file py_interface_file ;

              output_verbose
              @@ Format.sprintf "Removing Typescript code file %s\n%!"
                   ts_code_file ;
              remove_file ts_code_file ;
              output_verbose
              @@ Format.sprintf "Removing Typescript interface file %s\n%!"
                   ts_interface_file ;
              remove_file ts_interface_file ;
              output_verbose
              @@ Format.sprintf "Removing C# interface file %s\n%!"
                   csharp_interface_file ;
              remove_file csharp_interface_file ;
              output_verbose
              @@ Format.sprintf "Removing C# code file %s\n%!" csharp_code_file ;
              remove_file csharp_code_file
            ) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:true
              ~path:(Factori_config.get_ocaml_interface_dir ~dir () // "dune")
              (Factori_config.get_ocaml_interface_basename ~contract_name) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:true
              ~path:(Factori_config.get_ocaml_interface_dir ~dir () // "dune")
              (Factori_config.get_ocaml_abstract_interface_basename
                 ~contract_name) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
              ~path:(Factori_config.get_ocaml_scenarios_dir ~dir () // "dune")
              (Factori_config.get_ocaml_interface_basename ~contract_name) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
              ~path:(Factori_config.get_ocaml_scenarios_dir ~dir () // "dune")
              (Factori_config.get_ocaml_abstract_interface_basename
                 ~contract_name) ;
            (* crawlori files *)
            let crawlori_dir = Factori_config.get_ocaml_crawlori_dir ~dir () in
            if is_dir crawlori_dir then begin
              Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
                ~path:(Factori_config.get_ocaml_crawlori_dir ~dir () // "dune")
                (Factori_config.get_ocaml_crawlori_plugin_basename
                   ~contract_name) ;
              Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
                ~path:(Factori_config.get_ocaml_crawlori_dir ~dir () // "dune")
                (Factori_config.get_ocaml_crawlori_tables_basename
                   ~contract_name) ;
              Import_kt1.remove_upgrade_downgrade_tables dir contract_name ;
              Import_kt1.remove_plugin_register dir contract_name
            end ;
            (* dipdup files *)
            let dipdup_dir = Factori_config.get_dipdup_dir ~dir in
            if is_dir dipdup_dir then
              Import_kt1.remove_dipdup_indexing ~dir ~contract_name ;
            Factori_config.remove_contract_from_config_file ~dir
              ~name:contract_name) ;
          Format.eprintf "Successfully removed contract.\n%!"
      with e -> handle_exception e)
