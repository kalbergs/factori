open Format
open Factori_utils
open Types

let auxiliary_functions ppf () =
  fprintf ppf
    "\n\
     def myrandom (mn : int,mx : int):\n\
     \tr = random() * (mx - mn) + mn\n\
     \tres = floor(r)\n\
     \treturn res\n\n\n\
     def chooseFrom(l : List[T]):\n\
     \tmylist = l\n\
     \tn = len(mylist)\n\
     \tindex = myrandom(0,n)\n\
     \tchosenFun = mylist[index]\n\
     \treturn chosenFun;\n\n\n\n\
     def int64_generator():\n\
     \treturn myrandom(0,10)\n\n\n\
     def make_list(my_generator : Callable[[],T],k : int):\n\
     \tres = list(range(k))\n\
     \tfor i in range(k):\n\
     \t\tres[i] = my_generator()\n\
     \tdef result() -> list[T]:\n\
     \t\treturn res\n\n\
     \treturn result\n\n\n"

let all_base =
  [
    Never;
    String;
    Nat;
    Int;
    Byte;
    Address;
    Signature;
    Unit;
    Bool;
    Timestamp;
    Keyhash;
    Key;
    Mutez;
    Operation;
    Chain_id;
    Sapling_state;
    Sapling_transaction_deprecated;
  ]

let all_unary = [Set; List; Option; Ticket]

let all_binary = [Map; BigMap; Lambda]

let generate_all_kinds tuple_size_min tuple_size_max =
  [AContract]
  @ List.map (fun b -> ABase b) all_base
  @ List.map (fun b -> AUnary b) all_unary
  @ List.map (fun b -> ABinary b) all_binary
  @ List.map
      (fun i -> ATuple i)
      (list_integers ~from:tuple_size_min tuple_size_max)

let build_generator ppf kind =
  match kind with
  | ABase b -> (
    match b with
    | Bool ->
      fprintf ppf
        "def bool_generator() -> bool:\n\
         \tmyres = chooseFrom([True, False]);\n\
         \treturn myres"
    | Int ->
      fprintf ppf "def int_generator() -> int:\n\treturn myrandom(0,10)\n"
    | Mutez ->
      fprintf ppf "def tez_generator() -> tez:\n\treturn int64_generator();\n"
    | Nat ->
      fprintf ppf "def nat_generator() -> nat:\n\treturn int64_generator()\n"
    | Timestamp ->
      fprintf ppf "def timestamp_generator() -> timestamp:\n\treturn 1702474034"
    | Never ->
      fprintf ppf
        "def never_generator() -> NeverType:\n\
         \traise Exception(\"Never gonna generate a `never`\")\n"
    | String ->
      fprintf ppf
        "def string_generator () -> str:\n\treturn \"Very like a whale\""
    | Byte ->
      fprintf ppf "def bytes_generator():\n\treturn bytes.fromhex('ff')\n"
    | Address ->
      fprintf ppf
        "def address_generator():\n\
         \treturn 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb'\n"
    | Unit -> fprintf ppf "def unit_generator():\n\treturn unit()\n"
    | Keyhash ->
      fprintf ppf "def key_hash_generator():\n\treturn address_generator()\n"
    | Key ->
      fprintf ppf
        "def key_generator():\n\
         \treturn 'edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav'"
    | Signature ->
      fprintf ppf
        "def signature_generator():\n\
         \treturn \
         'edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg'"
    | Operation ->
      fprintf ppf
        "def operation_generator():\n\
         \treturn 'op92MgaBZvgq2Rc4Aa4oTeZAjuAWHb63J2oEDP3bnFNWnGeja2T'"
    | Chain_id | Sapling_state | Sapling_transaction_deprecated ->
      let typename = str_of_base b in
      fprintf ppf
        "def %s_generator():\n        return string_generator()\n        "
        typename)
  | AContract ->
    fprintf ppf
      "def contract_generator() -> contract:\n\
       \treturn 'KT1G7ziTpUgXQR9QymGj348jM5B8KdZgBp1B' # random KT1 pulled \
       from ghostnet"
  | AUnary u -> (
    match u with
    | List ->
      fprintf ppf
        "def list_generator(f : Callable[[], T]):\n\
         \tres = make_list(f,int_generator())()\n\
         \tdef result() -> list[T]:\n\
         \t\treturn res;\n\
         \treturn result\n"
    | Set ->
      fprintf ppf "def set_generator(f : Any):\n\treturn list_generator(f)\n"
    | Option ->
      fprintf ppf
        "def option_generator(f : Any):\n\
         \toptres = f()\n\
         \tdef result():\n\
         \t\treturn chooseFrom([(lambda: None),(lambda: optres)])()\n\
         \treturn result\n"
    | Ticket ->
      fprintf ppf
        "def ticket_generator(f):\n\
         \tdef result() -> Ticket:\n\
         \t\tres = f()\n\
         \t\treturn TicketType.dummy(AbstractContext())\n\
         \treturn result\n")
  | ABinary b -> (
    match b with
    | Map ->
      fprintf ppf
        "def map_generator(f:Any,g:Any):\n\
         \tdef result():\n\
         \t\treturn Map({})\n\
         \treturn result\n"
    | BigMap ->
      fprintf ppf
        "def big_map_generator(f,g):\n\
         \tdef result():\n\
         \t\treturn BigMap(Map({}))\n\
         \treturn result"
    | Lambda ->
      fprintf ppf
        "def lambda_generator(f : Any,g : Any):\n\
         \tdef result():\n\
         \t\tlmbd = LambdaType.match(michelson_to_micheline('lambda int \
         int')).from_micheline_value(michelson_to_micheline('{ DROP ; UNIT ; \
         FAILWITH }'))\n\
         \t\treturn lmbd\n\
         \treturn result")
  | ATuple k ->
    fprintf ppf
      "def tuple%d_generator(%a) -> Callable[[],Tuple[%a]]:\n\
       \tdef result() -> Tuple[%a]:\n\
       \t\treturn %a\n\
       \treturn result\n"
      k
      (* (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "T%d" i))
       * (list_integers ~from:1 k) *)
      (pp_print_list ~pp_sep:(tag ", ") (fun ppf i ->
           fprintf ppf "f%d : Callable[[],T%d]" i i))
      (list_integers ~from:1 k)
      (pp_print_list ~pp_sep:(tag ", ") (fun ppf i -> fprintf ppf "T%d" i))
      (list_integers ~from:1 k)
      (pp_print_list ~pp_sep:(tag ", ") (fun ppf i -> fprintf ppf "T%d" i))
      (list_integers ~from:1 k)
      (* (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "T%d" i))
       * (list_integers ~from:1 k) *)
      (pp_print_list ~pp_sep:(tag ", ") (fun ppf i -> fprintf ppf "f%d()" i))
      (list_integers ~from:1 k)

(* | _ -> failwith "TODO" *)
