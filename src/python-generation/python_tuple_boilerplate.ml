open Factori_utils

let tuple_encode_generate ppf i =
  let open Format in
  fprintf ppf
    "def tuple%d_encode(%a):\n\
     \tdef result(arg : Tuple) -> PairType:\n\
     \t\ttry:\n\
     \t\t\treturn PairType.from_comb([%s])\n\
     \t\texcept Exception as e:\n\
     \t\t\tprint('Error in tuple%d_encode')\n\
     \t\t\tprint(e)\n\
     \t\t\traise(e)\n\n\
     \treturn result" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "enc%d" i))
    (list_integers i)
    (* (pp_print_list
     *    ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
     *    (fun ppf i -> fprintf ppf "x%d" i))
     * (list_integers i) *)
    (String.concat ", "
       (List.map
          (fun i -> Format.sprintf "enc%d(arg[%d])" i (i - 1))
          (list_integers i)))
    i

let repeat n x = List.map (fun _ -> x) (list_integers ~from:1 n)

let tuple_decode_generate ppf i =
  let open Format in
  fprintf ppf
    "def tuple%d_decode(%a):\n\
     \tdef result(p) -> Tuple[%a]:\n\
     \t\ttry:\n\
     \t\t\tx1 = p.items[0]\n\
     \t\t\tprerest : Tuple[%a] = tuple%d_decode(%a)(p.items[1])\n\
     \t\t\tfirst : Tuple[T1] = (dec1(x1),)\n\
     \t\t\trest : Tuple[%a] = first + prerest\n\
     \t\t\treturn rest\n\
     \t\texcept Exception as e:\n\
     \t\t\tprint('Error in tuple%d_decode')\n\
     \t\t\tprint(e)\n\
     \t\t\traise e\n\n\
     \treturn result\n\n\n\
     assert tuple%d_decode(%a)(tuple%d_encode(%a)((%a))) == (%a)\n"
    i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "dec%d : Callable[[MichelsonType],T%d]" i i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:2 i) (i - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "dec%d" i))
    (list_integers ~from:2 i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:1 i) i i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf s -> fprintf ppf "%s" s))
    (repeat i "int_decode") i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf s -> fprintf ppf "%s" s))
    (repeat i "int_encode")
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "%d" i))
    (list_integers ~from:1 i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "%d" i))
    (list_integers ~from:1 i)

let tuple_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:3 in
  Format.asprintf "\n\n%a\n\n\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_encode_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> tuple_decode_generate ppf i))
    (list_integers n)
