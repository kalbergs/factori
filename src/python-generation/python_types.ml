open Python_generator_generation
open Format

let python_types =
  let prelude =
    {|
from typing import Type, Any, List, NewType, TypeVar, Dict, Callable, Optional, Union, Generic, Tuple

from random import random
from math import floor
from pytezos.michelson.parse import michelson_to_micheline
from pytezos.michelson.types.base import MichelsonType
from pytezos.context.abstract import AbstractContext
from pytezos.michelson.types.domain import KeyType, TimestampType, MutezType, SignatureType
from pytezos.michelson.types.core import UnitType
from pytezos.michelson.types.core import NeverType
from pytezos.michelson.types.core import StringType
from pytezos.michelson.types.core import IntType
from pytezos.michelson.types.core import NatType
from pytezos.michelson.types.core import BytesType
from pytezos.michelson.types.core import BoolType
from pytezos.michelson.types.core import UnitLiteral, unit
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.option import OptionType
from pytezos.michelson.micheline import Micheline
from pytezos.michelson.types.list import ListType
from pytezos.michelson.types.set import SetType
from pytezos.michelson.types.map import MapType
from pytezos.michelson.types.big_map import BigMapType
from pytezos.michelson.types.sum import OrType
from pytezos.michelson.types.domain import KeyHashType
from pytezos.michelson.types.domain import ChainIdType
from pytezos.michelson.types.domain import ContractType
from pytezos.michelson.types.domain import LambdaType
from pytezos.michelson.types.operation import OperationType
from pytezos.michelson.types.ticket import TicketType



# Exceptions


class NeverHappens(Exception):
    pass


class NotElementOfType(Exception):
    pass


# Utils
T = TypeVar('T')

def decode_from_list(x_decode : Callable[[MichelsonType],T]) -> Callable[...,list[T]]:
    def result(l):
        res = []
        for x in l:
            res.append(x_decode(x))
        return res

    return result


def make_left(x, right_type=MichelsonType):
    return OrType.from_left(x,right_type)


def make_right(y, left_type=MichelsonType):
    return OrType.from_right(y, left_type)

# Bigmaps
# Abstract big maps are represented by an int
# Literal big maps are represented by a list of pairs of (key,value)


# Contract = NewType('Contract', str)

KT = TypeVar('KT')
VT = TypeVar('VT')


class Map(Generic[KT, VT]):
    def __init__(self, mydict=None):
        # Initialize the dictionary that will store the key-value pairs
        if mydict is None:
            mydict = {}
        self.dictionary = mydict

    def set(self, key: KT, value: VT):
        # Set the value for the given key in the dictionary
        self.dictionary[key] = value

    def get(self, key: KT) -> VT:
        # Return the value for the given key from the dictionary
        value: VT = self.dictionary[key]
        return value

    def is_empty(self) -> bool:
        # Return True if the dictionary is empty, False otherwise
        return len(self.dictionary) == 0

    def __eq__(self, other):
        # Check if the other object is a Map and if its dictionary is equal to this Map's dictionary
        return isinstance(other, Map) and self.dictionary == other.dictionary


class BigMap(Generic[KT, VT]):
    def __init__(self, value: Map[KT, VT] | int):
        self.value = value

    # Define the BigMap class as a subtype of Union, which can be either a Map or an int
    pass

class MyBigMapType(BigMapType):
    def __init__(
        self,
        items: List[Tuple[MichelsonType, MichelsonType]],
        ptr: Optional[int] = None,
        removed_keys: Optional[List[MichelsonType]] = None,
    ):
        super(BigMapType, self).__init__(items=items)
        self.ptr = ptr
        self.removed_keys = removed_keys or []
        self.context: Optional[AbstractContext] = None
    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)

    @staticmethod
    def empty(key_type: Type[MichelsonType], val_type: Type[MichelsonType]) -> 'MyBigMapType':
        cls = MyBigMapType.create_type(args=[key_type, val_type])
        return cls(items=[])  # type: ignore

    def to_micheline_value(self, mode='readable', lazy_diff: Optional[bool] = False):
        #print("entering to_micheline_value!");
        if self.ptr is None:
            #print("self.ptr is None")
            if len(self.items) == 0:
                return []
            else:
                return [{"prim": "Elt", "args": [k.to_micheline_value(), v.to_micheline_value()]} for k, v in self.items()]
        else:
            #print("self.ptr is not None")
            return super().to_micheline_value(mode, lazy_diff)

#
# preBigMap = Union[Map[KT, VT], int]
#
#
# class BigMap(preBigMap):
#     pass


# Map = Dict[X, Y]


def string_encode(s: str) -> StringType:
    return StringType.from_value(s)


def string_decode(m: MichelsonType) -> str:
    if isinstance(m,StringType):
        return str(m)
    else:
        raise(ValueError("[string_decode] Not a String"))


assert (string_decode(string_encode('coucou')) == 'coucou')

address = str

signature = str


def signature_encode(s):
    return SignatureType(s)


def signature_decode(s: MichelsonType) -> str:
    if isinstance(s, SignatureType):
        return str(s)
    else:
        raise ValueError("Input is not a valid SignatureType")


key = str


def key_encode(k):
    return KeyType.from_value(k)


def key_decode(k: MichelsonType) -> key:
    if isinstance(k, KeyType):
        return key(k)
    else:
        raise ValueError("Input is not a valid KeyType")


key_hash = str


def key_hash_encode(k):
    return KeyHashType.from_value(k)


def key_hash_decode(k: MichelsonType) -> key_hash:
    if isinstance(k, KeyHashType):
        return key_hash(k)
    else:
        raise ValueError("Input is not a valid KeyHashType")

chain_id = str

def chain_id_encode(c):
    return ChainIdType.from_value(c)


def chain_id_decode(c: MichelsonType) -> str:
    if isinstance(c, ChainIdType):
        return str(c)
    else:
        raise ValueError("Input is not a valid ChainIdType")

timestamp = int

contract = str

def contract_encode(c: contract):
    try:
        return ContractType.from_value(c)
    except Exception as e:
        print("Error in contract_encode on input " + str(c) + ' with error: ' + str(e))
        raise e

def contract_decode(c: MichelsonType) -> Any:
    if isinstance(c, ContractType):
        try:
            return c.to_python_object()
        except Exception as e:
            print("Error in contract_decode on input" + str(c.to_micheline_value()) + ' and error: ' + str(e))
            raise e
    else:
        raise ValueError("Input is not a valid ContractType")


Lambda = LambdaType

def lambda_encode(x=None,y=None):
    def result(l: Lambda) -> LambdaType:
        return l

    return result

def lambda_decode(x=None,y=None):
    def result(l: MichelsonType) -> Lambda:
        if isinstance(l, LambdaType):
            return l
        else:
            raise ValueError("Input is not a valid LambdaType")
    return result


Operation = OperationType


def operation_encode(o: Operation) -> OperationType:
    return o


def operation_decode(o: MichelsonType) -> Operation:
    if isinstance(o, OperationType):
        return o
    else:
        raise ValueError("Input is not a valid OperationType")


Ticket = TicketType

def ticket_encode(t: Ticket) -> TicketType:
    return t

def ticket_decode(t: MichelsonType) -> Ticket:
    if isinstance(t, TicketType):
        return t
    else:
        raise ValueError("Input is not a valid TicketType")

def timestamp_encode(t: timestamp):
    return TimestampType.from_value(t)


def timestamp_decode(t: MichelsonType) -> int:
    if isinstance(t, TimestampType):
        return int(t)
    else:
        raise ValueError("Input is not a valid TimestampType")


assert(timestamp_decode(timestamp_encode(1702474034)) == 1702474034)


tez = int


def tez_encode(t : tez):
    return MutezType.from_value(t)


def tez_decode(t : MichelsonType) -> tez:
    if isinstance(t, MutezType):
        res : tez = int(t)
        return res
    else:
        raise ValueError("Input is not a valid MutezType")

address_encode = string_encode

address_decode = string_decode


def int_encode(i: int) -> IntType:
    return IntType.from_value(i)


def int_decode(m: MichelsonType) -> int:
    if isinstance(m, IntType):
        return int(m)
    else:
        raise ValueError("Input is not a valid IntType")


# print("int_decode: " + str(int_decode(int_encode(42))))
assert (int_decode(int_encode(42)) == 42)

int_michelson = int

int_michelson_encode = int_encode
int_michelson_decode = int_decode

nat = int


def nat_encode(i: int) -> NatType:
    return NatType.from_value(i)


def nat_decode(m: MichelsonType) -> int:
    if isinstance(m, NatType):
        return int(m)
    else:
        raise ValueError("Input is not a valid NatType")


assert (nat_decode(nat_encode(42)) == 42)


def bytes_encode(b: bytes) -> BytesType:
    return BytesType.from_value(b)


def bytes_decode(m: MichelsonType) -> bytes:
    if isinstance(m, BytesType):
        return bytes(m)
    else:
        raise ValueError("Input is not a valid BytesType")


assert (bytes_decode(bytes_encode(bytes.fromhex('ff'))) == bytes.fromhex('ff'))


def bool_encode(b: bool) -> BoolType:
    return BoolType.from_value(b)


def bool_decode(m: MichelsonType) -> bool:
    if isinstance(m, BoolType):
        return bool(m)
    else:
        raise ValueError("Input is not a valid BoolType")


assert (bool_decode(bool_encode(True)) == True)
assert (bool_decode(bool_encode(False)) == False)


def unit_encode(u: Any) -> UnitType:
    return UnitType.dummy(u)


def unit_decode(m: MichelsonType) -> unit:
    if isinstance(m, UnitType):
        return unit()
    else:
        raise ValueError("Input is not a valid UnitType")


assert (unit_decode(unit_encode(None)) == unit())
assert (unit_decode(unit_encode([])) == unit())


def never_encode(x) -> NeverType:
    raise (NeverHappens("never say never"))


def never_decode(m: MichelsonType) -> Any:
    if isinstance(m, NeverType):
        raise (NeverHappens("never say never"))
    else:
        raise ValueError("Input is not a valid NeverType")

def option_encode(x_encode):
    def result(x: Optional[T]):
        if x is None:
            # TODO: check this is not a problem
            return OptionType.none(StringType)
        else:
            return OptionType.from_some(x_encode(x))

    return result


def option_decode(x_decode: Callable[[MichelsonType], T]) -> Callable[[MichelsonType], Optional[T]]:
    def result(x: MichelsonType) -> Optional[T]:
        if isinstance(x, OptionType):
            if x.is_none():
                return None
            else:
                return x_decode(x.get_some())
        else:
            raise ValueError("Input is not a valid OptionType")
    return result


# def option_decode(x_decode):
#     def result(x: OptionType[T]):
#         if x.is_none():
#             return None
#         else:
#             return x.get_some()
#
#     return result


assert (option_decode(int_decode)(option_encode(int_encode)(3)) == 3)
assert (option_decode(int_decode)(option_encode(int_encode)(None)) is None)

# Michelson Key type
MKT = TypeVar('MKT')
# Michelson Value type
MVT = TypeVar('MVT')


def map_encode(k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType]):
    def result(m: Map[KT, VT]):
        if m.is_empty():
            return MapType.empty(MichelsonType, MichelsonType)
        else:
            encoded_keys = map(k_encode, m.dictionary.keys())
            encoded_values = map(v_encode, m.dictionary.values())
            pre_res = list(zip(encoded_keys, encoded_values))
            res: MapType = MapType.from_items(pre_res)
            return res

    return result


test = map_encode(map_encode(int_encode, string_encode), int_encode)


#
# def map_encode(k_encode, v_encode):
#     def result(m: Map):
#         res = MapType.empty(X, Y)
#         for k in m.keys():
#             res = res.update(k_encode(k), v_encode(m[k]))
#         return res
#
#     return result


def map_decode(k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT]):
    def result(m: MichelsonType) -> Map:
        if isinstance(m, MapType):
            preres: dict = {}
            for k, v in m.items:
                preres[k_decode(k)] = v_decode(v)
            res: Map = Map(preres)
            return res
        else:
            raise ValueError("Input is not a valid MapType")
    return result


# print(map_decode(int_decode, string_decode)(map_encode(int_encode, string_encode)(Map({1: 'mary'}))).dictionary)
assert (map_decode(int_decode, string_decode)(map_encode(int_encode, string_encode)(Map({1: 'mary'}))) == Map(
    {1: 'mary'}))


def big_map_encode(k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType]):
    def result(m: BigMap[KT, VT]):
        if isinstance(m.value, int):
            #here we make the choice to return an empty bigmap to make deployment easier
            return MyBigMapType.empty(MichelsonType, MichelsonType)
        elif isinstance(m.value,Map):
            if m.value.is_empty():
                return MyBigMapType.empty(MichelsonType, MichelsonType)
            else:
                encoded_keys = map(k_encode, m.value.dictionary.keys())
                encoded_values = map(v_encode, m.value.dictionary.values())
                pre_res = list(zip(encoded_keys, encoded_values))
                res = MyBigMapType(items=pre_res)
                return res

    return result


def big_map_decode(k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT]):
    def result(m: MichelsonType) -> BigMap:
        if isinstance(m, MyBigMapType):
            if m.ptr is not None:
                return BigMap(int(m.ptr))
            preres: dict = {}
            for k, v in m.items:
                preres[k_decode(k)] = v_decode(v)
            res: BigMap = BigMap(Map(preres))
            return res
        else:
            raise ValueError("Input is not a valid MyBigMapType")
    return result


def list_encode(x_encode):
    def result(l) -> ListType:
        lres = []
        for i in l:
            lres.append(x_encode(i))
        res = ListType(lres)  # I suspect this will fail on empty lists TODO
        return res

    return result


def list_decode(x_decode: Callable[[MichelsonType], T]):
    def result(l: MichelsonType) -> List[T]:
        if isinstance(l, ListType):
            return decode_from_list(x_decode)(l)
        else:
            raise ValueError("Input is not a valid ListType")
    return result


toto = (list_encode(int_encode)([1, 2, 3, 4]))
tata = list_decode(int_decode)(toto)
assert (list_decode(int_decode)(list_encode(
    int_encode)([1, 2, 3, 4])) == [1, 2, 3, 4])


class Set(Generic[T]):
    def __init__(self, mylist: List[T]):
        self.list = mylist


def set_encode(x_encode: Callable[[T], MichelsonType]):
    def result(x: Set[T]) -> SetType:
        res: SetType = SetType([x_encode(i) for i in x.list])
        return res


def set_decode(x_decode: Callable[[MichelsonType], T]):
    def result(x: MichelsonType) -> Set:
        if isinstance(x, SetType):
            preres = [x_decode(i) for i in x.items]
            return Set(preres)
        else:
            raise ValueError("Input is not a valid SetType")
    return result


def tuple2_encode(x_encode, y_encode):
    def result(arg) -> PairType:
        x = x_encode(arg[0])
        # print("[tuple2_encode] x : " + str(x))
        y = y_encode(arg[1])
        return PairType.from_comb([x, y])

    return result


def tuple2_decode(x_decode, y_decode):
    def result(p: MichelsonType) -> Tuple:
        if isinstance(p, PairType):
            x = p.items[0]
            y = p.items[1]
            res = (x_decode(x), y_decode(y))
            return res
        else:
            raise ValueError("Input is not a valid PairType")
    return result


#     tuple3_encode(string_encode, int_encode, bytes_encode)("toto", 3, bytes.fromhex('ff'))) == ["toto", 3,
#                                                                                                 bytes.fromhex('ff')])


#x1 = int_encode(2)
# print("x1 : " + str(x1))
#x2 = int_encode(3)
#x3 = int_encode(4)
#p1 = PairType.from_comb([x1, x2, x3])
#ep1 = tuple2_encode(string_encode, int_encode)("toto", 3)
#dp1 = tuple2_decode(string_decode, int_decode)(ep1)

# print(dp1)
# res = (tuple2_encode(int_encode, int_encode)(2, 3))
# print(res)
# resinv = tuple2_decode(string_decode, int_decode)(res)
# print(resinv)


assert tuple2_decode(string_decode, int_decode)(
    tuple2_encode(string_encode, int_encode)(("toto", 3))) == ("toto", 3)


#
# # The from_comb is probably not the right way to build a pair with three elements
# def tuple3_encode(x_encode, y_encode, z_encode):
#     def result(x, y, z) -> PairType:
#         x = x_encode(x)
#         y = y_encode(y)
#         z = z_encode(z)
#         return PairType.from_comb([x, y, z])
#
#     return result
#
#
# # This doesn't type well and there's a chance it won't work on data from the blockchain.. TODO: investigate
# def tuple3_decode(x_decode, y_decode, z_decode):
#     def result(p: PairType):
#         x = p.items[0]
#         rest = tuple2_decode(y_decode, z_decode)((p.items[1]))
#         rest.insert(0, x_decode(x))
#         return rest
#
#     return result


# print(tuple3_decode(string_decode, int_decode, bytes_decode)(
#     tuple3_encode(string_encode, int_encode, bytes_encode)("toto", 3, bytes.fromhex('ff'))))
# assert (tuple3_decode(string_decode, int_decode, bytes_decode)(

|}
  in
  Format.asprintf "%s\n%a\n%a\n\n%a\n%s" prelude auxiliary_functions ()
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n") (fun ppf i ->
         fprintf ppf "T%d = TypeVar('T%d')" i i))
    (Factori_utils.list_integers ~from:1 25)
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n\n") (fun ppf x ->
         fprintf ppf "%a" build_generator x))
    (generate_all_kinds 2 25)
    (Python_tuple_boilerplate.tuple_boilerplate 25)
