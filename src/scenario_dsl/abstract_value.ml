(* open Abstract_pair_type(\* Pair_type *\) *)
open Factori_utils
module Z = Z
module Hex = Hex
module Lambda = Lambda

(* Two ways to represent an object in a scenario: a universe id (a
   number and a universe like (17,[0;1]) or a scenario identifier
   (simply a name like "address0". There is a phantom type to
   facilitate native OCaml typing in the DSL despite reification *)
type 'a abstract_id =
  | UniId : Universes.universe_id -> 'a abstract_id
  | ScenId : Scenario_id.id -> 'a abstract_id

(* Because of the phantom type, we need to be able to convert values
   with such dumb functions *)
let convert : 'a 'b. 'a abstract_id -> 'b abstract_id =
  fun (type a b) (x : a abstract_id) ->
   match x with
   | UniId id ->
     let res = (UniId id : b abstract_id) in
     res
   | ScenId id ->
     let res = ScenId id in
     res

type ('a, 'b) oRTYPE =
  | LEFT of 'a
  | RIGHT of 'b

(* A type of abstract values which permits building storage and
   parameters in a way that can be compiled both to OCaml and to
   Typescript *)
type _ value =
  | VAbstract : 'a abstract_id -> 'a value
  | VUnit : unit -> unit value
  | VInt : Z.t -> Z.t value
  | VString : string -> string value
  | VBytes : Hex.hex -> Hex.hex value
  | VTimestamp : string -> string value
  | VBool : bool -> bool value
  | VAddress : string -> string value
  | VTez : Z.t -> Z.t value
  | VKey : string -> string value
  | VKeyHash : string -> string value
  | VSignature : string -> string value
  | VOperation : string -> string value
  | VTuple : 'a value tuple -> 'a tuple value
  | VOnChainBigMap : Z.t -> ('a * 'b) list value
  | VLiteralBigMap : ('a * 'b) value simple_pair list -> ('a * 'b) list value
  | VMap : ('a * 'b) value simple_pair list -> ('a * 'b) list value
  | VSet : 'a value list -> 'a list value
  | VList : 'a value list -> 'a list value
  | VOption : 'a value option -> 'a option value
  | VLambda : Lambda.lambda -> Lambda.lambda value
  | VRecord : 'a abstract_paired_type -> 'a abstract_paired_type value
  | VSumtype : 'a value abstract_or_type -> 'a value
  | VChain_id : string -> string value
  | VTicket :
      'a Factori_utils.Ticket.ticket
      -> 'a Factori_utils.Ticket.ticket value
  | VSaplingState : string -> string value
(* [@@deriving encoding { recursive }] *)

and _ simple_pair =
  (* abstract pair *)
  | SP : ('a * 'b) value -> ('a * 'b) value simple_pair
  (* actual pair *)
  | SP2 : ('a value * 'b value) -> ('a * 'b) value simple_pair

and _ abstract_or_type =
  | LeafOr :
      Factori_utils.sanitized_name * 'a value
      -> 'a value abstract_or_type
  | SumOrL : 'a value abstract_or_type -> ('a, 'b) oRTYPE value abstract_or_type
  | SumOrR : 'b value abstract_or_type -> ('a, 'b) oRTYPE value abstract_or_type

and _ abstract_paired_type =
  | LeafP : (sanitized_name * 'a value) -> 'a value abstract_paired_type
  | PairP :
      'a value abstract_paired_type * 'b value abstract_paired_type
      -> ('a * 'b) value abstract_paired_type

and _ tuple =
  | Single : 'a value -> 'a value tuple
  | Multi : 'a value * 'b value tuple -> ('a * 'b) value tuple
(* let x : unit value = VUnit ()
 * let x : string option value = VOption (Some (VString "s"))
 * let y = x
 * let fn = Factori_utils.sanitized_of_str "coucou"
 * let z = VLeaf ((fn,x))
 * let t = VRecord(VLeaf(fn,x),VLeaf(fn,x)) *)
(* let x : int list value = VList [VInt Z.one;VInt Z.one]
 * let f : int -> string value = fun _ -> VString "coucou"
 * let y = List.map f [3;4] *)

let rec pp_value : 'a. Format.formatter -> 'a value -> unit =
  fun (type a) ppf (v : a value) ->
   match v with
   | VAbstract (ScenId s) -> Format.fprintf ppf "ScenId %s@." s
   | VAbstract (UniId u_id) ->
     Format.fprintf ppf "UniId %s@." (Universes.show_universe_id u_id)
   | VRecord (LeafP (sn, v)) ->
     Format.fprintf ppf "LeafP(%s,%a)" (show_sanitized_name sn) pp_value v
   | VRecord (PairP (r1, r2)) ->
     Format.fprintf ppf "VRecord(%a,%a)" pp_value (VRecord r1) pp_value
       (VRecord r2)
   | _ -> Format.fprintf ppf "<val TODO>"

let show_value v = Format.asprintf "%a" pp_value v
