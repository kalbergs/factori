open Format

(* The Ast which should be the target of any scenario compiler *)
open Scenario_value

type ast_id = string [@@deriving show, encoding]

(** Keep track of attributed names  *)
module AstNames = Set.Make (struct
  type t = ast_id

  let compare = Stdlib.compare
end)

type ast_names = AstNames.t

let pp_ast_names ppf (a : ast_names) =
  AstNames.iter (fun (x : ast_id) -> fprintf ppf "%s" x) a

type ast_value = ast_id value [@@deriving show]

(* the 'a enforces some typing of arguments, but plays no role in
   computation *)
type 'a astobj = {
  id : Scenario_id.id;
  ast_value : ast_value;
}

type ast_context = {
  ast_names : ast_names;
  ast_values : (ast_id * ast_value) list;
}
[@@deriving show]

let empty_ast = { ast_names = AstNames.empty; ast_values = [] }

let exists_name ast_names name = AstNames.mem name ast_names

let assign_name ast_names name =
  if exists_name ast_names name then
    failwith (sprintf "Error: name %s already attributed" name)
  else
    AstNames.add name ast_names

let build_ast_context ast_values =
  let rec aux ast_names ast_values = function
    | [] -> { ast_names; ast_values = List.rev ast_values }
    | (ast_id, ast_value) :: xs ->
      let ast_names = assign_name ast_names ast_id in
      aux ast_names (ast_value :: ast_values) xs in
  aux AstNames.empty [] ast_values

let insert_at_end ast_id ast_value ast =
  {
    ast_names = AstNames.add ast_id ast.ast_names;
    ast_values = ast.ast_values @ [(ast_id, ast_value)];
  }

(* let linearize_scenario_ast : scenario_ast -> scenario_ast list =
 *   fun ast ->
 *   let rec aux (res : instruction list) uni names *)
