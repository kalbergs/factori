open Ppxlib
open Ast_builder.Default

let fresh_id =
  let counter = ref 0 in
  fun () ->
    let res = !counter in
    incr counter ;
    res

let has_id_in_deep_ldot id x =
  match x with
  | Lident id1 -> id1 = id
  | Ldot (_, id1) -> id1 = id
  | Lapply (_, _) -> false

let aux =
  object (_my_object)
    inherit Ast_traverse.map as super

    method! expression expr =
      (* let id = fresh_id () in
       * Format.eprintf "[%d]processing expr %s@." id (Pprintast.string_of_expression expr); *)
      match expr.pexp_desc with
      (* field of a record *)
      | Pexp_field
          ( { pexp_desc = Pexp_ident { txt = _; _ }; pexp_loc_stack = []; _ },
            { txt = Lident _; _ } ) ->
        let loc = expr.pexp_loc in
        [%expr Factori_abstract_types.Abstract.lift [%e expr]]
      | Pexp_construct ({ txt = Lident "[]"; _ }, _) ->
        (* Format.eprintf "[in aux Pexp_construct []]%s@." (Pprintast.string_of_expression expr); *)
        (* let loc = expr.pexp_loc in *)
        [%expr [%e expr]]
      | Pexp_construct
          ( { txt; _ },
            Some
              { pexp_desc = Pexp_construct ({ txt = Lident "[]"; _ }, None); _ }
          )
        when has_id_in_deep_ldot "Literal" txt ->
        let loc = expr.pexp_loc in
        [%expr Factori_abstract_types.Abstract.lift [%e expr]]
      | Pexp_apply
          ( {
              pexp_desc = Pexp_ident { txt = Ldot (Lident "Z", "of_int"); _ };
              _;
            },
            [(_, _)] ) ->
        let loc = expr.pexp_loc in
        [%expr Factori_abstract_types.Abstract.lift [%e expr]]
      | Pexp_apply
          ( {
              pexp_desc = Pexp_ident { txt = Ldot (Lident "Z", "of_string"); _ };
              _;
            },
            [(_, _)] ) ->
        let loc = expr.pexp_loc in
        [%expr Factori_abstract_types.Abstract.lift [%e expr]]
      | Pexp_construct ({ txt; _ }, _) when has_id_in_deep_ldot "Id" txt ->
        (* Format.eprintf "[in aux Pexp_construct Id]%s@." (Pprintast.string_of_expression expr); *)
        expr
      | Pexp_open (open_declaration, expr) ->
        (* Format.eprintf "[in aux Pexp_open]%s@." (Pprintast.string_of_expression expr); *)
        {
          expr with
          pexp_desc =
            Pexp_open
              ( open_declaration,
                let loc = expr.pexp_loc in
                [%expr
                  Factori_abstract_types.Abstract.lift
                    [%e super#expression expr]] );
        }
      | Pexp_construct
          ( ({ txt = Lident "::"; _ } as li),
            Some ({ pexp_desc = Pexp_tuple [x; rest]; _ } as e) ) ->
        (* Format.eprintf "[list case, before]%s@." (Pprintast.string_of_expression expr); *)
        let e1 = _my_object#expression x in
        let e2 = _my_object#expression rest in
        let new_expr =
          {
            expr with
            pexp_desc =
              Pexp_construct
                (li, Some { e with pexp_desc = Pexp_tuple [e1; e2] });
          } in
        (* (Ocaml_common.Printast.expression 0 Format.str_formatter (rest));
         * let s = Format.flush_str_formatter () in *)
        (* Format.eprintf "[list case, after]%s@." s; *)
        new_expr
      | Pexp_record (l, x) ->
        (* Format.eprintf "record@."; *)
        let aux_record = function
          (* case field_name = list *)
          | ( t,
              {
                pexp_desc =
                  Pexp_construct
                    ( ({ txt = Lident "::"; _ } as li),
                      Some ({ pexp_desc = Pexp_tuple l; _ } as e) );
                _;
              } ) ->
            let new_expr =
              {
                expr with
                pexp_desc =
                  Pexp_construct
                    ( li,
                      Some
                        {
                          e with
                          pexp_desc =
                            Pexp_tuple (List.map _my_object#expression l);
                        } );
              } in
            ( t,
              let loc = new_expr.pexp_loc in
              [%expr Factori_abstract_types.Abstract.lift [%e new_expr]] )
          | ( t,
              ({
                 pexp_desc = Pexp_construct ({ txt = Lident "[]"; _ }, None);
                 _;
               } as subexpr) ) ->
            ( t,
              let loc = subexpr.pexp_loc in
              [%expr
                Factori_abstract_types.Abstract.lift
                  [%e _my_object#expression subexpr]] )
          (* general case *)
          | t, expr ->
            (* Format.eprintf "[in aux, general case]%s@." (Pprintast.string_of_expression expr); *)
            (t, _my_object#expression expr) in
        let rec_expr =
          { expr with pexp_desc = Pexp_record (List.map aux_record l, x) } in
        let loc = rec_expr.pexp_loc in
        [%expr Factori_abstract_types.Abstract.lift [%e rec_expr]]
      | _ ->
        let new_expr =
          let loc = expr.pexp_loc in
          [%expr
            Factori_abstract_types.Abstract.lift [%e super#expression expr]]
        in
        (* Ocaml_common.Printast.expression 0 Format.str_formatter new_expr ;
         * let s = Format.flush_str_formatter () in
         * Format.eprintf "[list case, after]%s@." s ; *)
        new_expr
  end

let to_ml =
  object (_self)
    inherit Ast_traverse.map as super

    method! value_binding vb =
      match
        ( List.exists
            (fun a -> a.attr_name.txt = "storage" || a.attr_name.txt = "param")
            vb.pvb_attributes,
          vb.pvb_pat.ppat_desc )
      with
      | true, Ppat_var { txt = _id; _ } ->
        let expr = aux#expression vb.pvb_expr in
        (* Format.eprintf "[in value binding]%s@." (Pprintast.string_of_expression expr); *)
        value_binding ~loc:vb.pvb_loc ~pat:vb.pvb_pat ~expr
      | _ ->
        (* Format.eprintf "[in value binding, default case]%s@." (Pprintast.string_of_expression vb.pvb_expr); *)
        super#value_binding vb
  end

let () =
  Ppxlib.Driver.register_transformation "factori_scenario_ppx"
    ~impl:to_ml#structure
