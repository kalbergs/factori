open Scenario_dsl
open AstInterface
module AstToContextInstance = Ast_to_context.AstToContext (Context.Context)
open Format

(* Compilation to OCaml *)
let ocaml_of_scenario ?(funding = true) ?(emptying = true) ppf (s : scenario) =
  let context_from_ast = AstToContextInstance.compile (get_ast s) in
  fprintf ppf "%a"
    (Scenario_to_ocaml.pp_scenario_from_sequences_to_ocaml ~funding ~emptying)
    (List.concat
       (List.map Context.Context.context_to_sequences context_from_ast))

(* Compilation to Typescript *)
let typescript_of_scenario ?(funding = true) ?(emptying = true) ppf
    (s : scenario) =
  let context_from_ast = AstToContextInstance.compile (get_ast s) in
  fprintf ppf "%a"
    (Scenario_to_typescript.pp_scenario_from_sequences_to_typescript ~funding
       ~emptying)
    (List.concat
       (List.map Context.Context.context_to_sequences context_from_ast))

let shell_of_scenario ?(funding = true) ppf (s : scenario) =
  let context_from_ast = AstToContextInstance.compile (get_ast s) in
  fprintf ppf "%a"
    (Scenario_to_shell.pp_scenario_from_sequences_to_shell ~funding)
    (List.concat
       (List.map Context.Context.context_to_sequences context_from_ast))
