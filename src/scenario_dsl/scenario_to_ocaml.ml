open Context
open Format
open Tzfunc.Proto
open Context
open Scenario_value

let show_code c =
  Format.asprintf "EzEncoding.destruct script_expr_enc.json {|%s|}"
    (EzEncoding.construct script_expr_enc.json c)

let show_micheline m =
  Format.asprintf "EzEncoding.destruct micheline_enc.json {|%s|}"
    (EzEncoding.construct micheline_enc m)

let show_generate_identity universe i =
  Format.asprintf "Blockchain.get_identity (\"%d_%a\")" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ";")
       (fun ppf d -> fprintf ppf "%d" d))
    universe

let show_amount a = Format.sprintf "(Int64.of_string \"%s\")" (Z.to_string a)

let show_named_value vl =
  let rec aux res ?(first = false) = function
    | x :: xs ->
      let x =
        if first then
          String.capitalize_ascii x
        else
          x in
      let res =
        sprintf "%s%s%s" res
          (if first then
            ""
          else
            ".")
          x in
      aux res xs
    | [] -> res in
  aux "" ~first:true vl

(* let show_baseNative_ocaml contract_name b =
 *   match b with
 *   | RandomB s -> sprintf "%s_generator ()" s
 *   | InitialBlockchainStorage -> sprintf "%s_ocaml_interface.initial_blockchain_storage" (String.capitalize_ascii contract_name)
 *
 * let show_native_type_ocaml names universe base contract_name n =
 *   let open Factori_utils in
 *   let rec aux subscripts = function
 *     | Unchanged -> (show_baseNative_ocaml contract_name base)
 *     | Subst(id,field_name) ->
 *        asprintf " with %s = %a" field_name (print_name ~underscore:true names universe)
 *     id
 *     | Subfield (field_name,n) ->
 *        asprintf " with %s = {%s.%a %s}"
 *          field_name
 *          (show_baseNative_ocaml contract_name base)
 *          (pp_print_list
 *             ~pp_sep:(tag ".")
 *             (fun ppf x -> fprintf ppf "%s" x))
 *          (subscripts@[field_name])
 *          (aux (field_name::subscripts) n)
 *   in sprintf "%s_ocaml_interface.storage_encode{%s %s}\n" (String.capitalize_ascii contract_name) (show_baseNative_ocaml contract_name base)
 *        (aux [] n) *)

let show_kt1_ocaml names universe = function
  | StaticKt1 kt1 -> sprintf "%s" kt1
  | IdKt1 id -> asprintf "%a" (print_name ~underscore:true names universe) id

let pp_use_uv_ocaml ?(static = false) ?(alter = fun x -> x) names universe ppf
    id =
  fprintf ppf "%a"
    (print_name ~underscore:true ~static ~alter names universe)
    id

let print_id_ocaml names universe ppf idpointer =
  fprintf ppf "%a" (pp_use_uv_ocaml names universe) idpointer

let show_uv_ocaml names universe = function
  (* | NamedValue vl -> show_named_value vl *)
  | DefaultIdentity ParameterNetwork ->
    sprintf "(Blockchain.get_default_identity network)"
  | DefaultIdentity (SpecificNetwork n) ->
    sprintf "(Blockchain.get_node %s)" (Factori_utils.get_default_identity n)
  | Code_uv c -> show_code c
  | Amount_uv amount -> show_amount amount
  | Kt1_uv kt1 -> show_kt1_ocaml names universe kt1 (* phantom printer *)
  | Network_uv ParameterNetwork -> "network"
  | Network_uv network -> show_network network
  | String_uv s -> sprintf "\"%s\"" s
  | Entrypoint_name_uv s -> sprintf "\"%s\"" s
  | Micheline_uv c -> show_micheline c
  | Address_uv address -> show_address address
  | Generate_key seed -> show_generate_identity universe seed
  | Id id_pointer -> asprintf "%a" (print_id_ocaml names universe) id_pointer
  | NativeAbstractValue (AValue v) ->
    asprintf "%a" (Pp_abstract_value.pp_value_to_ocaml names universe) v
  | FunctionCall (GetAddress, [id]) ->
    asprintf "(%a).pkh" (print_id_ocaml names universe) id
  | FunctionCall (GetBalance, [id]) ->
    asprintf
      "Blockchain.get_balance ~base:(EzAPI.BASE (Blockchain.get_node network)) \
       ~addr:(%a) ()"
      (print_id_ocaml names universe)
      id
  | FunctionCall (GetStorage, [id]) ->
    asprintf "get_storage (%a)" (print_id_ocaml names universe) id
  | FunctionCall (f, _) ->
    failwith
      (sprintf "Wrong number of arguments: %s has one argument" (show_func f))

let pp_definition_uv_ocaml names universe id ppf uv =
  fprintf ppf "let %a = %s in@."
    (print_name ~underscore:true names universe)
    id
    (show_uv_ocaml names universe uv)

let print_network_ocaml ppf network =
  match network with
  | SpecificNetwork n -> fprintf ppf "\"%s\"" n
  | ParameterNetwork -> fprintf ppf "network"

let print_deploy_ocaml names universe ppf id (d : u_id deploy) =
  let ct_ocml_intf =
    sprintf "%s_ocaml_interface" (String.capitalize_ascii d.contract_name) in
  fprintf ppf
    "let>? %a,_ = %s.deploy ~node:(Blockchain.get_node %a) ~from:%a ~amount:%a \
     %a in\n\
     Format.eprintf \"[scenario%a]Deployed KT1: %%s%@.\" %a;\n"
    (pp_use_uv_ocaml names universe)
    id ct_ocml_intf
    (pp_use_uv_ocaml names universe)
    d.network
    (pp_use_uv_ocaml names universe)
    d.from
    (pp_use_uv_ocaml names universe)
    d.amount
    (pp_use_uv_ocaml names universe)
    d.storage Universes.print_universe universe
    (pp_use_uv_ocaml names universe)
    id

let print_call_ocaml names universe id ppf (c : u_id call) =
  let contract_interface =
    sprintf "%s_ocaml_interface" (String.capitalize_ascii c.contract_name) in
  fprintf ppf
    "let>? %a = %s(%s.call_%s ~node:(Blockchain.get_node %a) ~amount:%a \
     ~from:%a ~kt1:%a) %a in@."
    (pp_use_uv_ocaml names universe)
    id
    (if c.assert_success then
      asprintf "Blockchain.assert_success ~msg:\"%s\" ~prefix:\"%a\"" c.msg
        Universes.print_universe universe
    else
      "")
    contract_interface c.entrypoint
    (pp_use_uv_ocaml names universe)
    c.network
    (pp_use_uv_ocaml names universe)
    c.amount
    (pp_use_uv_ocaml names universe)
    c.from
    (pp_use_uv_ocaml names universe)
    c.contract
    (pp_use_uv_ocaml names universe)
    c.param

let print_failed_call_ocaml names universe id ppf (expected_bc_error : string)
    msg (c : u_id call) =
  let contract_interface =
    sprintf "%s_ocaml_interface" (String.capitalize_ascii c.contract_name) in
  fprintf ppf
    "let>? %a = %s.assert_failwith_str_%s ~node:(Blockchain.get_node %a) \
     ~expected:\"%s\" ~amount:%a ~from:%a ~kt1:%a ~prefix:\"[scenario%a]\" \
     ~msg:\"%s\" %a in@."
    (pp_use_uv_ocaml names universe)
    id contract_interface c.entrypoint
    (pp_use_uv_ocaml names universe)
    c.network expected_bc_error
    (pp_use_uv_ocaml names universe)
    c.amount
    (pp_use_uv_ocaml names universe)
    c.from
    (pp_use_uv_ocaml names universe)
    c.contract Universes.print_universe universe msg
    (pp_use_uv_ocaml names universe)
    c.param

let print_print_ocaml ~msg names universe id ppf (u : u_id uv) =
  let msg =
    if msg = "" then
      ""
    else
      sprintf "[%s]" msg in
  let prefix = asprintf "[%a]" Universes.print_universe universe in
  match u with
  | FunctionCall (GetAddress, [_identity]) ->
    fprintf ppf "let %a = Format.eprintf \"%s[address]%s %%s@@.\" %s in\n"
      (pp_use_uv_ocaml names universe)
      id prefix msg
      (show_uv_ocaml names universe u)
  | FunctionCall (GetBalance, [_]) ->
    fprintf ppf
      "let>? balance = %s in\n\
       let %a = Format.eprintf \"%s[balance]%s %%s@@.\" (Int64.to_string \
       balance) in\n"
      (show_uv_ocaml names universe u)
      (pp_use_uv_ocaml names universe)
      id prefix msg
  | Code_uv _
  | Amount_uv _
  | Kt1_uv _
  | Network_uv _
  | String_uv _
  | Entrypoint_name_uv _
  | Micheline_uv _
  | Address_uv _
  | Generate_key _
  | DefaultIdentity _
  | Id _
  | NativeAbstractValue _
  | FunctionCall ((GetBalance | GetStorage | GetAddress), _) ->
    failwith
      (asprintf "[print_print_ocaml] TODO: %s"
         (show_uv (fun ppf _ -> fprintf ppf "<>") u))

let pp_seq_instruction_to_ocaml names universe ppf
    ((id : u_id), (v : u_id value)) =
  match v with
  | Unary u -> pp_definition_uv_ocaml names universe id ppf u
  | Checkpoint -> (* fprintf ppf "(\* Checkpoint before fork *\)@." *) ()
  | Deploy deploy -> print_deploy_ocaml names universe ppf id deploy
  | Call call -> print_call_ocaml names universe id ppf call
  | FailedCall (Failed (s, msg, call)) ->
    print_failed_call_ocaml names universe id ppf s msg call
  | PrintUnary (msg, u) -> print_print_ocaml ~msg names universe id ppf u
  | Fork _ | Seq _ -> ()

let open_abstract_interfaces ppf (s : sequence) =
  let contract_names = Context.collect_contract_names s in
  fprintf ppf "\n%a"
    (pp_print_list ~pp_sep:(Factori_utils.tag "") (fun ppf contract_name ->
         fprintf ppf "let open %s_ocaml_interface in\n"
           (String.capitalize_ascii contract_name)))
    contract_names

(* Print a linear sequence of instructions to an OCaml scenario *)
let pp_sequence_to_ocaml ppf (s : sequence) =
  List.iter
    (fprintf ppf
       "(* Universe %a *)@.let scenario%a network () =\n\
        %alet> _ = Lwt_io.printf \"[scenario%a]Entering scenario%a\\n%%!\" in@."
       Universes.print_universe s.seq_universe Universes.print_universe
       s.seq_universe open_abstract_interfaces s Universes.print_universe
       s.seq_universe Universes.print_universe s.seq_universe ;
     pp_seq_instruction_to_ocaml s.seq_names s.seq_universe ppf)
    s.seq ;
  let n = List.length s.seq in
  if n = 0 then
    fprintf ppf
      "Lwt.return_ok () (* Warning: there was an empty list of instructions *)@.\n"
  else
    let last_element = List.nth s.seq (n - 1) in
    let last_name =
      Format.asprintf "%a"
        (print_name ~underscore:true s.seq_names s.seq_universe)
        (fst last_element) in
    fprintf ppf "Lwt.return_ok (%s)@.\n" last_name

let pp_funding ppf (ss : sequence list) =
  let addr_amount_map =
    Compilation_utils.get_addr_balances ~show_uv:show_uv_ocaml ss in
  fprintf ppf
    "let fund network =\n\
     let>? _ = Blockchain.make_mass_transfer ~node:(Blockchain.get_node \
     network) ~from:Blockchain.alice_flextesa\n\
     [%a;\n\
     (\"tz1burnburnburnburnburnburnburjAYjjX\",Int64.one);\n\
     (\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",Int64.one)]\n\
     in Lwt.return_ok ()@.@."
    (pp_print_seq ~pp_sep:(Factori_utils.tag ";\n      ") (fun ppf (k, v) ->
         fprintf ppf "(%s).pkh,(Int64.of_string (\"%s\"))" k v))
    (AddrBalances.to_seq
       (AddrBalances.filter (fun _k v -> v <> "0") addr_amount_map))

let pp_empty_accounts ppf (ss : sequence list) =
  let addr_amount_map =
    Compilation_utils.get_addr_balances ~show_uv:show_uv_ocaml ss in
  fprintf ppf
    "let empty_accounts network =\n\
     let> _ = Blockchain.do_empty_accounts ~node:(Blockchain.get_node network) \n\
     [%a]\n\
     in Lwt.return ()@.@."
    (pp_print_seq ~pp_sep:(Factori_utils.tag ";\n      ") (fun ppf (k, _) ->
         fprintf ppf "%s" k))
    (AddrBalances.to_seq addr_amount_map)

let pp_scenario_from_sequences_to_ocaml ?(funding = true) ?(emptying = true) ppf
    (ss : sequence list) =
  Format.fprintf ppf "open Tzfunc.Proto@.\nopen Tzfunc.Rp@.\n" ;
  if funding then pp_funding ppf ss ;
  if emptying then pp_empty_accounts ppf ss ;
  List.iter (pp_sequence_to_ocaml ppf) ss ;
  fprintf ppf
    "let network = \"flextesa\"\n\n\
     let all_in_parallel =\n\
    \  Tzfunc.Node.set_silent true;\n\
    \  Lwt_main.run @@@@\n" ;
  if emptying then fprintf ppf "let> _ = empty_accounts network in\n" ;
  if funding then fprintf ppf "let>? _ = fund network in\n" ;
  if List.length ss = 1 then
    fprintf ppf "  let>? _ = scenario network () in\nLwt.return_ok ()\n"
  else
    fprintf ppf
      "let catch (type a) (scenario : string -> unit -> (a,error) result \
       Lwt.t) =\n\
      \    let> res = scenario network () in\n\
      \    match res with\n\
      \    | Ok _res -> Lwt.return ()\n\
      \    | Error e -> failwith (Format.sprintf \"[catch] error: %%s\" \
       (Tzfunc.Rp.string_of_error e)) in\n\n\
      \  let> _l = Lwt.join [%a] in\n\
      \               Lwt.return_ok ()"
      (pp_print_list ~pp_sep:(Factori_utils.tag ";") (fun ppf s ->
           fprintf ppf "catch scenario%a" Universes.print_universe
             s.seq_universe))
      ss

(* fprintf ppf "let network = \"jakarta\"\nlet _ =
 * Lwt_main.run @@@@ let>? _ = fund network in scenario0 network ()" *)
