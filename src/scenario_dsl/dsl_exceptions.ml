open Factori_errors

exception Scenario_key_already_used

exception Scenario_key_not_found

(* TODO: add a function to handle all these new errors *)
exception OpenForkRemaining of int

let _ =
  add_exception_handler (function
    | Scenario_key_already_used ->
      Format.eprintf "Error: Scenario key already used"
    | Scenario_key_not_found -> Format.eprintf "Error: Scenario key not found"
    | OpenForkRemaining f ->
      Format.eprintf "You forgot to close an open fork (#%d) in your scenario" f
    | e -> raise e)
