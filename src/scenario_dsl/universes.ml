open Format

(* Every scenario happens inside a 'universe'. A traditional scenario,
   with no forks, lives in one boring universe U (also represented as
   U[], as a convention) and there isn't much to say about
   it. However, a universe can be duplicated into two subuniverses,
   say U[0] and U[1]. Every object gets a subscript so that these
   universes are (usually) disjoint. For example, if the 'mother'
   universe had a generated tz1 account Alice, then universe U[0] has
   a generated tz1 account Alice[0] and universe U[1] has a generated
   tz1 account Alice[1], and Alice[0] and Alice[1] are distinct. Of
   course, universes can be subdivided arbitrarily, so that U[0] can
   be split into U[0;0] and U[0;1] *)
type universe = int list [@@deriving show, encoding]

let print_universe ppf universe =
  fprintf ppf "%a"
    (pp_print_list ~pp_sep:(Factori_utils.tag "_") (fun ppf x ->
         fprintf ppf "%d" x))
    universe

let subuniverse universe i = universe @ [i]

type universe_id = {
  universe : universe;
  id : int;
}
[@@deriving show, encoding]
