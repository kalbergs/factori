open Format
open Context

(* We need to be able to convert the AST to a Context *)
module AstToContext (Ctxt : CONTEXT) = struct
  open Ctxt
  open Scenario_value
  open Ast
  module S = Scenario.Scenario (Ctxt)

  let clean_assoc ast_id (assoc : (string * 'id) list) =
    try List.assoc ast_id assoc
    with Not_found ->
      let assoc_str =
        Format.asprintf "[%a]"
          (pp_print_list
             ~pp_sep:(fun ppf _ -> fprintf ppf ";")
             (fun ppf x -> fprintf ppf "%s" (fst x)))
          assoc in
      failwith
        (Format.sprintf "Couldn't find ast_id %s (assoc was %s)" ast_id
           assoc_str)

  let rec infer_abstract_value_dependencies :
            'a. u_id -> context -> 'a Abstract_value.value -> context =
    fun (type a) (cur_id : u_id) (cur_ctxt : context)
        (v : a Abstract_value.value) ->
     let open Abstract_value in
     match v with
     | VAbstract (UniId id) -> add_dependency cur_ctxt id cur_id
     | VAbstract (ScenId _id) ->
       (* add_dependency cur_ctxt id cur_id *)
       failwith "there can't be scenario ids here"
     | VUnit _
     | VInt _
     | VString _
     | VBytes _
     | VTimestamp _
     | VBool _
     | VAddress _
     | VTez _
     | VKey _
     | VKeyHash _
     | VSignature _
     | VOperation _
     | VOnChainBigMap _ -> cur_ctxt
     | VTuple (Single v) -> infer_abstract_value_dependencies cur_id cur_ctxt v
     | VTuple (Multi (v, rest)) ->
       let ctxt = infer_abstract_value_dependencies cur_id cur_ctxt v in
       infer_abstract_value_dependencies cur_id ctxt (VTuple rest)
     | VLiteralBigMap l ->
       List.fold_right
         (fun ab ctxt ->
           match ab with
           | SP ab -> infer_abstract_value_dependencies cur_id ctxt ab
           | SP2 (a, b) ->
             let ctxt1 = infer_abstract_value_dependencies cur_id ctxt a in
             let ctxt2 = infer_abstract_value_dependencies cur_id ctxt1 b in
             ctxt2)
         l cur_ctxt
     | VMap l ->
       List.fold_right
         (fun ab ctxt ->
           match ab with
           | SP ab -> infer_abstract_value_dependencies cur_id ctxt ab
           | SP2 (a, b) ->
             let ctxt1 = infer_abstract_value_dependencies cur_id ctxt a in
             let ctxt2 = infer_abstract_value_dependencies cur_id ctxt1 b in
             ctxt2)
         l cur_ctxt
     | VSet l ->
       List.fold_right
         (fun x ctxt ->
           let ctxt = infer_abstract_value_dependencies cur_id ctxt x in
           ctxt)
         l cur_ctxt
     | VList l ->
       List.fold_right
         (fun x ctxt ->
           let ctxt = infer_abstract_value_dependencies cur_id ctxt x in
           ctxt)
         l cur_ctxt
     | VOption None -> cur_ctxt
     | VOption (Some v) -> infer_abstract_value_dependencies cur_id cur_ctxt v
     | VLambda _ -> cur_ctxt
     | VRecord (LeafP (_, v)) ->
       infer_abstract_value_dependencies cur_id cur_ctxt v
     | VRecord (PairP (r1, r2)) ->
       let ctxt1 =
         infer_abstract_value_dependencies cur_id cur_ctxt (VRecord r1) in
       let ctxt2 = infer_abstract_value_dependencies cur_id ctxt1 (VRecord r2) in
       ctxt2
     | VSumtype (LeafOr (_, v)) ->
       infer_abstract_value_dependencies cur_id cur_ctxt v
     | VSumtype (SumOrL v1) ->
       infer_abstract_value_dependencies cur_id cur_ctxt (VSumtype v1)
     | VSumtype (SumOrR v2) ->
       infer_abstract_value_dependencies cur_id cur_ctxt (VSumtype v2)
     | VChain_id _ | VTicket _ | VSaplingState _ -> cur_ctxt

  let infer_unary_dependencies cur_id (cur_ctxt : context) (u : u_id uv) =
    match u with
    | Id id -> add_dependency cur_ctxt id cur_id
    | Kt1_uv (IdKt1 id) ->
      let cur_ctxt = add_dependency cur_ctxt id cur_id in
      cur_ctxt
    | NativeAbstractValue (AValue av) ->
      infer_abstract_value_dependencies cur_id cur_ctxt av
    | FunctionCall (_f, l) ->
      List.fold_right
        (fun id cur_ctxt -> add_dependency cur_ctxt id cur_id)
        l cur_ctxt
    | Kt1_uv (StaticKt1 _)
    | Code_uv _
    | Amount_uv _
    | Network_uv _
    | String_uv _
    | Entrypoint_name_uv _
    | Micheline_uv _
    | Address_uv _
    | Generate_key _
    | DefaultIdentity _ -> cur_ctxt

  (** This crucial function takes a scenario which is essentially a
     tree of unlimited arity, and linearizes it into line graph
     scenarios ("contexts"). Each context lives in a separate
     universe, indexed by its position in the original tree. *)
  let compile (ast : Ast.ast_context) : context list =
    (* Format.eprintf "entering compile@." ; *)
    let rec aux (cur_ctxt : context) (assoc : (ast_id * u_id) list) :
        (ast_id * ast_id value) list -> context list = function
      | [] -> [cur_ctxt]
      | (ast_id, ast_value) :: xx ->
        let _ids, cur_ctxts, assocs, xxs =
          one_step cur_ctxt assoc ast_id ast_value xx in
        List.concat
        @@ Factori_utils.map3 ~reference:"aux"
             (fun cur_ctxt assoc xx -> aux cur_ctxt assoc xx)
             cur_ctxts assocs xxs
    and one_step cur_ctxt assoc ast_id ast_value xx =
      let ids, ctxts, assocs, xxs =
        match ast_value with
        | Unary u1 ->
          let u1 =
            map_unary_different_types
              ~f:(fun ast_id -> clean_assoc ast_id assoc)
              u1 in
          let u = Unary u1 in
          let id, cur_ctxt = build_cst ~name:(Some ast_id) cur_ctxt u in
          let cur_ctxt = infer_unary_dependencies id cur_ctxt u1 in
          let assoc = (ast_id, id) :: assoc in
          ([id], [cur_ctxt], [assoc], [xx])
        | Fork (l : (ast_id * ast_id Scenario_value.value) list) ->
          let open Universes in
          (* Function to specialize assoc to sub-universe i *)
          let subassoc i =
            List.map
              (fun (ast_id, id) ->
                ( ast_id,
                  { id with universe = subuniverse (get_universe cur_ctxt) i }
                ))
              assoc in
          (* let cur_ctxt = add_node cur_ctxt checkpoint_id new_value in *)
          (* let _ = Format.eprintf "Before: %a@." Ctxt.print_context cur_ctxt in *)
          let all_contexts =
            List.mapi (fun i _ -> subcontext (cur_ctxt : context) i) l in
          (* let _ = List.iter (fun c -> Format.eprintf "After: %a@." Ctxt.print_context c) all_contexts in *)
          let l_with_indices :
              (int * (ast_id * ast_id Scenario_value.value)) list =
            List.mapi (fun i x -> (i, x)) l in

          let cur_ctxts, whatisthis, assocs, xxs =
            Factori_utils.split4
            @@ List.map2
                 (fun context
                      ( (index : int),
                        ((id : ast_id), (value : ast_id Scenario_value.value))
                      ) ->
                   (* Context has already been 'subbed' into subuniverse index *)
                   let new_universe = get_universe context in
                   let subid (x : u_id) = { x with universe = new_universe } in
                   let assoc = subassoc index in
                   let id_curvalues, cur_ctxts, assocs, xx =
                     one_step context assoc id value xx in
                   let id_curvalues = List.map subid id_curvalues in
                   ((cur_ctxts, (id, id_curvalues)), (assocs, xx)))
                 all_contexts l_with_indices in
          ( List.concat @@ List.map snd whatisthis,
            List.concat cur_ctxts,
            List.concat assocs,
            List.concat xxs )
        | Seq [] ->
          let new_id, cur_ctxt = fresh_id cur_ctxt in
          ([new_id], [cur_ctxt], [assoc], [xx])
        | Seq ((ast_id_h, h) :: hs) ->
          (* eprintf "[one_step] Processing Seq(%a)@." (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ";") (fun ppf (ast_id,_) -> fprintf ppf "%s" ast_id)) seq; *)
          let ids, cur_ctxts, assoc, xxs =
            one_step cur_ctxt assoc ast_id_h h xx in
          ( ids,
            cur_ctxts,
            assoc,
            List.map (fun xx -> (ast_id, Seq hs) :: xx) xxs )
        | Deploy deploy ->
          let deploy =
            {
              from = clean_assoc deploy.from assoc;
              amount = clean_assoc deploy.amount assoc;
              network = clean_assoc deploy.network assoc;
              storage = clean_assoc deploy.storage assoc;
              contract_name = deploy.contract_name;
            } in
          let id, cur_ctxt = S.make_deploy ~name:(Some ast_id) cur_ctxt deploy in
          let cur_ctxt = add_dependency cur_ctxt deploy.storage id in
          let cur_ctxt = add_dependency cur_ctxt deploy.from id in
          let cur_ctxt = add_dependency cur_ctxt deploy.amount id in
          let cur_ctxt = add_dependency cur_ctxt deploy.network id in

          let assoc = (ast_id, id) :: assoc in
          ([id], [cur_ctxt], [assoc], [xx])
        | Call call ->
          let call =
            {
              from = clean_assoc call.from assoc;
              amount = clean_assoc call.amount assoc;
              entrypoint = call.entrypoint;
              contract = clean_assoc call.contract assoc;
              network = clean_assoc call.network assoc;
              param = clean_assoc call.param assoc;
              contract_name = call.contract_name;
              assert_success = call.assert_success;
              msg = call.msg;
            } in
          let id, cur_ctxt = S.make_call ~name:(Some ast_id) cur_ctxt call in
          let cur_ctxt = add_dependency cur_ctxt call.from id in
          let cur_ctxt = add_dependency cur_ctxt call.contract id in
          let cur_ctxt = add_dependency cur_ctxt call.param id in
          let cur_ctxt = add_dependency cur_ctxt call.amount id in
          let cur_ctxt = add_dependency cur_ctxt call.network id in
          let assoc = (ast_id, id) :: assoc in
          ([id], [cur_ctxt], [assoc], [xx])
        | FailedCall (Failed (x, y, call)) ->
          let call =
            {
              from = clean_assoc call.from assoc;
              amount = clean_assoc call.amount assoc;
              entrypoint = call.entrypoint;
              contract = clean_assoc call.contract assoc;
              network = clean_assoc call.network assoc;
              param = clean_assoc call.param assoc;
              contract_name = call.contract_name;
              assert_success = call.assert_success;
              msg = call.msg;
            } in
          let id, cur_ctxt =
            S.make_failed_call ~name:(Some ast_id) cur_ctxt x y call in
          let cur_ctxt = add_dependency cur_ctxt call.from id in
          let cur_ctxt = add_dependency cur_ctxt call.contract id in
          let cur_ctxt = add_dependency cur_ctxt call.param id in
          let cur_ctxt = add_dependency cur_ctxt call.amount id in
          let cur_ctxt = add_dependency cur_ctxt call.network id in
          let assoc = (ast_id, id) :: assoc in
          ([id], [cur_ctxt], [assoc], [xx])
        | PrintUnary (msg, u1) ->
          let u1 =
            map_unary_different_types
              ~f:(fun ast_id -> clean_assoc ast_id assoc)
              u1 in
          let u = PrintUnary (msg, u1) in
          let id, cur_ctxt = build_cst ~name:(Some ast_id) cur_ctxt u in
          let cur_ctxt = infer_unary_dependencies id cur_ctxt u1 in
          let assoc = (ast_id, id) :: assoc in
          ([id], [cur_ctxt], [assoc], [xx])
        | Checkpoint ->
          let new_id, cur_ctxt = fresh_id cur_ctxt in
          let new_value = Checkpoint in
          let cur_ctxt = add_node cur_ctxt new_id new_value in
          let assoc = (ast_id, new_id) :: assoc in
          ([new_id], [cur_ctxt], [assoc], [xx]) in
      (ids, ctxts, assocs, xxs) in
    let res = aux (init_context ()) [] ast.ast_values in
    (* Format.eprintf "leaving compile@." ; *)
    res
end
