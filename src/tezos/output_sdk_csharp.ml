open Format
open Infer_entrypoints
open Pair_type
open Value
open Or_type
open Types
open Factori_utils
open Factori_errors

(* C# *)

let rec pp_value_to_csharp ?(first_tuple_as_parameters = false) ~prefix ppf =
  let pp_value_to_csharp =
    pp_value_to_csharp ~first_tuple_as_parameters:false ~prefix in
  function
  | VChain_id s -> fprintf ppf "\"%s\"" s
  | VUnit _ -> fprintf ppf "new %s.Unit()" prefix
  | VKeyHash kh -> fprintf ppf "\"%s\"" kh
  | VKey k -> fprintf ppf "\"%s\"" k
  | VSignature s -> fprintf ppf "\"%s\"" s
  | VOperation o -> fprintf ppf "\"%s\"" o
  | VOption (t, None) ->
    fprintf ppf "new %s.None<%a>()" prefix (output_type_pretty ~prefix) t
  | VOption (t, Some x) ->
    fprintf ppf "new %s.Some<%a>((%a) %a)" prefix
      (output_type_pretty ~prefix)
      t
      (output_type_pretty ~prefix)
      t pp_value_to_csharp x
  | VInt z -> print_z_int_csharp ppf z
  | VTez z -> print_z_int_csharp ppf z
  | VString s -> fprintf ppf "\"%s\"" s
  | VBytes s -> fprintf ppf "Utils.StringToByteArray(\"%s\")" (s :> string)
  | VTimestamp s -> fprintf ppf "(\"%s\")" s
  | VBool b ->
    fprintf ppf "%s"
      (if b then
        "true"
      else
        "false")
  | VAddress addr -> fprintf ppf "\"%s\"" addr
  | VSumtype (constructor, _, v) -> (
    match v with
    | VUnit () -> fprintf ppf "{ 'kind' : \"%s_constructor\" }" constructor
    | _ ->
      fprintf ppf "{ 'kind' : \"%s_constructor\", '%s_element' : (%a)}"
        constructor constructor pp_value_to_csharp v)
  | VMap (t1, t2, m) ->
    fprintf ppf "new %s.Map<%a,%a>(){%a}" prefix
      (output_type_pretty ~prefix)
      t1
      (output_type_pretty ~prefix)
      t2
      (pp_print_list ~pp_sep:(tag ",") (fun ppf (x, y) ->
           fprintf ppf "{%a,%a}" pp_value_to_csharp x pp_value_to_csharp y))
      m
  | VSet (t, m) ->
    fprintf ppf "new %s.Set<%a>(){%a}" prefix
      (output_type_pretty ~prefix)
      t
      (pp_print_list ~pp_sep:(tag ",") pp_value_to_csharp)
      m
  | VList (t, m) ->
    fprintf ppf "new List<%a>(){%a}"
      (output_type_pretty ~prefix)
      t
      (pp_print_list ~pp_sep:(tag ",") pp_value_to_csharp)
      m
  | VBigMap (t1, t2, i) ->
    fprintf ppf "new %s.AbstractBigMap<%a,%a>(%a)" prefix
      (output_type_pretty ~prefix)
      t1
      (output_type_pretty ~prefix)
      t2 print_z_int_csharp i
  | VTuple (tl, vl) ->
    if first_tuple_as_parameters then
      fprintf ppf "%a" (pp_print_list ~pp_sep:(tag ",") pp_value_to_csharp) vl
    else
      fprintf ppf "new %s.MyTuple<%a>(%a)" prefix
        (pp_print_list ~pp_sep:(tag ",") (output_type_pretty ~prefix))
        tl
        (pp_print_list ~pp_sep:(tag ",") pp_value_to_csharp)
        vl
  | VLambda _l -> fprintf ppf "new %s.Lambda ()" prefix
  | VRecord (LeafP (field_name, (_, v))) ->
    fprintf ppf "%s = %a" (show_sanitized_name field_name) pp_value_to_csharp v
  | VRecord (PairP _ as p) ->
    fprintf ppf "(new (%a))"
      (pp_pairedtype_typescript ~pairparens:false (* TODO *)
         ~f_leaf:(fun name ppf (t, x) ->
           match t with
           | TVar (n, Base _) ->
             fprintf ppf "%s : new %s(%a)" (show_sanitized_name name)
               (show_sanitized_name @@ bind_sanitize_capitalize n)
               pp_value_to_csharp x
           | _ ->
             fprintf ppf "%s : (%a) %a" (show_sanitized_name name)
               (output_type_pretty ~prefix)
               t pp_value_to_csharp x)
         ~f_pair:(fun ppf _d -> fprintf ppf "")
         ~delimiters:Nothing ~sep:",\n")
      p
  | VTicket s -> fprintf ppf "\"%s\"" s
  | VSaplingState s -> fprintf ppf "\"%s\"" s

and pp_sumtype ~prefix f l ppf x =
  let rec aux ppf = function
    | true :: a -> fprintf ppf "%s.make_left(%a)" prefix aux a
    | false :: b -> fprintf ppf "%s.make_right(%a)" prefix aux b
    | [] -> fprintf ppf "%a" f x in
  aux ppf l

and output_sumtype_encode ~prefix ~typename ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a\n"
    (pp_print_list ~pp_sep:(tag "\n") (fun ppf (k, path) ->
         match k with
         | Annot (t, name) ->
           let name = show_sanitized_name @@ bind_sanitize_capitalize name in
           fprintf ppf
             "\t\tcase(%s_%s_constructor_subtype localarg):\n\t\t\treturn %a;"
             (show_sanitized_name typename)
             name
             (pp_sumtype ~prefix
                (fun ppf at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "%s.UnitEncode(new %s.Unit ())" prefix prefix
                  | _ ->
                    fprintf ppf
                      "%a(localarg.%s_element == null ? throw new \
                       EncodeError(\"null\") : localarg.%s_element)"
                      (output_type_encode ~prefix)
                      at name name)
                path)
             t
         | t_local ->
           let name = constructor_from_path path in
           fprintf ppf
             "\t\tcase(%s_%s_constructor_subtype localarg):\n\t\t\treturn %a;"
             (show_sanitized_name typename)
             name
             (pp_sumtype ~prefix
                (fun ppf _at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "%s.UnitEncode(%s.unit)" prefix prefix
                  | _ ->
                    fprintf ppf
                      "%a(localarg.%s_element == null ? throw new \
                       EncodeError(\"null\") : localarg.%s_element)"
                      (output_type_encode ~prefix)
                      t_local name name)
                path)
             t))
    lst

and discriminate_path ~prefix ~typename argname path ppf t_ =
  let rec aux offset mpname ppf = function
    | [] -> begin
      match t_ with
      | Annot (u_, name) when is_unit u_ ->
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf
          "%s%s_%s_constructor_subtype res = new %s_%s_constructor_subtype();\n\
           res.kind = (\"%s_constructor\");\n\
           return res;\n"
          offset
          (show_sanitized_name typename)
          name
          (show_sanitized_name typename)
          name name
      | Annot (t_, name) ->
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf
          "%s%s_%s_constructor_subtype res = new %s_%s_constructor_subtype();\n\
           res.kind = \"%s_constructor\";\n\
           res.%s_element = %a(%s);\n\
          \ return res;\n"
          offset
          (show_sanitized_name typename)
          name
          (show_sanitized_name typename)
          name name name
          (output_type_decode ~prefix)
          t_ argname
      | t_ ->
        let name = sanitized_of_str @@ constructor_from_path path in
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf
          "%s%s_%s_constructor_subtype res = new %s_%s_constructor_subtype();\n\
           res.kind = \"%s_constructor\";\n\
           res.%s_element = %a(%s);\n\
           return res;" offset
          (show_sanitized_name typename)
          name
          (show_sanitized_name typename)
          name name name
          (output_type_decode ~prefix)
          t_ argname
    end
    | b :: path ->
      fprintf ppf "\nswitch(%s){" argname ;
      begin
        match b with
        | true ->
          (* true is left *)
          let new_offset = sprintf "%s\t" offset in
          fprintf ppf
            "//Left case\n\
             %s\n\
             case MichelinePrim %s:\n\
             switch(%s.Prim){\n\
             case PrimType.Left:\n\
             %s=%s.Args[0];%s\n\
             %a\n\
             %s;\n\n\
            \             case PrimType.Right:\n\
             break;\n\
             default:\n\
             throw new DecodeError($\"{%s} should be Left or Right\");}\n\
             break;"
            offset mpname mpname argname mpname new_offset
            (aux new_offset (mpname ^ "l"))
            path
            (if path = [] then
              ""
            else
              "break")
            mpname
        | false ->
          (* false is right *)
          let new_offset = sprintf "%s  " offset in
          fprintf ppf
            "//Right case\n\
             %s\n\
             case MichelinePrim %s:\n\
             switch(%s.Prim){\n\
             case PrimType.Right:\n\
             %s=%s.Args[0];%s\n\
             %a\n\
             %s;\n\
             case PrimType.Left:\n\
             break;\n\
             default:\n\
            \ throw new DecodeError($\"{%s} should be Left or Right\");}\n\
             break;"
            offset mpname mpname argname mpname new_offset
            (aux new_offset (mpname ^ "r"))
            path
            (if path = [] then
              ""
            else
              "break")
            mpname
      end ;
      fprintf ppf
        "\n\
         default:\n\
         throw new DecodeError($\"Could not decode {%s}, it should be a \
         Prim\");}"
        argname in

  aux "\t\t" "mp_" ppf path

and output_sumtype_decode ~prefix ~typename argname ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "")
       (fun ppf (t_, path) ->
         discriminate_path ~prefix ~typename argname path ppf t_))
    lst

and output_type_pretty ~prefix ppf =
  let output_type_pretty = output_type_pretty in
  function
  | Base String -> fprintf ppf "string"
  | Base Byte -> fprintf ppf "%s.Bytes" prefix
  | Base b ->
    fprintf ppf "%a"
      (pp_base_type ~language:Factori_config.Csharp ~abstract:false
         ~prefix:(Some prefix))
      b
  | Annot (ep, _name) -> fprintf ppf "%a" (output_type_pretty ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.MyTuple<%a>" prefix
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn ->
    fprintf ppf "%s" (show_sanitized_name @@ bind_sanitize_capitalize (fst tn))
  | Unary (List, a) -> fprintf ppf "List<%a>" (output_type_pretty ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.Set<%a>" prefix (output_type_pretty ~prefix) a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.Ticket" prefix (* (output_type_pretty ~prefix) t *)
  | Contract _c ->
    fprintf ppf "%s.Contract" prefix (* (output_type_pretty ~prefix) c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.BigMap<%a,%a>" prefix
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v
  | Binary (Lambda, (_k, _v)) -> fprintf ppf "%s.Lambda" prefix
  (* (output_type_pretty ~prefix)
   * k
   * (output_type_pretty ~prefix)
   * v *)
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.Map<%a,%a>" prefix
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.Option<%a>" prefix (output_type_pretty ~prefix) a

(* Some day, on some contract, this will get more complicated *)
and get_constructor ~prefix b ppf type_ =
  Format.fprintf ppf "%s of (%a)"
    (if b then
      "Left"
    else
      "Right")
    (output_type_content ~prefix)
    type_

and output_type_content ~prefix ppf =
  let output_type_pretty = output_type_pretty in
  let output_type_content = output_type_content in
  function
  | Base _ as t -> (output_type_pretty ~prefix) ppf t
  | Annot (_, _) as t -> (output_type_pretty ~prefix) ppf t
  | Pair al ->
    fprintf ppf "%s.MyTuple<%a>" prefix
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_content ~prefix))
      al
  | Record (LeafP (_n, x)) -> fprintf ppf "%a" (output_type_pretty ~prefix) x
  | Record (PairP l) ->
    let l = flattenP [] l in
    fprintf ppf "%a"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (n, x) ->
           fprintf ppf "%s: %a" (show_sanitized_name n)
             (output_type_pretty ~prefix)
             x))
      l
  (* | Record _ -> fprintf ppf "<TODO: record type content>" *)
  | Or (OrAbs (a1, a2)) ->
    fprintf ppf "| %a | %a"
      (get_constructor ~prefix true)
      (Or a1)
      (get_constructor ~prefix false)
      (Or a2)
  | Or (LeafOr _) -> fprintf ppf "LeafOr TODO"
  | TVar tn ->
    fprintf ppf "%s" (show_sanitized_name @@ bind_sanitize_capitalize (fst tn))
  | Unary (List, a) -> fprintf ppf "List<%a>" (output_type_content ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.Set<%a>" prefix (output_type_content ~prefix) a
  | Contract _c -> fprintf ppf "%s.Contract" prefix (* pp_micheline_ c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.BigMap<%a,%a>" prefix
      (output_type_content ~prefix)
      k
      (output_type_content ~prefix)
      v
  | Binary (Lambda, (_k, _v)) -> fprintf ppf "%s.Lambda" prefix
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.Map<%a,%a>" prefix
      (output_type_content ~prefix)
      k
      (output_type_content ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.Option<%a>" prefix (output_type_content ~prefix) a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.Ticket" prefix (* (output_type_content ~prefix) t *)

and define_sumtype_subtypes ~typename ~prefix ppf t =
  let lst = sumtype_to_list t in
  let rec aux res = function
    | [] -> res
    | (t_, path) :: xs ->
      let new_res =
        let cur_elem =
          match t_ with
          | Annot (u_, name) when is_unit u_ ->
            let name = show_sanitized_name @@ bind_sanitize_capitalize name in
            Format.asprintf
              "class %s_%s_constructor_subtype: %s{\n\
               \tpublic string kind = \"%s_constructor\";}\n"
              (show_sanitized_name typename)
              name
              (show_sanitized_name typename)
              name
          | Annot (t, name) ->
            let name = show_sanitized_name @@ bind_sanitize_capitalize name in
            Format.asprintf
              "class %s_%s_constructor_subtype: %s{\n\
               \tpublic string kind = \"%s_constructor\";\n\
               \tpublic %a? %s_element {get; set; }}\n"
              (show_sanitized_name typename)
              name
              (show_sanitized_name typename)
              name
              (output_type_pretty ~prefix)
              t name
          | t ->
            let name = constructor_from_path path in
            Format.asprintf
              "class %s_%s_constructor_subtype: %s{\n\
               \tpublic string kind = \"%s_constructor\";\n\
               \tpublic %a? %s_element {get; set; }}\n"
              (show_sanitized_name typename)
              name
              (show_sanitized_name typename)
              name
              (output_type_pretty ~prefix)
              t name in
        cur_elem :: res in
      aux new_res xs in
  fprintf ppf "\n%s\n\n" (String.concat "\n" (aux [] lst))

and get_csharp_basetype = function
  | Bool -> Some "bool"
  | String -> Some "string"
  | Address -> Some "string"
  | Int -> Some "BigInteger"
  | Nat -> Some "BigInteger"
  | Byte -> Some "byte[]"
  | Unit -> Some "Types.Unit"
  | _ -> None

and make_converter_with_class typename classname ppf b =
  let basetypename = get_csharp_basetype b in
  match basetypename with
  | None -> ()
  | Some basetypename ->
    fprintf ppf
      "public static implicit operator %s(%s a) => (%s) (%a) a;\n\
       public static implicit operator %s(%s a) => (%s) (%a) a;" basetypename
      typename basetypename classname b typename basetypename typename classname
      b

and make_converter typename ppf b =
  let basetypename = get_csharp_basetype b in
  match basetypename with
  | None -> ()
  | Some basetypename ->
    fprintf ppf
      "public static implicit operator %s(%s a) => a;\n\
       public static implicit operator %s(%s a) => a;" basetypename typename
      typename basetypename

and declare_type_ ~prefix (m : type_) ppf (name : sanitized_name) =
  output_verbose ~level:1
  @@ sprintf "[csharp][declare_type_] Entering with name %s"
       (show_sanitized_name name) ;
  let mk_init (name : sanitized_name) = name in
  let output_type_pretty = output_type_pretty ~prefix in
  let output_type_content = output_type_content in
  match m with
  | Or _ ->
    let name = bind_sanitize_capitalize name in
    (* let sumtype_content =
     *   Format.asprintf "%a" (declare_sumtype ~typename:name) m in *)
    fprintf ppf "public class %s {}" (show_sanitized_name name) ;
    fprintf ppf "%a" (define_sumtype_subtypes ~prefix ~typename:name) m
  | Base String ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf
      "public class %s {\n\
       private string _value;\n\
       public %s(string str){\n\
       _value = str;\n\
       }\n\
       public static implicit operator string(%s v) => v._value;\n\
       public static implicit operator %s(string s) => new %s(s);\n\
       }"
      typename typename typename typename typename
  (* | Base Bool ->
   *   let typename =
   *     show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
   *   fprintf ppf "public class %s : %s.Bool {}" typename prefix *)
  | Base Unit ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf
      "public class %s : %s.Unit {\n\
       public %s() : base() {}\n\
       public %s(%s.Unit u) : base() {}\n\
       };"
      typename prefix typename typename prefix
  | Base b ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    let classname =
      pp_base_type ~language:Csharp ~abstract:false ~prefix:(Some prefix) in
    fprintf ppf
      "public class %s : %a {\n\
       public %s(%a v) : base(v) {}\n\
       %a\n\
      \                 }" typename classname b typename classname b
      (make_converter_with_class typename classname)
      b
  | TVar (vname, Base Unit) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "public class %s : %s{\npublic %s() : base() {}\n}" typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
      typename
  | TVar (vname, Base Address) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf
      "public class %s : %s{\npublic %s(string addr) : base(addr) {}\n%a\n}"
      typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
      typename (make_converter typename) Address
  | TVar (vname, Base String) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "public class %s : %s{\npublic %s(string str) : base(str) {}\n}"
      typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
      typename
  | TVar (vname, Base Int) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "public class %s : %s{\npublic %s(BigInteger v) : base(v) {}\n}"
      typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
      typename
  | TVar (vname, Base Nat) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "public class %s : %s{\npublic %s(BigInteger v) : base(v) {}\n}"
      typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
      typename
  | TVar (vname, Base b) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "public class %s : %s{\npublic %s(%a v) : base(v) {}\n}"
      typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
      typename
      (pp_base_type ~language:Csharp ~abstract:false ~prefix:(Some prefix))
      b
    (* let typename =
     *   show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
     * fprintf ppf "using %s = %s /*%s*/;" typename
     *   (show_sanitized_name @@ bind_sanitize_capitalize vname) (show_basetype b) *)
  | TVar (vname, Record r) ->
    let l =
      match r with
      | PairP l -> l
      | x -> [x] in
    let l = flattenP [] l in

    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "public record %s : %s{\npublic %s(%a) : base(%a) {}\n}\n"
      typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
      typename
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (n, x) ->
           fprintf ppf "%a %s" output_type_pretty x (show_sanitized_name n)))
      l
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (n, _x) -> fprintf ppf "%s" (show_sanitized_name n)))
      l
  | TVar (vname, _) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "public class %s : %s{}\n" typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "public class %s : %s.BigMap<%a,%a> {}"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix output_type_pretty k output_type_pretty v
  | Binary (Lambda, (_k, _v)) ->
    fprintf ppf "public class %s : %s.Lambda {}"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix (* output_type_pretty k output_type_pretty v *)
  | Binary (Map, (k, v)) ->
    fprintf ppf "public class %s : %s.Map<%a,%a> {}"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix output_type_pretty k output_type_pretty v
  | Unary (List, t) ->
    fprintf ppf "public class %s : List<%a> {}"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      output_type_pretty t
  | Unary (Set, t) ->
    fprintf ppf
      "public class %s : %s.Set<%a> {} //maybe make this into an explicit set \
       later?"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix output_type_pretty t
  | Unary (Ticket, _t) ->
    fprintf ppf "public class %s : %s.Ticket {}"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix (* output_type_pretty t *)
  | Unary (Option, t) ->
    fprintf ppf "public class %s : %s.Option<%a> {}"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix output_type_pretty t
  | Contract _t ->
    fprintf ppf "public class %s : %s.Contract {}"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix
  (* output_type_pretty t *)
  | Annot (ep, _) -> declare_type_ ~prefix ep ppf (mk_init name)
  | Pair l ->
    let class_name =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    let numbered_type_list = List.mapi (fun i x -> (i, x)) l in
    fprintf ppf
      "public class %s : %a {\n\
       public %s(%a) : base(%a) {}\n\
       public %s(%a tuple) : base(%a) {}\n\
       }"
      class_name
      (output_type_content ~prefix)
      m class_name
      (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, t) ->
           fprintf ppf "%a x%d" (output_type_content ~prefix) t i))
      numbered_type_list
      (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, _) -> fprintf ppf "x%d" i))
      numbered_type_list class_name
      (output_type_content ~prefix)
      m
      (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, _) ->
           fprintf ppf "tuple.Item%d" (i + 1)))
      numbered_type_list
  | Record r ->
    let name = bind_sanitize_capitalize name in
    let l =
      match r with
      | PairP l -> l
      | x -> [x] in
    let l = flattenP [] l in
    let classname = show_sanitized_name @@ mk_init name in
    fprintf ppf "public record %s{\n\t%a\npublic %s(%a){\n\t\t%a}\n}" classname
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n\t")
         (fun ppf (n, x) ->
           fprintf ppf "public %a %s  { get; set;}" output_type_pretty x
             (show_sanitized_name n)))
      l classname
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (n, x) ->
           fprintf ppf "%a %s" output_type_pretty x (show_sanitized_name n)))
      l
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n\t\t")
         (fun ppf (n, _x) ->
           fprintf ppf "this.%s = %s;" (show_sanitized_name n)
             (show_sanitized_name n)))
      l

and output_type_encode ~prefix ppf = function
  (* | Base Unit -> fprintf ppf "%s.UnitEncode" prefix *)
  | Base b ->
    let typename =
      String.capitalize_ascii
        (asprintf "%a"
           (pp_base_type ~language:Factori_config.Csharp ~abstract:false
              ~prefix:None)
           b) in
    fprintf ppf "%s.%sEncode" prefix typename
  | Annot (ep, _) -> fprintf ppf "%a" (output_type_encode ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.Tuple%dEncode<%a>(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_encode ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%sEncode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%sEncode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%sEncode" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.ListEncode<%a>(%a)" prefix
      (output_type_pretty ~prefix)
      a
      (output_type_encode ~prefix)
      a
  | Unary (Set, a) ->
    fprintf ppf "%s.SetEncode<%a>(%a)" prefix
      (output_type_pretty ~prefix)
      a
      (output_type_encode ~prefix)
      a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.TicketEncode" prefix (* (output_type_encode ~prefix) t *)
  | Contract _c -> fprintf ppf "%s.ContractEncode" prefix
  (* pp_micheline_
     * c *)
  (* (output_type_encode ~prefix) c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.BigMapEncode<%a,%a>(%a,%a)" prefix
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
  | Binary (Lambda, (_k, _v)) ->
    fprintf ppf "%s.LambdaEncode" prefix
    (* (output_type_encode ~prefix)
     * k
     * (output_type_encode ~prefix)
     * v *)
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.MapEncode<%a,%a>(%a,%a)" prefix
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.OptionEncode<%a>(%a)" prefix
      (output_type_pretty ~prefix)
      a
      (output_type_encode ~prefix)
      a

and encode_type_decl ~prefix (m : type_) ppf (name : sanitized_name) =
  let output_type_encode = output_type_encode ~prefix in
  let typename = bind_sanitize_capitalize @@ name in
  let prelude =
    asprintf "public static IMicheline %sEncode (%s arg){\n\ttry{\n"
      (show_sanitized_name name)
      (show_sanitized_name @@ typename) in
  begin
    match m with
    | Base String ->
      (* this is a special case *)
      fprintf ppf "%s\t\treturn %s.StringEncode(arg);" prelude prefix
    | Base b ->
      let basename =
        String.capitalize_ascii
        @@ asprintf "%a"
             (pp_base_type ~language:Factori_config.Csharp ~abstract:false
                ~prefix:None)
             b in
      fprintf ppf "%s\t\treturn %s.%sEncode(arg);" prelude prefix basename
    | Annot (ep, _) -> fprintf ppf "%a" (encode_type_decl ~prefix ep) name
    | TVar tn ->
      fprintf ppf "\n%s\t\treturn %sEncode(arg);" prelude
        (show_sanitized_name (fst tn))
    | Unary (List, a) ->
      fprintf ppf "%s\t\treturn %s.ListEncode<%a>(%a)(arg);" prelude prefix
        (output_type_pretty ~prefix)
        a output_type_encode a
    | Unary (Set, a) ->
      fprintf ppf "%s\t\treturn %s.SetEncode<%a>(%a)(arg);" prelude prefix
        (output_type_pretty ~prefix)
        a output_type_encode a
    | Unary (Ticket, _t) ->
      fprintf ppf "%s\t\treturn %s.TicketEncode(arg);" prelude prefix
    (* output_type_encode t *)
    | Contract _c ->
      fprintf ppf "%s\t\treturn %s.ContractEncode(arg);" prelude prefix
    (* output_type_encode *)
    (* pp_micheline_ c *)
    | Binary (BigMap, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.BigMapEncode<%a,%a>(%a,%a)(arg);" prelude
        prefix
        (output_type_pretty ~prefix)
        k
        (output_type_pretty ~prefix)
        v output_type_encode k output_type_encode v
    | Binary (Lambda, (_k, _v)) ->
      fprintf ppf "%s\t\treturn %s.LambdaEncode(arg);\t\t" prelude
        prefix (* output_type_encode k output_type_encode v *)
    | Binary (Map, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.MapEncode<%a,%a>(%a,%a)(arg);" prelude prefix
        (output_type_pretty ~prefix)
        k
        (output_type_pretty ~prefix)
        v output_type_encode k output_type_encode v
    | Unary (Option, a) ->
      fprintf ppf "%s\t\treturn (%s.OptionEncode<%a>(%a))(arg);" prelude prefix
        (output_type_pretty ~prefix)
        a output_type_encode a
    | Or _ as t ->
      fprintf ppf
        "public static IMicheline %sEncode (%s arg){\n\
         \ttry{\n\
         switch(arg){\n\
         %a\t\tdefault:\n\
         \t\t\tthrow new EncodeError(\"Unknown case in sumtype_encode\");\n\
         }"
        (show_sanitized_name name)
        (show_sanitized_name typename)
        (output_sumtype_encode ~prefix ~typename)
        t
    | Pair _ as t ->
      fprintf ppf
        "public static IMicheline %sEncode(%s arg){\n\
         \ttry{\n\
         \t\treturn %a(arg); " (show_sanitized_name name)
        (show_sanitized_name typename)
        output_type_encode t
    | Record r ->
      fprintf ppf
        "public static IMicheline %sEncode(%s arg){\n\ttry{\n\t\treturn %a;"
        (show_sanitized_name name)
        (show_sanitized_name typename)
        (encode_record ~prefix) r
  end ;
  fprintf ppf
    "}\n\
     \tcatch (Exception e){\n\
     \t\tthrow new EncodeError($\"Error in %sEncode, with input:{arg}, and \
     error: {e}\");\n\
     \t\tthrow;}}"
    (show_sanitized_name name)

and record_extract_decoded_values ~typename ?(is_one_field_record = false) m ppf
    record_name =
  let sep =
    if is_one_field_record then
      Nothing
    else
      Parens in
  let f _ (k : sanitized_name) (path, _) =
    if is_one_field_record then
      Format.asprintf "(%s%a)" record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf ".%a" print_item (x + 1)))
        path
    else
      Format.asprintf "%s : (%s%a)" (show_sanitized_name k) record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf ".%a" print_item (x + 1)))
        path in
  fprintf ppf "return new %s%a;"
    (show_sanitized_name typename)
    (fun ppf -> iterate_over_fields ppf m f sep)
    ",\n"

and decode_type_decl ~prefix (m : type_) ppf (name : sanitized_name) =
  let typename = bind_sanitize_capitalize @@ name in
  let prelude =
    asprintf "public static %s %sDecode(IMicheline arg)"
      (show_sanitized_name typename)
      (show_sanitized_name name) in
  let output_type_decode = output_type_decode ~prefix in
  match m with
  | Or _ as t ->
    fprintf ppf
      "public static %s %sDecode(IMicheline arg){\n\
       %a;\n\
       throw new DecodeError($\"Could not identify any known sumtype path for \
       this value: {arg}\");}"
      (show_sanitized_name typename)
      (show_sanitized_name name)
      (output_sumtype_decode ~prefix ~typename "arg")
      t (* fprintf ppf "%s%a" prelude output_type_decode t *)
  | Annot (ep, _) -> fprintf ppf "%a" (decode_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s {return (%s )%sDecode(arg);}" prelude
      (show_sanitized_name typename)
      (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s{return (%s) (%s.ListDecode<%a>(%a))(arg);}" prelude
      (show_sanitized_name typename)
      prefix
      (output_type_pretty ~prefix)
      a output_type_decode a
  | Unary (Set, a) ->
    fprintf ppf "%s{return (%s) (%s.setDecode<%a>(%a))(arg);}" prelude
      (show_sanitized_name typename)
      prefix
      (output_type_pretty ~prefix)
      a output_type_decode a
  | Unary (Ticket, _) ->
    fprintf ppf "%s{return (%s) %s.TicketDecode(arg);}" prelude
      (show_sanitized_name typename)
      prefix
  | Contract _c ->
    fprintf ppf "%s{return (%s) %s.ContractDecode(arg);}" prelude
      (show_sanitized_name typename)
      prefix
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s{ return (%s.BigMapDecode<%a,%a>(%a,%a)(arg));}" prelude
      prefix
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v output_type_decode k output_type_decode v
  | Binary (Lambda, (_k, _v)) ->
    fprintf ppf "%s{ return (%s) (%s.LambdaDecode(arg));}" prelude
      (show_sanitized_name typename)
      prefix
    (* output_type_decode k output_type_decode v *)
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s{return ((%s) (%s.MapDecode<%a,%a>(%a,%a)(arg)));}" prelude
      (show_sanitized_name typename)
      prefix
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v output_type_decode k output_type_decode v
  | Unary (Option, a) ->
    fprintf ppf "%s{return ((%s) (%s.OptionDecode<%a>(%a)(arg)));}" prelude
      (show_sanitized_name typename)
      prefix
      (output_type_pretty ~prefix)
      a output_type_decode a
  | Base String ->
    (* special case *)
    fprintf ppf
      "public static %s %sDecode(IMicheline m){ return (%s.StringDecode(m));}"
      (show_sanitized_name typename)
      (show_sanitized_name name) prefix
  | Base b ->
    let cast =
      match get_csharp_basetype b with
      | None -> ""
      | Some cast -> sprintf "(%s)" cast in
    let basename =
      String.capitalize_ascii
      @@ asprintf "%a"
           (pp_base_type ~language:Factori_config.Csharp ~abstract:false
              ~prefix:None)
           b in
    fprintf ppf
      "public static %s %sDecode(IMicheline m){ return new \
       %s(%s(%s.%sDecode(m)));}"
      (show_sanitized_name typename)
      (show_sanitized_name name)
      (show_sanitized_name typename)
      cast prefix basename
  | Pair _ as t ->
    fprintf ppf
      "public static %s %sDecode(IMicheline arg){ return new %s(%a(arg)); }"
      (show_sanitized_name typename)
      (show_sanitized_name name)
      (show_sanitized_name typename)
      output_type_decode t
  | Record r ->
    let name = bind_sanitize_uncapitalize name in
    let is_one_field_record = is_one_field_record m in
    let record_decode =
      Format.asprintf "%a" (fun ppf type_ -> decode_record ~prefix ppf type_) r
    in
    let lst = record_to_list r in
    fprintf ppf
      "public static %s %sDecode(IMicheline arg) {\n\
       var before_projection = %s(arg);\n\
       %a\n\
       %!}"
      (show_sanitized_name typename)
      (show_sanitized_name name) record_decode
      (record_extract_decoded_values ~typename ~is_one_field_record lst)
      "before_projection" (* (opening d) record_content (closing d) *)

and encode_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_encode ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_csharp_build
        (fun s ppf t ->
          fprintf ppf "(%a(arg.%s))"
            (output_type_encode ~prefix)
            t (show_sanitized_name s))
        ppf p in
  aux ep

and decode_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_decode ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_typescript ~pairparens:true
        ~f_leaf:(fun _name ppf x -> output_type_decode ~prefix ppf x)
        ~f_pair:(fun ppf d -> fprintf ppf "%s.Tuple%dDecode " prefix d)
        ~delimiters:Parens ~sep:"," ppf p in
  aux ep

and output_type_decode ~prefix ppf = function
  | Base Unit -> fprintf ppf "%s.UnitDecode" prefix
  | Base b ->
    let typename =
      String.capitalize_ascii
        (asprintf "%a"
           (pp_base_type ~language:Factori_config.Csharp ~abstract:false
              ~prefix:None)
           b) in
    fprintf ppf "%s.%sDecode" prefix typename
  | Annot (ep, _) -> fprintf ppf "%a" (output_type_decode ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.Tuple%dDecode<%a>(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_decode ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%sDecode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%sDecode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%sDecode" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.ListDecode(%a)" prefix (output_type_decode ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.SetDecode(%a)" prefix (output_type_decode ~prefix) a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.TicketDecode" prefix (* (output_type_decode ~prefix) t *)
  | Contract _c -> fprintf ppf "%s.ContractDecode" prefix
  (* pp_micheline_
     * c *)
  (* (output_type_decode ~prefix) c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.BigMapDecode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Binary (Lambda, (_k, _v)) ->
    fprintf ppf "%s.LambdaDecode" prefix
    (* (output_type_decode ~prefix)
     * k
     * (output_type_decode ~prefix)
     * v *)
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.MapDecode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.OptionDecode<%a>(%a)" prefix
      (output_type_pretty ~prefix)
      a
      (output_type_decode ~prefix)
      a

let rec iterate_over_fields ppf (lst : (sanitized_name * _) list)
    (f : int -> sanitized_name -> 'a -> string) (d : delimiter) (sep : string) =
  let rec aux res count = function
    | [] -> List.rev res
    | (k, v) :: xs ->
      let new_res =
        let cur_elem = f count k v in
        cur_elem :: res in
      aux new_res (count + 1) xs in
  fprintf ppf "%s%s%s\n" (opening d)
    (String.concat sep (aux [] 0 lst))
    (closing d)

and record_extract_decoded_values ?(is_one_field_record = false) m ppf
    record_name =
  let sep =
    if is_one_field_record then
      Nothing
    else
      Curly in
  let f _ (k : sanitized_name) (path, _) =
    if is_one_field_record then
      Format.asprintf "(%s%a)" record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path
    else
      Format.asprintf "'%s' : (%s%a)" (show_sanitized_name k) record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path in
  fprintf ppf "return %a" (fun ppf -> iterate_over_fields ppf m f sep) ",\n"

and record_to_list ep =
  (* path is how we get to the element in the tuple *)
  let rec aux ~path = function
    | LeafP (field_name, t) -> [(field_name, (path, t))]
    | PairP l -> List.concat (List.mapi (fun i x -> aux ~path:(path @ [i]) x) l)
  in
  aux ~path:[] ep

and micheline_type_decl (m : type_) ppf name =
  let _ = (m, ppf, name) in
  ()

and output_sumtype_generator ~prefix ~typename ppf t_ =
  let lst = sumtype_to_list t_ in
  let lst_i = add_indices lst in
  fprintf ppf "%a;\nreturn %s.chooseFrom<%s>(new List<%s>{\n%a\n})"
    (pp_print_list ~pp_sep:(tag ";\n\t") (fun ppf (i, (t_, path)) ->
         fprintf ppf "%a"
           (fun ppf at ->
             match at with
             | Annot (u_, name) when is_unit u_ ->
               let name = show_sanitized_name @@ bind_sanitize_capitalize name in
               fprintf ppf
                 "%s_%s_constructor_subtype res%d = new \
                  %s_%s_constructor_subtype();\n\
                  res%d.kind = \"%s_constructor\";\n"
                 (show_sanitized_name typename)
                 name i
                 (show_sanitized_name typename)
                 name i name
             | Annot (t_, name) ->
               let name = show_sanitized_name @@ bind_sanitize_capitalize name in
               fprintf ppf
                 "%s_%s_constructor_subtype res%d = new \
                  %s_%s_constructor_subtype();\n\
                  res%d.kind = \"%s_constructor\";\n\
                  res%d.%s_element=  %a ();\n"
                 (show_sanitized_name typename)
                 name i
                 (show_sanitized_name typename)
                 name i name i name
                 (output_type_generator ~prefix ~typename)
                 t_
             | t ->
               let name = constructor_from_path path in
               fprintf ppf
                 "%s_%s_constructor_subtype res%d = new \
                  %s_%s_constructor_subtype();\n\
                  res%d.kind = \"%s\";\n\
                  res%d.%s_element = %a ();\n"
                 (show_sanitized_name typename)
                 name i
                 (show_sanitized_name typename)
                 name i
                 (sprintf "%s_constructor" (constructor_from_path path))
                 i
                 (show_sanitized_name @@ bind_sanitize_capitalize
                @@ sanitized_of_str name)
                 (output_type_generator ~prefix ~typename)
                 t)
           t_))
    lst_i prefix
    (show_sanitized_name typename)
    (show_sanitized_name typename)
    (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, _) -> fprintf ppf "res%d" i))
    lst_i

and output_type_generator ~prefix ~typename ppf t =
  let output_type_generator = output_type_generator ~prefix ~typename in
  match t with
  | Base Int -> fprintf ppf "%s.IntGenerator" prefix
  | Base b ->
    let basename =
      String.capitalize_ascii
      @@ asprintf "%a"
           (pp_base_type ~language:Factori_config.Csharp ~abstract:false
              ~prefix:None)
           b in
    fprintf ppf "%s.%sGenerator" prefix basename
  | Annot (ep, _) -> fprintf ppf "%a" output_type_generator ep
  | Pair al ->
    fprintf ppf "(%s.Tuple%dGenerator<%a>(%a))" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",\n")
         output_type_generator)
      al
  | Record _ as t ->
    fprintf ppf "%sGenerator"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_sumtype_generator ~prefix ~typename ppf t
  | TVar tn -> fprintf ppf "%sGenerator" (show_sanitized_name (fst tn))
  | Unary (Ticket, _) -> fprintf ppf "(%s.TicketGenerator())" prefix
  | Unary (u, a) ->
    fprintf ppf "(%s.%sGenerator<%a>(%a))" prefix
      (str_of_unary ~camelcase:true u)
      (output_type_pretty ~prefix)
      a output_type_generator a
  | Contract _c -> fprintf ppf "%s.ContractGenerator" prefix
  | Binary (Lambda, (_k, _v)) -> fprintf ppf "%s.LambdaGenerator" prefix
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s.%sGenerator<%a,%a>(%a,%a))" prefix
      (str_of_binary ~camelcase:true b)
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v output_type_generator k output_type_generator v

and generator_record ~prefix ~typename ppf ep =
  let aux = function
    | LeafP (_n, x) ->
      fprintf ppf "%a ()" (output_type_generator ~prefix ~typename) x
    | PairP l ->
      let l = flattenP [] l in
      fprintf ppf "new %s(\t\t%a)"
        (show_sanitized_name typename)
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",\n\t\t")
           (fun ppf (n, x) ->
             fprintf ppf "%s : %a ()" (show_sanitized_name n)
               (output_type_generator ~prefix ~typename)
               x))
        l in
  aux ep

and generator_type_decl ~prefix (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let typename = bind_sanitize_capitalize name in
  let prelude =
    asprintf "public static %s %sGenerator(){\n\treturn "
      (show_sanitized_name typename)
      (show_sanitized_name name) in
  let closing = sprintf "}" in
  match m with
  | Base Int ->
    fprintf ppf "%s (%s) new %s(%s.IntGenerator ());%s" prelude
      (show_sanitized_name typename)
      (show_sanitized_name typename)
      prefix closing
  | Base Unit ->
    fprintf ppf "%s(%s) new %s();%s" prelude
      (show_sanitized_name typename)
      (show_sanitized_name typename)
      closing
  | Base b ->
    let basename =
      String.capitalize_ascii
      @@ asprintf "%a"
           (pp_base_type ~language:Factori_config.Csharp ~abstract:false
              ~prefix:None)
           b in
    fprintf ppf "%s(%s) new %s(%s.%sGenerator ());%s" prelude
      (show_sanitized_name typename)
      (show_sanitized_name typename)
      prefix basename closing
  | Annot (ep, _) -> fprintf ppf "%a" (generator_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s (%s) %sGenerator ();%s" prelude
      (show_sanitized_name typename)
      (show_sanitized_name (fst tn))
      closing
  | Unary (Ticket, _a) ->
    fprintf ppf "%s (%s)%s.TicketGenerator ();%s" prelude
      (show_sanitized_name typename)
      prefix closing
  | Unary (u, a) ->
    fprintf ppf "%s (%s)(%s.%sGenerator (%a)) ();%s" prelude
      (show_sanitized_name typename)
      prefix
      (str_of_unary ~camelcase:true u)
      (output_type_generator ~prefix ~typename)
      a closing
  | Contract _c ->
    fprintf ppf "%s (%s)%s.ContractGenerator ();%s" prelude
      (show_sanitized_name typename)
      prefix closing
  | Binary (Lambda, (_k, _v)) ->
    fprintf ppf "%s (%s) %s.LambdaGenerator();%s" prelude
      (show_sanitized_name typename)
      prefix closing
  | Binary (b, (k, v)) ->
    fprintf ppf "%s (%s) (%s.%sGenerator (%a,%a)) ();%s" prelude
      (show_sanitized_name typename)
      prefix
      (str_of_binary ~camelcase:true b)
      (output_type_generator ~prefix ~typename)
      k
      (output_type_generator ~prefix ~typename)
      v closing
  | Or _ as t ->
    fprintf ppf "public static %s %sGenerator(){\n\t%a;}"
      (show_sanitized_name typename)
      (show_sanitized_name name)
      (output_sumtype_generator ~prefix ~typename)
      t
  | Pair _ as t ->
    fprintf ppf "public static %s %sGenerator(){\n\treturn (%s) %a();}"
      (show_sanitized_name typename)
      (show_sanitized_name name)
      (show_sanitized_name typename)
      (output_type_generator ~prefix ~typename)
      t
  | Record r ->
    fprintf ppf "public static %s %sGenerator(){\n\treturn %a;\n}"
      (show_sanitized_name typename)
      (show_sanitized_name name)
      (generator_record ~prefix ~typename)
      r

and call_entrypoint ~contract_name ppf name =
  let _ = contract_name in
  let capitalized_ep_name =
    show_sanitized_name @@ bind_sanitize_capitalize name in
  let ep_name = show_sanitized_name name in
  fprintf ppf
    "public static async Task<string?> Call%s(Netezos.Keys.Key from, string \
     kt1, %s param, long fee, long gasLimit, long storageLimit, long amount = \
     0, string networkName = \"ghostnet\", bool debug = false){\n\
     var input = %sEncode(param).ToJson();\n\
     var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));\n\
     var encodedParam = %sEncode(param);\n\
     var opHash = await Blockchain.Functions.contractCall(from, kt1, \"%s\", \
     encodedParam, fee, gasLimit, storageLimit, amount,networkName, debug);\n\
     if(opHash != null) Console.WriteLine($\"Successful call to %s: {opHash}\");\n\
     else{ Console.WriteLine(\"Failed call to %s\");}\n\
     return opHash;\n\
     }"
    capitalized_ep_name capitalized_ep_name ep_name ep_name ep_name ep_name
    ep_name

and deploy ~storage_name ~contract_name ~network ppf () =
  let _ = (storage_name, network, ppf) in
  let contract_name = sanitized_of_str contract_name in
  let storage_type =
    show_sanitized_name @@ bind_sanitize_capitalize storage_name in
  fprintf ppf
    {|public static async Task<string?> Deploy(Netezos.Keys.Key from, %s initial_storage, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug=false){
  var encoded_initial_storage = %sEncode(initial_storage);
  var options = new System.Text.Json.JsonSerializerOptions();
  options.Converters.Add(new Netezos.Encoding.Serialization.MichelineConverter());
  string json1 = System.Text.Json.JsonSerializer.Serialize(encoded_initial_storage, options);
  if(debug){
    Console.WriteLine(json1);
  }
  string? kt1 = await Blockchain.Functions.DeployContract("%s",from, encoded_initial_storage,fee,gasLimit,storageLimit,amount,networkName,debug);
  return kt1;
}
|}
    storage_type
    (show_sanitized_name storage_name)
    (Factori_config.get_csharp_code_path ~relative:true
       ~dir:(Factori_config.get_dir ())
       ~contract_name ())

let print_original_blockchain_storage ~prefix ~final_storage_type_ ~storage_name
    ~kt1 ~network ppf () =
  let open Tzfunc.Rp in
  let open Tzfunc.Proto in
  match
    Lwt_main.run
      (let>? storage =
         Factori_config.get_storage ~network
           ~debug:(!Factori_options.verbosity > 0)
           kt1 in
       (* add_modules_to_ocaml_file ~file_must_exist:false
        *   ~path:scenario_example_file
        *   [Factori_config.get_ocaml_interface_basename ~contract_name] ; *)
       try
         let value =
           value_of_typed_micheline
             (* ~debug:(!Factori_options.verbosity > 0) *)
             storage final_storage_type_ in
         let return_storage_expression ppf value =
           match value with
           | VRecord _ ->
             fprintf ppf "return (%s) %a;"
               (show_sanitized_name @@ bind_sanitize_capitalize storage_name)
               (pp_value_to_csharp ~first_tuple_as_parameters:true ~prefix)
               value
           | VTuple (_tl, _vl) ->
             fprintf ppf "return (%s) new %s(%a);"
               (show_sanitized_name @@ bind_sanitize_capitalize storage_name)
               (show_sanitized_name @@ bind_sanitize_capitalize storage_name)
               (pp_value_to_csharp ~first_tuple_as_parameters:true ~prefix)
               value
           | _ ->
             fprintf ppf "return (%s) %a;"
               (show_sanitized_name @@ bind_sanitize_capitalize storage_name)
               (pp_value_to_csharp ~first_tuple_as_parameters:true ~prefix)
               value in
         fprintf ppf
           "\n\
            public static %s initial_blockchain_storage(){\n\
            %a}\n\
            /* %s\n\
            %a*/\n\n"
           (show_sanitized_name @@ bind_sanitize_capitalize storage_name)
           return_storage_expression value
           (EzEncoding.construct micheline_enc.json storage)
           show_value value ;
         Lwt.return_ok ()
       with e ->
         handle_exception e ;
         fprintf ppf
           "\n\
            var initial_blockchain_storage = raise \"Could not download \
            initial storage, this is a bug\"\n\
            (*%s*)\n"
           (EzEncoding.construct micheline_enc.json storage) ;
         Lwt.return_ok ())
  with
  | Ok () -> ()
  | Error e -> print_error e

let pp_interface_instruction_csharp_defs_only ~prefix ppf ii =
  match ii with
  | Define_type (name, type_) ->
    fprintf ppf "%a\n\n" (declare_type_ ~prefix type_) name
  | _ -> fprintf ppf ""

let pp_interface_instruction_csharp_calls_only ~contract_name ppf ii =
  match ii with
  | Entrypoint (name, _) -> call_entrypoint ~contract_name ppf name
  | _ -> fprintf ppf ""

let rec pp_interface_instruction_csharp ~prefix ~contract_name ~storage_name
    ~network ppf ii =
  match ii with
  | Define_type (name, type_) ->
    fprintf ppf "%a\n\n%a\n\n%a\n\n/*%a*/"
      (generator_type_decl ~prefix type_)
      name
      (encode_type_decl ~prefix type_)
      name
      (decode_type_decl type_ ~prefix)
      name
      (micheline_type_decl type_)
      name
  | Deploy -> deploy ~storage_name ~contract_name ~network ppf ()
  | Seq ii ->
    (pp_interface_csharp ~prefix ~contract_name ~storage_name ~network) ppf ii
  | Entrypoint (_, _) -> (* call_entrypoint ~contract_name ppf name *) ()

and pp_interface_csharp ~prefix ~network ~contract_name ~storage_name
    (ppf : formatter) (i : interface) =
  output_verbose ~level:3
    (asprintf "Entering pp_interface_csharp with interface %a" pp_interface i) ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_csharp_defs_only ~prefix)
    ppf i ;
  (* In C#, functions have to be wrapped inside a class *)
  fprintf ppf "public class Functions{\n" ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_csharp ~prefix ~contract_name ~storage_name
       ~network)
    ppf i ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_csharp_calls_only ~contract_name)
    ppf i ;
  fprintf ppf "}"

let download_and_decode_storage ~storage_name ppf () =
  let _ = storage_name in
  fprintf ppf ""

let process_entrypoints ~prefix ~contract_name ~storage_name
    ~final_storage_type_ ~network ?(kt1 = None) ppf interface =
  let contract_name = show_sanitized_name contract_name in
  output_verbose ~level:2
    (asprintf
       "[csharp] Entering process_entrypoints with final storage: %a\n\
       \ and interface %a" pp_type_ final_storage_type_ pp_interface interface) ;
  let interface =
    try_overflow ~msg:"extract interface ocaml" (fun () -> interface) in
  let _ = check_interface_for_doubles interface in
  let original_blockchain_storage =
    match kt1 with
    | None -> "//No KT1"
    | Some kt1 ->
      Format.asprintf "\npublic class initialBlockchainStorage{\n%a\n}"
        (print_original_blockchain_storage ~prefix ~final_storage_type_
           ~storage_name ~kt1 ~network)
        () in
  fprintf ppf
    "using FactoriTypes;\n\
     using System.Numerics;\n\
     using Netezos.Encoding;\n\
     using Netezos.Rpc;\n\
     using Netezos.Keys;\n\
     namespace %s{\n\
     %a\n\n\
     %a%s\n\
     }"
    contract_name
    (pp_interface_csharp ~prefix ~contract_name ~storage_name ~network)
    interface
    (download_and_decode_storage ~storage_name)
    () original_blockchain_storage
