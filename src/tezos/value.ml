open Types
open Pair_type
open Factori_utils

type value =
  | VUnit of unit
  | VInt of Z.t
  | VString of string
  | VBytes of Hex.hex
  | VTimestamp of string
  | VBool of bool
  | VAddress of string
  | VTez of Z.t
  | VKey of string
  | VKeyHash of string
  | VSignature of string
  | VOperation of string
  | VTuple of type_ list * value list
  | VBigMap of type_ * type_ * Z.t
  | VMap of type_ * type_ * (value * value) list
  | VSet of type_ * value list
  | VList of type_ * value list
  | VOption of type_ * value option
  | VLambda of Tzfunc.Proto.script_expr
  | VRecord of (type_ * value) paired_type
  | VSumtype of string * type_ * value
  | VChain_id of string
  | VTicket of string
  | VSaplingState of string

let rec show_value ppf = function
  | VUnit _ -> Format.fprintf ppf "VUnit"
  | VInt n -> Format.fprintf ppf "VInt %s" (Z.to_string n)
  | VString s -> Format.fprintf ppf "VString %s" s
  | VBytes h -> Format.fprintf ppf "VBytes %s" (h :> string)
  | VTimestamp t -> Format.fprintf ppf "VTimestamp %s" t
  | VBool b -> Format.fprintf ppf "VBool %b" b
  | VAddress a -> Format.fprintf ppf "VAddress %s" a
  | VTez t -> Format.fprintf ppf "VTez %s" (Z.to_string t)
  | VKey k -> Format.fprintf ppf "VKey %s" k
  | VKeyHash kh -> Format.fprintf ppf "VKeyHash %s" kh
  | VSignature sign -> Format.fprintf ppf "VSignature %s" sign
  | VOperation op -> Format.fprintf ppf "VOperation %s" op
  | VTuple (_, lst) ->
    Format.fprintf ppf "VTuple (%a)"
      (Format.pp_print_list ~pp_sep:(tag ",") show_value)
      lst
  | VBigMap (_, _, n) -> Format.fprintf ppf "VBigMap %s" (Z.to_string n)
  | VMap (_, _, lst) ->
    Format.fprintf ppf "VMap (%a)"
      (Format.pp_print_list (fun ppf (v1, v2) ->
           Format.fprintf ppf "(%a, %a)" show_value v1 show_value v2))
      lst
  | VSet (_, lst) ->
    Format.fprintf ppf "VSet (%a)" (Format.pp_print_list show_value) lst
  | VList (_, lst) ->
    Format.fprintf ppf "VList (%a)" (Format.pp_print_list show_value) lst
  | VOption (_, v) ->
    Format.fprintf ppf "VOption (%a)" (Format.pp_print_option show_value) v
  | VLambda _le -> Format.fprintf ppf "VLambda"
  | VRecord r ->
    Format.fprintf ppf "VRecord(%s)"
      (show_paired_type (fun ppf (_, v) -> show_value ppf v) r)
  | VSumtype (s, _, v) -> Format.fprintf ppf "VSumtype (%s, %a)" s show_value v
  | VChain_id cid -> Format.fprintf ppf "VChain_id %s" cid
  | VTicket t -> Format.fprintf ppf "VTicket %s" t
  | VSaplingState s -> Format.fprintf ppf "VSaplingState %s" s
