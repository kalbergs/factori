let ( // ) = Factori_options.concat

open Factori_options
open Factori_errors
open Tzfunc.Proto
open Format
open Factori_utils
open Ocaml_dune
open Factori_file

let produce_csharp_interface ~storage_name ~final_storage_type_ ~kt1 ~network
    dir contract_name interface =
  output_verbose ~level:3
    (asprintf
       "Entering produce_csharp_interface with final storage: %a and interface \
        %a"
       Types.pp_type_ final_storage_type_ Infer_entrypoints.pp_interface
       interface) ;
  let res_csharp =
    Format.asprintf "%a"
      (Output_sdk_csharp.process_entrypoints ~prefix:"Types" ~contract_name
         ~storage_name ~final_storage_type_ ~kt1 ~network)
      interface in
  let interface_filename =
    Factori_config.get_csharp_interface_path ~dir ~contract_name () in
  write_file interface_filename res_csharp

let produce_python_interface ~storage_name ~final_storage_type_ ~kt1 ~network
    dir contract_name interface =
  output_verbose ~level:3
    (asprintf
       "Entering produce_python_interface with final storage: %a and interface \
        %a"
       Types.pp_type_ final_storage_type_ Infer_entrypoints.pp_interface
       interface) ;
  let res_python =
    Format.asprintf "%a"
      (Output_sdk_python.process_entrypoints ~prefix:"factori_types"
         ~contract_name ~storage_name ~final_storage_type_ ~kt1 ~network)
      interface in
  let interface_filename =
    Factori_config.get_python_interface_path ~dir ~contract_name () in
  write_file interface_filename res_python

let produce_typescript_interface ~storage_name ~final_storage_type_ ~kt1
    ~network dir contract_name interface =
  let res_typescript =
    Format.asprintf "%a"
      (Output_sdk_typescript.process_entrypoints ~storage_name
         ~final_storage_type_ ~network ~kt1 ~prefix:"functolib" ~contract_name)
      interface in
  let ts_interface_file =
    Factori_config.get_typescript_interface_path ~dir ~contract_name () in
  write_file ts_interface_file res_typescript ;
  let typescript_indent_command_content =
    Artifacts.typescript_indent_command ts_interface_file in
  let indent_command =
    Factori_utils.command_if_exists
      ~error_msg:"typescript-formatter is not installed" ~cmd_name:"tsfmt"
      typescript_indent_command_content in
  let _ = Sys.command indent_command in
  ()

let get_code_from_KT1 () : script_expr =
  match !kt1 with
  | None -> raise NoKT1Provided
  | Some kt1 -> (
    match
      Lwt_main.run
        (Tzfunc.Node.get_account_info ~base:(get_base_of_network !network) kt1)
    with
    | Error e -> raise (TzfuncError e)
    | Ok { ac_script = None; _ } -> raise (NoScript kt1)
    | Ok { ac_script = Some { code = c; _ }; _ } -> Micheline c)

let marshal_code_ocaml dir contract_name code =
  let code_str =
    sprintf
      "open Tzfunc.Proto\n\
       let code = EzEncoding.destruct script_expr_enc.json  {|%s|}"
      (EzEncoding.construct script_expr_enc.json code) in
  write_file
    (Factori_config.get_ocaml_code_path ~dir ~contract_name ())
    code_str

let marshal_code_python dir contract_name code =
  let code_str = sprintf "%s" (EzEncoding.construct script_expr_enc.json code) in
  write_file
    (Factori_config.get_python_code_path ~dir ~contract_name ())
    code_str

let marshal_code_typescript dir contract_name code =
  let code_str = EzEncoding.construct script_expr_enc.json code in
  write_file
    (Factori_config.get_typescript_code_path ~dir ~contract_name ())
    code_str

let marshal_code_csharp dir contract_name code =
  let code_str = EzEncoding.construct script_expr_enc.json code in
  write_file
    (Factori_config.get_csharp_code_path ~dir ~contract_name ())
    code_str

let marshal_code_ocaml_from_KT1 dir contract_name =
  (* Fetch the Michelson code *)
  let code = get_code_from_KT1 () in
  (* Marshal the Michelson code  *)
  marshal_code_ocaml dir contract_name code

let marshal_code_python_from_KT1 dir contract_name =
  (* Fetch the Michelson code *)
  let code = get_code_from_KT1 () in
  (* Marshal the Michelson code  *)
  marshal_code_python dir contract_name code

let marshal_code_typescript_from_KT1 dir contract_name =
  (* Fetch the Michelson code *)
  let code = get_code_from_KT1 () in
  (* Marshal the Michelson code  *)
  marshal_code_typescript dir contract_name code

let marshal_code_csharp_from_KT1 dir contract_name =
  (* Fetch the Michelson code *)
  let code = get_code_from_KT1 () in
  (* Marshal the Michelson code  *)
  marshal_code_csharp dir contract_name code

let produce_ocaml_interface ~signature ~storage_name ~final_storage_type_ ~kt1
    dir contract_name network interface =
  let has_original_blockchain_storage = Option.is_some kt1 in
  match
    ( not !do_not_regroup,
      Factori_config.find_same_signature_opt ~contract_name ~dir signature )
  with
  | true, Some contract_to_include
    when contract_to_include <> contract_name.sanitized ->
    output_verbose ~level:1
      (sprintf "Regrouping contract %s into (contract to be included) %s;@."
         contract_name.original contract_to_include) ;
    let res_ocaml =
      Format.asprintf "%a"
        (Output_sdk_ocaml.regroup ~contract_to_include ~final_storage_type_
           ~network ~kt1)
        () in
    (* Write the OCaml interface file *)
    let interface_filename =
      Factori_config.get_ocaml_interface_path ~dir ~contract_name () in
    write_file interface_filename res_ocaml ;
    (* Content of the abstract OCaml Interface *)
    let abstract_res_ocaml =
      Format.asprintf "%a"
        (Output_abstract_types_ocaml.process_entrypoints ~contract_name
           ~storage_name ~final_storage_type_ ~kt1 ~network)
        interface in
    (* Write the abstract OCaml interface file *)
    let abstract_interface_filename =
      Factori_config.get_ocaml_abstract_interface_path ~dir ~contract_name ()
    in
    write_file abstract_interface_filename abstract_res_ocaml ;
    (* (\* write OCaml interface mli file *\)
     * let mli_file =
     *   Format.asprintf "%a"
     *     (Output_sdk_ocaml.pp_interface__mli ~storage_name
     *        ~has_original_blockchain_storage)
     *     interface in *)
    let code_interface_mli_filename =
      Factori_config.get_ocaml_code_mli_path ~dir ~contract_name () in
    (* let interface_mli_filename =
     *   Factori_config.get_ocaml_interface_mli_path ~dir ~contract_name () in *)
    (* write_file interface_mli_filename mli_file ; *)
    write_file code_interface_mli_filename "val code : Tzfunc.Proto.script_expr" ;
    Some contract_to_include
  | true, None | true, Some _ | false, _ ->
    output_verbose ~level:3
      (asprintf
         "Entering produce_ocaml_interface with final storage: %a and \
          interface %a"
         Types.pp_type_ final_storage_type_ Infer_entrypoints.pp_interface
         interface) ;
    (* Content of the abstract OCaml Interface *)
    let abstract_res_ocaml =
      Format.asprintf "%a"
        (Output_abstract_types_ocaml.process_entrypoints ~contract_name
           ~storage_name ~final_storage_type_ ~kt1 ~network)
        interface in
    (* Content of the OCaml Interface *)
    let res_ocaml =
      Format.asprintf "open %s\n%a"
        (String.capitalize_ascii
           (Factori_config.get_ocaml_code_basename ~contract_name ()))
        (Output_sdk_ocaml.process_entrypoints ~contract_name ~storage_name
           ~final_storage_type_ ~kt1 ~network)
        interface in
    (* Write the abstract OCaml interface file *)
    let abstract_interface_filename =
      Factori_config.get_ocaml_abstract_interface_path ~dir ~contract_name ()
    in
    write_file abstract_interface_filename abstract_res_ocaml ;
    (* Write the OCaml interface file *)
    let interface_filename =
      Factori_config.get_ocaml_interface_path ~dir ~contract_name () in
    write_file interface_filename res_ocaml ;
    (* write OCaml interface mli file *)
    let mli_file =
      Format.asprintf "%a"
        (Output_sdk_ocaml.pp_interface__mli ~storage_name
           ~has_original_blockchain_storage)
        interface in
    let code_interface_mli_filename =
      Factori_config.get_ocaml_code_mli_path ~dir ~contract_name () in
    let interface_mli_filename =
      Factori_config.get_ocaml_interface_mli_path ~dir ~contract_name () in
    write_file interface_mli_filename mli_file ;
    write_file code_interface_mli_filename "val code : Tzfunc.Proto.script_expr" ;
    None

let produce_ocaml_crawlori_tables dir contract_name tables =
  let res_ocaml =
    Format.asprintf "%a" Output_sdk_crawlori.process_tables tables in
  (* Write the tables file *)
  let tables_filename =
    Factori_config.get_ocaml_crawlori_tables_path ~dir ~contract_name () in
  write_file tables_filename res_ocaml

let tortoise_kind_of_field_type ~is_bm f =
  let open Infer_entrypoints in
  let open Python_file in
  match f.crawlori_column_type with
  | CVarchar -> begin
    match f.crawlori_column_index with
    | Some _ -> CharField
    | None -> TextField
  end
  | CZarith -> BigIntField
  | CInteger -> IntField
  | CJson ->
    if is_bm then
      TextField
    else
      JSONField
  | CTimestamp -> DatetimeField
  | CBoolean -> BooleanField
  | CBigint -> BigIntField
  | CBytea | CArray _ -> failwith "tortoise undefined field"

let field_to_dipdup_model ~is_bm field =
  let open Infer_entrypoints in
  let open Python_file in
  let pcf_kind = tortoise_kind_of_field_type ~is_bm field in
  {
    pcf_name = field.crawlori_column_name;
    pcf_kind;
    pcf_index = Option.bind field.crawlori_column_index (fun _ -> Some true);
    pcf_max_length =
      (if pcf_kind = CharField then
        Some 36
      else
        None);
  }

let add_dipdup_model python_file table =
  let open Infer_entrypoints in
  let open Python_file in
  let is_bm = table.crawlori_table_is_bm in
  let fields =
    List.filter
      (fun f ->
        (f.crawlori_column_name <> "transaction" || not is_bm)
        && f.crawlori_column_name <> "block"
        && f.crawlori_column_name <> "index"
        && f.crawlori_column_name <> "main")
      table.crawlori_table_fields in
  let model =
    {
      pc_name = table.crawlori_table_name;
      pc_inheritances = ["Model"];
      pc_fields = List.map (field_to_dipdup_model ~is_bm) fields;
    } in
  { python_file with python_classes = python_file.python_classes @ [model] }

let add_dipdup_models dir tables =
  let open Infer_entrypoints in
  let open Python_file in
  let filename = Factori_config.get_dipdup_models_path ~dir () in
  let python_file =
    Python_file.read_backup_file ~check_if_already_there:false ~filename ()
  in
  let python_file =
    if python_file = empty_python_file then
      {
        python_imports =
          [
            { pi_from = Some "tortoise"; pi_imports = ["fields"]; pi_as = None };
            {
              pi_from = Some "dipdup.models";
              pi_imports = ["Model"];
              pi_as = None;
            };
          ];
        python_classes = [];
        python_defs = [];
      }
    else
      python_file in
  let tables =
    List.filter
      (fun t ->
        not
        @@ List.exists
             (fun c -> c.pc_name = t.crawlori_table_name)
             python_file.python_classes)
      tables in
  let python_file = List.fold_left add_dipdup_model python_file tables in
  write_python_file ~path:filename ~python_file ()

let add_dipdup_ep_handlers_import ~db_name ~contract_name ~ep_name =
  let db_name =
    if db_name = "test" then
      "testt"
    else
      db_name in
  let ep_name =
    try
      if String.get ep_name 0 = '_' then
        String.sub ep_name 1 (String.length ep_name - 1)
      else
        ep_name
    with _ -> ep_name in
  let open Python_file in
  [
    {
      pi_from =
        Some (sprintf "%s.types.%s.parameter.%s" db_name contract_name ep_name);
      pi_imports =
        [
          sprintf "%sParameter" @@ String.capitalize_ascii
          @@ snake_to_camel ep_name;
        ];
      pi_as = None;
    };
    {
      pi_from = Some "dipdup.models";
      pi_imports = ["Transaction"];
      pi_as = None;
    };
    {
      pi_from = Some (sprintf "%s.types.%s.storage" db_name contract_name);
      pi_imports =
        [
          sprintf "%sStorage"
            (String.capitalize_ascii @@ snake_to_camel contract_name);
        ];
      pi_as = None;
    };
    {
      pi_from = Some "dipdup.context";
      pi_imports = ["HandlerContext"];
      pi_as = None;
    };
    {
      pi_from =
        Some (sprintf "%s.handlers.on_%s_new_storage" db_name contract_name);
      pi_imports = [sprintf "on_%s_new_storage" contract_name];
      pi_as = None;
    };
    {
      pi_from = None;
      pi_imports = [sprintf "%s.models" db_name];
      pi_as = Some "models";
    };
    { pi_from = None; pi_imports = ["json"]; pi_as = None };
  ]

let add_dipdup_ep_handler ~dir ~db_name ~contract_name table =
  let open Infer_entrypoints in
  let open Python_file in
  let table_name =
    Str.global_replace (Str.regexp_string "__") "_" table.crawlori_table_name
  in
  let table_name = camel_to_snake table_name in
  let filename = Factori_config.get_dipdup_ep_handler_path ~dir ~table_name in
  let python_file =
    Python_file.read_backup_file ~check_if_already_there:false ~filename ()
  in
  if python_file <> empty_python_file then
    ()
  else
    let fun_name = sprintf "on_%s" table_name in
    let ep_name =
      if table.crawlori_table_entrypoint_name = Some "_default" then
        "default"
      else
        Option.value ~default:"NO_EP_NAME" table.crawlori_table_entrypoint_name
    in
    let parameter =
      sprintf "%sParameter" @@ String.capitalize_ascii @@ snake_to_camel ep_name
    in
    let storage =
      sprintf "%sStorage"
        (String.capitalize_ascii @@ snake_to_camel contract_name) in
    let model =
      String.capitalize_ascii @@ sprintf "%s_%s" contract_name ep_name in
    let python_file =
      {
        python_imports =
          add_dipdup_ep_handlers_import ~db_name ~contract_name ~ep_name;
        python_classes = [];
        python_defs =
          [
            {
              pd_async = Some true;
              pd_name = fun_name;
              pd_args =
                [
                  { pd_arg_name = "ctx"; pd_arg_type = "HandlerContext" };
                  {
                    pd_arg_name = "tr";
                    pd_arg_type =
                      sprintf "Transaction [%s, %s]" parameter storage;
                  };
                ];
              pd_return_type = "None";
              pd_body =
                sprintf
                  {|
  await on_%s_new_storage(contract=tr.data.target_address,data=tr.data,storage=tr.storage)
  try:
    m = await models.%s.create(
          contract=tr.data.target_address,
          transaction=tr.data.hash,
          level=tr.data.level,
          tsp=tr.data.timestamp,
          %s_parameter=tr.data.parameter_json)
    await m.save()
  except:
    parameter_json = json.dumps(tr.parameter, default=lambda o: o.__dict__, sort_keys=True)
    m = await models.%s.create(
          contract=tr.data.target_address,
          transaction=tr.data.hash,
          level=tr.data.level,
          tsp=tr.data.timestamp,
          %s_parameter=parameter_json)
    await m.save()
|}
                  contract_name model ep_name model ep_name;
            };
          ];
      } in
    write_python_file ~path:filename ~python_file ()

let add_dipdup_ep_handlers ~dir ~db_name ~contract_name ~entrypoint_tables =
  List.iter
    (add_dipdup_ep_handler ~dir ~db_name ~contract_name)
    entrypoint_tables

let add_dipdup_bm_handlers_import ~db_name ~contract_name ~bm_name =
  let db_name =
    if db_name = "test" then
      "testt"
    else
      db_name in
  let bm_name =
    if bm_name = "0" then
      "big_map"
    else
      bm_name in
  let open Python_file in
  [
    {
      pi_from =
        Some
          (sprintf "%s.types.%s.big_map.%s_value" db_name contract_name bm_name);
      pi_imports =
        [sprintf "%sValue" @@ String.capitalize_ascii @@ snake_to_camel bm_name];
      pi_as = None;
    };
    {
      pi_from =
        Some
          (sprintf "%s.types.%s.big_map.%s_key" db_name contract_name bm_name);
      pi_imports =
        [sprintf "%sKey" @@ String.capitalize_ascii @@ snake_to_camel bm_name];
      pi_as = None;
    };
    {
      pi_from = Some "dipdup.context";
      pi_imports = ["HandlerContext"];
      pi_as = None;
    };
    {
      pi_from = Some "dipdup.models";
      pi_imports = ["BigMapDiff"; "BigMapAction"];
      pi_as = None;
    };
    {
      pi_from = None;
      pi_imports = [sprintf "%s.models" db_name];
      pi_as = Some "models";
    };
  ]

let add_dipdup_bm_handler ~dir ~db_name ~contract_name bm_id =
  let open Infer_entrypoints in
  let open Python_file in
  let bm_name =
    Option.value ~default:"NO_BM_NAME" bm_id.crawlori_column_bm_name in
  let bm_dd_name =
    Option.value ~default:"NO_BM_NAME" bm_id.crawlori_column_dipdup_bm_name
  in
  let bm_dd_name =
    String.map
      (fun c ->
        if c = '.' then
          '_'
        else
          c)
      bm_dd_name in
  let bm_dd_name =
    if bm_dd_name = "0" then
      "big_map"
    else
      bm_dd_name in
  let bm_dd_name = camel_to_snake bm_dd_name in
  let filename =
    Factori_config.get_dipdup_bm_handler_path ~dir ~contract_name
      ~bm_name:bm_dd_name in
  let python_file =
    Python_file.read_backup_file ~check_if_already_there:false ~filename ()
  in
  if python_file <> empty_python_file then
    ()
  else
    let fun_name = sprintf "on_%s_%s_updates" contract_name bm_dd_name in
    let key_type =
      sprintf "%sKey" @@ String.capitalize_ascii @@ snake_to_camel bm_dd_name
    in
    let value_type =
      sprintf "%sValue" @@ String.capitalize_ascii @@ snake_to_camel bm_dd_name
    in
    let model = sprintf "Bm_%s_%s" contract_name bm_name in
    let python_file =
      {
        python_imports =
          add_dipdup_bm_handlers_import ~db_name ~contract_name
            ~bm_name:bm_dd_name;
        python_classes = [];
        python_defs =
          [
            {
              pd_async = Some true;
              pd_name = fun_name;
              pd_args =
                [
                  { pd_arg_name = "ctx"; pd_arg_type = "HandlerContext" };
                  {
                    pd_arg_name = "bmd";
                    pd_arg_type =
                      sprintf "BigMapDiff [%s, %s]" key_type value_type;
                  };
                ];
              pd_return_type = "None";
              pd_body =
                sprintf
                  {|
    if (bmd.data.action.has_key):
        u = await models.%s.get_or_none(contract=bmd.data.contract_address,key_%s_parameter=bmd.key)
        if (u is None):
            u = await models.%s.create(
                contract=bmd.data.contract_address,
                level=bmd.data.level,
                tsp=bmd.data.timestamp,
                key_%s_parameter=bmd.key,
                value_%s_parameter=bmd.value)
            await u.save()
        else:
            u.level=bmd.data.level
            u.tsp=bmd.data.timestamp
            u.value=bmd.value
            await u.save()
|}
                  model bm_name model bm_name bm_name;
            };
          ];
      } in
    write_python_file ~path:filename ~python_file ()

let add_dipdup_bm_handlers ~dir ~db_name ~contract_name ~bm_ids =
  List.iter (add_dipdup_bm_handler ~dir ~db_name ~contract_name) bm_ids

let add_dipdup_storage_handlers_import ~db_name ~contract_name =
  let open Python_file in
  let db_name =
    if db_name = "test" then
      "testt"
    else
      db_name in
  [
    {
      pi_from = Some "dipdup.models";
      pi_imports = ["Transaction"; "OperationData"];
      pi_as = None;
    };
    {
      pi_from = Some (sprintf "%s.types.%s.storage" db_name contract_name);
      pi_imports =
        [
          sprintf "%sStorage" @@ String.capitalize_ascii
          @@ snake_to_camel contract_name;
        ];
      pi_as = None;
    };
    {
      pi_from = None;
      pi_imports = [sprintf "%s.models" db_name];
      pi_as = Some "models";
    };
    { pi_from = None; pi_imports = ["json"]; pi_as = None };
  ]

let add_dipdup_storage_handler ~dir ~db_name ~contract_name =
  let open Python_file in
  let name = sprintf "on_%s_new_storage" contract_name in
  let filename =
    Factori_config.get_dipdup_storage_handler_path ~dir ~contract_name in
  let python_file =
    Python_file.read_backup_file ~check_if_already_there:false ~filename ()
  in
  if python_file <> empty_python_file then
    ()
  else
    let storage_type =
      sprintf "%sStorage" @@ String.capitalize_ascii
      @@ snake_to_camel contract_name in
    let python_file =
      {
        python_imports =
          add_dipdup_storage_handlers_import ~db_name ~contract_name;
        python_classes = [];
        python_defs =
          [
            {
              pd_async = Some true;
              pd_name = name;
              pd_args =
                [
                  { pd_arg_name = "contract"; pd_arg_type = "str" };
                  { pd_arg_name = "data"; pd_arg_type = "OperationData" };
                  { pd_arg_name = "storage"; pd_arg_type = storage_type };
                ];
              pd_return_type = "None";
              pd_body =
                sprintf
                  {|
    storage_json = json.dumps(storage, default=lambda o: o.__dict__, sort_keys=True)
    storage = await models.%s_storage.create(
        contract=contract,
        transaction=data.hash,
        level=data.level,
        tsp=data.timestamp,
        storage_content=storage_json)
    await storage.save()
|}
                  (String.capitalize_ascii contract_name);
            };
          ];
      } in
    write_python_file ~path:filename ~python_file ()

let add_dipdup_handlers ~dir ~db_name ~contract_name ~entrypoint_tables ~bm_ids
    =
  add_dipdup_storage_handler ~dir ~db_name ~contract_name ;
  add_dipdup_ep_handlers ~dir ~db_name ~contract_name ~entrypoint_tables ;
  add_dipdup_bm_handlers ~dir ~db_name ~contract_name ~bm_ids

let dipdup_index_handler_from_ep_table ~contract_name table =
  let open Yml_file in
  let open Infer_entrypoints in
  let table_name =
    Str.global_replace (Str.regexp_string "__") "_" table.crawlori_table_name
  in
  let table_name = camel_to_snake table_name in
  let pattern =
    {
      dipdup_pattern_destination = contract_name;
      dipdup_pattern_entrypoint =
        (if table.crawlori_table_entrypoint_name = Some "_default" then
          "default"
        else
          Option.value ~default:"no_entrypoint"
            table.crawlori_table_entrypoint_name);
    } in
  {
    dipdup_handler_callback = sprintf "on_%s" table_name;
    dipdup_handler_pattern = Some pattern;
    dipdup_handler_contract = None;
    dipdup_handler_path = None;
  }

let dipdup_index_handler_from_bm_col ~contract_name col =
  let open Yml_file in
  let open Infer_entrypoints in
  let bm_name =
    Option.value ~default:"NO_BM_NAME" col.crawlori_column_dipdup_bm_name in
  let bm_name =
    String.map
      (fun c ->
        if c = '.' then
          '_'
        else
          c)
      bm_name in
  let bm_name =
    if bm_name = "0" then
      "big_map"
    else
      bm_name in
  let bm_name = camel_to_snake bm_name in
  {
    dipdup_handler_callback = sprintf "on_%s_%s_updates" contract_name bm_name;
    dipdup_handler_pattern = None;
    dipdup_handler_contract = Some contract_name;
    dipdup_handler_path = col.crawlori_column_dipdup_bm_name;
  }

let add_dipdup_indexing ~dir ~contract_name ~network ~entrypoint_tables ~bm_ids
    =
  let open Yml_file in
  let filename = Factori_config.get_dipdup_configuration_path ~dir () in
  let yml_file =
    Yml_file.read_backup_file ~check_if_already_there:false ~filename () in
  let datasource, _ = Factori_utils.get_tzkt_datasource_url network in
  let yml_file =
    let index_name = sprintf "%s_operations" contract_name in
    if
      not
      @@ List.exists
           (fun i -> i.yml_dipdup_index_name = index_name)
           yml_file.yml_dipdup_indexes
    then
      let new_index =
        {
          yml_dipdup_index_name = index_name;
          yml_dipdup_index_kind = DDOperation contract_name;
          yml_dipdup_index_datasource = datasource;
          yml_dipdup_index_handlers =
            List.map
              (dipdup_index_handler_from_ep_table ~contract_name)
              entrypoint_tables;
        } in
      {
        yml_file with
        yml_dipdup_indexes = yml_file.yml_dipdup_indexes @ [new_index];
      }
    else
      yml_file in
  let yml_file =
    let index_name = sprintf "%s_state" contract_name in
    if
      not
      @@ List.exists
           (fun i -> i.yml_dipdup_index_name = index_name)
           yml_file.yml_dipdup_indexes
    then
      let new_index =
        {
          yml_dipdup_index_name = index_name;
          yml_dipdup_index_kind = DDBig_map;
          yml_dipdup_index_datasource = datasource;
          yml_dipdup_index_handlers =
            List.map (dipdup_index_handler_from_bm_col ~contract_name) bm_ids;
        } in
      {
        yml_file with
        yml_dipdup_indexes = yml_file.yml_dipdup_indexes @ [new_index];
      }
    else
      yml_file in
  write_yml_file ~path:filename ~yml_file ()

let remove_dipdup_indexing ~dir ~contract_name =
  let open Yml_file in
  let open Factori_config in
  let filename = get_dipdup_configuration_path ~dir () in
  let yml_file =
    Yml_file.read_backup_file ~check_if_already_there:false ~filename () in
  if yml_file = empty_yml_file then
    ()
  else
    let contract_name = show_sanitized_name contract_name in
    let yml_file =
      {
        yml_file with
        yml_dipdup_contracts =
          List.filter
            (fun dd_c ->
              Format.eprintf "%s = %s@." dd_c.yml_dipdup_contract_name
                contract_name ;
              dd_c.yml_dipdup_contract_name <> contract_name)
            yml_file.yml_dipdup_contracts;
        yml_dipdup_indexes =
          List.filter
            (fun dd_ind ->
              dd_ind.yml_dipdup_index_name
              <> sprintf "%s_operations" contract_name
              && dd_ind.yml_dipdup_index_name
                 <> sprintf "%s_state" contract_name)
            yml_file.yml_dipdup_indexes;
      } in
    write_yml_file ~path:filename ~yml_file ()

let add_dipdup_contract ~dir ~kt1 ~contract_name ~db_name ~network =
  let open Yml_file in
  let filename = Factori_config.get_dipdup_configuration_path ~dir () in
  let yml_file =
    Yml_file.read_backup_file ~check_if_already_there:false ~filename () in
  let yml_file =
    if yml_file = empty_yml_file then
      let yml_dipdup_datasources =
        let datasource, url = Factori_utils.get_tzkt_datasource_url network in
        [(datasource, Tzkt url)] in
      {
        yml_dipdup_spec = Some "1.2";
        (* There seems to be a weird behavior with dipdup when the package is named test *)
        yml_dipdup_package =
          Some
            (if db_name = "test" then
              "testt"
            else
              db_name);
        yml_dipdup_database =
          Some
            {
              yml_dipdup_db_kind = "sqlite";
              yml_dipdup_db_path = sprintf "%s.sqlite3" db_name;
            };
        yml_dipdup_contracts = [];
        yml_dipdup_datasources;
        yml_dipdup_indexes = [];
        yml_dipdup_advanced =
          [
            Reindex
              {
                yml_dipdup_reindex_manual = Some Exception;
                yml_dipdup_reindex_migration = Some Exception;
                yml_dipdup_reindex_rollback = Some Wipe;
                yml_dipdup_reindex_config_modified = Some Wipe;
                yml_dipdup_reindex_schema_modified = Some Wipe;
              };
          ];
      }
    else
      yml_file in
  let yml_file =
    if
      not
      @@ List.exists
           (fun c -> c.yml_dipdup_contract_name = contract_name)
           yml_file.yml_dipdup_contracts
    then
      let new_contract =
        {
          yml_dipdup_contract_name = contract_name;
          yml_dipdup_contract_address = kt1;
          yml_dipdup_contract_typename = contract_name;
        } in
      {
        yml_file with
        yml_dipdup_contracts = yml_file.yml_dipdup_contracts @ [new_contract];
      }
    else
      yml_file in
  write_yml_file ~path:filename ~yml_file ()

let add_bigmap_id_to_info dir bms =
  let open Infer_entrypoints in
  let open Ocaml_file in
  let filename = Factori_config.get_ocaml_crawlori_info_path ~dir () in
  let ocaml_file = read_backup_file ~check_if_already_there:false ~filename () in
  let ocaml_file =
    if ocaml_file = empty_ocaml_file then
      {
        open_modules = ["tzfunc"; "proto"];
        types =
          [
            TypeString
              {
                string_name = "z";
                string_value = "(A.uzarith[@encoding A.uzarith_enc.json])";
                string_ppx = Some "encoding";
              };
            TypeRecord
              {
                record_name = "contract_info";
                record_fields =
                  [
                    {
                      field_name = "ci_address";
                      field_type = "A.contract [@encoding A.contract_enc.json]";
                    };
                    { field_name = "ci_kind"; field_type = "string" };
                  ];
                record_ppx = Some "encoding";
              };
          ];
        body =
          [
            Let
              {
                name = "info_of_row";
                args = ["r"];
                type_ = None;
                content =
                  RecordSet [("ci_address", "r#address"); ("ci_kind", "r#kind")];
              };
            Let
              {
                name = "track_contract";
                args = ["address"; "kind"];
                type_ = None;
                content =
                  RecordSet [("ci_address", "address"); ("ci_kind", "kind")];
              };
          ];
      }
    else
      ocaml_file in
  let ocaml_file =
    List.fold_left
      (fun ocaml_file bm ->
        let field =
          { field_name = bm.crawlori_column_name; field_type = "z option" }
        in
        let ocaml_file =
          add_field_to_type ~ocaml_file ~type_name:"contract_info" ~field in
        let ocaml_file = add_field_to_of_row ~ocaml_file ~field in
        add_field_to_track ~ocaml_file ~field)
      ocaml_file bms in
  write_ocaml_file ~path:filename ~ocaml_file ()

let add_plugin_register dir contract_name =
  let open Ocaml_file in
  let filename = Factori_config.get_ocaml_crawlori_register_path ~dir () in
  let ocaml_file = read_backup_file ~check_if_already_there:false ~filename () in
  let ocaml_file =
    if ocaml_file = empty_ocaml_file then
      {
        open_modules = ["crawlori"; "common"];
        types = [];
        body =
          [
            Let
              {
                name = "register_all_plugins";
                args = ["config"];
                type_ = None;
                content = LetOpen ("Make(Pg)(E)", SeqContent []);
              };
            (* TODO this is ugly *)
            Any ";";
            Any
              {|Lwt.map (Result.iter_error Rp.print_error) @@
let>? () = init config in
loop config|};
          ];
      }
    else
      ocaml_file in
  let ocaml_file = add_plugin_register_to_seq ~ocaml_file ~contract_name in
  write_ocaml_file ~path:filename ~ocaml_file ()

let remove_plugin_register dir contract_name =
  let open Ocaml_file in
  let filename = Factori_config.get_ocaml_crawlori_register_path ~dir () in
  let ocaml_file = read_backup_file ~check_if_already_there:false ~filename () in
  let ocaml_file = remove_plugin_register_to_seq ~ocaml_file ~contract_name in
  write_ocaml_file ~path:filename ~ocaml_file ()

let add_contract_register ~ocaml_file ~contract_name bm_ids =
  let open Infer_entrypoints in
  let open Ocaml_file in
  let reg_name =
    Printf.sprintf "%s_register" (show_sanitized_name contract_name) in
  if
    List.exists
      (function
        | Let { name; _ } -> name = reg_name
        | _ -> false)
      ocaml_file.body
  then
    ocaml_file
  else
    match List.map (fun c -> c.crawlori_column_name) bm_ids with
    | [] ->
      {
        ocaml_file with
        body =
          ocaml_file.body
          @ [
              Let
                {
                  name = reg_name;
                  args = ["~dbh"; "~forward"; "config"; "address"; "kind"; "op"];
                  type_ = None;
                  content =
                    StrContent
                      (Printf.sprintf
                         {|let>? () = [%%pgsql dbh "insert into contracts\
(address,kind,transaction,block,index,level,tsp,main) \
values \
($address,$kind,${op.bo_hash},${op.bo_block},${op.bo_index},\
${op.bo_level},${op.bo_tsp},$forward)"] in
config.contracts <- CSet.add (track_contract address kind) config.contracts ;
rok ()|});
                };
            ];
      }
    | cols ->
      {
        ocaml_file with
        body =
          ocaml_file.body
          @ [
              Let
                {
                  name = reg_name;
                  args =
                    ["~dbh"; "~forward"; "config"; "address"; "kind"; "op"]
                    @ cols;
                  type_ = None;
                  content =
                    StrContent
                      (Printf.sprintf
                         {|let>? () = [%%pgsql dbh "insert into contracts\
(address,kind,transaction,block,index,level,tsp,main,%s) \
values \
($address,$kind,${op.bo_hash},${op.bo_block},${op.bo_index},\
${op.bo_level},${op.bo_tsp},$forward,%s)"] in
config.contracts <- CSet.add (track_contract address kind %s) config.contracts ;
rok ()|}
                         (String.concat "," cols)
                         (String.concat ","
                         @@ List.map
                              (fun c -> "$" ^ c.crawlori_column_name)
                              bm_ids)
                         (String.concat " "
                         @@ List.map
                              (fun c ->
                                "~ci_" ^ c.crawlori_column_name ^ ":"
                                ^ c.crawlori_column_name)
                              bm_ids));
                };
            ];
      }

let add_contract_register_to_contracts ~storage_name ~contract_name dir bm_ids =
  let open Ocaml_file in
  let filename = Factori_config.get_ocaml_crawlori_contracts_path ~dir () in
  let ocaml_file = read_backup_file ~check_if_already_there:false ~filename () in
  let ocaml_file =
    if ocaml_file = empty_ocaml_file then
      {
        open_modules = ["pg"; "crp"; "common"; "info"; "e"];
        types = [];
        body =
          [
            Any
              (asprintf "%a"
                 (Output_sdk_crawlori.pp_insert_fun ~storage_name ~contract_name)
                 (Output_sdk_crawlori.all_operations_table ()));
          ];
      }
    else
      ocaml_file in
  let ocaml_file = add_contract_register ~ocaml_file ~contract_name bm_ids in
  write_ocaml_file ~path:filename ~ocaml_file ()

let enrich_contracts_table dir bm_ids =
  let open Infer_entrypoints in
  let open Ocaml_file in
  let table_path =
    Factori_config.get_ocaml_crawlori_contracts_table_path ~dir () in
  let backup_filename = get_backup_file_name ~filename:table_path in
  let contracts_table =
    read_enc_backup_file ~check_if_already_there:false ~filename:table_path
      ~enc:crawlori_table_enc () in
  let contracts_table =
    match contracts_table with
    | None -> Output_sdk_crawlori.contracts_table bm_ids
    | Some ct ->
      let new_fields =
        List.filter
          (fun f ->
            not
            @@ List.exists
                 (fun cf -> cf.crawlori_column_name = f.crawlori_column_name)
                 ct.crawlori_table_fields)
          bm_ids in
      { ct with crawlori_table_fields = ct.crawlori_table_fields @ new_fields }
  in
  let content =
    asprintf "%a" Output_sdk_crawlori.process_tables [contracts_table] in
  write_file table_path content ;
  Factori_utils.serialize_file ~filename:backup_filename ~enc:crawlori_table_enc
    contracts_table

let produce_ocaml_crawlori_plugin dir (storage_name : sanitized_name)
    contract_name tables bms =
  let res_ocaml =
    Format.asprintf "%a"
      (Output_sdk_crawlori.process_plugin ~storage_name ~contract_name ~bms)
      tables in
  let plugin_filename =
    Factori_config.get_ocaml_crawlori_plugin_path ~dir ~contract_name () in
  write_file plugin_filename res_ocaml

let produce_ocaml_crawlori_bm_utils dir =
  let res_ocaml = Ocaml_crawlori.bm_utils in
  let bm_utils_filename =
    Factori_config.get_ocaml_crawlori_bm_utils_path ~dir () in
  write_file bm_utils_filename res_ocaml

let produce_ocaml_crawlori_common dir =
  let res_ocaml = Ocaml_crawlori.common in
  let common_filename = Factori_config.get_ocaml_crawlori_common_path ~dir () in
  write_file common_filename res_ocaml

let produce_ocaml_crawlori_converters dir =
  let res_ocaml = Ocaml_crawlori.converters in
  let converters_filename =
    Factori_config.get_ocaml_crawlori_converters_path ~dir () in
  write_file converters_filename res_ocaml

let produce_ocaml_crawlori_crawler dir =
  let res_ocaml = Ocaml_crawlori.crawler in
  let crawler_filename =
    Factori_config.get_ocaml_crawlori_crawler_path ~dir () in
  write_file crawler_filename res_ocaml

let produce_ocaml_crawlori_static_tables dir =
  let res_ocaml = Ocaml_crawlori.static_tables in
  let static_tables_filename =
    Factori_config.get_ocaml_crawlori_static_tables_path ~dir () in
  write_file static_tables_filename res_ocaml

let remove_upgrade_downgrade_tables dir contract_name =
  let open Ocaml_file in
  let filename = Factori_config.get_ocaml_crawlori_update_path ~dir () in
  let ocaml_file = read_backup_file ~check_if_already_there:false ~filename () in
  let ocaml_file =
    remove_tables_to_upgrade_downgrade ~ocaml_file ~contract_name in
  write_ocaml_file ~path:filename ~ocaml_file ()

let add_upgrade_downgrade_tables dir contract_name =
  let open Ocaml_file in
  let filename = Factori_config.get_ocaml_crawlori_update_path ~dir () in
  let ocaml_file = read_backup_file ~check_if_already_there:false ~filename () in
  let ocaml_file =
    if ocaml_file = empty_ocaml_file then
      {
        open_modules = [];
        types = [];
        body =
          [
            Let
              {
                name = "upgrade";
                args = [];
                type_ = None;
                content =
                  ListConcat
                    ["Static_tables.upgrade"; "Contracts_table.upgrade"];
              };
            Let
              {
                name = "downgrade";
                args = [];
                type_ = None;
                content =
                  ListConcat
                    ["Static_tables.downgrade"; "Contracts_table.downgrade"];
              };
            Any Ocaml_crawlori.update;
          ];
      }
    else
      ocaml_file in
  let ocaml_file = add_tables_to_upgrade_downgrade ~ocaml_file ~contract_name in
  write_ocaml_file ~path:filename ~ocaml_file ()

let produce_ocaml_crawlori_config dir =
  let config_filename = Factori_config.get_ocaml_crawlori_config_path ~dir () in
  if not (Sys.file_exists config_filename) then
    let res_ocaml = Artifacts.config in
    write_file config_filename res_ocaml
  else
    ()

let enrich_crawlori_dune_file ~dir ~contract_name ~db_name =
  let path = Factori_config.get_ocaml_crawlori_dir ~dir () // "dune" in
  let plugin_basename =
    Factori_config.get_ocaml_crawlori_plugin_basename ~contract_name in
  let tables_basename =
    Factori_config.get_ocaml_crawlori_tables_basename ~contract_name in
  let _interface = Factori_config.get_ocaml_interface_basename ~contract_name in
  let stanzas =
    Ocaml_dune.read_backup_file ~check_if_already_there:false ~filename:path ()
  in
  begin
    if stanzas = [] then
      let dune_file =
        [
          Artifacts.crawlori_rule1 ~db_name;
          Artifacts.crawlori_db_env_box;
          Artifacts.crawlori_static_tables;
          Artifacts.crawlori_contracts_table;
          Artifacts.crawlori_contracts;
          Artifacts.crawlori_update;
          Artifacts.crawlori_rule2;
          Artifacts.crawlori_common;
          Artifacts.crawlori_bm_utils;
          Artifacts.crawlori_exec;
        ] in
      write_dune_file ~dune_file ~path ()
  end ;
  add_stanza_to_dune_file ~stanza_must_not_exist:true ~path
    (Env "(dev\n    (flags (:standard -w -27)))") ;
  add_stanza_to_dune_file ~stanza_must_not_exist:true ~path
    (Artifacts.crawlori_tables ~contract_name) ;
  add_library_to_stanza_in_dune_file ~path ~stanza_name:"update" tables_basename ;
  add_stanza_to_dune_file ~stanza_must_not_exist:true ~path
    (Artifacts.crawlori_plugin ~contract_name) ;
  add_library_to_stanza_in_dune_file ~path ~stanza_name:"crawler"
    plugin_basename

let import_from_michelson ?(original_kt1 = None) ?(library = false)
    ?(crawlori = false) ~db_name ~ctrct_name () =
  match !michelson_file with
  | None -> raise NoMichelsonFileProvided
  | Some michelson_file -> (
    let se, format =
      try
        let res =
          deserialize_file ~filename:michelson_file
            ~enc:Tzfunc.Proto.script_expr_enc.json in
        (res, Factori_config.MichelsonJson)
      with CouldNotDestruct _ | CouldNotReadAsJson _ ->
        Factori_utils.output_verbose
          "Could not deserialize file as Json, will try to parse it as an \
           actual Michelson file" ;
        let res = Parse_michelson.parse_michelson_file ~file:michelson_file in
        (res, Michelson) in
    let entrypoints = wrap_get_entrypoints_michelson_file se in
    match get_storage_from_script_expr se with
    | None ->
      raise
        (GenericError
           ( "import_from_michelson",
             "Couldn't get storage type from Michelson file" ))
    | Some storage ->
      (* Michelson *)
      let dir = Option.value ~default:Filename.current_dir_name !dir in
      let contract_name =
        sanitize_basename ctrct_name
        (* Option.value ~default:michelson_file_name !contract_name *) in
      (* Entrypoint serialization *)
      let entrypoints_simplified =
        List.map (fun (x, y) -> (show_sanitized_name x, y)) entrypoints in
      Factori_config.marshal_contract_signature ~dir ~contract_name storage
        entrypoints_simplified ;

      (* Typenaming *)
      let typenaming_file =
        Factori_config.get_typenaming_file ~dir ~contract_name in
      (* AST *)
      output_verbose
      @@ sprintf "The typenaming file name is %s\n%!" typenaming_file ;
      let _ = Infer_entrypoints.Naming.init_names () in
      let _ = Infer_entrypoints.init_edges () in
      let initial_nodes =
        if Sys.file_exists typenaming_file then (
          output_verbose "Unmarshalling known types..\n" ;
          Infer_entrypoints.Marshal_names.unmarshal_names typenaming_file
        ) else
          [] in
      let storage_mich =
        Infer_entrypoints.type__of_micheline ~records_allowed:true storage in
      let storage_name, final_storage_type_, interface =
        Infer_entrypoints.extract_interface ~initial_nodes ~storage:storage_mich
          entrypoints in
      (* Marshal type names *)
      let marshalled_types = Infer_entrypoints.Marshal_names.marshal_names () in
      output_verbose "Writing newly marshalled types" ;
      write_file typenaming_file marshalled_types ;

      (* OCaml interface *)
      if !ocaml then (
        let additional_libraries =
          match
            produce_ocaml_interface
              ~signature:(storage, entrypoints_simplified)
              ~storage_name ~final_storage_type_ dir contract_name ~kt1:None
              !network interface
          with
          | Some contract_to_include ->
            [Format.sprintf "%s_ocaml_interface" contract_to_include]
          | None -> [] in
        let code = se in
        marshal_code_ocaml dir contract_name code ;
        (if not library then
          let scenario_dir = Factori_config.get_ocaml_scenarios_dir ~dir () in
          (* Add OCaml interface as a library to the scenario
             stanza of the OCaml scenario dune file *)
          add_library_to_stanza_in_dune_file ~path:(scenario_dir // "dune")
            ~stanza_name:"scenario"
            (Factori_config.get_ocaml_interface_basename ~contract_name)) ;
        if crawlori then begin
          let crawlori_dir = Factori_config.get_ocaml_crawlori_dir ~dir () in
          if not (is_dir crawlori_dir) then make_dir crawlori_dir ;
          let storage_table, bigmap_tables, entrypoint_tables, bm_ids =
            Output_sdk_crawlori.extract_tables ~storage_name ~contract_name
              ~interface
              ~entrypoints:(fst @@ List.split entrypoints) in
          produce_ocaml_crawlori_tables dir contract_name
            ((storage_table :: bigmap_tables) @ entrypoint_tables) ;
          enrich_contracts_table dir bm_ids ;
          add_contract_register_to_contracts ~storage_name ~contract_name dir
            bm_ids ;

          produce_ocaml_crawlori_plugin dir storage_name contract_name
            ((storage_table :: bigmap_tables) @ entrypoint_tables)
            bm_ids ;

          add_bigmap_id_to_info dir bm_ids ;
          add_plugin_register dir contract_name ;
          add_upgrade_downgrade_tables dir contract_name ;

          produce_ocaml_crawlori_static_tables dir ;
          produce_ocaml_crawlori_crawler dir ;
          produce_ocaml_crawlori_common dir ;
          produce_ocaml_crawlori_converters dir ;
          produce_ocaml_crawlori_bm_utils dir ;
          produce_ocaml_crawlori_config dir ;
          enrich_crawlori_dune_file ~dir ~contract_name ~db_name
        end ;
        (* Add the OCaml interface to the list of modules in
           the dune file of the OCaml interface dir *)
        add_to_dune_file ~file_must_exist:false
          ~path:(Factori_config.get_ocaml_interface_dir ~dir () // "dune")
          (Artifacts.dune_chunk_new_ocaml_interface ~additional_libraries
             ~name:contract_name ())
      ) ;
      (* Python interface *)
      if !python then begin
        produce_python_interface ~kt1:None ~network:!network ~storage_name
          ~final_storage_type_ dir contract_name interface ;
        let code = se in
        marshal_code_python dir contract_name code
      end ;
      (* Csharp interface *)
      if !csharp then begin
        produce_csharp_interface ~kt1:None ~network:!network ~storage_name
          ~final_storage_type_ dir contract_name interface ;
        let code = se in
        marshal_code_csharp dir contract_name code
      end ;
      (* Typescript interface *)
      if !typescript then
        produce_typescript_interface ~kt1:None ~network:!network ~storage_name
          ~final_storage_type_ dir contract_name interface ;
      let code = se in
      marshal_code_typescript dir contract_name code ;
      (* General: add contract to local config file *)
      Factori_config.add_contract ~original_kt1 ~dir ~format ~name:contract_name
        () ;

      (* Print file structure for comfort of user *)
      let tree_command_content =
        Format.sprintf "tree -P '%s|%s' --prune %s || exit 0"
          (Factori_config.get_ocaml_interface_filename ~contract_name)
          (Factori_config.get_typescript_interface_filename ~contract_name)
          dir in
      let tree_command =
        Factori_utils.command_if_exists
          ~error_msg:
            "(soft warning) \"tree\" could not be found, not printing source \
             tree"
          ~cmd_name:"tree" tree_command_content in
      let _ = Sys.command tree_command in
      ())

let get_code_from_michelson () =
  let open Factori_config in
  match !dir with
  | None -> raise NoDirectoryProvided
  | Some dir -> (
    if not (Sys.file_exists dir) then make_dir dir ;
    if not (is_dir dir) then
      raise (NotADirectory (dir, "get_code_from_michelson"))
    else
      match !michelson_file with
      | None -> raise NoMichelsonFileProvided
      | Some michelson_file ->
        let michelson_file_name = Filename.remove_extension michelson_file in
        (* let dir = Option.value ~default:current_dir_name !dir in *)
        let contract_name =
          sanitize_basename
          @@ Option.value
               ~default:
                 (Filename.basename
                 @@ Filename.remove_extension michelson_file_name)
               !contract_name in
        let filename =
          dir // "src" // "contracts" // "michelson"
          // (show_sanitized_name contract_name ^ ".json") in
        copy_file michelson_file filename)

let import_kt1 ?(network = "mainnet") ?(library = false) ?(crawlori = false)
    ?(dipdup = false) ~db_name () =
  let open Infer_entrypoints in
  let open Tzfunc.Rp in
  match !kt1 with
  | None ->
    Format.eprintf "No provided KT1" ;
    raise NoKT1Provided
  | Some kt1 ->
    let entrypoints =
      Factori_utils.wrap_get_entrypoints_from_kt1 ~kt1 ~network () in
    let entrypoints =
      if entrypoints <> [] then
        List.map (fun (n, m) -> (sanitized_of_str n, m)) entrypoints
      else
        let se = get_code_from_KT1 () in
        match get_parameter_from_script_expr se with
        | None ->
          raise
            (GenericError ("import_kt1", "no parameter found in script_expr"))
        | Some se -> [(sanitized_of_str "default", se)] in
    let dir = Option.value ~default:Filename.current_dir_name !dir in
    let contract_name =
      sanitize_basename
      @@ Option.value
           ~default:(Factori_config.get_default_contract_name ~kt1)
           !contract_name in
    (* Type naming *)
    let typenaming_file =
      Factori_config.get_typenaming_file ~dir ~contract_name in
    (* AST *)
    let _ = Infer_entrypoints.Naming.init_names () in
    let _ = Infer_entrypoints.init_edges () in
    let initial_nodes =
      if Sys.file_exists typenaming_file then
        Infer_entrypoints.Marshal_names.unmarshal_names typenaming_file
      else
        [] in
    let storage = get_storage kt1 in
    let storage_type_ =
      Infer_entrypoints.type__of_micheline ~msg:"storage" ~records_allowed:true
        storage in
    let storage_name, final_storage_type_, interface =
      Infer_entrypoints.extract_interface ~initial_nodes ~storage:storage_type_
        entrypoints in
    (* Entrypoint serialization *)
    let entrypoints_simplified =
      List.map (fun (x, y) -> (x.original, y)) entrypoints in
    Factori_config.marshal_contract_signature ~dir ~contract_name storage
      entrypoints_simplified ;
    (* Marshal type names *)
    let marshalled_types = Infer_entrypoints.Marshal_names.marshal_names () in
    output_verbose "Writing newly marshalled types" ;
    write_file typenaming_file marshalled_types ;

    (* Dip{Dup} interface *)
    if dipdup then begin
      let dipdup_dir = Factori_config.get_dipdup_dir ~dir in
      let dipdup_handlers_dir = Factori_config.get_dipdup_handlers_dir ~dir in
      if not (is_dir dipdup_dir) then make_dir dipdup_dir ;
      if not (is_dir dipdup_handlers_dir) then make_dir dipdup_handlers_dir ;
      let storage_table, entrypoint_tables, bigmap_tables, bm_ids =
        Output_sdk_crawlori.extract_tables ~storage_name ~contract_name
          ~interface
          ~entrypoints:(fst @@ List.split entrypoints) in
      (* DipDup doesn't allow indexing of entrypoint that can be specified *)
      let contract_name = show_sanitized_name contract_name in
      let entrypoint_tables =
        List.filter
          (fun t ->
            match t.crawlori_table_type with
            | Some (Or _) -> false
            | _ -> true)
          entrypoint_tables in
      add_dipdup_models dir
        ((storage_table :: bigmap_tables) @ entrypoint_tables) ;
      add_dipdup_contract ~dir ~kt1 ~contract_name ~db_name ~network ;
      add_dipdup_indexing ~dir ~contract_name ~network ~entrypoint_tables
        ~bm_ids ;
      add_dipdup_handlers ~dir ~db_name ~contract_name ~entrypoint_tables
        ~bm_ids
    end ;
    (* OCaml interface *)
    if !ocaml then (
      marshal_code_ocaml_from_KT1 dir contract_name ;
      (* Produce the OCaml interface *)
      let additional_libraries =
        match
          produce_ocaml_interface
            ~signature:(storage, entrypoints_simplified)
            dir contract_name ~storage_name ~final_storage_type_ ~kt1:(Some kt1)
            network interface
        with
        | Some contract_to_include ->
          [Format.sprintf "%s_ocaml_interface" contract_to_include]
        | None -> [] in
      (* Complement the OCaml interface dune file *)
      add_to_dune_file ~file_must_exist:false ~stanza_must_not_exist:true
        ~path:(Factori_config.get_ocaml_interface_dir ~dir () // "dune")
        [Artifacts.dune_env_remove_warnings] ;
      add_to_dune_file ~file_must_exist:false
        ~path:(Factori_config.get_ocaml_interface_dir ~dir () // "dune")
        (Artifacts.dune_chunk_new_ocaml_interface ~name:contract_name
           ~additional_libraries ()) ;
      (* Scenario: both example file and actual file *)
      if not library then begin
        let scenario_dir = Factori_config.get_ocaml_scenarios_dir ~dir () in
        if not (is_dir scenario_dir) then make_dir scenario_dir ;
        if not (Sys.file_exists (scenario_dir // "dune")) then
          write_dune_file ~with_readable_backup:true
            ~dune_file:
              (Artifacts.scenarios_dunefile (Factori_config.get_contracts ~dir))
            ~path:(scenario_dir // "dune") () ;

        let scenario_example_file =
          Factori_config.get_ocaml_scenarios_example_path ~dir () in
        let scenario_file = Factori_config.get_ocaml_scenarios_path ~dir () in
        (* Complement the scenario dune file *)
        add_library_to_stanza_in_dune_file ~file_must_exist:false
          ~path:(scenario_dir // "dune") ~stanza_name:"scenario"
          (Factori_config.get_ocaml_interface_basename ~contract_name) ;
        (* Write the scenario file *)
        if not (Sys.file_exists scenario_file) then
          write_file scenario_file Artifacts.scenario_ml ;
        (* Fetch blockchain storage to include initial storage in
           the example file *)
        match
          Lwt_main.run
            (let>? storage =
               Factori_config.get_storage ~network ~debug:(!verbosity > 0) kt1
             in
             Ocaml_file.add_modules_to_ocaml_file ~file_must_exist:false
               ~path:scenario_example_file
               [Factori_config.get_ocaml_interface_basename ~contract_name] ;
             try
               let value =
                 value_of_typed_micheline (* ~debug:(!verbosity > 0) *) storage
                   final_storage_type_ in
               Ocaml_file.append_body_to_ocaml_file ~file_must_exist:false
                 ~path:scenario_example_file
                 (Let
                    {
                      name =
                        sprintf "initial_storage_%s"
                          (show_sanitized_name contract_name);
                      args = [];
                      type_ = None;
                      content =
                        StrContent
                          (asprintf "%a\n(*\n %s\n*)\n"
                             Output_sdk_ocaml.pp_value_to_ocaml value
                             (EzEncoding.construct micheline_enc.json storage));
                    }) ;
               Lwt.return_ok ()
             with e ->
               handle_exception e ;
               Lwt.return_ok ())
        with
        | Ok () -> ()
        | Error e -> print_error e
      end ;
      if crawlori then begin
        let crawlori_dir = Factori_config.get_ocaml_crawlori_dir ~dir () in
        if not (is_dir crawlori_dir) then make_dir crawlori_dir ;
        let storage_table, entrypoint_tables, bigmap_tables, bm_ids =
          Output_sdk_crawlori.extract_tables ~storage_name ~contract_name
            ~interface
            ~entrypoints:(fst @@ List.split entrypoints) in

        produce_ocaml_crawlori_tables dir contract_name
          ((storage_table :: bigmap_tables) @ entrypoint_tables) ;
        enrich_contracts_table dir bm_ids ;
        add_contract_register_to_contracts ~storage_name ~contract_name dir
          bm_ids ;

        produce_ocaml_crawlori_plugin dir storage_name contract_name
          ((storage_table :: bigmap_tables) @ entrypoint_tables)
          bm_ids ;

        add_bigmap_id_to_info dir bm_ids ;
        add_plugin_register dir contract_name ;
        add_upgrade_downgrade_tables dir contract_name ;

        produce_ocaml_crawlori_static_tables dir ;
        produce_ocaml_crawlori_crawler dir ;
        produce_ocaml_crawlori_common dir ;
        produce_ocaml_crawlori_converters dir ;
        produce_ocaml_crawlori_bm_utils dir ;
        produce_ocaml_crawlori_config dir ;
        enrich_crawlori_dune_file ~dir ~contract_name ~db_name
      end
    ) ;
    (* Add contract to local config file *)
    Factori_config.add_contract
      ~original_kt1:(Some (kt1, network))
      ~dir ~format:Factori_config.KT1 ~name:contract_name () ;
    Console.info "Import of %s successful.\n%!" kt1 ;

    (* Python interface *)
    if !python then begin
      produce_python_interface ~kt1:(Some kt1) ~network ~storage_name
        ~final_storage_type_ dir contract_name interface ;
      marshal_code_python_from_KT1 dir contract_name
    end ;
    (* Csharp interface *)
    if !csharp then begin
      produce_csharp_interface ~kt1:(Some kt1) ~network ~storage_name
        ~final_storage_type_ dir contract_name interface ;
      marshal_code_csharp_from_KT1 dir contract_name
    end ;
    (* Typescript interface *)
    if !typescript then (
      marshal_code_typescript_from_KT1 dir contract_name ;
      produce_typescript_interface ~kt1:(Some kt1) ~network ~storage_name
        ~final_storage_type_ dir contract_name interface
    ) ;
    (* Web forms *)
    if !typescript && !web_mode then (
      let all_contracts = Factori_config.get_contracts ~dir in
      let web_contracts =
        List.filter
          (fun c -> List.exists (fun s -> s = "web") c.Factori_config.options)
          all_contracts in
      Handle_web.produce_web ~kt1:(Some kt1) ~network
        ~name:(show_sanitized_name contract_name)
        dir interface web_contracts ~storage_type:final_storage_type_ ;
      let marshalled_types = Infer_entrypoints.Marshal_names.marshal_names () in
      write_file typenaming_file marshalled_types ;

      (* Print file structure for comfort of user *)
      (* let tree_command_content =
           Format.sprintf "tree -P '%s|%s' --prune %s || exit 0"
             (Factori_config.get_ocaml_interface_filename ~contract_name)
             (Factori_config.get_typescript_interface_filename ~contract_name)
             dir in
         let tree_command =
           Artifacts.command_if_exists
             ~error_msg:
               "(soft warning) \"tree\" could not be found, not printing source tree"
             ~cmd_name:"tree" tree_command_content in
             let _ = Sys.command tree_command in *)
      ()
    )
