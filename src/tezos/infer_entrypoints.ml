open Factori_errors
open Format
open Tzfunc.Proto
open Factori_utils
open Pair_type
open Or_type
open Types
open Value

let nothandled_se ?(msg = "nothandled_script_expr") ?(name = "unknown name")
    (x : script_expr) =
  let open Tzfunc.Proto in
  let s : string = EzEncoding.construct script_expr_enc.json x in
  raise
    (Nothandled
       (Format.sprintf "[not handled] [%s] in entrypoint \"%s\" [%s]" msg name s))

let nothandled_mich ?(msg = "nothandled_micheline") ?(name = "unknown name")
    (x : micheline) =
  let open Tzfunc.Proto in
  let s : string = EzEncoding.construct micheline_enc.json x in
  raise
    (Nothandled
       (Format.sprintf "[not handled] [%s] in entrypoint \"%s\" [%s]" msg name s))

let catch_and_print ppf f x =
  try f x with Nothandled s -> Format.fprintf ppf "%s" s

let remove_expansions ~expand x l = List.filter (fun y -> y <> expand x) l

(** remove all double entries in a list, preserving the order of first
   appearance *)
let make_unique ~eq l =
  let rec aux = function
    | [] -> []
    | x :: xs -> x :: aux (remove ~eq x xs) in
  aux l

let make_unique_up_to_expansion ~expand l =
  let rec aux = function
    | [] -> []
    | x :: xs -> x :: aux (remove_expansions ~expand x xs) in
  aux l

(* This is actually a bad idea because there is no unicity of pattern matching of given tuples *)
(* let pp_pairedtype_ocaml_pattern_match f ppf x =
 *   let rec aux ppf = function
 *     | LeafP (n,l) -> fprintf ppf "%a" (f n) l
 *     | PairP l -> fprintf ppf "Mprim {prim = `Pair;\nargs = [%a];\nannots=_}\n" (pp_print_list ~pp_sep:(tag ";") aux) l in
 *   aux ppf x *)

let reserved_words =
  [
    "default";
    "and";
    "as";
    "assert";
    "begin";
    "class";
    "constraint";
    "delete";
    "do";
    "done";
    "downto";
    "else";
    "end";
    "exception";
    "external";
    "false";
    "for";
    "fun";
    "function";
    "functor";
    "from";
    "if";
    "in";
    "include";
    "inherit";
    "initializer";
    "lambda";
    "lazy";
    "let";
    "match";
    "method";
    "module";
    "mutable";
    "new";
    "nonrec";
    "object";
    "of";
    "open";
    "operator";
    "option";
    "or";
    "params";
    "private";
    "rec";
    "sig";
    "struct";
    "then";
    "to";
    "true";
    "try";
    "type";
    "val";
    "virtual";
    "when";
    "while";
    "with";
    "lor";
    "lxor";
    "mod";
    "land";
    "lsl";
    "lsr";
    "asr";
  ]

(* to complete as problematic elements of records arise... *)

let letters_of_digit = function
  | '0' -> "zero"
  | '1' -> "one"
  | '2' -> "two"
  | '3' -> "three"
  | '4' -> "four"
  | '5' -> "five"
  | '6' -> "six"
  | '7' -> "seven"
  | '8' -> "eight"
  | '9' -> "nine"
  | c ->
    raise
      (GenericError
         ("letters_of_digit", Format.sprintf "taking %c for a digit" c))

let bind_sanitize_uncapitalize a =
  output_verbose ~level:1
  @@ sprintf "[bind_sanitize_uncapitalize] Entering with %s"
       (show_sanitized_name a) ;
  let res = { a with sanitized = String.uncapitalize_ascii a.sanitized } in
  output_verbose ~level:1
  @@ sprintf "[bind_sanitize_uncapitalize] Leaving with result %s"
       (show_sanitized_name res) ;
  res

(** Add '_' before a reserved word in a type name *)
let filter s =
  output_verbose ~level:1
  @@ sprintf "[filter] Entering with %s" (show_sanitized_name s) ;
  let s_uncapitalized = String.uncapitalize_ascii (show_sanitized_name s) in
  let res =
    if
      List.mem s_uncapitalized reserved_words
      || List.mem s_uncapitalized reserved_words
    then
      sprintf "_%s" (show_sanitized_name s)
    else
      sprintf "%s" (show_sanitized_name s) in
  output_verbose ~level:1 @@ sprintf "[filter] Leaving with %s" res ;
  res

let bind_filter a =
  output_verbose ~level:1
  @@ sprintf "[bind_filter] Entering with %s" (show_sanitized_name a) ;
  let res = { a with sanitized = filter a } in
  output_verbose ~level:1
  @@ sprintf "[bind_filter] Leaving with %s" (show_sanitized_name res) ;
  res

(** Remove '%' and capital letters in names extracted from
   Michelson *)
let sanitize_annot_aux (annot : string) =
  let n = String.length annot in
  let first_char = String.get annot 0 in
  let annot =
    if List.mem first_char ['%'; ':'] then
      String.sub annot 1 (n - 1)
    else
      annot in
  let first_char = String.get annot 0 in
  let annot =
    if
      int_of_char '0' <= int_of_char first_char
      && int_of_char first_char <= int_of_char '9'
    then
      let prefix = letters_of_digit first_char in
      prefix ^ String.sub annot 1 (n - 2)
    else
      annot in
  filter @@ sanitized_of_str @@ String.uncapitalize_ascii annot

let sanitize_annot a =
  { original = a; sanitized = sanitize_annot_aux a; accessors = [] }

let bind_sanitize_annot a =
  {
    original = a.original;
    sanitized = sanitize_annot_aux a.sanitized;
    accessors = a.accessors;
  }

(** Remove '%' but keep a capital letter at the beginning; this is
   useful for sum type constructors for example. *)
let sanitize_capitalize (annot : string) =
  let res =
    (fun x -> filter @@ sanitized_of_str @@ String.capitalize_ascii x)
      (sanitize_annot_aux annot) in
  let is_capital_letter c =
    int_of_char c >= int_of_char 'A' && int_of_char c <= int_of_char 'Z' in
  if is_capital_letter (String.get res 0) then
    res
  else
    Format.sprintf "Constructor%s" res

let bind_sanitize_capitalize a =
  {
    original = a.original;
    sanitized = sanitize_capitalize a.sanitized;
    accessors = a.accessors;
  }

type opt_type_ = type_ option [@@deriving show]

let list t = Unary (List, t)

let set t = Unary (Set, t)

let ticket t = Unary (Ticket, t)

let option t = Unary (Option, t)

let map (t1, t2) = Binary (Map, (t1, t2))

let big_map (t1, t2) = Binary (BigMap, (t1, t2))

let lambda (t1, t2) = Binary (Lambda, (t1, t2))

let eq_types t1 t2 =
  let rec aux t1 t2 =
    match (t1, t2) with
    | TVar (n1, t1), TVar (n2, t2) -> n1 = n2 && aux t1 t2
    | TVar (_, t1), t2 -> aux t1 t2
    | t1, TVar (_, t2) -> aux t1 t2
    | Record r1, Record r2 -> eq_paired_type ~eq:aux r1 r2
    | Pair l1, Pair l2 -> List.for_all2 aux l1 l2
    | Annot (t1, tn1), Annot (t2, tn2) -> aux t1 t2 && tn1 = tn2
    | Or o1, Or o2 -> eq_or_type ~eq:aux o1 o2
    | Base b1, Base b2 -> b1 = b2
    | Unary (u1, t1), Unary (u2, t2) -> u1 = u2 && aux t1 t2
    | Binary (b1, (t1, t2)), Binary (b2, (t3, t4)) ->
      b1 = b2 && aux t1 t3 && aux t2 t4
    | _, _ -> false in
  aux t1 t2

(* This can't gp into the Sumtype module because it depends on type_ *)
let sumtype_to_list t =
  let rec aux firststep path =
    let aux = aux false in
    function
    | Or (LeafOr t) -> [(t, List.rev path)]
    | Or (OrAbs (t1, t2)) ->
      aux (true :: path) (Or t1) @ aux (false :: path) (Or t2)
      (* true is left, false is right *)
    | t ->
      if firststep then
        failwith "Invalid sum type in sumtype to list"
      else
        [(t, List.rev path)] in
  aux true [] t

let rec derive_constructor ?(path = []) mich (ort : type_ or_type) =
  output_verbose ~level:2
    (asprintf "[derive_constructor] Entering with %a" (pp_or_type pp_type_) ort) ;
  match (mich, ort) with
  | Mprim { prim = `Left; args = [arg]; _ }, OrAbs (t1, _) ->
    derive_constructor ~path:("Left" :: path) arg t1
  | Mprim { prim = `Right; args = [arg]; _ }, OrAbs (_, t2) ->
    derive_constructor ~path:("Right" :: path) arg t2
  | m, LeafOr (Annot (t, name)) -> (bind_sanitize_capitalize name, m, t)
  | m, LeafOr t -> (sanitized_of_str @@ String.concat "_" (List.rev path), m, t)
  | m, _t ->
    raise (UnexpectedMichelson (EzEncoding.construct micheline_enc.json m))

module Naming = struct
  (** Fresh index i for creating a new type "typei" *)
  let sumtype_var_index = ref 0

  (** A (name -> type) map of registered names and corresponding types
     *)
  let registered_names = ref (Hashtbl.create 100 : (string, type_) Hashtbl.t)

  (** Index for naming duplicate constructors *)
  let constructor_index = ref 0

  (** Constructors of sum types, we can't repeat them because
     typescript doesn't like it *)
  let registered_constructors = Hashtbl.create 100

  (** A (type -> typename) map of registered types and the
     corresponding names *)
  let type_names = ref (Hashtbl.create 100 : (type_, sanitized_name) Hashtbl.t)

  (** A (type -> typename) map of preferred names optionally chosen
     by the user, which will take precedence in naming *)
  let preferred_type_names =
    ref (Hashtbl.create 100 : (type_, sanitized_name) Hashtbl.t)

  (** Real names of entrypoints so that we may call them correctly,
     even if we had to modify them to make the interface compile in
     e.g. OCaml or Typescript *)
  let entrypoint_real_names = Hashtbl.create 100

  let register_entrypoint_real_name name real_name =
    Hashtbl.add entrypoint_real_names name real_name

  let get_real_name name = Hashtbl.find entrypoint_real_names name

  let init_names () =
    sumtype_var_index := 0 ;
    let h1 = Hashtbl.create 100 in
    let h2 = Hashtbl.create 100 in
    let h3 = Hashtbl.create 100 in
    registered_names := h1 ;
    type_names := h2 ;
    preferred_type_names := h3

  let get_fresh_index () =
    let res = !sumtype_var_index in
    incr sumtype_var_index ;
    res

  let get_fresh_constructor_index () =
    let res = !constructor_index in
    incr constructor_index ;
    res

  let get_constructor_name constructor_name =
    if Hashtbl.mem registered_constructors constructor_name then
      let i = get_fresh_constructor_index () in
      constructor_name ^ string_of_int i
    else begin
      Hashtbl.add registered_constructors constructor_name () ;
      constructor_name
    end

  let has_name t =
    let id = get_fresh_id () in
    output_verbose ~id ~level:2
    @@ asprintf "[has_name] Checking if type already has a name...\n%!" ;
    let res = Hashtbl.mem !type_names t in
    output_verbose ~id ~level:2
    @@ sprintf "the answer is: %b (%s)\n%!" res
         (if res then
           show_sanitized_name @@ Hashtbl.find !type_names t
         else
           "") ;
    res

  (** Return a name if one already has been registered for the input
     type_ *)
  let get_name_opt t = Hashtbl.find_opt !type_names t

  let get_name_unsafe t = Hashtbl.find !type_names t

  (** Return a name if one already has been registered, or create one
     if none has.  *)
  let rec get_or_create_name t =
    match
      (Hashtbl.find_opt !type_names t, Hashtbl.find_opt !preferred_type_names t)
    with
    | None, None ->
      output_verbose ~level:2
      @@ asprintf "Couldn't find name for type %a\n" pp_type_ t ;
      let i = get_fresh_index () in
      let typei_str = "type" ^ string_of_int i in
      let res =
        make_sanitized ~original:typei_str ~sanitized:typei_str ~accessors:[]
      in
      output_verbose ~level:1
      @@ sprintf "Assigning name %s\n" (show_sanitized_name res) ;
      add_name t res
    | None, Some preferred_name ->
      output_verbose ~level:2
      @@ asprintf "Couldn't find name for type %a\n but found preferred name %s"
           pp_type_ t
           (show_sanitized_name preferred_name) ;
      add_name t preferred_name
    | Some name, _ ->
      output_verbose ~level:1
      @@ sprintf "Found name %s\n%!" (show_sanitized_name name) ;
      name

  and register t name =
    output_verbose ~level:2
    @@ asprintf "Registering name %s for type %a%!\n" (show_sanitized_name name)
         pp_type_ t ;
    Hashtbl.add !registered_names (show_sanitized_name name) t ;
    Hashtbl.add !type_names t name

  and add_preferred_name (t : type_) name =
    output_verbose ~level:1
    @@ asprintf "Registering preferred name %s%!\n" (show_sanitized_name name) ;
    Hashtbl.add !preferred_type_names t name

  and add_name (t : type_) (name : sanitized_name) =
    let id = get_fresh_id () in
    output_verbose ~id ~level:2
    @@ asprintf "Attempting to add name %s for type %a%!\n"
         (show_sanitized_name name) pp_type_ t ;
    (* If t already has a name, we use that name and drop the suggested
       name *)
    if has_name t then (
      let res = get_name_unsafe t in
      if not (eq_sanitized res name) then
        output_verbose ~id ~level:1
        @@ asprintf "Type already had a name: %s, dropping %s\n%!"
             (show_sanitized_name res) (show_sanitized_name res)
      else
        output_verbose ~id ~level:1
        @@ asprintf "Type already registered under the same name.\n%!" ;
      res
    ) else
      let sanitized_name = bind_sanitize_annot name in
      (* let sanitized_name = show_sanitized_name name in *)
      let new_sanitized_name =
        (* If the name is already registered then we append a fresh_index
         *)
        if Hashtbl.mem !registered_names (show_sanitized_name sanitized_name)
        then (
          let res =
            add_accessor sanitized_name (string_of_int (get_fresh_index ()))
          in
          output_verbose ~id ~level:1
          @@ sprintf "Name %s already taken, choosing %s instead.\n%!"
               (show_sanitized_name sanitized_name)
               (show_sanitized_name res) ;
          res
        ) else (
          output_verbose ~id ~level:1
          @@ sprintf "Confirming name %s.\n%!"
               (show_sanitized_name sanitized_name) ;
          sanitized_name
        ) in
      register t new_sanitized_name ;
      new_sanitized_name

  let exists_name name = Hashtbl.mem !registered_names name

  let get_type_opt name = Hashtbl.find_opt !registered_names name

  let get_type_unsafe name = Hashtbl.find !registered_names name

  (** Encoding of marshalled types  *)
  let typenames_encoding =
    Json_encoding.(
      list (obj2 (req "name" sanitized_name_enc) (req "type" type__enc)))

  let edges_encoding =
    Json_encoding.(
      list (obj2 (req "from" sanitized_name_enc) (req "to" sanitized_name_enc)))

  let subst varOld varNew t =
    let rec aux = function
      | Pair l -> Pair (List.map aux l)
      | Record p -> Record (map_paired_type ~f:(fun s x -> LeafP (s, aux x)) p)
      | TVar (n, t) ->
        if eq_sanitized n varOld then
          TVar (varNew, aux t)
        else
          TVar (n, aux t)
      | Annot (t, n) -> Annot (aux t, n)
      | Or o -> Or (mapOr_pure ~f:aux o)
      | Base b -> Base b
      | Unary (u, t) -> Unary (u, aux t)
      | Binary (b, (t1, t2)) -> Binary (b, (aux t1, aux t2))
      | Contract _ as c -> c in
    aux t
end

open Naming

let is_one_field_record ep =
  let rec aux = function
    | Base _ -> true
    | Record (LeafP _) -> true
    | Record (PairP l) -> List.length l = 1
    | TVar _ -> true
    | Annot (ep, _) -> aux ep (* TODO: check again *)
    | Pair _ -> false
    | Unary _ -> true
    | Or _ -> true
    | Binary _ -> true
    | Contract _ -> true in
  aux ep

type name = string [@@deriving show, encoding]

type interface_instruction =
  | Define_type of sanitized_name * type_
  | Seq of interface_instruction list
  | Deploy
  | Entrypoint of sanitized_name * type_
[@@deriving show, encoding { recursive }]

type interface = interface_instruction list [@@deriving show]

type crawlori_type =
  | CVarchar
  | CZarith
  | CInteger
  | CJson
  | CTimestamp
  | CBoolean
  | CBigint
  | CBytea
  | CArray of crawlori_type
[@@deriving show, encoding { recursive }]

type crawlori_index =
  | No_order
  | Asc
  | Desc
[@@deriving show, encoding]

type crawlori_source =
  | Operation
  | Mic_parameter
  | Fun_parameter
[@@deriving show, encoding]

type crawlori_column = {
  crawlori_column_name : string;
  crawlori_column_bm_name : string option;
  crawlori_column_dipdup_bm_name : string option;
  crawlori_column_type : crawlori_type;
  crawlori_column_option : bool;
  crawlori_column_index : crawlori_index option;
  crawlori_column_source : crawlori_source;
}
[@@deriving show, encoding]

type crawlori_table = {
  crawlori_table_name : string;
  crawlori_table_is_bm : bool;
  crawlori_table_entrypoint_name : string option;
  crawlori_table_fields : crawlori_column list;
  crawlori_table_type : type_ option;
}
[@@deriving show, encoding]

type crawlori_tables = crawlori_table list [@@deriving show]

(** Equality on interface instructions *)
let rec eq_interface_instruction ~eq_types n1 n2 =
  match (n1, n2) with
  | Define_type (n1, t1), Define_type (n2, t2) ->
    eq_sanitized n1 n2 && eq_types t1 t2
  | Seq s, Seq s' -> List.for_all2 (eq_interface_instruction ~eq_types) s s'
  | Deploy, Deploy -> true
  | Entrypoint (n1, t1_), Entrypoint (n2, t2_) -> n1 = n2 && eq_types t1_ t2_
  | _, _ -> false

(** A weak notion of equality on interface instructions  *)
let eq_interface_instruction_light n1 n2 =
  match (n1, n2) with
  | Define_type (n1, _t1), Define_type (n2, _t2) -> eq_sanitized n1 n2
  | Seq s, Seq s' -> List.for_all2 (eq_interface_instruction ~eq_types) s s'
  | Deploy, Deploy -> true
  | Entrypoint (n1, _), Entrypoint (n2, _) -> n1 = n2
  | _, _ -> false

(** Dependency edges (e.g. type definition T2 depends on type
   definition T1) *)
let edges : (interface_instruction * interface_instruction, unit) Hashtbl.t ref
    =
  ref (Hashtbl.create 100)

let init_edges () =
  let h3 = Hashtbl.create 100 in
  edges := h3

let register_edge nfrom nto = Hashtbl.add !edges (nfrom, nto) ()

(** Expand type_ tfrom using registered aliases (i.e. replacing type0,
   type1, etc... in the AST *)
let rec expand tfrom =
  match tfrom with
  | Pair l -> Pair (List.map expand l)
  | Record p -> Record (map_paired_type ~f:(fun s x -> LeafP (s, expand x)) p)
  | TVar (_n, t) ->
    expand t
    (* (match get_type_opt n with
       * | Some t -> expand t
       * | None -> TVar n) *)
  | Annot (t, n) -> Annot (expand t, n)
  | Or o -> Or (mapOr_pure ~f:expand o)
  | Base b -> Base b
  | Unary (u, t) -> Unary (u, expand t)
  | Binary (b, (t1, t2)) -> Binary (b, (expand t1, expand t2))
  | Contract _ as c -> c

(** Do the inverse of expand: replace any subtypes which are
   registered under a name <typename> with their alias Tvar
   <typename>, excluding <exclude> *)
let unexpand ~exclude tfrom =
  let rec unexpand tfrom =
    match Naming.get_name_opt tfrom with
    | Some n when n <> exclude -> TVar (n, tfrom)
    | _ -> (
      match tfrom with
      | Pair l -> Pair (List.map unexpand l)
      | Record p ->
        Record (map_paired_type ~f:(fun s x -> LeafP (s, unexpand x)) p)
      | TVar n -> TVar n
      | Annot (t, n) -> Annot (unexpand t, n)
      | Or o -> Or (mapOr_pure ~f:unexpand o)
      | Base b -> Base b
      | Unary (u, t) -> Unary (u, unexpand t)
      | Binary (b, (t1, t2)) -> Binary (b, (unexpand t1, unexpand t2))
      | Contract _ as c -> c) in
  unexpand tfrom

(** Determines whether tfrom expands to tto *)
let expands_to tfrom tto = expand tfrom = tto

let rec define_type (name : sanitized_name) type_ =
  output_verbose ~level:2
  @@ asprintf "[define_type] called with name %s and type %a\n%!"
       (show_sanitized_name name) pp_type_ type_ ;
  match type_ with
  | TVar (varname, t) when eq_sanitized varname name -> define_type varname t
  | Annot (TVar (varname, _), _) when varname = name ->
    [] (* we don't want type t = t *)
  | Annot (t, n) ->
    output_verbose ~level:1
    @@ asprintf "[define_type] Ignoring annotation %s\n%!"
         (show_sanitized_name n) ;
    let final_type_of_definition = (* unexpand ~exclude:name *) t in
    output_verbose ~level:1
    @@ asprintf "[define_type] final_type_of_definition = %a\n%!" pp_type_
         final_type_of_definition ;
    define_type name final_type_of_definition
  | t -> [Define_type (name, (* unexpand ~exclude:name *) t)]

(** A light printer for interface instructions  *)
let rec pp_interface_instruction_light ppf ii =
  match ii with
  | Define_type (s, _) ->
    fprintf ppf "Define_type(%s,[...])\n" (show_sanitized_name s)
  | Deploy -> pp_interface_instruction ppf Deploy
  | Entrypoint _ as t -> pp_interface_instruction ppf t
  | Seq iis ->
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ";")
       (fun ppf x -> pp_interface_instruction_light ppf x))
      ppf iis

(** Prints the interface without all the information (i.e. only the
   type names in definitions), so as not to clutter the output as
   opposed to print_interface *)
let pp_interface_light ppf iis = pp_interface_instruction_light ppf (Seq iis)

(** Check if some name has been defined twice with different
   types. Unfortunately this can happen illegitimately because of
   alpha-renaming *)
let check_interface_for_doubles (iis : interface) =
  let rec aux defs = function
    | [] -> defs
    | Define_type (name, type_) :: xs -> (
      match List.assoc_opt name defs with
      | None -> aux ((name, type_) :: defs) xs
      | Some t ->
        if eq_types type_ t then
          aux ((name, type_) :: defs) xs
        else (
          eprintf
            "Error in check_interface_for_doubles: name %s is defined (at \
             least) twice, for types\n\
             %a and\n\
             %a"
            (show_sanitized_name name) pp_type_ type_ pp_type_ t ;
          failwith "Double in interface list"
        ))
    | Seq iis :: xs ->
      let defs = aux defs iis in
      aux defs xs
    | _ :: xs -> aux defs xs in
  aux [] iis

(** Check any list for duplicates  *)
let check_for_doubles (l : 'a list) (get_name : 'a -> string) =
  let rec aux res = function
    | [] -> List.rev res
    | x :: xs ->
      if List.mem (get_name x) (List.map get_name res) then (
        eprintf "[check_for_doubles] : %s is present twice\n%!" (get_name x) ;
        failwith "Double"
      ) else
        aux (x :: res) xs in
  aux [] l

module Marshal_names = struct
  (** Marshal all attributed names except for storage, so that the
     user can rename them if they wish. *)
  let marshal_names () =
    let list_of_names = List.of_seq @@ Hashtbl.to_seq !type_names in
    let list_of_names =
      check_for_doubles list_of_names (fun (_, n) -> show_sanitized_name n)
    in
    serialize_value ~enc:typenames_encoding
    (* for easier reading of the json file, invert name and
       description *)
    @@ List.filter (fun (x, _) -> get_sanitized x <> "storage")
    @@ List.map (fun (x, y) -> (y, x))
    @@ list_of_names

  let marshal_edges namelist () =
    serialize_value ~enc:edges_encoding
    @@ List.filter_map (function
         | (Define_type (nfrom, _t1), Define_type (nto, _t2)), () ->
           if List.mem nfrom namelist && List.mem nto namelist then
             Some (nfrom, nto)
           else
             None
         | _, _ -> None)
    @@ List.of_seq @@ Hashtbl.to_seq !edges

  let unmarshal_names filename =
    let existing_names = deserialize_file ~filename ~enc:typenames_encoding in
    let _ =
      List.iter
        (fun (name, type_) ->
          output_verbose ~level:1
          @@ asprintf "Unmarshaling existing name: %s\n"
               (show_sanitized_name name)
          (* pp_type_ type_ *) ;
          ignore @@ add_preferred_name type_ name
          (* (define_type name type_)@nodes *))
        existing_names in
    []
end

let pp_base_type ?(language = Factori_config.OCaml) ?(abstract = false)
    ?(prefix = None) ppf =
  let prefix =
    match prefix with
    | Some prefix -> sprintf "%s." prefix
    | None -> "" in
  function
  | Never -> fprintf ppf "%snever_type" prefix
  | Chain_id -> (
    match language with
    | Factori_config.Csharp -> fprintf ppf "%sChainId" prefix
    | _ -> fprintf ppf "%schain_id" prefix)
  | String ->
    fprintf ppf "%s%s" prefix
      (if abstract then
        "string'"
      else
        "string")
  | Nat -> (
    match language with
    | Csharp -> fprintf ppf "%sNat" prefix
    | _ -> fprintf ppf "%snat" prefix)
  | Int -> begin
    match language with
    | Factori_config.Typescript -> fprintf ppf "%sint" prefix
    | Factori_config.OCaml | Factori_config.Python ->
      fprintf ppf "%sint_michelson" prefix
    | Factori_config.Csharp -> fprintf ppf "%sInt" prefix
  end
  | Byte -> (
    match language with
    | Factori_config.Csharp -> fprintf ppf "%sBytes" prefix
    | Factori_config.Python -> fprintf ppf "bytes"
    | _ -> fprintf ppf "%sbytes" prefix)
  | Address -> (
    match language with
    | Factori_config.Csharp -> fprintf ppf "%sAddress" prefix
    | _ -> fprintf ppf "%saddress" prefix)
  | Signature -> (
    match language with
    | Csharp -> fprintf ppf "%sSignature" prefix
    | _ -> fprintf ppf "%ssignature" prefix)
  | Unit -> (
    match language with
    | Csharp -> fprintf ppf "%sUnit" prefix
    | _ ->
      fprintf ppf "%s%s" prefix
        (if abstract then
          "unit'"
        else
          "unit"))
  | Bool -> (
    match language with
    | Factori_config.Python -> fprintf ppf "bool"
    | Factori_config.Csharp -> fprintf ppf "%sBool" prefix
    | _ ->
      fprintf ppf "%s%s" prefix
        (if abstract then
          "bool'"
        else
          "bool"))
  | Keyhash -> (
    match language with
    | Factori_config.Csharp -> fprintf ppf "%sKeyHash" prefix
    | Factori_config.Typescript -> fprintf ppf "%skey_hash" prefix
    | Factori_config.Python -> fprintf ppf "%skey_hash" prefix
    | _ -> fprintf ppf "key_hash")
  | b -> begin
    let typename =
      match b with
      | Timestamp -> "timestamp"
      | Mutez -> "tez"
      | Key -> "key"
      | Operation -> "operation"
      | Sapling_state -> "sapling_state"
      | Sapling_transaction_deprecated -> "sapling_transaction_deprecated"
      | b ->
        failwith
          (sprintf
             "[pp_base_type] %s is supposed to already have been taken care of \
              above"
             (str_of_base b)) in
    fprintf ppf "%s%s" prefix
      (match language with
      | Csharp -> String.capitalize_ascii typename
      | _ -> typename)
  end

let rec is_record_leaf t_ =
  match t_ with
  | Pair _ -> false
  | Annot (ep, _name) -> is_record_leaf ep
  | _ -> true

(* let get_projections ppf ep =
 *   let rec aux ppf = function *)

(* type record = {recordname : string;
 *                fields : (string * type_) list} *)

let rec ep_or_of_mich ?(annots = []) a1 a2 =
  let res =
    match (type__of_micheline a1, type__of_micheline a2) with
    | Or x, Or y -> Or (OrAbs (x, y))
    | Or x, t -> Or (OrAbs (x, LeafOr t))
    | t, Or y -> Or (OrAbs (LeafOr t, y))
    | x, y -> Or (OrAbs (LeafOr x, LeafOr y)) in
  match annots with
  | annot :: _ -> Annot (res, sanitize_annot annot)
  | _ -> res

and ep_list_of_mich ~annots m =
  let t = Unary (List, type__of_micheline m) in
  match annots with
  | annot :: _ -> Annot (t, sanitized_of_str annot)
  | _ -> t

and ep_option_of_mich ~annots m =
  let t = Unary (Option, type__of_micheline m) in
  match annots with
  | annot :: _ -> Annot (t, sanitized_of_str annot)
  | _ -> t

(* Says whether a pair constituted from the arguments al can be
   changed into a record, e.g. if all its fields have a name *)
and is_pair_record (al : type_ list) =
  let id = get_fresh_id () in
  output_verbose ~id ~level:2
  @@ asprintf "trying to evaluate whether %a is a record...\n%!" pp_type_
       (Pair al) ;
  let res =
    List.for_all
      (function
        | Pair l -> is_pair_record l
        | Record _ -> true
        | Annot (_, _) -> true
        | _t ->
          (* eprintf "%s is clearly not a record: %a\n\n\n" (Option.value ~default:"<no annot name>" name) pp_type_ t; *)
          false)
      al in
  output_verbose ~id ~level:2
  @@ asprintf "... result: %b" (* pp_type_ (Pair al) *) res ;
  res

and build_record ?(name = None) al : type_ paired_type list =
  let id = get_fresh_id () in
  output_verbose ~id ~level:1 @@ sprintf "[build_record] Entering" ;
  output_verbose ~id ~level:2
  @@ asprintf "[build_record] Entering with input %s"
       (EzEncoding.construct (Json_encoding.list type__enc) al) ;
  (* let prefix = match name with
   *   | Some name -> name
   *   | None -> "" in *)
  let res =
    match al with
    | [] -> []
    | Annot (Record r, nr) :: al ->
      LeafP (bind_sanitize_annot nr, Record r) :: build_record al
    | Annot ((Pair pl as ep), nm) :: al -> (
      if not (is_pair_record pl) then
        LeafP (bind_sanitize_annot nm, Pair pl) :: build_record al
      else
        match
          build_record ~name:(Some (* Format.sprintf "%s%s" prefix  *) nm) [ep]
        with
        | [] -> raise (GenericError ("build_record", "empty list"))
        | [PairP l] ->
          LeafP (bind_sanitize_annot nm, Record (PairP l)) :: build_record al
        | [x] -> x :: build_record al
        | l -> PairP l :: build_record al)
    | Annot (t, nr) :: al ->
      LeafP (bind_sanitize_annot nr, t) :: build_record al
    | Pair al :: als -> PairP (build_record al) :: build_record als
    | Record r :: als -> r :: build_record als
    | [t] -> (
      match name with
      | Some name -> [LeafP (bind_sanitize_annot name, t)]
      | None ->
        eprintf "[single element list]in build record there should not be %a"
          pp_type_ (Pair [t]) ;
        failwith "building record on wrong list")
    | t ->
      output_verbose ~id ~level:2
      @@ asprintf "[build_record] Reaching default case with type t = %a"
           (pp_print_list ~pp_sep:(tag ";\n") pp_type_)
           t ;
      [PairP (List.map (fun x -> LeafP (sanitized_of_str "unnamed_leaf", x)) t)]
    (* eprintf "[other] in build record there should not be %a" pp_type_ (Pair t) ;
     * failwith "building record on wrong list" *) in
  output_verbose ~id ~level:1 @@ sprintf "[build_record] Leaving" ;
  output_verbose ~id ~level:2
  @@ asprintf "[build_record] Leaving with result %s"
       (EzEncoding.construct
          (Json_encoding.list (paired_type_enc type__enc))
          res) ;
  res

(* Avoid records with duplicate field names *)
and make_names_unique r =
  let get_fresh_ident counter () =
    let temp = !counter in
    counter := !counter + 1 ;
    temp in
  (* Get fresh identifiers until one is free *)
  let get_fresh_until p =
    let counter = ref 0 in
    let rec aux () =
      let ident = get_fresh_ident counter () in
      if p ident then
        ident
      else
        aux () in
    aux () in
  let make_name name d =
    make_sanitized ~original:name.original ~sanitized:name.sanitized
      ~accessors:[string_of_int d] in
  fst
  @@ fold_and_replace_paired_type
       ~f:(fun name t names ->
         if List.mem name names then
           let ident =
             get_fresh_until (fun d -> not (List.mem (make_name name d) names))
           in
           let name = make_name name ident in
           (LeafP (name, t), name :: names)
         else
           (LeafP (name, t), name :: names))
       ~init:[] r

and ep_pair_or_record_of_mich ~records_allowed ?(annots = []) al =
  let id = get_fresh_id () in
  output_verbose ~id ~level:1 @@ sprintf "[ep_pair_or_record_of_mich] Entering" ;
  output_verbose ~id ~level:2
  @@ sprintf
       "[ep_pair_or_record_of_mich] Entering with input %s\nand with annots %s"
       (EzEncoding.construct (Json_encoding.list micheline__enc) al)
       (EzEncoding.construct (Json_encoding.list Json_encoding.string) annots) ;
  (* name only for debugging *)
  let name =
    match annots with
    | annot :: _ -> Some (sanitized_of_str annot)
    | _ -> None (* "<no name found for this record candidate>" *) in
  let al = List.map (type__of_micheline ~records_allowed:true) al in
  let res =
    if is_pair_record al && records_allowed then
      Record (make_names_unique @@ PairP (build_record ~name al))
    else
      Pair al in
  let res =
    match annots with
    | annot :: _ -> Annot (res, sanitized_of_str annot)
    | _ ->
      output_verbose ~id ~level:1
      @@ sprintf
           "Since there was no annot on input, we return a record without \
            annot." ;
      res in
  output_verbose ~id ~level:1 @@ sprintf "[ep_pair_or_record_of_mich] Leaving" ;
  output_verbose ~id ~level:2
  @@ sprintf "[ep_pair_or_record_of_mich] Leaving with result %s"
       (EzEncoding.construct type__enc res) ;
  res

and ep_base_of_mich ?(annots = []) b () =
  let res = Base b in
  match annots with
  | annot :: _ -> Annot (res, sanitize_annot annot)
  | _ -> res

and ep_contract_of_mich ?(annots = []) m =
  (* let ctype = type__of_micheline ~records_allowed:false m in *)
  match annots with
  | annot :: _ -> Annot (Contract m, sanitized_of_str annot)
  | _ -> Contract m

and ep_bigmap_of_mich ?(annots = []) m1 m2 =
  let ktype = type__of_micheline ~records_allowed:true m1 in
  let vtype = type__of_micheline ~records_allowed:true m2 in
  let ctype = big_map (ktype, vtype) in
  match annots with
  | annot :: _ -> Annot (ctype, sanitized_of_str annot)
  | _ -> ctype

and ep_map_of_mich ?(annots = []) m1 m2 =
  let ktype = type__of_micheline ~records_allowed:true m1 in
  let vtype = type__of_micheline ~records_allowed:true m2 in
  let ctype = map (ktype, vtype) in
  match annots with
  | annot :: _ -> Annot (ctype, sanitized_of_str annot)
  | _ -> ctype

and ep_set_of_mich ?(annots = []) m1 =
  let vtype = type__of_micheline ~records_allowed:true m1 in
  match annots with
  | annot :: _ -> Annot (set vtype, sanitized_of_str annot)
  | _ -> set vtype

and ep_ticket_of_mich ?(annots = []) m1 =
  let vtype = type__of_micheline ~records_allowed:true m1 in
  match annots with
  | annot :: _ -> Annot (ticket vtype, sanitized_of_str annot)
  | _ -> ticket vtype

and ep_lambda_of_mich ?(annots = []) m1 m2 =
  let argtype = type__of_micheline ~records_allowed:false m1 in
  let outtype = type__of_micheline ~records_allowed:false m2 in
  let ctype = lambda (argtype, outtype) in
  match annots with
  | annot :: _ -> Annot (ctype, sanitized_of_str annot)
  | _ -> ctype

and type__of_micheline ?(msg = "no message given") ?(records_allowed = true)
    (m : micheline) =
  let id = get_fresh_id () in
  output_verbose ~id ~level:2
  @@ sprintf "[type__of_micheline][msg:%s] Entering with\n%s" msg
       (EzEncoding.construct micheline__enc m) ;
  let res =
    match m with
    | Mprim { prim; args; annots } as m -> (
      (* Format.eprintf "  Type__Of_Micheline: %s@." (EzEncoding.construct (micheline_enc.json) m); *)
      match (prim, args) with
      | `pair, al -> ep_pair_or_record_of_mich ~records_allowed al ~annots
      | `or_, [a1; a2] -> ep_or_of_mich a1 a2 ~annots
      | `nat, [] -> ep_base_of_mich Nat ~annots ()
      | `int, [] -> ep_base_of_mich Int ~annots ()
      | `bool, [] -> ep_base_of_mich Bool ~annots ()
      | `unit, [] -> ep_base_of_mich Unit ~annots ()
      | `address, [] -> ep_base_of_mich Address ~annots ()
      | `string, [] -> ep_base_of_mich String ~annots ()
      | `bytes, [] -> ep_base_of_mich Byte ~annots ()
      | `signature, [] -> ep_base_of_mich Signature ~annots ()
      | `list, [m] -> ep_list_of_mich ~annots m
      | `contract, [c] -> ep_contract_of_mich c ~annots
      | `storage, [s] -> type__of_micheline s
      | `big_map, [k; v] -> ep_bigmap_of_mich ~annots k v
      | `map, [k; v] -> ep_map_of_mich ~annots k v
      | `option, [k] -> ep_option_of_mich ~annots k
      | `timestamp, [] -> ep_base_of_mich Timestamp ~annots ()
      | `key_hash, [] -> ep_base_of_mich Keyhash ~annots ()
      | `mutez, [] -> ep_base_of_mich Mutez ~annots ()
      | `set, [v] -> ep_set_of_mich ~annots v
      | `lambda, [arg; out] -> ep_lambda_of_mich ~annots arg out
      | `key, [] -> ep_base_of_mich ~annots Key ()
      | `operation, [] -> ep_base_of_mich ~annots Operation ()
      | `never, [] -> ep_base_of_mich ~annots Never ()
      | `chain_id, [] -> ep_base_of_mich Chain_id ~annots ()
      | `sapling_state, [_] -> ep_base_of_mich Sapling_state ~annots ()
      | `sapling_transaction, [_] ->
        ep_base_of_mich Sapling_transaction_deprecated ~annots ()
      | `ticket, [t] -> ep_ticket_of_mich ~annots t
      | p, args ->
        failwith
          (Format.sprintf "not handled: %s (args has size %d), args is %s"
             (EzEncoding.construct primitive_or_macro_enc.json p)
             (List.length args)
             (EzEncoding.construct micheline_enc.json m)))
    | m -> raise (NotAType m) in
  output_verbose ~id ~level:2
  @@ sprintf "[type__of_micheline][msg:%s] Leaving with result: \n%s" msg
       (EzEncoding.construct type__enc res) ;
  res

exception Wrong_value_for_type of micheline option * type_ option * string

module ToolsForExtractingPairs = struct
  (* let print_list ppf l =
   *   let open Format in
   *   fprintf ppf "%s" (EzEncoding.construct (Json_encoding.list micheline_enc.json) l) *)

  let print_extract_value_error mich t_ s =
    output_verbose ~level:2
    @@ sprintf
         "[Wrong_value_for_type]%s:\n\
         \                   %s\n\
         \                   %s\n\
         \                   %!" s
         (match mich with
         | None -> "<mich : None>"
         | Some mich -> EzEncoding.construct micheline_enc.json mich)
         (match t_ with
         | None -> "<t_ : None>"
         | Some t_ -> EzEncoding.construct type__enc t_)

  let _ =
    add_exception_handler (function
      | Wrong_value_for_type (m, t, s) -> print_extract_value_error m t s
      | e -> raise e)

  let print_configuration n l =
    let open Format in
    output_verbose ~level:2
    @@ asprintf "configuration for %d: %a\n(%a)\n%!\n" n
         (pp_print_list
            ~pp_sep:(fun ppf _ -> fprintf ppf " ")
            (fun ppf x -> fprintf ppf "%d" (List.length x)))
         l
         (pp_print_list
            ~pp_sep:(fun ppf _ -> fprintf ppf " ")
            (fun ppf x ->
              fprintf ppf "%s"
                (EzEncoding.construct (Json_encoding.list micheline_enc.json) x)))
         l

  let print_list ppf l =
    let open Format in
    fprintf ppf "%s"
      (EzEncoding.construct (Json_encoding.list micheline_enc.json) l)

  exception Tried_all_partitions_and_failed of int * micheline list * string

  let print_tried_all_partitions n l ppf msg =
    fprintf ppf "n = %d\n%s\nerror message: %s\n" n
      (EzEncoding.construct (Json_encoding.list micheline_enc.json) l)
      msg

  let _ =
    add_exception_handler (function
      | Tried_all_partitions_and_failed (i, ml, s) ->
        Format.eprintf "%a" (print_tried_all_partitions i ml) s
      | e -> raise e)

  (* n >=2  *)
  let try_all_partitions_multi ?(msg = "") (* ?(debug = false) *) n
      (l : micheline list) (f : 'a list list -> 'b) =
    let open Format in
    output_verbose ~level:2
    @@ asprintf
         "[try_all_partitions_multi]\n%s\nEntering with n=%d and input %a!\n"
         msg n print_list l ;
    let rec findkposts k (l : 'a list) =
      if List.length l = k + 1 then
        [List.map (fun x -> [x]) l]
      else
        match l with
        | [] -> failwith "empty list, shouldn't happen" (* or [] ? *)
        | [x] ->
          if k = 0 then
            [[[x]]]
          else
            []
        | x1 :: xs ->
          List.map (fun y -> [x1] :: y) (findkposts (k - 1) xs)
          @ List.map
              (function
                | (y1 :: ys : 'a list list) -> (x1 :: y1) :: ys
                | [] -> [[x1]])
              (findkposts k xs) in
    let rec aux = function
      | [] ->
        output_verbose ~level:1
        @@ sprintf "No winning configuration for n=%d%!\n" n ;
        raise (Tried_all_partitions_and_failed (n, l, msg))
      | l :: ls -> (
        output_verbose ~level:1 @@ sprintf "Trying configuration: " ;
        (print_configuration n) l ;
        try
          let res = f l in
          output_verbose ~level:2
          @@ sprintf "Winning configuration for %d:\n%!" n ;
          print_configuration n l ;
          res
        with
        | Wrong_value_for_type (mich, t_, msg) ->
          print_extract_value_error mich t_ msg ;
          aux ls
        | e -> raise e)
      (* | _ -> if debug then eprintf "Unexpected error"; aux ls *) in
    aux @@ findkposts (n - 1) l
end

(* open ToolsForExtractingPairs *)

let make_pair l = Mprim { prim = `Pair; args = l; annots = [] }

let flatten m =
  (*   output_debug @@
   *     Format.sprintf "[flatten] Entering with argument %s
   * %!" (EzEncoding.construct micheline_enc.json m); *)
  let rec flatten_aux = function
    | Mprim { prim = `Pair; args = l; _ } ->
      List.concat (List.map flatten_aux l)
    | x -> [x] in
  let res = flatten_aux m in
  (*   output_debug @@
   *     Format.sprintf "[flatten] Exiting with output %s
   * %!" (EzEncoding.construct (Json_encoding.list micheline_enc.json) res); *)
  res

let rec value_of_typed_micheline (mich : micheline) (t : type_) =
  let id = get_fresh_id () in
  output_verbose ~id ~level:2
  @@ asprintf "[value_of_typed_micheline] Entering with %s\nand %a"
       (EzEncoding.construct micheline__enc mich)
       pp_type_ t ;
  let record_or_field_of_paired_type = function
    | PairP _ as p -> (Record p, None)
    | LeafP (n, t) -> (t, Some (t, n)) in
  let merge_pair_types r1 r2 =
    match (r1, r2) with
    | PairP p1, PairP p2 -> PairP (p1 @ p2)
    | (LeafP (_n, _) as l), PairP p2 -> PairP (l :: p2)
    | PairP p1, (LeafP _ as l) -> PairP (p1 @ [l])
    | (LeafP _ as l1), (LeafP _ as l2) -> PairP [l1; l2] in
  let merge_records r1 n1 r2 n2 =
    match (n1, n2, r1, r2) with
    | None, None, VRecord r1, VRecord r2 -> VRecord (merge_pair_types r1 r2)
    | Some (t1, n), None, x, VRecord r2 ->
      VRecord (merge_pair_types (LeafP (n, (t1, x))) r2)
    | None, Some (t2, n), VRecord r1, x ->
      VRecord (merge_pair_types r1 (LeafP (n, (t2, x))))
    | Some (t1, n1), Some (t2, n2), x, y ->
      VRecord (merge_pair_types (LeafP (n1, (t1, x))) (LeafP (n2, (t2, y))))
    | _, _, _, _ -> failwith "could not merge records" in
  match (mich, t) with
  | m, Annot (t_, _) -> value_of_typed_micheline m t_
  | m, TVar (_n, t_) -> value_of_typed_micheline m t_
  | Mprim { prim = `None; args = []; _ }, Unary (Option, t) -> VOption (t, None)
  | Mprim { prim = `Some; args = [x]; _ }, Unary (Option, t) ->
    VOption (t, Some (value_of_typed_micheline x t))
  | Mprim { prim = `Unit; _ }, Base Unit -> VUnit ()
  | Mint x, Base Int -> VInt x
  | Mint x, Base Nat -> VInt x
  | Mint x, Base Mutez -> VInt x
  | Mstring s, Base Keyhash -> VString s
  | Mstring s, Base Key -> VString s
  | Mstring s, Base Signature -> VSignature s
  | Mstring s, Base Operation -> VOperation s
  (* | Mstring s, Base Byte -> VBytes ((Crypto.H.mk s)) *)
  | Mbytes s, Base Byte -> VBytes s
  | Mstring s, Base Chain_id -> VChain_id s
  (*   | Mprim {
   * prim = `LAMBDA;
   * _}, Lambda _ -> VString "TODO: Lambda" *)
  | (_ as l), Binary (Lambda, _) ->
    (* FIXME/TODO: put the actual type. One way to do this would be
       to help/decorate the call to lambda_decode in the generation
       from infer_entrypoints_ocaml *)
    output_verbose ~level:1
    @@ Format.sprintf
         "Warning: Lambda Decoding support is incomplete, for now input and \
          output types will be Unit\n\
         \                     %!" ;
    VLambda (Micheline l)
  | Mseq l, Unary (Set, t) ->
    VSet (t, List.map (fun x -> value_of_typed_micheline x t) l)
  | Mbytes b, Unary (Ticket, _) -> VTicket (Crypto.coerce (Crypto.hex_to_raw b))
  | Mint x, Base Sapling_state -> VSaplingState (Z.to_string x)
  | Mseq l, Unary (List, t) ->
    VList (t, List.map (fun x -> value_of_typed_micheline x t) l)
  | Mseq l, (Binary (Map, (t1, t2)) as mapt) ->
    let aux = function
      | Mprim { prim = `Elt; args = [x; y]; annots = [] } ->
        (value_of_typed_micheline x t1, value_of_typed_micheline y t2)
      | _ ->
        raise
          (Wrong_value_for_type
             (Some (Mseq l), Some mapt, "ill-typed element of map")) in
    VMap (t1, t2, List.map aux l)
  | Mstring s, Base Timestamp -> VTimestamp s
  | Mstring s, Base String -> VString s
  | Mprim { prim = `True; _ }, Base Bool -> VBool true
  | Mprim { prim = `False; _ }, Base Bool -> VBool false
  | Mstring addr, Base Address -> VAddress addr
  | Mint i, Binary (BigMap, (t1, t2)) -> VBigMap (t1, t2, i)
  | Mseq _, Binary (BigMap, (t1, t2)) ->
    VBigMap (t1, t2, Z.neg Z.one)
    (* TODO: maybe
       preserve literal
       big_maps here? *)
  | m, Or t ->
    let constructor, m, t = derive_constructor m t in
    let v = value_of_typed_micheline m t in
    VSumtype (show_sanitized_name constructor, t, v)
  | (Mprim { prim = `Left; _ } as m), t ->
    raise (Wrong_value_for_type (Some m, Some t, "Left on a non-sum type"))
  | (Mprim { prim = `Right; _ } as m), t ->
    raise (Wrong_value_for_type (Some m, Some t, "Right on a non-sum type"))
  | Mseq [m1; m2], Pair [p1; p2]
  | Mprim { prim = `Pair; args = [m1; m2]; _ }, Pair [p1; p2] ->
    VTuple
      ( [p1; p2],
        [value_of_typed_micheline m1 p1; value_of_typed_micheline m2 p2] )
  | Mseq [m1; m2], Record (PairP [p1; p2])
  | Mprim { prim = `Pair; args = [m1; m2]; _ }, Record (PairP [p1; p2]) ->
    let p1, n1 = record_or_field_of_paired_type p1 in
    let p2, n2 = record_or_field_of_paired_type p2 in
    let v1 = value_of_typed_micheline m1 p1 in
    let v2 = value_of_typed_micheline m2 p2 in
    merge_records v1 n1 v2 n2
  | Mprim { prim = `Pair; args = [m1; m2]; _ }, Pair (p1 :: p) ->
    VTuple
      ( p1 :: p,
        [value_of_typed_micheline m1 p1; value_of_typed_micheline m2 (Pair p)]
      )
  | Mprim { prim = `Pair; args = [m1; m2]; _ }, Record (PairP (p1 :: p)) ->
    let p1, n1 = record_or_field_of_paired_type p1 in
    let v1 = value_of_typed_micheline m1 p1 in
    let v2 = value_of_typed_micheline m2 (Record (PairP p)) in
    merge_records v1 n1 v2 None
  | Mseq (m1 :: m), Pair [p1; p2]
  | Mprim { prim = `Pair; args = m1 :: m; _ }, Pair [p1; p2] ->
    VTuple
      ( [p1; p2],
        [value_of_typed_micheline m1 p1; value_of_typed_micheline (Mseq m) p2]
      )
  | Mseq (m1 :: m), Record (PairP [p1; p2])
  | Mprim { prim = `Pair; args = m1 :: m; _ }, Record (PairP [p1; p2]) ->
    let p1, n1 = record_or_field_of_paired_type p1 in
    let p2, n2 = record_or_field_of_paired_type p2 in
    let v1 = value_of_typed_micheline m1 p1 in
    let v2 = value_of_typed_micheline (Mseq m) p2 in
    merge_records v1 n1 v2 n2
  | Mseq (m1 :: m), Pair (p1 :: p)
  | Mprim { prim = `Pair; args = m1 :: m; _ }, Pair (p1 :: p) ->
    VTuple
      ( p1 :: p,
        [
          value_of_typed_micheline m1 p1;
          value_of_typed_micheline (Mseq m) (Pair p);
        ] )
  | Mseq (m1 :: m), Record (PairP (p1 :: p))
  | Mprim { prim = `Pair; args = m1 :: m; _ }, Record (PairP (p1 :: p)) ->
    let p1, n1 = record_or_field_of_paired_type p1 in
    let v1 = value_of_typed_micheline m1 p1 in
    let v2 = value_of_typed_micheline (Mseq m) (Record (PairP p)) in
    merge_records v1 n1 v2 None
  | _, _ ->
    (* VString (sprintf "TODO: {|%s|} <-> {|%s|}" (EzEncoding.construct type__enc t) (EzEncoding.construct micheline_enc.json mich)) *)
    raise (Wrong_value_for_type (Some mich, Some t, "not implemented yet"))

(* From here on the AST of our interface
 *)
module Graph = struct
  exception CycleFound of interface_instruction list

  let _ =
    add_exception_handler (function
      | CycleFound _ -> Format.eprintf "Cycle Found!"
      | e -> raise e)

  type 'a graph = ('a * 'a list) list

  let rec has_node ~eq g name =
    match g with
    | [] -> false
    | (x, _) :: xs -> eq name x || has_node ~eq xs name

  let rec get_node_unsafe ~print_node ~eq g x =
    match g with
    | [] ->
      raise
        (GenericError
           ("get_node_unsafe", asprintf "Couldn't find node %a" print_node x))
    | (y, _) :: ys ->
      if eq x y then
        y
      else
        get_node_unsafe ~print_node ~eq ys x

  let add_node ~print_node ~eq (g : 'a graph) x =
    if has_node ~eq g x then (
      let y = get_node_unsafe ~print_node ~eq g x in
      output_verbose ~level:1
      @@ asprintf
           "Not adding node %a to graph as some version of it is already \
            present (%a)"
           print_node x print_node y ;
      g
    ) else if List.assoc_opt x g = None then
      (x, []) :: g
    else
      g

  let add_nodes ~print_node ~eq g xs =
    List.fold_right (fun n g -> add_node ~print_node ~eq g n) xs g

  let add_edge ~print_node ~eq g x y =
    let g = add_node ~print_node ~eq g x in
    let g = add_node ~print_node ~eq g y in
    List.filter_map
      (fun (a, l) ->
        if a = x then
          if eq y x || List.mem y l then
            Some (a, l)
          else
            Some (a, y :: l)
        else
          Some (a, l))
      g

  let add_edges ~print_node ~eq g edges =
    List.fold_right (fun (x, y) g -> add_edge ~print_node ~eq g x y) edges g

  let find_node g xname =
    output_verbose ~level:2
    @@ asprintf "[find_node] graph = %a, xname = %s"
         (pp_print_list ~pp_sep:(tag ";") (fun ppf x ->
              match x with
              | Define_type (s, _), _ -> pp_sanitized_name ppf s
              | _ -> fprintf ppf "[...]"))
         g
         (show_sanitized_name xname) ;
    let rec aux = function
      | [] -> None
      | ((Define_type (s, _) as t), _) :: _ when eq_sanitized s xname -> Some t
      | _ :: xs -> aux xs in
    aux g

  (* type 'a eedge = 'a graph -> string -> 'a graph *)

  type 'a eedge =
    | EdgeFrom of sanitized_name
    | FullEdge of sanitized_name * sanitized_name

  let add_edge_by_name ~eq g (xname : sanitized_name) (yname : sanitized_name) =
    if xname = yname then
      g
    else (
      output_verbose ~level:1
      @@ sprintf "Adding edge %s -> %s%!\n"
           (show_sanitized_name xname)
           (show_sanitized_name yname) ;
      match (find_node g xname, find_node g yname) with
      | Some xnode, Some ynode ->
        add_edge ~print_node:pp_interface_instruction ~eq g xnode ynode
      | Some _xnode, None ->
        output_verbose ~level:1
        @@ sprintf
             "[add_edge_by_name] Warning: When adding the edge %s -> %s, %s is \
              not defined@."
             (show_sanitized_name xname)
             (show_sanitized_name yname)
             (show_sanitized_name yname) ;
        g
      | None, Some _ynode ->
        output_verbose ~level:1
        @@ sprintf
             "[add_edge_by_name] Warning: When adding the edge %s -> %s, %s is \
              not defined@."
             (show_sanitized_name xname)
             (show_sanitized_name yname)
             (show_sanitized_name xname) ;
        g
      | None, None ->
        output_verbose ~level:1
        @@ sprintf
             "[add_edge_by_name] Warning: Neither of these two nodes is \
              defined in the graph: \n\
              node1: %s\n\
              node2: %s\n"
             (show_sanitized_name xname)
             (show_sanitized_name yname) ;
        g
    )

  let eadd_edge_by_name xname edges =
    output_verbose ~level:1
    @@ Format.sprintf "Adding edge from %s to current type@."
         (show_sanitized_name xname) ;
    EdgeFrom xname :: edges
  (* (fun g yname -> add_edge_by_name g xname yname)::edges *)

  let dfs graph visited start_node =
    let rec explore path visited node =
      if List.mem node path then
        raise (CycleFound path)
      else if List.mem node visited then
        visited
      else
        let new_path = node :: path in
        let edges =
          match List.assoc_opt node graph with
          | None -> []
          | Some l -> l in
        let visited = List.fold_left (explore new_path) visited edges in
        node :: visited in
    explore [] visited start_node

  let toposort graph =
    List.fold_left (fun visited (node, _) -> dfs graph visited node) [] graph

  (* let micheline_of_script_expr ~name se =
   *   match se with
   *   | Bytes _ -> nothandled_se ~msg:"Bytes" ~name se
   *   | Micheline m -> m *)

  (* (\* type eedge = interface_instruction -> (interface_instruction * interface_instruction) *\)
   * let add_dependency var *)

  let apply_eedge (e : 'a eedge) cur_name =
    match e with
    | EdgeFrom n -> FullEdge (n, cur_name)
    | FullEdge _ as f -> f

  let apply_eedge_to_graph cur_name e g =
    match e with
    | EdgeFrom s -> add_edge_by_name g s cur_name
    | FullEdge (x, y) -> add_edge_by_name g x y

  (** Make all eedges point to the current name  *)
  let apply_eedges cur_name (eedges : 'a eedge list) : 'a graph -> 'a graph =
   (* fun g -> List.fold_right (fun f g -> f g cur_name) eedges g *)
   fun g ->
    List.fold_right
      (fun e g ->
        apply_eedge_to_graph ~eq:eq_interface_instruction_light cur_name e g)
      eedges g

  let eapply_eedges cur_name (eedges : 'a eedge list) : 'a eedge list =
    (* List.map (fun eedge -> (fun g _ -> eedge g cur_name)) eedges *)
    List.map (fun e -> apply_eedge e cur_name) eedges
end

open Graph

(** Take the type_ obtained from micheline and replace records and sum
   types with type variable names. *)
let extract_AST ?(preferred_name = None)
    ?(msg = "No entrypoint or type name given") t_ =
  let id = get_fresh_id () in
  output_verbose ~id ~level:2
  @@ asprintf
       "[extract_AST][msg:%s] Entering with input %a and preferred name \"%s\""
       msg pp_type_ t_
       (match preferred_name with
       | None -> "<no preferred name provided>"
       | Some preferred_name -> show_sanitized_name preferred_name) ;
  (* first_call tells us whether we are inside the first recursive
     call to aux. In that case we don't want to give our type a name,
     because extract_AST is only called on named types.

     ~preferred_name is what we would like to call the type called
     recursively if it doesn't already have a name *)
  let rec aux ~first_call ?(preferred_name = None) = function
    | Base b as t -> (
      if has_name t then
        let name = get_name_unsafe t in
        TVar (name, t)
      else
        match preferred_name with
        | Some name ->
          if show_sanitized_name name <> str_of_base b then
            let name = add_name t name in
            TVar (name, t)
          else
            t
        | None -> t)
    | Unary (Ticket, _) as t ->
      if has_name t then
        let name = get_name_unsafe t in
        TVar (name, t)
      else
        t
    | Annot (t, name) ->
      let name = bind_sanitize_annot name in
      Annot (aux ~preferred_name:(Some name) ~first_call:false t, name)
    | Record (LeafP (field_name, t)) as r ->
      if has_name r then
        let name =
          match preferred_name with
          | None -> get_name_unsafe r
          | Some pname -> add_name r pname in
        TVar (name, r)
      else if first_call then
        aux ~first_call:false ~preferred_name:(Some field_name) t
      else
        let t' = aux ~first_call:false t in
        let name = add_name t' field_name in
        TVar (name, t')
    | Record (PairP _ as t) as r ->
      if has_name r then
        let name =
          match preferred_name with
          | None -> get_name_unsafe r
          | Some pname -> add_name r pname in
        TVar (name, r)
      else
        let t', _ =
          fold_and_replace_paired_type
            ~f:(fun name t () ->
              let name = bind_sanitize_annot name in
              let t' = aux ~preferred_name:(Some name) ~first_call:false t in
              (LeafP (name, t'), ()))
            ~init:() t in
        if first_call then
          Record t'
        else
          let name =
            match preferred_name with
            | None -> get_or_create_name (Record t')
            | Some name -> add_name (Record t') name in
          TVar (name, Record t')
    | Pair l as p -> (
      if has_name p then
        let name = get_name_unsafe p in
        TVar (name, p)
      else
        let ts = Pair (List.map (aux ~first_call:false) l) in
        match preferred_name with
        | Some pname -> TVar (pname, ts)
        | None -> ts)
    | TVar (name, t) ->
      let name =
        match preferred_name with
        | Some pname -> pname
        | None -> name in
      let _name = add_name t name in
      (* No need to add a TVar below: the naming system will already
         guess it *)
      aux ~first_call:false t
    | Or o as t ->
      if has_name t then
        let name = get_name_unsafe t in
        TVar (name, t)
      else
        let t', _ =
          fold_and_replace_or_type
            ~f:(fun t () ->
              let t = aux ~first_call:false t in
              (LeafOr t, ()))
            ~init:() o in
        if first_call then
          Or t'
        else
          let name =
            match preferred_name with
            | None -> get_or_create_name t
            | Some pname -> add_name t pname in
          TVar (name, Or t')
    | Unary (List, t) ->
      let t = aux ~first_call:false t in
      Unary (List, t)
    | Unary (Set, t) ->
      let t = aux ~first_call:false t in
      Unary (Set, t)
    | Unary (Option, t) ->
      let t = aux ~first_call:false t in
      Unary (Option, t)
    | Binary (BigMap, (k, t)) ->
      let k = aux ~first_call:false k in
      let t = aux ~first_call:false t in
      Binary (BigMap, (k, t))
    | Binary (Lambda, (k, t)) ->
      let k = aux ~first_call:false k in
      let t = aux ~first_call:false t in
      Binary (Lambda, (k, t))
    | Binary (Map, (k, t)) ->
      let k = aux ~first_call:false k in
      let t = aux ~first_call:false t in
      Binary (Map, (k, t))
    | Contract _ as t -> t in
  let res = aux ~first_call:false ~preferred_name t_ in
  output_verbose ~id ~level:2
  @@ asprintf "[extract_AST][msg:%s] Exiting with result %a" msg pp_type_ res ;
  res

(** Extract all type variables included in t_ so that they may be
   marked as dependencies *)
let extract_type_variables ?(msg = "no specific message") t_ =
  let id = get_fresh_id () in
  output_verbose ~id ~level:1
  @@ asprintf "[extract_type_variables][msg:%s] Entering" msg ;
  output_verbose ~id ~level:2
  @@ asprintf "[extract_type_variables][msg:%s] Entering with input %s" msg
       (EzEncoding.construct type__enc t_) ;
  let rec aux (tvs : (sanitized_name * type_) list) :
      type_ -> (sanitized_name * type_) list = function
    | Base _ -> tvs
    | Annot (t, _n) -> aux tvs t
    | Record r ->
      fold_paired_type ~f:(fun _name tvs_accu t -> aux t tvs_accu) tvs r
    | Pair l -> List.fold_right (fun t tvs_accu -> aux tvs_accu t) l tvs
    | TVar (name, t) -> aux ((name, t) :: tvs) t
    | Or o -> fold_or_type (fun t tvs_accu -> aux tvs_accu t) tvs o
    | Unary (_, t) -> aux tvs t
    | Binary (Map, (k, v)) -> aux (aux tvs k) v
    | Binary (BigMap, (k, v)) -> aux (aux tvs k) v
    | Binary (Lambda, (k, v)) -> aux (aux tvs k) v
    | Contract _c -> tvs in

  let res = aux [] t_ in
  output_verbose ~id ~level:1
  @@ asprintf "[extract_type_variables][msg:%s] Leaving with result %s" msg
       (EzEncoding.construct
          (Json_encoding.list sanitized_name_enc)
          (List.map fst res)) ;
  res

(** Build the dependency graph of the AST *)
let extract_dependencies ?(msg = "no message given") nodes edges t_ =
  let id = get_fresh_id () in
  output_verbose ~id ~level:2
  @@ asprintf "[extract_dependencies][msg:%s] Entering with input %a" msg
       pp_type_ t_ ;
  (* first_call indicates that we are in the first recursive call; in
     that case we don't to add a node as this part is already taken
     care of in extract_interface *)
  let rec aux ~first_call (nodes : interface_instruction list)
      (edges : ((sanitized_name * type_) * (sanitized_name * type_)) list) =
    function
    | TVar (name, t) ->
      (* all type variables which appear in t *)
      let tvs = extract_type_variables ~msg:"TVAR in extract_dependencies" t in
      (* A new node for the graph representing the current type t
         with its name *)
      let nodes =
        if first_call then
          nodes
        else
          let node = define_type name t in
          make_unique ~eq:(eq_interface_instruction ~eq_types) (node @ nodes)
      in
      (* For every type variable tv in t, add a dependency tv -> name
         (meaning tv must be defined before name) *)
      let edges = List.map (fun x -> (x, (name, t))) tvs @ edges in

      aux ~first_call:false nodes edges t
    | Base _ -> (nodes, edges)
    | Annot (t, _) -> aux ~first_call:false nodes edges t
    (* raise (GenericError ("extract_dependencies", "There should be no Annot at this stage")) *)
    | Unary (Ticket, t) -> aux ~first_call:false nodes edges t
    | Record (LeafP (_, t)) -> aux ~first_call:false nodes edges t
    | Record r ->
      fold_paired_type
        ~f:(fun _name t (nodes, edges) -> aux ~first_call:false nodes edges t)
        (nodes, edges) r
    | Pair l ->
      List.fold_right
        (fun t (nodes, edges) -> aux ~first_call:false nodes edges t)
        l (nodes, edges)
    | Or o ->
      fold_or_type
        (fun t (nodes, edges) -> aux ~first_call:false nodes edges t)
        (nodes, edges) o
    | Unary (List, t) -> aux ~first_call:false nodes edges t
    | Unary (Set, t) -> aux ~first_call:false nodes edges t
    | Unary (Option, t) -> aux ~first_call:false nodes edges t
    | Binary (Map, (k, v)) ->
      let nodes, edges = aux ~first_call:false nodes edges k in
      aux ~first_call:false nodes edges v
    | Binary (BigMap, (k, v)) ->
      let nodes, edges = aux ~first_call:false nodes edges k in
      aux ~first_call:false nodes edges v
    | Binary (Lambda, (k, v)) ->
      let nodes, edges = aux ~first_call:false nodes edges k in
      aux ~first_call:false nodes edges v
    | Contract _ -> (nodes, edges) in
  let res = aux ~first_call:true nodes edges t_ in
  output_verbose ~id ~level:2
  @@ asprintf "[extract_dependencies][msg:%s] Leaving with nodes %a" msg
       (pp_print_list ~pp_sep:(tag ";") pp_interface_instruction_light)
       (fst res) ;
  res

let extract_interface ~storage ~initial_nodes
    (eps : (sanitized_name * micheline) list) :
    sanitized_name * type_ * interface_instruction list =
  let id = get_fresh_id () in
  output_verbose ~id ~level:1 "[extract interface] Entering" ;
  let eq = eq_interface_instruction_light in
  if eps = [] then
    output_verbose ~id ~level:1
      "[extract interface] WARNING: list of entrypoints is empty" ;
  let rec aux g =
    output_verbose ~id ~level:1 "[extract interface]Entering aux" ;
    function
    | (entrypoint_name, expr) :: xs ->
      (* Checking that the entrypoint name is not a forbidden value: *)
      let real_name = entrypoint_name in
      let entrypoint_name = sanitize_annot @@ filter entrypoint_name in
      let entrypoint_name =
        { entrypoint_name with original = real_name.original } in
      register_entrypoint_real_name entrypoint_name real_name ;

      (* Process type of entrypoint input *)
      output_verbose ~id ~level:1
        (sprintf "[extract interface][aux] Processing entrypoint: %s"
           (show_sanitized_name entrypoint_name)) ;

      let type_ =
        type__of_micheline ~msg:(show_sanitized_name entrypoint_name) expr in

      output_verbose ~id ~level:2
      @@ Format.asprintf "[extract_interface][entrypoint %s] %a@."
           (show_sanitized_name entrypoint_name)
           pp_type_ type_ ;
      let entrypoint_ast =
        extract_AST ~preferred_name:(Some entrypoint_name)
          ~msg:(show_sanitized_name entrypoint_name)
          type_ in
      let new_nodes1, new_edges1 =
        extract_dependencies
          ~msg:(show_sanitized_name entrypoint_name)
          [] [] entrypoint_ast in
      let new_nodes =
        make_unique
          ~eq:(eq_interface_instruction ~eq_types)
          new_nodes1 (* @ new_nodes2 *) in
      output_verbose ~id ~level:2
      @@ asprintf "[extract_interface] About to add new_nodes [%a]"
           (pp_print_list ~pp_sep:(tag ";\n") pp_interface_instruction)
           new_nodes ;
      let g = add_nodes ~print_node:pp_interface_instruction ~eq g new_nodes in
      output_verbose ~id ~level:1
      @@ asprintf
           "[extract_interface] About to add edges inferred from dependencies \
            for entrypoint %s"
           (show_sanitized_name entrypoint_name) ;
      (* We need to give entrypoint_ast the name entrypoint_name so
         that types with the same name are not created later *)
      let _reserved_entrypoint_name = add_name entrypoint_ast entrypoint_name in
      output_verbose ~id ~level:1
      @@ asprintf "[extract_interface] Reserved name %s for entrypoint %s"
           (show_sanitized_name _reserved_entrypoint_name)
           (show_sanitized_name entrypoint_name) ;
      let def_ep =
        (* if not(exists_name entrypoint_name) then *)
        define_type entrypoint_name
          (* (if has_name entrypoint_ast then
           *   TVar (get_name_unsafe entrypoint_ast, entrypoint_ast)
           * else *)
          entrypoint_ast
        (* ) *)
        (* else
         *   [] *) in
      let call_ep = Entrypoint (entrypoint_name, entrypoint_ast) in
      let g =
        add_nodes ~print_node:pp_interface_instruction ~eq g
          (make_unique ~eq:eq_interface_instruction_light (def_ep @ [call_ep]))
      in

      let g =
        List.fold_right
          (fun ((tv_name1, _), (tv_name2, _)) g ->
            add_edge_by_name ~eq g tv_name1 tv_name2)
          new_edges1 g in
      let g =
        let entrypoint_node = find_node g entrypoint_name in
        match entrypoint_node with
        | None ->
          raise
            (GenericError
               ( "extract_interface",
                 sprintf "couldn't find node for entrypoint %s"
                   (show_sanitized_name entrypoint_name) ))
        | Some entrypoint_node ->
          List.fold_right
            (fun def_ep g ->
              add_edge ~print_node:pp_interface_instruction
                ~eq:eq_interface_instruction_light g def_ep call_ep)
            [entrypoint_node] g in
      (* Call depends on the definition of the type *)
      (* let g = List.fold_right (fun f g -> f g entrypoint_name) new_edges1 g in *)
      let type_variables_ast =
        extract_type_variables
          ~msg:(show_sanitized_name entrypoint_name)
          entrypoint_ast in
      let g =
        List.fold_right
          (fun (tv_name, _) g ->
            add_edge_by_name ~eq:eq_interface_instruction_light g tv_name
              entrypoint_name)
          type_variables_ast g in
      let g = aux g xs in
      g
    | [] -> g in
  (* Build the initial graph from the entrypoints *)
  let initial_graph =
    add_nodes ~print_node:pp_interface_instruction ~eq [] initial_nodes in
  let graph = aux initial_graph eps in
  output_verbose ~id ~level:1 "[extract_interface] Leaving aux" ;
  (* Now deal with the storage *)
  let storage_type_ast =
    extract_AST
      ~preferred_name:(Some (sanitized_of_str "storage"))
      ~msg:"storage_AST" storage in
  let storage_name =
    match storage_type_ast with
    | TVar (storage_name, _t) ->
      output_verbose ~id ~level:1
      @@ sprintf "[extract_interface] storage_name is %s"
           (show_sanitized_name storage_name) ;
      storage_name
    | _ ->
      output_verbose ~id ~level:2
      @@ asprintf
           "[extract_interface] storage type does not have shape TVar \
            (storage_name, ...): storage_type_ast\n\
            %a"
           pp_type_ storage_type_ast ;
      sanitized_of_str "storage" in
  let def_storage = define_type storage_name storage_type_ast in
  let graph =
    add_nodes ~print_node:pp_interface_instruction ~eq graph def_storage in
  let type_variables_storage =
    extract_type_variables ~msg:"storage variables" storage_type_ast in
  let new_nodes2, new_edges2 =
    extract_dependencies ~msg:"storage" [] [] storage_type_ast in
  output_verbose ~id ~level:2
  @@ asprintf "Extracted new nodes: %a"
       (pp_print_list ~pp_sep:(tag ";") pp_interface_instruction)
       new_nodes2 ;
  let graph =
    add_nodes ~print_node:pp_interface_instruction ~eq graph new_nodes2 in
  let graph =
    List.fold_right
      (fun ((tv_name1, _), (tv_name2, _)) g ->
        add_edge_by_name ~eq g tv_name1 tv_name2)
      new_edges2 graph in
  let graph =
    List.fold_right
      (fun (tv_name, _) g ->
        add_edge_by_name ~eq:eq_interface_instruction_light g tv_name
          storage_name)
      type_variables_storage graph in

  let graph = add_node ~print_node:pp_interface_instruction ~eq graph Deploy in

  let storage_def_node =
    match find_node graph storage_name with
    | None -> failwith "Could not find storage node"
    | Some stnode -> stnode in
  let graph =
    add_edge ~print_node:pp_interface_instruction
      ~eq:eq_interface_instruction_light graph storage_def_node Deploy in

  let id = get_fresh_id () in
  output_verbose ~id ~level:1 "Extracting storage AST" ;

  output_verbose ~id ~level:2 (asprintf "Storage: %a" pp_type_ storage_type_ast) ;
  (* Register all edges in order to then be able to marshal type
     dependencies *)
  List.iter
    (fun (x, edges) -> List.iter (fun y -> register_edge x y) edges)
    graph ;
  try
    ( storage_name,
      storage_type_ast,
      make_unique ~eq:(eq_interface_instruction ~eq_types) @@ toposort graph )
  with CycleFound path as e ->
    Format.eprintf "Cycle: %a\n%!" pp_interface_light path ;
    raise e
