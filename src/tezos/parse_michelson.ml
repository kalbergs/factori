open Tezos_micheline.Micheline
open Tzfunc.Proto
open Factori_errors

let string_to_primitive = function
  | "parameter" -> `parameter
  | "storage" -> `storage
  | "code" -> `code
  | "False" -> `False
  | "Elt" -> `Elt
  | "Left" -> `Left
  | "None" -> `None
  | "Pair" -> `Pair
  | "Right" -> `Right
  | "Some" -> `Some
  | "True" -> `True
  | "Unit" -> `Unit
  | "PACK" -> `PACK
  | "UNPACK" -> `UNPACK
  | "BLAKE2B" -> `BLAKE2B
  | "SHA256" -> `SHA256
  | "SHA512" -> `SHA512
  | "ABS" -> `ABS
  | "ADD" -> `ADD
  | "AMOUNT" -> `AMOUNT
  | "AND" -> `AND
  | "BALANCE" -> `BALANCE
  | "CAR" -> `CAR
  | "CDR" -> `CDR
  | "CHECK_SIGNATURE" -> `CHECK_SIGNATURE
  | "COMPARE" -> `COMPARE
  | "CONCAT" -> `CONCAT
  | "CONS" -> `CONS
  | "CREATE_ACCOUNT" -> `CREATE_ACCOUNT
  | "CREATE_CONTRACT" -> `CREATE_CONTRACT
  | "IMPLICIT_ACCOUNT" -> `IMPLICIT_ACCOUNT
  | "DIP" -> `DIP
  | "DROP" -> `DROP
  | "DUP" -> `DUP
  | "EDIV" -> `EDIV
  | "EMPTY_MAP" -> `EMPTY_MAP
  | "EMPTY_SET" -> `EMPTY_SET
  | "EQ" -> `EQ
  | "EXEC" -> `EXEC
  | "FAILWITH" -> `FAILWITH
  | "GE" -> `GE
  | "GET" -> `GET
  | "GT" -> `GT
  | "HASH_KEY" -> `HASH_KEY
  | "IF" -> `IF
  | "IF_CONS" -> `IF_CONS
  | "IF_LEFT" -> `IF_LEFT
  | "IF_NONE" -> `IF_NONE
  | "INT" -> `INT
  | "LAMBDA" -> `LAMBDA
  | "LE" -> `LE
  | "LEFT" -> `LEFT
  | "LOOP" -> `LOOP
  | "LSL" -> `LSL
  | "LSR" -> `LSR
  | "LT" -> `LT
  | "MAP" -> `MAP
  | "MEM" -> `MEM
  | "MUL" -> `MUL
  | "NEG" -> `NEG
  | "NEQ" -> `NEQ
  | "NIL" -> `NIL
  | "NONE" -> `NONE
  | "NOT" -> `NOT
  | "NOW" -> `NOW
  | "OR" -> `OR
  | "PAIR" -> `PAIR
  | "PUSH" -> `PUSH
  | "RIGHT" -> `RIGHT
  | "SIZE" -> `SIZE
  | "SOME" -> `SOME
  | "SOURCE" -> `SOURCE
  | "SENDER" -> `SENDER
  | "SELF" -> `SELF
  | "STEPS_TO_QUOTA" -> `STEPS_TO_QUOTA
  | "SUB" -> `SUB
  | "SWAP" -> `SWAP
  | "TRANSFER_TOKENS" -> `TRANSFER_TOKENS
  | "SET_DELEGATE" -> `SET_DELEGATE
  | "UNIT" -> `UNIT
  | "UPDATE" -> `UPDATE
  | "XOR" -> `XOR
  | "ITER" -> `ITER
  | "LOOP_LEFT" -> `LOOP_LEFT
  | "ADDRESS" -> `ADDRESS
  | "CONTRACT" -> `CONTRACT
  | "ISNAT" -> `ISNAT
  | "CAST" -> `CAST
  | "RENAME" -> `RENAME
  | "bool" -> `bool
  | "contract" -> `contract
  | "int" -> `int
  | "key" -> `key
  | "key_hash" -> `key_hash
  | "lambda" -> `lambda
  | "list" -> `list
  | "map" -> `map
  | "big_map" -> `big_map
  | "nat" -> `nat
  | "option" -> `option
  | "or" -> `or_
  | "pair" -> `pair
  | "set" -> `set
  | "signature" -> `signature
  | "string" -> `string
  | "bytes" -> `bytes
  | "mutez" -> `mutez
  | "timestamp" -> `timestamp
  | "unit" -> `unit
  | "operation" -> `operation
  | "address" -> `address
  | "SLICE" -> `SLICE
  (* 005 specific *)
  | "DIG" -> `DIG
  | "DUG" -> `DUG
  | "EMPTY_BIG_MAP" -> `EMPTY_BIG_MAP
  | "APPLY" -> `APPLY
  | "chain_id" -> `chain_id
  | "CHAIN_ID" -> `CHAIN_ID
  (* 008 specific *)
  | "LEVEL" -> `LEVEL
  | "SELF_ADDRESS" -> `SELF_ADDRESS
  | "never" -> `never
  | "NEVER" -> `NEVER
  | "UNPAIR" -> `UNPAIR
  | "VOTING_POWER" -> `VOTING_POWER
  | "TOTAL_VOTING_POWER" -> `TOTAL_VOTING_POWER
  | "KECCAK" -> `KECCAK
  | "SHA3" -> `SHA3
  | "PAIRING_CHECK" -> `PAIRING_CHECK
  | "bls12_381_g1" -> `bls12_381_g1
  | "bls12_381_g2" -> `bls12_381_g2
  | "bls12_381_fr" -> `bls12_381_fr
  | "sapling_state" -> `sapling_state
  | "sapling_transaction" -> `sapling_transaction
  | "SAPLING_EMPTY_STATE" -> `SAPLING_EMPTY_STATE
  | "SAPLING_VERIFY_UPDATE" -> `SAPLING_VERIFY_UPDATE
  | "ticket" -> `ticket
  | "TICKET" -> `TICKET
  | "READ_TICKET" -> `READ_TICKET
  | "SPLIT_TICKET" -> `SPLIT_TICKET
  | "JOIN_TICKETS" -> `JOIN_TICKETS
  | "GET_AND_UPDATE" -> `GET_AND_UPDATE
  | "chest" -> `chest
  | "chest_key" -> `chest_key
  | "OPEN_CHEST" -> `OPEN_CHEST
  | "VIEW" -> `VIEW
  | "view" -> `view
  | "constant" -> `constant
  (* 012 specific *)
  | "SUB_MUTEZ" -> `SUB_MUTEZ
  | s -> `macro s (* failwith (Format.sprintf "Unknown primitive: %s\n%!" s) *)

let rec node_to_mprim (n : Tezos_micheline.Micheline_parser.node) : micheline =
  match n with
  | Int (_, i) -> Mint i
  | String (_, s) -> Mstring s
  | Bytes (_, b) -> Mbytes (Crypto.H.mk (Bytes.to_string b))
  | Prim (_, p, args, annots) ->
    Mprim
      {
        prim = string_to_primitive p;
        args = List.map node_to_mprim args;
        annots;
      }
  | Seq (_, l) -> Mseq (List.map node_to_mprim l)

let node_list_to_script_expr nl = Micheline (Mseq (List.map node_to_mprim nl))

let parse_michelson_file ~file =
  let open Tezos_micheline.Micheline_parser in
  let file_content = Factori_utils.read_file file in
  let micheline_tokens = tokenize file_content in
  match no_parsing_error micheline_tokens with
  | Error _ -> raise (ParsingError "Micheline Tokens")
  | Ok res -> (
    match no_parsing_error @@ parse_toplevel res with
    | Error _ -> raise (ParsingError "Toplevel")
    | Ok res -> node_list_to_script_expr res)

let parse_michelson_micheline ~file =
  let open Tezos_micheline.Micheline_parser in
  let file_content = Factori_utils.read_file file in
  let micheline_tokens = tokenize file_content in
  match no_parsing_error micheline_tokens with
  | Error _ -> raise (ParsingError "Micheline Tokens")
  | Ok res -> (
    match no_parsing_error @@ parse_toplevel res with
    | Error _ -> raise (ParsingError "Toplevel")
    | Ok res -> res)
