open Format
open Infer_entrypoints

let pp_field_type ppf = function
  | CVarchar -> pp_print_string ppf "TextField"
  | CZarith -> pp_print_string ppf "BigIntField"
  | CInteger -> pp_print_string ppf "IntField"
  | CJson -> pp_print_string ppf "JsonField"
  | CTimestamp -> pp_print_string ppf "DateTimeField"
  | CBoolean -> pp_print_string ppf "BooleanFieldD"
  | CBigint -> pp_print_string ppf "BigInt"
  | CBytea -> failwith "TODO-dipdup : custom field for CBytea"
  | CArray _typ -> failwith "TODO-dipdup : custom field for CArray"

let pp_field_extra ppf field =
  fprintf ppf "%s%s"
    (if field.crawlori_column_option then
      "null=True"
    else
      "")
    (match field.crawlori_column_index with
    | Some _ -> "index=True"
    | _ -> "")

let pp_field ppf field =
  fprintf ppf "  %s = fields.%a(%a)" field.crawlori_column_name pp_field_type
    field.crawlori_column_type pp_field_extra field

let pp_fields ppf fields =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
    pp_field ppf fields

let pp_model ppf table =
  fprintf ppf "class %s(Model):\n%a"
    (String.capitalize_ascii table.crawlori_table_name)
    pp_fields table.crawlori_table_fields

let models_header ppf () =
  fprintf ppf
    {|from tortoise import Tortoise, fields
from dipdup.models import Model
|}

let process_models ppf tables =
  models_header ppf () ;
  pp_print_newline ppf () ;
  pp_print_list
    ~pp_sep:(fun ppf _ ->
      pp_print_newline ppf () ;
      pp_print_newline ppf ())
    pp_model ppf tables
