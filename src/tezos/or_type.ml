open Format
open Factori_utils

type 'a or_type =
  | LeafOr of 'a
  | OrAbs of 'a or_type * 'a or_type
[@@deriving show, encoding { recursive }]

let rec eq_or_type ~eq p1 p2 =
  match (p1, p2) with
  | LeafOr x1, LeafOr x2 -> eq x1 x2
  | OrAbs (l1, l2), OrAbs (l3, l4) ->
    eq_or_type ~eq l1 l3 && eq_or_type ~eq l2 l4
  | _, _ -> false

let pp_ortype_ocaml (f : formatter -> 'a -> unit) (ppf : formatter) x : unit =
  let rec aux ppf = function
    | LeafOr l -> fprintf ppf "%a" f l
    | OrAbs (l1, l2) ->
      fprintf ppf "Mprim {prim = `or_;\nargs = [%a];\nannots=[]}\n"
        (pp_print_list ~pp_sep:(tag ";") aux)
        [l1; l2] in
  aux ppf x

let pp_ortype_typescript (f : formatter -> 'a -> unit) (ppf : formatter) x :
    unit =
  let rec aux ppf = function
    | LeafOr l -> fprintf ppf "%a" f l
    | OrAbs (l1, l2) ->
      fprintf ppf "{prim : \"or\",\nargs : [%a]}\n"
        (pp_print_list ~pp_sep:(tag ",") aux)
        [l1; l2] in
  aux ppf x

let fold_or_type (f : 'a -> 'b -> 'b) (init : 'b) (x : 'a or_type) =
  let rec aux res = function
    | LeafOr x -> f x res
    | OrAbs (x, y) -> aux (aux res y) x in
  aux init x

let fold_and_replace_or_type ~f ~init x =
  let rec aux res = function
    | LeafOr x -> f x res
    | OrAbs (x, y) ->
      let t1, res1 = aux res x in
      let t2, res2 = aux res1 y in
      (OrAbs (t1, t2), res2) in
  aux init x

let fold_and_replace_or_type_combine ~f combine x =
  let rec aux = function
    | LeafOr x -> f x
    | OrAbs (x, y) ->
      let t1, res1 = aux x in
      let t2, res2 = aux y in
      (OrAbs (t1, t2), combine res1 res2) in
  aux x

let rec mapOr_pure ~f = function
  | LeafOr t_ -> LeafOr (f t_)
  | OrAbs (x, y) -> OrAbs (mapOr_pure ~f x, mapOr_pure ~f y)

(** Can't remember why this is called mapOr, it does something
     slightly different *)
let rec mapOr (f : 'a -> 'b -> 'b * 'a) (init : 'a) = function
  | LeafOr t_ ->
    let t_, res = f init t_ in
    (LeafOr t_, res)
  | OrAbs (x, y) ->
    let t_x, res_x = mapOr f init x in
    let t_y, res_y = mapOr f res_x y in
    (OrAbs (t_x, t_y), res_y)

let constructor_from_path path =
  String.concat "_"
    (List.map
       (fun b ->
         if b then
           "Left"
         else
           "Right")
       path)
