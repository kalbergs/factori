open Factori_file

type accessors =
  | Field of string
  | Index of int

type accessor_type =
  | TS
  | HTML
  | FUNCTION

let pp fmt (acc, a) =
  if acc = [] then
    ()
  else
    let inversed_acc = List.rev acc in
    let root = List.hd inversed_acc in
    begin
      match root with
      | Field s -> File.write fmt "%s" s
      | Index _ -> File.write fmt "Index"
    end ;
    List.iter
      (fun fi ->
        match fi with
        | Field s -> begin
          match a with
          | TS -> File.write fmt ".%s" s
          | HTML -> File.write fmt "-%s" s
          | FUNCTION -> File.write fmt "_%s" s
        end
        | Index i -> begin
          match a with
          | TS -> File.write fmt "[%d]" i
          | HTML -> File.write fmt "-%d" i
          | FUNCTION -> File.write fmt "_%d" i
        end)
      (List.tl inversed_acc)

let pp_ts fmt acc = pp fmt (acc, TS)

let pp_html fmt acc =
  let escape s =
    let s = Str.global_replace (Str.regexp_string ".") "-" s in
    Str.global_replace (Str.regexp_string "_") "-" s in
  let escaped =
    List.map
      (fun a ->
        match a with
        | Field s -> Field (escape s)
        | Index _ -> a)
      acc in
  pp fmt (escaped, HTML)

let pp_fun fmt acc = pp fmt (acc, FUNCTION)
