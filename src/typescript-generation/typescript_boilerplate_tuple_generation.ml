open Format

let one_to_n n =
  let rec aux res = function
    | 0 -> res
    | k -> aux (k :: res) (k - 1) in
  aux [] n

let k_to_n k n =
  let rec aux res = function
    | i when i < k -> res
    | k -> aux (k :: res) (k - 1) in
  aux [] n

let concat ppf d =
  let l = one_to_n d in
  fprintf ppf
    "export function concat%d<%a>(x : T1, l : [%a]) : [%a] {\n\
    \   return [x,%a]\n\
    \   }" d
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "T%d" x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "T%d" x))
    (k_to_n 2 d)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "T%d" x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "l[%d]" x))
    (k_to_n 0 (d - 2))

let encode ppf d =
  let l = one_to_n d in
  fprintf ppf
    "export function tuple%d_encode<%a>(%a)\n\
    \       {\n\
    \         return (x : [%a]) =>\n\
    \           {\n\
    \             return {prim : 'Pair', args : [%a]}\n\
    \           }\n\
    \       }" d
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "T%d" x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x ->
         fprintf ppf "x%d_encode : (x%d : T%d) => MichelsonV1Expression" x x x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "T%d" x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "x%d_encode(x[%d])" x (x - 1)))
    l

let micheline ppf d =
  let l = one_to_n d in
  fprintf ppf
    "export function tuple%d_micheline(%a) : MichelsonV1Expression\n\
    \           {\n\
    \             return {prim : 'Pair', args : [%a]}\n\
    \           }\n\
    \       " d
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "x%d_micheline : MichelsonV1Expression" x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "x%d_micheline" x))
    l

let decode ppf d =
  let l = one_to_n d in
  fprintf ppf
    "export function tuple%d_decode<%a>(%a): (x : MichelsonV1Expression) => \
     [%a]{\n\
     return (x : MichelsonV1Expression) =>\n\
    \       {\n\
    \       if(isExtended(x)) {\n\
    \       if(x.prim == 'Pair' && x.args !== undefined && x.args.length >=2) {\n\
    \       return \
     concat%d(t1_decode(x.args[0]),(tuple%d_decode(%a)(x.args.slice(1))))\n\
    \       }\n\
    \       else{\n\
    \       fail_on_micheline(\"tuple%d_decode\",x)\n\
    \       throw \"tuple%d_decode\"\n\
    \       }\n\
    \       }\n\
    \       else{//x is not a 'Pair' hence it is a Seq\n\
    \       if(isList(x) && x.length >= %d){\n\
    \       return concat%d(t1_decode(x[0]),(tuple%d_decode(%a)(x.slice(1))))}\n\
    \       else{\n\
    \       fail_on_micheline(\"tuple%d_decode\",x)\n\
    \       throw \"tuple%d_decode\"\n\
    \       }\n\
    \       }\n\
    \       }\n\
     }\n"
    d
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "T%d" x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x ->
         fprintf ppf "t%d_decode : (x : MichelsonV1Expression) => T%d" x x))
    l
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "T%d" x))
    l d (d - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "t%d_decode" x))
    (List.tl l) d d d d (d - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf x -> fprintf ppf "t%d_decode" x))
    (List.tl l) d d

(* let main () =
 *   List.iter (fun d -> Format.eprintf "%a\n" encode d (\* concat d decode d *\)) (k_to_n 7 50) *)

(* let _ = main () *)
