open Unstack_micheline
module LiveInter = Inter_procedural.Make (Live_analysis)
module Permission = Inter_procedural.Make (Perm_test)
module Label = Inter_procedural.Label
module Test = Auxilary.Aux
module Printer = Unstack_printer.Make (Nested_arg)

let test sc =
  Linter_warning.check_storage sc ;
  Linter_warning.abs_presence sc ;
  Reentrancy.reentrancy_detection sc ;
  let new_sc = Inline_unstack.transform_sc sc in
  let tool = Auxilary.Aux.info_smart new_sc in
  let _ = Permission.solveMF tool in
  let use_inter = LiveInter.solveMF tool in
  let storage =
    match Nested_arg.gets_pair (Z.of_int 2) new_sc.begin_stack with
    | Error e -> raise e
    | Ok e -> e in
  let storage_fields = Nested_arg.name_string_list [storage] in
  let open Inter_procedural in
  let unused_field =
    Label.fold
      (fun lab _chain_map acc ->
        let fv = ChainMap.find_opt [] (Label.find lab use_inter) in
        match fv with
        | None -> acc
        | Some fv ->
          List.filter
            (fun field ->
              match Live_analysis.find field fv with
              | None -> true
              | Some _ -> false)
            acc)
      use_inter storage_fields in
  match unused_field with
  | [] -> Format.eprintf "All the field of the storage are used@."
  | _ ->
    Format.eprintf "Warning : Fields of the storage are not used : @." ;
    Format.pp_print_list ~pp_sep:Format.pp_print_newline
      (fun format (s, m) ->
        Format.fprintf format "%s -> %s" s
          (Unstack_micheline.string_of_micheline m))
      Format.err_formatter unused_field ;
    Format.pp_print_newline Format.err_formatter ()
