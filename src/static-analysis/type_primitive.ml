open Tzfunc.Proto
open Factori_errors

let split_at z l =
  let rec aux_split acc cpt l =
    if Z.compare cpt Z.zero <= 0 then
      Some (List.rev acc, l)
    else
      match l with
      | [] -> None
      | x :: xs -> aux_split (x :: acc) (Z.pred cpt) xs in
  aux_split [] z l

let pair_or_simple = function
  | [] -> Error (UnstackMicheline "Not enough element for a type pair")
  | [solo] -> Ok solo
  | args -> Ok (Mprim { prim = `pair; args; annots = [] })

let make_option miche =
  Ok (Mprim { prim = `option; args = [miche]; annots = [] })

let make_lambda arg ret =
  Ok (Mprim { prim = `lambda; args = [arg; ret]; annots = [] })

let rec type_gets n ty =
  if Z.equal n Z.zero then
    Ok ty
  else
    match ty with
    | Mprim { prim = `pair; args = x :: _; _ } when Z.equal n Z.one -> Ok x
    | Mprim { prim = `pair; args = _ :: sub_miche_l; _ } ->
      let new_n = Z.sub n (Z.of_int 2) in
      Result.bind (pair_or_simple sub_miche_l) (type_gets new_n)
    | _ -> Error (UnstackMicheline "Not good for a GET n instruction")

let is_lambda (m : micheline) : bool =
  match m with
  | Mprim { prim = `lambda; _ } -> true
  | _ -> false

let type_updates n new_ty ty =
  let rec aux_update acc n ty =
    if Z.equal n Z.zero then
      pair_or_simple (List.rev_append acc [ty])
    else
      match ty with
      | Mprim { prim = `pair; args = _ :: sub_miche_l; _ } when Z.equal n Z.one
        -> pair_or_simple (List.rev_append acc (new_ty :: sub_miche_l))
      | Mprim { prim = `pair; args = x :: sub_miche_l; _ } ->
        let new_n = Z.sub n (Z.of_int 2) in
        Result.bind (pair_or_simple sub_miche_l) (aux_update (x :: acc) new_n)
      | _ -> Error (UnstackMicheline "Not good for a UPDATE n instruction")
  in
  aux_update [] n ty

let type_arg_collection ty =
  match ty with
  | Mprim { prim = `list; args; _ }
  | Mprim { prim = `set; args; _ }
  | Mprim { prim = `map; args; _ } -> pair_or_simple args
  | _ -> Error (UnstackMicheline "Not an iterable type")

let type_map arg1 arg2 =
  match (arg1, arg2) with
  | Mprim { prim = `pair; args = k :: v :: v_all; _ }, Mprim { prim = `map; _ }
    ->
    Result.bind
      (pair_or_simple (v :: v_all))
      (fun miche -> Ok (Mprim { prim = `map; args = [k; miche]; annots = [] }))
  | _, Mprim { prim = `list; _ } ->
    Ok (Mprim { prim = `list; args = [arg1]; annots = [] })
  | _ -> Error (UnstackMicheline "Wrong stack for a MAP instruction")

(*let split_pair_at n pair =
  let rec aux_split_pair acc n pair =
    match pair with
    | Mprim { prim = `pair; args = ty_l; _ }
      when Z.geq (Z.of_int (List.length ty_l)) n -> (
      let l_opt = split_at (Z.pred n) ty_l in
      match l_opt with
      | None -> Error (GenericError ("split_pair_at", "Unreachable branch"))
      | Some (tops, bots) ->
        Result.bind (pair_or_simple bots) (fun pair ->
            Ok (List.rev_append acc tops @ [pair])))
    | Mprim { prim = `pair; args = ty_l; _ } -> (
      match List.rev ty_l with
      | [] -> Error (UnstackMicheline "Empty argument for pair primitive")
      | x :: xs ->
        let new_n = Z.sub n (Z.of_int @@ List.length xs) in
        aux_split_pair (s @ acc) new_n x)
    | _ -> Error (UnstackMicheline "Not a pair") in
  aux_split_pair [] n pair*)

let split_pair_at_general n unpair_fun pair_fun pair =
  let rec aux_split_pair acc n pair =
    Result.bind (unpair_fun pair) (fun ty_l ->
        if Z.geq (Z.of_int (List.length ty_l)) n then
          let l_opt = split_at (Z.pred n) ty_l in
          match l_opt with
          | None -> Error (GenericError ("split_pair_at", "Unreachable branch"))
          | Some (tops, bots) ->
            Result.bind (pair_fun bots) (fun pair ->
                Ok (List.rev_append acc tops @ [pair]))
        else
          match List.rev ty_l with
          | [] -> Error (UnstackMicheline "Empty argument for pair primitive")
          | x :: xs ->
            let new_n = Z.sub n (Z.of_int @@ List.length xs) in
            aux_split_pair (xs @ acc) new_n x) in
  aux_split_pair [] n pair

let split_pair_at n pair =
  let unpair_fun = function
    | Mprim { prim = `pair; args = ty_l; _ } -> Ok ty_l
    | e ->
      Error
        (UnstackMicheline
           (Format.sprintf "%s is not a pair"
              (Unstack_micheline.string_of_micheline e))) in
  split_pair_at_general n unpair_fun pair_or_simple pair

(*
  let get_entrypoint annot michety =
    let rec aux_or miche_l =
      match miche_l with
      | [] -> None
      | e :: xs -> (
        let recup_annot = field_opt (get_annot e) in
        match recup_annot with
        | Some name when name = annot -> Some e
        | _ -> (
          match uncompute_or_opt e with
          | None -> aux_or xs
          | Some (l, r) -> aux_or (l :: r :: xs))) in
    aux_or [michety]*)

let type_update ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `set; args = [_]; _ } -> Ok ty2
  | ( Mprim { prim = `option; args = [_]; _ },
      Mprim { prim = `map; args = [_; _]; _ } ) -> Ok ty2
  | ( Mprim { prim = `option; args = [_]; _ },
      Mprim { prim = `big_map; args = [_; _]; _ } ) -> Ok ty2
  | _ -> Error (UnstackMicheline "Wrong type for an UPDATE instruction")

let type_or arg1 arg2 =
  match (arg1, arg2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Wrong stack for or instruction")

let type_address ty1 =
  match ty1 with
  | Mprim { prim = `contract; _ } ->
    Ok (Mprim { prim = `address; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for address instruction")

let type_add ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `timestamp; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `timestamp; _ } ->
    Ok (Mprim { prim = `timestamp; args = []; annots = [] })
  | Mprim { prim = `mutez; _ }, Mprim { prim = `mutez; _ } ->
    Ok (Mprim { prim = `mutez; args = []; annots = [] })
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `bls12_381_fr; _ } ->
    Ok (Mprim { prim = `bls12_381_fr; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g1; _ }, Mprim { prim = `bls12_381_g1; _ } ->
    Ok (Mprim { prim = `bls12_381_g1; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g2; _ }, Mprim { prim = `bls12_381_g2; _ } ->
    Ok (Mprim { prim = `bls12_381_g2; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an addition")

let type_sub ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `timestamp; _ }, Mprim { prim = `timestamp; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `timestamp; _ }, Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `timestamp; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for a substraction")

let type_sub_mutez ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `mutez; _ }, Mprim { prim = `mutez; _ } ->
    make_option (Mprim { prim = `mutez; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for a mutez substraction")

let type_mul ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `mutez; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `nat; _ }, Mprim { prim = `mutez; _ } ->
    Ok (Mprim { prim = `mutez; args = []; annots = [] })
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `bls12_381_fr; _ }
  | Mprim { prim = `nat; _ }, Mprim { prim = `bls12_381_fr; _ }
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `bls12_381_fr; _ } ->
    Ok (Mprim { prim = `bls12_381_fr; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g1; _ }, Mprim { prim = `bls12_381_g1; _ } ->
    Ok (Mprim { prim = `bls12_381_g1; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g2; _ }, Mprim { prim = `bls12_381_g2; _ } ->
    Ok (Mprim { prim = `bls12_381_g2; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for a MUL instruction")

(* let concat (ty1,_) (ty2,_) =
   let ty_desc =
     match (ty1,ty2) with
     |String,String -> *)

let type_ediv ty1 ty2 =
  let type_sub_ediv ty1 ty2 =
    match (ty1, ty2) with
    | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
      Ok
        [
          Mprim { prim = `nat; args = []; annots = [] };
          Mprim { prim = `nat; args = []; annots = [] };
        ]
    | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
    | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
    | Mprim { prim = `int; _ }, Mprim { prim = `int; _ } ->
      Ok
        [
          Mprim { prim = `int; args = []; annots = [] };
          Mprim { prim = `nat; args = []; annots = [] };
        ]
    | Mprim { prim = `mutez; _ }, Mprim { prim = `nat; _ } ->
      Ok
        [
          Mprim { prim = `mutez; args = []; annots = [] };
          Mprim { prim = `mutez; args = []; annots = [] };
        ]
    | Mprim { prim = `mutez; _ }, Mprim { prim = `mutez; _ } ->
      Ok
        [
          Mprim { prim = `nat; args = []; annots = [] };
          Mprim { prim = `mutez; args = []; annots = [] };
        ]
    | _ -> Error (UnstackMicheline "Bad type for an euclidean division") in
  let ty_l = type_sub_ediv ty1 ty2 in
  let pair = Result.bind ty_l pair_or_simple in
  Result.bind pair make_option

let type_and ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an and")

(*let type_and ty1 ty2 =
    match (ty1, ty2) with
    | Bool, Bool -> Bool
    | Nat, Nat | Int, Nat -> Nat
    | _ -> failwith "Bad type for an and"*)

let type_not ty1 =
  match ty1 with
  | Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ } | Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an not")

let type_neq ty1 =
  match ty1 with
  | Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for not equal")

let type_or_xor ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an or/xor")

(*let type_not ty1 =
      match ty1 with
      | Bool -> Bool
      | Nat | Int -> Int
      | _ -> failwith "Bad type for an not"

  let type_neq ty1 =
    match ty1 with
    | Int -> (Bool, [])
    | _ -> failwith "Bad type for not equal"

  let type_or_xor (ty1) (ty2) =
      match (ty1, ty2) with
      | Bool, Bool -> Bool
      | Nat, Nat -> Nat
      | _ -> failwith "Bad type for an or/xor"*)
let uncompute_pair = function
  | Mprim { prim = `pair; args; _ } -> Ok args
  | _ ->
    Error (UnstackMicheline "Trying to uncompute a type that is not a pair")

let sub_apply _ ret ty_l =
  match ty_l with
  | [] -> Error (UnstackMicheline "Error in apply")
  | _ :: xs ->
    let pair = pair_or_simple xs in
    Result.bind pair (fun arg -> make_lambda arg ret)

let type_apply arg lambda =
  match lambda with
  | Mprim { prim = `lambda; args = [ty_arg; ret]; _ } ->
    let ty_l = uncompute_pair ty_arg in
    Result.bind ty_l (sub_apply arg ret)
  | _ -> Error (UnstackMicheline "Not a lambda for an APPLY instruction")

let type_sub_concat arg1 =
  (* difference beetwen concat and concat_list is done elsewhere *)
  match arg1 with
  | Mprim { prim = `string; _ } ->
    Ok (Mprim { prim = `string; args = []; annots = [] })
  | Mprim { prim = `bytes; _ } ->
    Ok (Mprim { prim = `bytes; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Type mismatch for CONCAT instruction")

let type_concat arg1 arg2 =
  (* difference beetwen concat and concat_list is done elsewhere *)
  match (arg1, arg2) with
  | Mprim { prim = `string; _ }, Mprim { prim = `string; _ } ->
    Ok (Mprim { prim = `string; args = []; annots = [] })
  | Mprim { prim = `bytes; _ }, Mprim { prim = `bytes; _ } ->
    Ok (Mprim { prim = `bytes; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Type mismatch for CONCAT instruction")

let type_negativity arg1 =
  match arg1 with
  | Mprim { prim = `nat; _ } | Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `bls12_381_fr; _ } ->
    Ok (Mprim { prim = `bls12_381_fr; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g1; _ } ->
    Ok (Mprim { prim = `bls12_381_g1; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g2; _ } ->
    Ok (Mprim { prim = `bls12_381_g2; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not good type for NEG instruction")

let type_contract ty arg1 =
  match arg1 with
  | Mprim { prim = `address; _ } ->
    make_option (Mprim { prim = `contract; args = [ty]; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for contract")

let type_transfer _arg1 arg2 arg3 =
  match (arg2, arg3) with
  | Mprim { prim = `mutez; _ }, Mprim { prim = `contract; args = [_ty]; _ } ->
    Ok (Mprim { prim = `operation; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not good types for a transfer operation")

let type_mem _arg1 arg2 =
  match arg2 with
  | Mprim { prim = `set; args = [_ty]; _ }
  | Mprim { prim = `map; args = [_ty; _]; _ }
  | Mprim { prim = `big_map; args = [_ty; _]; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for a MEM instruction")

let type_check_sig arg1 arg2 arg3 =
  match (arg1, arg2, arg3) with
  | ( Mprim { prim = `key; _ },
      Mprim { prim = `signature; _ },
      Mprim { prim = `bytes; _ } ) ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for check_signature")

let type_pairing_check arg =
  let sub_pairing = function
    | [Mprim { prim = `bls12_381_g1; _ }; Mprim { prim = `bls12_381_g2; _ }] ->
      Ok (Mprim { prim = `bool; args = []; annots = [] })
    | _ ->
      Error (UnstackMicheline "Not good types for instruction PAIRING_CHECK")
  in
  match arg with
  | Mprim { prim = `list; args = [arg_ty]; _ } ->
    let pair = uncompute_pair arg_ty in
    Result.bind pair sub_pairing
  | _ -> failwith "Not a good type for pairing check"

let type_set_delegate arg =
  match arg with
  | Mprim { prim = `option; args = [Mprim { prim = `key_hash; _ }]; _ } ->
    Ok (Mprim { prim = `operation; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for set delegate instruction")

let type_bytes_trans arg =
  match arg with
  | Mprim { prim = `bytes; _ } ->
    Ok (Mprim { prim = `bytes; args = []; annots = [] })
  | _ ->
    Error (UnstackMicheline "Bytes transformation functions expect a bytes")

let voting_power arg =
  match arg with
  | Mprim { prim = `key_hash; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for voting power")

let type_hash_k arg =
  match arg with
  | Mprim { prim = `key; _ } ->
    Ok (Mprim { prim = `key_hash; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for HASH_KEY instruction")

let value_map arg =
  match arg with
  | Mprim { prim = `big_map; args = [key; value]; _ }
  | Mprim { prim = `map; args = [key; value]; _ } -> Ok (key, value)
  | _ -> Error (UnstackMicheline "Expected a map or a big_map")

let implicit_acc ty =
  match ty with
  | Mprim { prim = `key_hash; _ } ->
    let unit = prim `unit in
    Ok (prim `contract ~args:[unit])
  | _ ->
    Error (UnstackMicheline "Not a good type for IMPLICIT_ACCOUNT instruction")

let type_call_view ty _ arg2 =
  match arg2 with
  | Mprim { prim = `address; _ } -> make_option ty
  | _ -> Error (UnstackMicheline "Not a good type for CALL_VIEW instruction")
