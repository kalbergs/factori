open Tzfunc.Proto

type label = int

type args = micheline list

(* for node 'a is a representation of the variable *)

type 'a control_node =
  | N_IF of 'a * label * 'a node list * 'a node list
  | N_IF_NONE of 'a * label * 'a node list * 'a node list * 'a * label
  | N_IF_CONS of
      'a * label * 'a node list * 'a node list * 'a * label * 'a * label
  | N_IF_LEFT of
      'a * label * ('a * label * 'a node list) * ('a * label * 'a node list)
  | N_ITER of {
      arg_iter : 'a;
      acc_iter : 'a list;
      pat_acc_iter : 'a list;
      pat : 'a * label;
      body_iter : 'a node list;
      label_iter : label;
    }
  | N_LOOP of {
      arg : 'a;
      acc : 'a list;
      pat_acc : 'a list;
      body : 'a node list;
      label_pat_l : label;
      label_loop : label;
    }
  | N_LOOP_LEFT of {
      arg_lf : 'a;
      pat_lf : 'a * label;
      acc_lf : 'a list;
      pat_acc_lf : 'a list;
      node_loop : 'a node list;
      pat_res_lf : 'a * label;
      label_lf : label;
    }
  | N_MAP of 'a * label * 'a * 'a node list * label
  | N_FOLD_MAP of {
      arg_fold_map : 'a;
      acc_fold_map : 'a list;
      pat_acc_fold_map : 'a list;
      pat_fold_map : 'a * label;
      body_fold_map : 'a node list;
      label_map : label;
    }

and 'a node =
  | N_SIMPLE of primitive_or_macro * args * 'a list * 'a list * label
  | N_EXEC of 'a * 'a * 'a * label * label
  | N_START of 'a list * 'a control_node * label
  | FAIL_CONTROL of 'a control_node * label
  | N_END of 'a list * label
  | N_LAMBDA of 'a * label * 'a node list * 'a * label * label (* TODO *)
  | N_CONCAT of 'a * 'a * 'a * label (* MULTIPLE DEF *)
  | N_CONCAT_LIST of 'a * 'a * label
  | N_SELF of string option * 'a * label
  | N_CONTRACT of string option * 'a * 'a * label
  | N_CREATE_CONTRACT of micheline * 'a node list * label
  | N_NEVER of 'a * label
  | N_FAILWITH of 'a * label
  | N_CALL_VIEW of string * 'a * 'a * 'a * label

type name_arg = string * micheline

type stack = name_arg list

type 'a v_node = {
  node_name : string;
  view_label : label;
  view_stack : 'a;
  view_parameter : micheline;
  view_result : micheline;
  node_code_v : 'a node list;
}

type 'a sc_node = {
  begin_label : label;
  begin_stack : 'a;
  parameter : micheline;
  storage : micheline;
  node_l : 'a node list;
  views_node : 'a v_node list;
}

type 'a lambdas = {
  arg : 'a;
  lcode : 'a node list;
  label_start : label;
  label_end : label;
  res : 'a option;
}

type 'a complete_lambda = {
  lambda_name : 'a;
  lambda_code : 'a lambdas option;
  in_between : 'a list;
}

type 'a block_unstack =
  | BAssign of 'a node * label
  | BAnnoUse of 'a list * label
  | BAnnoAssign of 'a list * label
  | BReturnAssign of 'a list * 'a list * label
  | BIter of 'a list * 'a list * 'a * label
  | BMap of {
      top_body : 'a;
      return_coll : 'a;
      arg : 'a;
      initial_acc : 'a list;
      result_acc : 'a list;
      pos : label;
    }
  | BLoop of 'a * 'a list * 'a list * label
  | BStartExec of 'a (* arg *) * 'a * label
  | BEndExec of 'a (* res *) * 'a * label
  (*| BLoopL of 'a * 'a list * 'a list * label*)
  | BPattern of 'a list * 'a list * label
  | BIs of label
  | BEnd of label
  | BEndLambda of label

let prim_macro =
  List.map (fun (s, p) -> ((p : primitive :> primitive_or_macro), s)) primitives

let rec string_of_micheline (m : micheline) : string =
  match m with
  | Mprim { prim = n; args; annots } ->
    let s = List.assoc n prim_macro in
    let l = List.map string_of_micheline args in
    "(" ^ s ^ String.concat " " annots ^ String.concat " " l ^ ")"
  | Mstring s -> s
  | Mint i -> Z.to_string i
  | Mbytes _b -> "0x00"
  | Mseq miche_l ->
    let l = List.map string_of_micheline miche_l in
    "{" ^ String.concat " " l ^ "}"
