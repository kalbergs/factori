open Tzfunc.Proto
open Unstack_micheline
open Factori_errors

let name_cpt = ref 0

let new_name () =
  let name = Format.sprintf "var_%d" !name_cpt in
  incr name_cpt ;
  name

type t =
  | Name of string * micheline
  | Pair of t list * string list
  | Or of t * t * string list
  | Option of t * string list

let rec construct (m : micheline) : t =
  match m with
  | Mprim { prim = `pair; args; annots } ->
    let l = List.map construct args in
    Pair (l, annots)
  | Mprim { prim = `or_; args = [arg1; arg2]; annots } ->
    let arg1 = construct arg1 in
    let arg2 = construct arg2 in
    Or (arg1, arg2, annots)
  | Mprim { prim = `option; args = [arg]; annots } ->
    Option (construct arg, annots)
  | _ -> Name (new_name (), m)

let name (arg : t) : string option =
  match arg with
  | Name (str, _) -> Some str
  | _ -> None

let rec deconstruct (arg : t) : micheline =
  match arg with
  | Name (_, m) -> m
  | Pair (ty_l, annots) ->
    let args = List.map deconstruct ty_l in
    Mprim { prim = `pair; args; annots }
  | Or (left, right, annots) ->
    let left = deconstruct left in
    let right = deconstruct right in
    Mprim { prim = `or_; args = [left; right]; annots }
  | Option (opt, annots) ->
    let opt = deconstruct opt in
    Mprim { prim = `option; args = [opt]; annots }

let name_string_list (arg : t list) : name_arg list =
  let rec bring_all (acc : name_arg list) (next_arg : t list) : name_arg list =
    match next_arg with
    | [] -> acc
    | arg :: next_arg -> (
      match arg with
      | Name (s, ty) -> bring_all ((s, ty) :: acc) next_arg
      | Pair (l, _) -> bring_all acc (List.rev_append l next_arg)
      | Or (l, r, _) -> bring_all acc (l :: r :: next_arg)
      | Option (opt, _) -> bring_all acc (opt :: next_arg)) in
  bring_all [] arg

let to_name_arg (t : t) : name_arg option =
  match t with
  | Name (arg, ty) -> Some (arg, ty)
  | _ -> None

let match_or (l : t) =
  match l with
  | Or (l, r, _) -> Some (l, r)
  | _ -> None

let make_or ?(annots = []) (l : t) (r : t) = Or (l, r, annots)

let unconstruct_or (l : t) =
  match l with
  | Or (l, r, _) -> Some (l, r)
  | _ -> None

let simple ((n, m) : name_arg) : t = Name (n, m)

let rec to_string (arg : t) : string =
  match arg with
  | Name (s, _) -> s
  | Pair (l, _) ->
    let l = List.map to_string l in
    "Pair ( " ^ String.concat ", " l ^ " )"
  | Or (l, r, _) -> "Or (" ^ to_string l ^ ")  (" ^ to_string r ^ ")"
  | Option (opt, _) -> "Option (" ^ to_string opt ^ ")"

let pair_simple = function
  | [] -> Error (UnstackMicheline "Canno't pair 0 argument")
  | [x] -> Ok x
  | arg_l -> Ok (Pair (arg_l, []))

let unpair_arg n pair =
  let unpair_fun = function
    | Pair (l, _) -> Ok l
    | _ -> Error (UnstackMicheline "Arg is not of type pair") in
  let res = Type_primitive.split_pair_at_general n unpair_fun pair_simple pair in
  match res with
  | Ok res -> res
  | Error exn -> raise exn

let rec gets_pair n ty =
  if Z.equal n Z.zero then
    Ok ty
  else
    match ty with
    | Pair (x :: _, _) when Z.equal n Z.one -> Ok x
    | Pair (_ :: sub_miche_l, _) ->
      let new_n = Z.sub n (Z.of_int 2) in
      Result.bind (pair_simple sub_miche_l) (gets_pair new_n)
    | Name (s, _) ->
      Error (UnstackMicheline ("Empty" ^ s ^ " " ^ Z.to_string n))
    | _ ->
      Error
        (UnstackMicheline
           (Format.sprintf "Not good for a GET n instruction here %s"
              (to_string ty)))

let rec equal t1 t2 =
  match (t1, t2) with
  | Name (s1, _), Name (s2, _) -> Stdlib.compare s1 s2
  | Pair (ty_l1, _), Pair (ty_l2, _) ->
    let l = List.compare_lengths ty_l1 ty_l2 in
    if l = 0 then
      List.fold_left2
        (fun acc l1 l2 ->
          if acc = 0 then
            equal l1 l2
          else
            acc)
        0 ty_l1 ty_l2
    else
      l
  | Or (t_l1, t_r1, _), Or (t_l2, t_r2, _) ->
    let temp = equal t_l1 t_l2 in
    if temp = 0 then
      equal t_r1 t_r2
    else
      temp
  | Option (t1, _), Option (t2, _) -> equal t1 t2
  | Name _, _ -> -1
  | _, Name _ -> 1
  | Option _, _ -> -1
  | _, Option _ -> 1
  | Or _, _ -> -1
  | _, Or _ -> 1

let recup_parameter param_storage =
  match gets_pair (Z.of_int 1) param_storage with
  | Error exn -> raise exn
  | Ok res -> res

let updates_pair n new_ty ty =
  let rec aux_update acc n ty =
    if Z.equal n Z.zero then
      pair_simple (List.rev_append acc [ty])
    else
      match ty with
      | Pair (_ :: sub_miche_l, _) when Z.equal n Z.one ->
        pair_simple (List.rev_append acc (new_ty :: sub_miche_l))
      | Pair (x :: sub_miche_l, _) ->
        let new_n = Z.sub n (Z.of_int 2) in
        Result.bind (pair_simple sub_miche_l) (aux_update (x :: acc) new_n)
      | _ -> Error (UnstackMicheline "Not good for a UPDATE n instruction")
  in
  aux_update [] n ty

(*let map_nested_node (f :  (string * micheline) -> *)

module Ordered_nested = struct
  type nonrec t = t

  let compare = equal
end
