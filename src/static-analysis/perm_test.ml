open Auxilary
open Unstack_micheline
open Permission_analyze
module NameArg = Lib_unstack.NameArg

type prop =
  | Ident of name_arg
  | Propagate of Tzfunc.Proto.primitive_or_macro * args * prop list

let rec equal_prop (p : prop) (p2 : prop) =
  match (p, p2) with
  | Ident (n, _), Ident (n1, _) -> n = n1
  | Propagate (p, _, prop_l), Propagate (p2, _, prop_l2) ->
    if List.compare_lengths prop_l prop_l2 = 0 then
      p = p2 && List.for_all2 equal_prop prop_l prop_l2
    else
      false
  | _ -> false

let rec print_prop (ppf : Format.formatter) (prop : prop) =
  match prop with
  | Ident arg ->
    Format.fprintf ppf "(%s,%s)" (fst arg)
      (Unstack_micheline.string_of_micheline (snd arg))
  | Propagate (prim, _, prop_l) ->
    let print_l =
      Format.pp_print_list
        ~pp_sep:(fun format () -> Format.fprintf format ", ")
        print_prop in
    Format.fprintf ppf "%s (%a)"
      (List.assoc prim Unstack_micheline.prim_macro)
      print_l prop_l

let prof_prop (prop : prop) =
  let break_prop acc = function
    | Ident _ -> acc
    | Propagate (_, _, p) -> List.rev_append p acc in
  let rec aux_prof prof prop_l =
    let all = List.fold_left break_prop [] prop_l in
    match all with
    | [] -> prof
    | _ -> aux_prof (prof + 1) all in
  aux_prof 1 [prop]

let alone_arg arg =
  match Nested_arg.name_string_list arg with
  | [res] -> Some res
  | _ -> None

module Constant = Map.Make (struct
  type t = name_arg

  let compare (n, _) (n1, _) = compare n n1
end)

module Result = Set.Make (struct
  type t = prop Permission_tool.t

  let compare = compare
end)

module Perm = struct
  module SA = Auxilary.Aux

  type t = Result.t * prop Constant.t * Nested_arg.t fail_map

  let print ppf (t, cst, map) =
    let elems = Result.elements t in
    let cst_elems = Constant.bindings cst in
    Format.fprintf ppf "{%a}, {%a}, {%a}"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
         (Permission_tool.print print_prop))
      elems
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
         (fun ppf (l, s) -> Format.fprintf ppf "%s -> %a" (fst l) print_prop s))
      cst_elems
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
         (fun ppf (l, s) ->
           Format.fprintf ppf "%d -> %a" l
             (Permission_tool.print (fun ppf elt ->
                  Format.fprintf ppf "%s" (Nested_arg.to_string elt)))
             s))
      (M.bindings map)

  let lub ((a, b, c) : t) ((d, e, f) : t) =
    ( Result.union a d,
      Constant.union
        (fun _l elt_b elt_e ->
          if prof_prop elt_b > prof_prop elt_e then
            Some elt_b
          else
            Some elt_e)
        b e,
      M.union (fun _ l _ -> Some l) c f )
  (* in this analyse c and f should always be the same *)

  let leq ((a, b, e) : t) ((c, d, f) : t) =
    let result_check = Result.for_all (fun elt -> Result.mem elt c) a in
    if not result_check then
      false
    else
      let map_check = M.for_all (fun a _ -> Option.is_some (M.find_opt a f)) e in
      if not map_check then
        false
      else
        Constant.for_all (fun a _ -> Option.is_some (Constant.find_opt a d)) b

  let bot : t = (Result.empty, Constant.empty, M.empty)

  let union (a : t) (b : t) : t = lub a b

  let diff ((a, b, c) : t) ((d, e, f) : t) : t =
    if M.is_empty c then
      (Result.diff a d, Constant.union (fun _ _ _ -> None) b e, f)
    else
      (Result.diff a d, Constant.union (fun _ _ _ -> None) b e, c)
  (* in this analyse b and d should always be the same *)

  let iota_view (_tool : SA.analysis_tools) (_lab : label) : t =
    (Result.empty, Constant.empty, M.empty)

  let ext_view (tool : SA.analysis_tools) : label list =
    List.map (fun (_, l_end, _) -> l_end) (SA.view_labels tool)

  (*let appears_in (new_elt : prop) (arg : Nested_arg.t) (elt : Result.elt) =
    let rec aux_appears next =
      match next with
      | Ident n ->
        if Nested_arg.equal n arg = 0 then
          (new_elt, true)
        else
          (next, false)
      | Propagate (prim, l) ->
        let l = List.map aux_appears l in
        let result, conserve = List.split l in
        let conserve =
          List.fold_left (fun acc bool -> bool || acc) false conserve in
        (Propagate (prim, result), conserve) in
    let f elt =
      let res, conserve = aux_appears elt in
      if conserve then
        Some res
      else
        None in
    Permission_tool.map_opt f elt*)

  let iota (tool : SA.analysis_tools) : t =
    let sc_node = SA.get_original tool in
    let map = Permission_analyze.permission_check sc_node.node_l in
    (*M.iter
      (fun _l elt ->
        Permission_tool.print
          (fun ppf elt -> Format.fprintf ppf "%s" (Nested_arg.to_string elt))
          Format.err_formatter elt)
      map ;*)
    (Result.empty, Constant.empty, map)

  let make_simple prim args arg_l =
    let arg_l = List.map (fun arg -> Ident arg) arg_l in
    Propagate (prim, args, arg_l)

  let gen ((previous, constant, map) : t) (lab : label)
      (tool : SA.analysis_tools) : t =
    let block = SA.block_of_label lab tool in
    match block with
    | BAssign (N_SIMPLE (`FAILWITH, _, [_], [], lo), _l) -> (
      (*let _ = Format.eprintf "%d label_in fail %d label in bloock fail@." lo l in*)
      match M.find_opt lo map with
      | None -> (previous, constant, map)
      | Some perm -> (
        let new_elt =
          Permission_tool.map_opt
            (fun arg ->
              let alone = alone_arg [arg] in
              Option.bind alone (fun fail_arg ->
                  Constant.find_opt fail_arg constant))
            perm in
        match new_elt with
        | None -> (previous, constant, map)
        | Some e -> (Result.add e previous, constant, map)))
    | BAssign (N_SIMPLE (prim, args, used, elt, _), _) -> (
      let used_nested = Nested_arg.name_string_list used in
      let alone_elt = alone_arg elt in
      let compar = List.compare_lengths used_nested used in
      match (alone_elt, compar) with
      | Some res, 0 ->
        let used_prop =
          List.map
            (fun elt ->
              match Constant.find_opt elt constant with
              | None -> Ident elt
              | Some prop -> prop)
            used_nested in
        let simple = Propagate (prim, args, used_prop) in
        (previous, Constant.add res simple constant, map)
      | _ -> (previous, constant, map))
    | _ -> (previous, constant, map)

  let f_entry_analyze (tool : SA.analysis_tools) (chain : chain_call)
      (li : label) (le : label) (lab : label) (base_analyze : t) : t =
    (* link le resultat avec les potentiels fail imaginons que l'on fail sur le resultat de cette lambda
            alors préciser genre si x1 = EXEC l arg IF x1 then fail else continue dans notre map on sait que x1 a provoquer
       le fail donc link x1 avec res et dire que sur cette chain d'appel c'est res qui a fait fail le point d'entrée*)
    let block = SA.block_of_label lab tool in
    match block with
    | BStartExec (arg, name, _label) -> (
      let complete_lambdas =
        SA.find_lambda tool chain (Option.get @@ Nested_arg.to_name_arg name)
      in
      let opt =
        List.find_opt
          (fun lambda ->
            match lambda.lambda_code with
            | None -> false
            | Some l -> l.label_start = li && l.label_end = le)
          complete_lambdas in
      match opt with
      | Some { lambda_code = Some code; in_between; _ } ->
        let res, _, map = base_analyze in
        let arg_exec =
          Nested_arg.name_string_list (List.rev (arg :: in_between)) in
        let arg_lambda = Nested_arg.name_string_list [code.arg] in
        let constant =
          List.fold_left2
            (fun constant arg arg_lambda ->
              Constant.add arg_lambda (Ident arg) constant)
            Constant.empty arg_exec arg_lambda in
        (res, constant, map)
      | _ -> base_analyze)
    | _ -> failwith "Should be a start exec block"

  let f_return_analyze (tool : SA.analysis_tools) (_chain : chain_call)
      (_li : label) (_le : label) (lab : label)
      ((before_call, after_call) : t * t) : t =
    (* link les variables qui apparaissent dans previous à celles en argument genre si dans la lambda on fail quand SENDER = x1 et que la
       lambda a été appelé avec x1 = admin alors dire plutot qu'on fail sur SENDER = admin *)
    let block = SA.block_of_label lab tool in
    match block with
    | BEndExec (_res, _name, _label) ->
      let _, constant, map = before_call in
      let res, _, _ = after_call in
      (res, constant, map)
    | _ -> failwith "Should be an end exec block"

  let kill (lab : label) (tool : SA.analysis_tools) : t =
    let _block = SA.block_of_label lab tool in
    (Result.empty, Constant.empty, M.empty)

  let lambda_return (_, _, le, lret) = (lret, le)

  let lambda_entrance (lc, li, _, _) = (lc, li)

  let ext (tool : SA.analysis_tools) : label list = [snd (SA.get_init tool)]

  let flow (tool : SA.analysis_tools) : FlowSet.t = SA.get_flow tool

  let f (tool : SA.analysis_tools) (lab : label) (k : t) : t = gen k lab tool
end

include Perm
