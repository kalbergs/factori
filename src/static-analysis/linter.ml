open Tzfunc.Proto
open Factori_errors
open Tezos_micheline
module ForbiddenAnnot = Set.Make (String)
module ExistingAnnot = Map.Make (String)
open Micheline

type lint_err =
  | MultipleTypeForAnnotation of {
      origin : Micheline_parser.location;
      annotation : string;
      location : Micheline_parser.location;
      expected : Micheline_printer.node;
      actual : Micheline_printer.node;
    }
  | MissingAnnotation of {
      location : Micheline_parser.location;
      on : Micheline_printer.node;
    }
  | ForbiddenAnnotation of {
      location : Micheline_parser.location;
      annotation : string;
      actual : Micheline_printer.node;
    }
  | WrongOrder of string option * string * string

let make_printable node =
  let node = Micheline.strip_locations node in
  let printable_node = Micheline_printer.printable (fun s -> s) node in
  printable_node

let print_lint_err ppf err_lint =
  Format.fprintf ppf "Warning : " ;
  match err_lint with
  | MultipleTypeForAnnotation { location; origin; annotation; expected; actual }
    ->
    Micheline_parser.print_location ppf origin ;
    Format.fprintf ppf ", the %s annotation was defined like a " annotation ;
    Micheline_printer.print_expr ppf expected ;
    Format.fprintf ppf ". But " ;
    Micheline_parser.print_location ppf location ;
    Format.fprintf ppf " the same annotation is of type " ;
    Micheline_printer.print_expr ppf actual ;
    Format.pp_print_newline ppf ()
  | MissingAnnotation { location; on } ->
    Micheline_parser.print_location ppf location ;
    Format.fprintf ppf ", there is no annotation on type " ;
    Micheline_printer.print_expr ppf on ;
    Format.pp_print_newline ppf ()
  | ForbiddenAnnotation { location; annotation; actual = _ } ->
    Micheline_parser.print_location ppf location ;
    Format.fprintf ppf ", %s is a forbidden annotation.@." annotation
  | WrongOrder (None, expected, actual) ->
    Format.fprintf ppf
      "The first section of a contract was expected to be the %s but got the \
       %s section@."
      expected actual
  | WrongOrder (Some before, expected, actual) ->
    Format.fprintf ppf
      "After the %s section, the %s was expected but got the %s section@."
      before expected actual

let forbid_annot_set =
  List.fold_left
    (fun acc (s, _) -> ForbiddenAnnot.add (String.uncapitalize_ascii s) acc)
    ForbiddenAnnot.empty primitives

let is_forbidden elt = ForbiddenAnnot.mem elt forbid_annot_set

let dive_in_composition miche prim =
  let rec aux_dive acc = function
    | [] -> acc
    | (prof, node) :: next_prims -> (
      match node with
      | Prim (_, prim, args, annots) when prim = miche -> (
        let annot_opt = Factori_utils.get_name_annots annots in
        match annot_opt with
        | None ->
          let args = List.map (fun arg -> (prof + 1, arg)) args in
          aux_dive acc (List.rev_append args next_prims)
        | Some _ -> aux_dive ((prof, node) :: acc) next_prims)
      | Prim _ -> aux_dive ((prof, node) :: acc) next_prims
      | _ ->
        raise
          (GenericError ("dive_in_composition", "Unexpected node for a type")))
  in
  aux_dive [] [(0, prim)]

let compute_next_node node args =
  match node with
  | Prim (loc, "pair", args, _) ->
    let next = dive_in_composition "pair" (Prim (loc, "pair", args, [])) in
    let simple_pair = List.for_all (fun (prof, _) -> prof = 1) next in
    let next_node = List.map (fun (_, prim) -> (not simple_pair, prim)) next in
    next_node
  | Prim (loc, "or", args, _) ->
    let next = dive_in_composition "or" (Prim (loc, "or", args, [])) in
    let next_node = List.map (fun (_, prim) -> (true, prim)) next in
    next_node
  | _ ->
    let next = List.map (fun prim -> (false, prim)) args in
    next

let check_if_same_prim location annotation error_list origin prim node =
  let cannonical_node = Micheline.strip_locations node in
  let cannonical_prim = Micheline.strip_locations prim in
  if cannonical_node <> cannonical_prim then
    MultipleTypeForAnnotation
      {
        location;
        origin;
        annotation;
        expected = make_printable prim;
        actual = make_printable node;
      }
    :: error_list
  else
    error_list

let verify_annotation_on_prim map p =
  let rec verify_prim_l map acc = function
    | [] -> (map, acc)
    | (need_annot, node) :: next_prims -> (
      match node with
      | Prim (location, _prim, args, annots) ->
        let name_annot = Factori_utils.get_name_annots annots in
        let acc =
          match name_annot with
          | None when need_annot ->
            MissingAnnotation { location; on = make_printable node } :: acc
          | Some e when is_forbidden e ->
            ForbiddenAnnotation
              { location; annotation = e; actual = make_printable node }
            :: acc
          | Some e ->
            let opt = ExistingAnnot.find_opt e map in
            Option.fold ~none:acc
              ~some:(fun (origin, prim) ->
                check_if_same_prim location e acc origin prim node)
              opt
          | _ -> acc in
        let map =
          Option.fold ~none:map
            ~some:(fun annot ->
              ExistingAnnot.update annot
                (function
                  | None -> Some (location, node)
                  | e -> e)
                map)
            name_annot in
        let next_node = compute_next_node node args in
        verify_prim_l map acc (List.rev_append next_node next_prims)
      | _ -> verify_prim_l map acc next_prims) in
  verify_prim_l map [] [(false, p)]

let verify_program node_l =
  let store =
    List.find_map
      (function
        | Prim (_, "storage", [node_store], _) -> Some node_store
        | _ -> None)
      node_l in
  let param =
    List.find_map
      (function
        | Prim (_, "parameter", [node_param], _) -> Some node_param
        | _ -> None)
      node_l in
  let map, acc_store =
    verify_annotation_on_prim ExistingAnnot.empty (Option.get store) in
  let _map, acc_param = verify_annotation_on_prim map (Option.get param) in
  match List.rev_append acc_param acc_store with
  | [] -> ()
  | l ->
    Format.pp_print_list ~pp_sep:Format.pp_print_newline print_lint_err
      Format.err_formatter l

let next_node_in_sc = function
  | "parameter" -> "storage"
  | "storage" -> "code"
  | "code" | "view" -> "view"
  | _ ->
    raise
      (GenericError
         ( "next_node_in_sc",
           "Wrong primitive name in a smart contract michelson" ))

let verify_order node_l =
  let rec aux_order before next = function
    | [] -> None
    | node :: next_node -> (
      match node with
      | Prim (_, prim, _, _) when prim = next ->
        aux_order (Some prim) (next_node_in_sc next) next_node
      | Prim (_, prim, _, _) -> Some (WrongOrder (before, next, prim))
      | _ ->
        raise
          (GenericError
             ( "next_node_in_sc",
               "Wrong primitive name in a smart contract michelson" ))) in
  let res_opt = aux_order None "parameter" node_l in
  match res_opt with
  | None -> ()
  | Some err -> print_lint_err Format.err_formatter err

let lint node_l =
  verify_order node_l ;
  verify_program node_l
