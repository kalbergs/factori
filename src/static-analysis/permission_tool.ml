open Unstack_micheline

type or_ =
  | Left
  | Right

type permission =
  | True
  | Is_none
  | Is_left
  | Is_cons
  | Not_empty

type 'a t =
  | Exec_fail of 'a * label * label
  | Will_fail of permission * 'a * label
  | Other_fail of permission * 'a * label

let map (f : 'a -> 'b) (l : 'a t) : 'b t =
  match l with
  | Exec_fail (elt, l1, l2) -> Exec_fail (f elt, l1, l2)
  | Will_fail (perm, elt, l) -> Will_fail (perm, f elt, l)
  | Other_fail (perm, elt, l) -> Other_fail (perm, f elt, l)

let map_opt (f : 'a -> 'b option) (l : 'a t) : 'b t option =
  match l with
  | Exec_fail (elt, l1, l2) ->
    Option.map (fun elt -> Exec_fail (elt, l1, l2)) (f elt)
  | Will_fail (perm, elt, l) ->
    Option.map (fun elt -> Will_fail (perm, elt, l)) (f elt)
  | Other_fail (perm, elt, l) ->
    Option.map (fun elt -> Other_fail (perm, elt, l)) (f elt)

let get_arg (elt : 'a t) =
  match elt with
  | Exec_fail (elt, _, _) | Will_fail (_, elt, _) | Other_fail (_, elt, _) ->
    elt

let string_of_permission = function
  | True -> "true"
  | Is_none -> "None"
  | Is_left -> "left"
  | Is_cons -> "Not empty"
  | Not_empty -> "Not empty"

let print (f : Format.formatter -> 'a -> unit) (ppf : Format.formatter)
    (elt : 'a t) : unit =
  match elt with
  | Exec_fail (elt, _l1, _l2) -> Format.fprintf ppf "Fail on call %a" f elt
  | Will_fail (perm, elt, _l) ->
    Format.fprintf ppf "Fail if %a is %s" f elt (string_of_permission perm)
  | Other_fail (perm, elt, _l) ->
    Format.fprintf ppf "Fail if %a is not %s" f elt (string_of_permission perm)
