open Tzfunc.Proto
open Unstack_micheline
open Unstack_iterator

let abs_instructions fold () acc node =
  match node with
  | N_SIMPLE (`ABS, _, _, _, _) -> acc + 1
  | _ -> default_folder.node_fold fold () acc node

let abs_folder = { default_folder with node_fold = abs_instructions }

module WrongUse = Map.Make (String)

let add_wrong m annot map =
  WrongUse.update m
    (function
      | None -> Some [annot]
      | Some e -> Some (annot :: e))
    map

let rec storage_check acc m =
  match m with
  | Mprim { prim = `map; args; annots } ->
    let acc = add_wrong "map" (Factori_utils.get_name_annots annots) acc in
    List.fold_left storage_check acc args
  | Mprim { prim = `set; args; annots } ->
    let acc = add_wrong "set" (Factori_utils.get_name_annots annots) acc in
    List.fold_left storage_check acc args
  | Mprim { prim = `list; args; annots } ->
    let acc = add_wrong "list" (Factori_utils.get_name_annots annots) acc in
    List.fold_left storage_check acc args
  | Mprim { args; _ } -> List.fold_left storage_check acc args
  | _ -> acc

let abs_presence sc =
  let nb = abs_folder.node_seq abs_folder () 0 sc.node_l in
  if nb > 0 then
    Format.eprintf "There is %d uses of ABS instruction@." nb
  else
    ()

let check_storage sc =
  let map = storage_check WrongUse.empty sc.storage in
  if WrongUse.is_empty map then
    ()
  else (
    Format.eprintf "Warning : Found unserializable type in the storage@." ;
    let format = Format.err_formatter in
    WrongUse.iter
      (fun k elt ->
        Format.fprintf format "%s is used %d times in the storage@.%a@." k
          (List.length elt)
          (Format.pp_print_list ~pp_sep:Format.pp_print_newline
             (fun format annot ->
               match annot with
               | None -> ()
               | Some field ->
                 Format.fprintf format "%s field is a %s in the storage" field k))
          elt)
      map
  )
