open Tzfunc.Proto
open Factori_errors
open Unstack_micheline

let se_ref = ref None

let cpt = ref 0

let new_id id =
  let res = !id in
  incr id ;
  res

let debug = ref (Mprim { prim = `storage; args = []; annots = [] })

let print_stack l =
  Format.pp_print_list
    ~pp_sep:(fun format () -> Format.fprintf format " :: ")
    (fun format elt ->
      Format.fprintf format "%s" (string_of_micheline (snd elt)))
    l

let name_from s =
  let res = new_id cpt in
  Format.sprintf "%s_%d" s res

let name_stack () = name_from "var"

let top_tail_of_z z l =
  let rec aux_split acc cpt l =
    if Z.compare cpt Z.zero <= 0 then
      Some (List.rev acc, l)
    else
      match l with
      | [] -> None
      | x :: xs -> aux_split (x :: acc) (Z.pred cpt) xs in
  aux_split [] z l

let no_annot node = node

let decomp_pair label _annots (n : Z.t) stack =
  let res_opt = top_tail_of_z n stack in
  match res_opt with
  | None -> Error (UnstackMicheline "Not enough element in stack for PAIR")
  | Some (parameter, new_stack) ->
    Result.bind
      (Type_primitive.pair_or_simple
         (List.map (fun (_, miche) -> miche) parameter))
      (fun pair ->
        let return = (name_stack (), pair) in
        (*let node = N_PAIRS (parameter, return) in*)
        let node = N_SIMPLE (`PAIR, [], parameter, [return], label) in
        Ok (Some (return :: new_stack), [node], label + 1))

let decomp_unpair label _annots n stack =
  match stack with
  | [] -> Error (UnstackMicheline "Empty stack for unpair")
  | (top, ty) :: xs -> (
    let ty_l = Type_primitive.split_pair_at n ty in
    (* let tops, tl = Utility.split_at n ty_l in *)
    match ty_l with
    | Error _ -> failwith ("UNPAIR " ^ Z.to_string n)
    | _ ->
      Result.bind ty_l (fun ty_l ->
          let l =
            List.map
              (fun ty ->
                let name = name_stack () in
                (name, ty))
              ty_l in
          (* let stack =
             match tl with
             | [] -> xs
             | _ ->
               let ty = (Pair tl, []) in
               let top = (name_from "remain", ty) in
               top :: xs in *)
          (*let node = N_UNPAIRS ((top, ty), l) in*)
          let node = N_SIMPLE (`UNPAIR, [], [(top, ty)], l, label) in
          Ok (Some (l @ xs), [node], label + 1)))

let stack_with_pos stack =
  let stack = List.rev stack in
  let rev_stack = List.mapi (fun i (name, ty) -> (i, name, ty)) stack in
  List.rev rev_stack

(* return the positions of what changed ?*)
let compare_stack stack1 stack2 =
  let stack1 = stack_with_pos stack1 and stack2 = stack_with_pos stack2 in
  let rec aux_stack cpt acc = function
    | [] -> acc
    | (pos1, name1, _ty1) :: xs ->
      let is_new =
        not
          (List.exists
             (fun (pos2, name2, _ty2) -> pos1 == pos2 && name1 = name2)
             stack1) in
      if is_new then
        aux_stack (cpt + 1) (cpt :: acc) xs
      else
        aux_stack (cpt + 1) acc xs in
  aux_stack 0 [] stack2

let apply_at_pos f pos stack =
  let opt = top_tail_of_z (Z.of_int pos) stack in
  match opt with
  | None | Some (_, []) -> Error (UnstackMicheline "Stack is too short")
  | Some (tops, x :: xs) -> Ok (tops @ (f x :: xs))
(* List.filter
   (fun (name1, ty1 ) ->
     not
       (List.exists
          (fun (name2, ty2) ->
            name1 = name2 && TypeMichelson.eq_michety ty1 ty2)
          stack1))
   stack2 *)

let change_fun (_, michety) = (name_stack (), michety)

let name_opt name opt =
  match opt with
  | Some (`VarAnnot e) -> name_from e
  | _ -> name

let decompile_if all_pos node stack =
  let rev_node = List.rev node in
  match rev_node with
  | N_END (_, lab) :: xs ->
    let recup stack =
      let size = List.length stack in
      List.fold_left
        (fun acc i ->
          if i >= size then
            acc
          else
            List.nth stack i :: acc)
        [] all_pos in
    let recup_stack = recup stack in
    let new_node = List.rev (N_END (recup_stack, lab) :: xs) in
    Ok new_node
  | _ -> Error (UnstackMicheline "IF branch should end with an END node")

let compile_if all_pos node_then stack_then node_else stack_else =
  let rev_else = List.rev node_else in
  let rev_then = List.rev node_then in
  match (rev_then, rev_else) with
  | N_END (_, lab_then) :: xs, N_END (_, lab_end) :: ys ->
    let recup stack =
      let size = List.length stack in
      (* can fail if i is negative *)
      List.fold_left
        (fun acc i ->
          if i >= size then
            acc
          else
            List.nth stack i :: acc)
        [] all_pos in
    let recup_then = recup stack_then in
    let recup_else = recup stack_else in
    let node_then = List.rev (N_END (recup_then, lab_then) :: xs) in
    let node_else = List.rev (N_END (recup_else, lab_end) :: ys) in
    Ok (node_then, node_else)
  | _ -> Error (UnstackMicheline "IF branch should end with an END node")

let compile_binary label name stack cons type_fun =
  match stack with
  | (name1, ty1) :: (name2, ty2) :: xs ->
    let ty = type_fun ty1 ty2 in
    Result.bind ty (fun ty ->
        let top = (name, ty) in
        let node = cons (name1, ty1) (name2, ty2) top in
        Ok (Some (top :: xs), [node], label + 1))
  | _ -> Error (UnstackMicheline "Stack is too short")

let compile_eq_prim label name stack cons =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | x :: xs ->
    let ty = Mprim { prim = `bool; args = []; annots = [] } in
    let top = (name, ty) in
    let node = cons x top in
    Ok (Some (top :: xs), [node], label + 1)

let compile_unary label name stack type_fun cons =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | (name_arg, ty_arg) :: xs ->
    let ty = type_fun ty_arg in
    Result.bind ty (fun ty ->
        let top = (name, ty) in
        let node = cons (name_arg, ty_arg) top in
        Ok (Some (top :: xs), [node], label + 1))

let post_decomp new_stack arg_l =
  let new_stack, recup =
    List.fold_left_map
      (fun stack i ->
        let item = List.nth new_stack i in
        let name, ty = change_fun item in
        let stack =
          Result.bind stack (apply_at_pos (fun (_, ty) -> (name, ty)) i) in
        (stack, (name, ty)))
      (Ok new_stack) arg_l in
  (List.rev recup, new_stack)

(* let make_args ty =
   match ty with
   | Pair ty_l, [] ->
     List.map (fun ty -> (name_from "arg", ty\0)) (TypeMichelson.flat_ty_l ty_l)
   | _ -> [(name_from "arg", ty)] *)

let loop_left_post label_lf top stack new_stack node_loop pat_left right =
  (*top is the top before loop left, stack is the stack before loop left without the top
     new stack is the stack after the loop left pat is the pattern use in the loop left
     and right is the type of the or right*)
  let label_pat_lf = label_lf + 1 in
  let label_pat_res = label_pat_lf + 1 in
  let label_node = label_pat_res + 1 in
  let pat_right = (name_from "pat_right", right) in
  match new_stack with
  | None ->
    let node =
      N_LOOP_LEFT
        {
          arg_lf = top;
          pat_lf = (pat_left, label_pat_lf);
          acc_lf = [];
          pat_acc_lf = [];
          node_loop;
          pat_res_lf = (pat_right, label_pat_res);
          label_lf;
        } in
    Ok (None, [FAIL_CONTROL (node, label_node)], label_node + 1)
  | Some new_stack -> (
    match new_stack with
    | [] ->
      Error (UnstackMicheline "Not implemented yet for loop_left instruction")
    | _ :: xs ->
      let res = (name_from "res_loop", right) in
      let arg_l = compare_stack stack xs in
      let acc = List.rev_map (fun i -> List.nth stack i) arg_l in
      let pat_acc = List.map (fun (_, ty) -> (name_stack (), ty)) acc in
      let node_loop = Lib_unstack.replace_ident node_loop acc pat_acc in
      let recup, new_stack = post_decomp xs arg_l in
      let loop_l =
        N_LOOP_LEFT
          {
            arg_lf = top;
            pat_lf = (pat_left, label_pat_lf);
            pat_acc_lf = pat_acc;
            acc_lf = acc;
            node_loop;
            pat_res_lf = (pat_right, label_pat_res);
            label_lf;
          } in
      let node = N_START (res :: recup, loop_l, label_node) in
      let node_annot = no_annot node in
      Result.bind new_stack (fun new_stack ->
          Ok (Some (res :: new_stack), [node_annot], label_node + 1)))

(* we must return the top of the stack even if it didn't change *)
let return_stack_special label acc init_stack stack =
  match stack with
  | [] -> Error (UnstackMicheline "Empty return")
  | (name, ty) :: xs ->
    let ys = List.tl init_stack in
    let arg_l = compare_stack ys xs in
    let recup = List.fold_left (fun acc i -> List.nth xs i :: acc) [] arg_l in
    let new_recup_end = (name, ty) :: recup in
    Ok (Some stack, List.rev (N_END (new_recup_end, label) :: acc), label + 1)

let car_cdr stack _instr f =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | (name_top, ty) :: xs ->
    let ty_l = Type_primitive.split_pair_at (Z.of_int 2) ty in
    Result.bind ty_l (fun ty_l ->
        match ty_l with
        | [ty1; ty2] ->
          let top = (name_top, ty) in
          let res, node, label = f (ty1, ty2) top in
          Ok (Some (res :: xs), [node], label)
        | _ -> Error (UnstackMicheline "Unexpected result from split_pair_at"))

(* let choose_map arg_l =
   let size = List.length arg_l and zero_change = List.mem 0 arg_l in
   match (size, zero_change) with
   | 1, true -> MAP
   | _ -> FOLD_MAP *)

let rec michelson2node_if label_arg if_cons init_stack (instr_then, stack_then)
    (instr_else, stack_else) =
  let result_then = decompile_instr label_arg false stack_then instr_then in
  let post_if label_post recup node_then node_else new_stack =
    let node = N_START (recup, if_cons node_then node_else, label_post) in
    Ok (Some new_stack, [no_annot node], label_post + 1) in
  Result.bind result_then (fun (new_stack_then, node_then, label_then) ->
      let result_else = decompile_instr label_then false stack_else instr_else in
      Result.bind result_else (fun (new_stack_else, node_else, label_else) ->
          match (new_stack_then, new_stack_else) with
          | None, None ->
            Ok
              ( None,
                [FAIL_CONTROL (if_cons node_then node_else, label_else)],
                label_else + 1 )
          | None, Some stack ->
            let arg = compare_stack init_stack stack in
            let node_else = decompile_if arg node_else stack in
            let recup, new_stack = post_decomp stack arg in
            Result.bind node_else (fun node_else ->
                Result.bind new_stack
                  (post_if label_else recup node_then node_else))
            (*let node = N_START (recup, if_cons node_then node_else) in
                Ok (Some new_stack, [no_annot node])*)
          | Some stack, None ->
            let arg = compare_stack init_stack stack in
            let node_then = decompile_if arg node_then stack in
            let recup, new_stack = post_decomp stack arg in
            Result.bind node_then (fun node_then ->
                Result.bind new_stack
                  (post_if label_else recup node_then node_else))
            (*let node = N_START (recup, if_cons node_then node_else) in
                Ok (Some new_stack, [node])*)
          | Some new_stack_then, Some new_stack_else ->
            let arg_then = compare_stack new_stack_else new_stack_then in
            let arg_else = compare_stack new_stack_then new_stack_else in
            (*let arg_then = compare_stack init_stack new_stack_then in
              let arg_else = compare_stack init_stack new_stack_else in*)
            let all_pos =
              List.fold_left
                (fun acc arg ->
                  if List.mem arg acc then
                    acc
                  else
                    arg :: acc)
                arg_then arg_else in
            let result_compile =
              compile_if all_pos node_then new_stack_then node_else
                new_stack_else in
            Result.bind result_compile (fun (node_then, node_else) ->
                let recup, new_stack = post_decomp new_stack_then all_pos in
                Result.bind new_stack (fun new_stack ->
                    let node =
                      N_START (recup, if_cons node_then node_else, label_else)
                    in
                    Ok (Some new_stack, [node], label_else + 1)))))

and collection_application label_start name stack special instr_l cons =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | coll :: xs ->
    let name = name_from name in
    let ty = Type_primitive.type_arg_collection (snd coll) in
    Result.bind ty (fun ty ->
        let top = (name, ty) in
        let result = decompile_instr label_start special (top :: xs) instr_l in
        Result.bind result (fun (new_stack, node_l, label_end) ->
            match (new_stack, special) with
            | None, true ->
              Error
                (UnstackMicheline
                   "FAIL cannot be the only instruction in the body. The \
                    proper type of the return list cannot be inferred")
            | None, false ->
              Ok
                ( None,
                  [FAIL_CONTROL (cons (coll, [], [], top, node_l), label_end)],
                  label_end + 1 )
            | Some new_stack, _ ->
              let stack_cmp =
                if special then
                  List.tl new_stack
                else
                  new_stack in
              let arg_l = compare_stack xs stack_cmp in
              let recup, post_stack = post_decomp stack_cmp arg_l in
              let accumulator =
                List.fold_left
                  (fun acc i ->
                    let item = List.nth xs i in
                    item :: acc)
                  [] arg_l in
              let pat_acc =
                List.map (fun (_, ty) -> (name_stack (), ty)) accumulator in
              let node_l =
                Lib_unstack.replace_ident node_l accumulator pat_acc in
              let recup_stack =
                Result.bind post_stack (fun post_stack ->
                    if special then
                      let ty =
                        Type_primitive.type_map
                          (snd (List.hd new_stack))
                          (snd coll) in
                      Result.bind ty (fun ty ->
                          let name = name_stack () in
                          let top = (name, ty) in
                          Ok (top :: recup, top :: post_stack))
                    else
                      Ok (recup, post_stack)) in
              Result.bind recup_stack (fun (recup, new_stack) ->
                  let node =
                    N_START
                      ( recup,
                        cons (coll, accumulator, pat_acc, top, node_l),
                        label_end ) in
                  Ok (Some new_stack, [node], label_end + 1))))

and decomp_code_loop label stack instr_l =
  match stack with
  | (name, Mprim { prim = `bool; args = []; annots }) :: xs ->
    let label_start = label in
    let label_pat = label + 1 in
    let label_loop = label_pat + 1 in
    let result = decompile_instr (label_loop + 1) false xs instr_l in
    Result.bind result (fun (new_stack, node_code, label_ret) ->
        match new_stack with
        | None ->
          let top = (name, prim `bool) in
          let ctrl_node =
            N_START
              ( [],
                N_LOOP
                  {
                    arg = top;
                    acc = [];
                    pat_acc = [];
                    label_pat_l = label_pat;
                    body = node_code;
                    label_loop;
                  },
                label_start ) in
          Ok (None, [ctrl_node], label_ret)
        | Some new_stack ->
          let top = (name, Mprim { prim = `bool; args = []; annots }) in
          let arg_l = compare_stack stack new_stack in
          let acc = List.rev_map (fun i -> List.nth stack i) arg_l in
          let acc = List.tl acc in
          let pat_acc = List.map (fun (_, ty) -> (name_stack (), ty)) acc in
          let node_code = Lib_unstack.replace_ident node_code acc pat_acc in
          let res, new_stack = post_decomp new_stack arg_l in
          let node =
            N_LOOP
              {
                arg = top;
                acc;
                pat_acc;
                body = node_code;
                label_loop;
                label_pat_l = label_pat;
              } in
          let node = N_START (List.tl res, node, label_start) in
          Result.bind new_stack (function
            | [] -> Error (UnstackMicheline "Stack is empty")
            | _ :: tl -> Ok (Some tl, [node], label_ret)))
  | _ -> Error (UnstackMicheline "Not a good stack for a LOOP instruction")

and decomp_dip label_start n stack instr_l =
  let opt = top_tail_of_z n stack in
  match opt with
  | None ->
    Error (UnstackMicheline "Stack is not long enough for a DIP instruction")
  | Some (tops, xs) ->
    let result = decompile_instr label_start false xs instr_l in
    Result.bind result (fun (new_stack, node_l, label_end) ->
        match new_stack with
        | None ->
          Error
            (UnstackMicheline
               "Fail instruction must appear at a terminal position")
        | Some new_stack -> (
          match List.rev node_l with
          | N_END _ :: xs -> Ok (Some (tops @ new_stack), xs, label_end)
          | _ ->
            Error
              (UnstackMicheline "Instruction list does not finish with end node")
          ))

and decompile_instr label_init loop_left init_stack instr_l =
  let rec aux_decomp label acc stack instr_l =
    match instr_l with
    | [] -> (
      match stack with
      | None -> Ok (None, List.rev acc, label + 1)
      | Some stack when loop_left ->
        return_stack_special label acc init_stack stack
      | Some stack ->
        let arg_l = compare_stack init_stack stack in
        let recup =
          List.fold_left (fun acc i -> List.nth stack i :: acc) [] arg_l in
        Ok (Some stack, List.rev (N_END (recup, label) :: acc), label + 1))
    | Mseq seq :: xs -> aux_decomp label acc stack (seq @ xs)
    | x :: xs -> (
      match stack with
      | None ->
        Error
          (UnstackMicheline "Fail instruction should be at a terminal position")
      | Some stack ->
        let result = decompile label stack x in
        Result.bind result (fun (new_stack, node_l, label) ->
            aux_decomp label (node_l @ acc) new_stack xs)) in
  aux_decomp label_init [] (Some init_stack) instr_l

and decompile label stack instr =
  let name = name_stack () in
  match instr with
  | Mprim { prim = `DROP; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for DROP instruction")
    | _ :: tl -> Ok (Some tl, [], label))
  | Mprim { prim = `DROP; args = [Mint n]; _ } -> (
    let stack = top_tail_of_z n stack in
    match stack with
    | None ->
      Error (UnstackMicheline "Not enough element for a DROP instruction")
    | Some (_, new_stack) -> Ok (Some new_stack, [], label))
  | Mprim { prim = `DUP; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for DUP instruction")
    | top :: _ -> Ok (Some (top :: stack), [], label))
  | Mprim { prim = `DUP; args = [Mint n]; _ } -> (
    let position = Z.pred n in
    match List.nth_opt stack (Z.to_int position) with
    | None ->
      Error (UnstackMicheline "Wrong argument/ stack for a DUP instruction")
    | Some top -> Ok (Some (top :: stack), [], label))
  | Mprim { prim = `SWAP; args = []; _ } -> (
    match stack with
    | x :: y :: xs -> Ok (Some (y :: x :: xs), [], label)
    | _ -> Error (UnstackMicheline "Not a good stack for a SWAP instruction"))
  | Mprim { prim = `DIG; args = [Mint n]; _ } -> (
    let opt = top_tail_of_z n stack in
    match opt with
    | None | Some (_, []) -> Error (UnstackMicheline "Empty stack")
    | Some (tops, x :: xs) -> Ok (Some (x :: (tops @ xs)), [], label))
  | Mprim { prim = `DIP; args = [Mint n; Mseq instr_l]; _ } ->
    decomp_dip label n stack instr_l
  | Mprim { prim = `DIP; args = [Mseq instr_l]; _ } ->
    decomp_dip label Z.one stack instr_l
  | Mprim { prim = `DUG; args = [Mint n]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Empty stack")
    | x :: xs -> (
      let opt = top_tail_of_z n xs in
      match opt with
      | None ->
        Error (UnstackMicheline "Not enough element for a DUG instruction")
      | Some (tops, bot) ->
        let stack = tops @ (x :: bot) in
        Ok (Some stack, [], label)))
  | Mprim { prim = `PUSH; args = [ty; data]; _ } ->
    let top = (name, ty) in
    (*let node = N_PUSH (data, top) in*)
    let node = N_SIMPLE (`PUSH, [ty; data], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `SOME; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Empty stack")
    | (n, ty) :: xs ->
      let ty_opt = Type_primitive.make_option ty in
      Result.bind ty_opt (fun ty_opt ->
          let top = (name, ty_opt) in
          let node = N_SIMPLE (`SOME, [], [(n, ty)], [top], label) in
          (*let node = N_SOME ((n, ty), top) in*)
          Ok (Some (top :: xs), [node], label + 1)))
  | Mprim { prim = `NONE; args = [ty]; _ } ->
    let ty_opt = Type_primitive.make_option ty in
    Result.bind ty_opt (fun ty_opt ->
        let top = (name, ty_opt) in
        (*let _node = N_NONE (ty, top) in*)
        let node = N_SIMPLE (`NONE, [ty], [], [top], label) in
        Ok (Some (top :: stack), [node], label + 1))
  | Mprim { prim = `UNIT; args = []; _ } ->
    let top = (name, prim `unit) in
    let node = N_SIMPLE (`UNIT, [], [], [top], label) in
    (*let node = N_UNIT top in*)
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `UNPAIR; args = []; annots } ->
    decomp_unpair label annots (Z.of_int 2) stack
  | Mprim { prim = `UNPAIR; args = [Mint n]; annots } ->
    decomp_unpair label annots n stack
  | Mprim { prim = `PAIR; args = []; annots } ->
    decomp_pair label annots (Z.of_int 2) stack
  | Mprim { prim = `PAIR; args = [Mint n]; annots } ->
    decomp_pair label annots n stack
  | Mprim { prim = `NIL; args = [ty]; _ } ->
    let ty_l = prim `list ~args:[ty] in
    let top = (name, ty_l) in
    (*let node = N_NIL (ty, top) in*)
    let node = N_SIMPLE (`NIL, [ty], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `IF; args = [Mseq instr_then; Mseq instr_else]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let label_arg = label + 1 in
      michelson2node_if label_arg
        (fun n_then n_else ->
          let node = N_IF (x, label, n_then, n_else) in
          no_annot node)
        xs (instr_then, xs) (instr_else, xs))
  | Mprim { prim = `IF_NONE; args = [Mseq instr_then; Mseq instr_else]; _ } -> (
    match stack with
    | (name, (Mprim { prim = `option; args = [pat_ty]; _ } as ty)) :: xs ->
      let name_some = name_from "pat_some" in
      let top = (name_some, pat_ty) in
      let label_pat = label + 1 in
      let label_next = label_pat + 1 in
      let stack_some = top :: xs in
      michelson2node_if label_next
        (fun n_then n_else ->
          N_IF_NONE ((name, ty), label, n_then, n_else, top, label_pat))
        xs (instr_then, xs) (instr_else, stack_some)
    | _ -> Error (UnstackMicheline "Wrong stack for a IF_NONE instruction"))
  | Mprim { prim = `IF_CONS; args = [Mseq instr_c; Mseq instr_n]; _ } -> (
    match stack with
    | (name, (Mprim { prim = `list; args = [ty_x]; _ } as ty)) :: xs ->
      let name_x = name_from "hd" in
      let name_xs = name_from "tl" in
      let top = (name_x, ty_x) in
      let snd = (name_xs, ty) in
      let label_pat_x = label + 1 in
      let label_pat_xs = label_pat_x + 1 in
      let label_next = label_pat_xs + 1 in
      let stack_cons = top :: snd :: xs in
      michelson2node_if label_next
        (fun n_then n_else ->
          N_IF_CONS
            ( (name, ty),
              label,
              n_then,
              n_else,
              top,
              label_pat_x,
              snd,
              label_pat_xs ))
        xs (instr_c, stack_cons) (instr_n, xs)
    | _ -> Error (UnstackMicheline "Wrong stack for a IF_CONS instruction"))
  | Mprim { prim = `ADD; args = []; _ } ->
    let type_fun = Type_primitive.type_add in
    let cons arg1 arg2 top =
      N_SIMPLE (`ADD, [], [arg1; arg2], [top], label)
      (*N_ADD (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `SUB; args = []; _ } ->
    let type_fun = Type_primitive.type_sub in
    let cons arg1 arg2 top =
      N_SIMPLE (`SUB, [], [arg1; arg2], [top], label)
      (*N_SUB (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `SUB_MUTEZ; args = []; _ } ->
    let type_fun = Type_primitive.type_sub_mutez in
    let cons arg1 arg2 top =
      N_SIMPLE (`SUB_MUTEZ, [], [arg1; arg2], [top], label)
      (*N_SUB_MUTEZ (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `MUL; args = []; _ } ->
    let type_fun = Type_primitive.type_mul in
    let cons arg1 arg2 top =
      N_SIMPLE (`MUL, [], [arg1; arg2], [top], label)
      (*N_MUL (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `CONCAT; args = []; _ } -> (
    match stack with
    | ((name, Mprim { prim = `list; args = [ty]; _ }) as arg) :: xs ->
      let ty = Type_primitive.type_sub_concat ty in
      Result.bind ty (fun ty ->
          let top = (name, ty) in
          let node = N_CONCAT_LIST (arg, top, label) in
          Ok (Some (top :: xs), [node], label + 1))
    | _ ->
      let type_fun = Type_primitive.type_concat in
      let cons arg1 arg2 top = N_CONCAT (arg1, arg2, top, label) in
      compile_binary label name stack cons type_fun)
  | Mprim { prim = `EDIV; args = []; _ } ->
    let type_fun = Type_primitive.type_ediv in
    let cons arg1 arg2 top =
      N_SIMPLE (`EDIV, [], [arg1; arg2], [top], label)
      (*N_EDIV (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `COMPARE; args = []; _ } ->
    let type_fun _ _ = Ok (prim `int) in
    let cons arg1 arg2 top =
      N_SIMPLE (`COMPARE, [], [arg1; arg2], [top], label)
      (*N_COMPARE (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `ABS; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let ty = prim `nat in
      let top = (name, ty) in
      (*let node = N_ABS (x, top) in*)
      let node = N_SIMPLE (`ABS, [], [x], [top], label) in
      Ok (Some (top :: xs), [node], label + 1))
  | Mprim { prim = `AND; args = []; _ } ->
    let type_fun = Type_primitive.type_and in
    let cons arg1 arg2 top =
      N_SIMPLE (`AND, [], [arg1; arg2], [top], label)
      (*N_AND (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `EQ; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`EQ, [], [arg1], [arg2], label)
      (*N_EQ (arg1, arg2)*) in
    compile_eq_prim label name stack cons
  | Mprim { prim = `GE; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`GE, [], [arg1], [arg2], label)
      (*N_GE (arg1, arg2)*) in
    compile_eq_prim label name stack cons
  | Mprim { prim = `GT; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`GT, [], [arg1], [arg2], label)
      (*N_GT (arg1, arg2)*) in
    compile_eq_prim label name stack cons
  | Mprim { prim = `LE; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`LE, [], [arg1], [arg2], label)
      (*N_LE (arg1, arg2)*) in
    compile_eq_prim label name stack cons
  | Mprim { prim = `LAMBDA; args = [arg; ret; Mseq instr_l]; _ } ->
    let label_in = label + 1 in
    let pat_lambda = (name_from "arg", arg) in
    let ty = prim `lambda ~args:[arg; ret] in
    let top = (name_from "lambda", ty) in
    let result = decompile_instr (label_in + 1) false [pat_lambda] instr_l in
    Result.bind result (function
      | Some [_], node_fun, label_ret | None, node_fun, label_ret ->
        let node =
          N_LAMBDA (pat_lambda, label, node_fun, top, label_in, label_ret) in
        Ok (Some (top :: stack), [node], label_ret + 1)
      | _ ->
        Error
          (UnstackMicheline
             "Error the stack for a LAMBDA instructions is not exactly one"))
  | Mprim { prim = `EXEC; args = []; _ } -> (
    match stack with
    | arg :: ((_, Mprim { prim = `lambda; args = [_; ret]; _ }) as lambda) :: xs
      ->
      let top = (name, ret) in
      let label_ret = label + 1 in
      let node = N_EXEC (arg, lambda, top, label, label_ret) in
      Ok (Some (top :: xs), [node], label_ret + 1)
    | _ -> Error (UnstackMicheline "Not a good stack for a EXEC instruction"))
  | Mprim { prim = `APPLY; args = []; _ } -> (
    match stack with
    | arg :: lambda :: xs ->
      let ty = Type_primitive.type_apply (snd arg) (snd lambda) in
      Result.bind ty (fun ty ->
          let top = (name_from "lambda", ty) in
          (*let node = N_APPLY (arg, lambda, top) in*)
          let node = N_SIMPLE (`APPLY, [], [arg; lambda], [top], label) in
          Ok (Some (top :: xs), [node], label + 1))
    | _ -> Error (UnstackMicheline "Stack is too short"))
  | Mprim { prim = `LT; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`LT, [], [arg1], [arg2], label)
      (*N_LT (arg1, arg2)*) in
    compile_eq_prim label name stack cons
  | Mprim { prim = `CONS; args = []; _ } -> (
    match stack with
    | x :: (arg, ty) :: xs ->
      let name = name_stack () in
      let top = (name, ty) in
      (*let node = N_CONS (x, (arg, ty), top) in*)
      let node = N_SIMPLE (`CONS, [], [x; (arg, ty)], [top], label) in
      Ok (Some (top :: xs), [node], label + 1)
    | _ -> Error (UnstackMicheline "Stack is too short"))
  | Mprim { prim = `ITER; args = [Mseq instr_l]; _ } ->
    let label_iter = label in
    let label_pat = label_iter + 1 in
    let cons (x, accumulator, pat_acc, pat, node_iter) =
      N_ITER
        {
          arg_iter = x;
          acc_iter = accumulator;
          pat_acc_iter = pat_acc;
          pat = (pat, label_pat);
          body_iter = node_iter;
          label_iter;
        } in
    collection_application (label_pat + 1) "iter" stack false instr_l cons
  | Mprim { prim = `SIZE; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let top = (name, prim `nat) in
      (*let node = N_SIZE (x, top) in*)
      let node = N_SIMPLE (`SIZE, [], [x], [top], label) in
      Ok (Some (top :: xs), [node], label + 1))
  | Mprim { prim = `EMPTY_SET; args = [ty]; _ } ->
    let ty = prim `set ~args:[ty] in
    (*let node = N_EMPTY_SET (name, ty) in*)
    let top = (name, ty) in
    let node = N_SIMPLE (`EMPTY_SET, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `EMPTY_MAP; args = [key; elt]; _ } ->
    let top = (name, prim `map ~args:[key; elt]) in
    (*let node = N_EMPTY_MAP top in*)
    let node = N_SIMPLE (`EMPTY_MAP, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `EMPTY_BIG_MAP; args = [key; elt]; _ } ->
    let top = (name, prim `big_map ~args:[key; elt]) in
    (*let node = N_EMPTY_BIG_MAP top in*)
    let node = N_SIMPLE (`EMPTY_BIG_MAP, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `MAP; args = [Mseq instr_l]; _ } ->
    let label_pat = label + 1 in
    let cons (arg, accumulator, pat_acc, pat, node_iter) =
      match accumulator with
      | [] -> N_MAP (arg, label_pat, pat, node_iter, label)
      | _ ->
        N_FOLD_MAP
          {
            arg_fold_map = arg;
            acc_fold_map = accumulator;
            pat_acc_fold_map = pat_acc;
            pat_fold_map = (pat, label_pat);
            body_fold_map = node_iter;
            label_map = label;
          } in
    collection_application (label_pat + 1) "map" stack true instr_l cons
  | Mprim { prim = `MEM; args = []; _ } ->
    let type_fun = Type_primitive.type_mem in
    let cons arg1 arg2 top =
      N_SIMPLE (`MEM, [], [arg1; arg2], [top], label)
      (*N_MEM (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `LOOP; args = [Mseq instr_l]; _ } ->
    decomp_code_loop label stack instr_l
  | Mprim { prim = `LOOP_LEFT; args = [Mseq instr_l]; _ } -> (
    match stack with
    | ((_, Mprim { prim = `or_; args = [left; right]; _ }) as x) :: xs ->
      let name_pat = name_from "loop" in
      let pat = (name_pat, left) in
      let result = decompile_instr label true (pat :: xs) instr_l in
      Result.bind result (fun (new_stack, node_loop, label_ret) ->
          loop_left_post label_ret x xs new_stack node_loop pat right)
    | _ -> Error (UnstackMicheline "Stack is empty"))
  | Mprim { prim = `NEVER; args = []; _ } -> (
    match stack with
    | ((_, Mprim { prim = `never; _ }) as top) :: _ ->
      let node = N_NEVER (top, label) in
      Ok (None, [node], label + 1)
    | _ -> Error (UnstackMicheline "Wrong stack"))
  | Mprim { prim = `CAR; args = []; _ } ->
    let f (ty, cdr) top =
      let res = (name, ty) in
      let node =
        N_SIMPLE (`CAR, [cdr], [top], [res], label)
        (*N_CAR (top, res, cdr)*) in
      (res, node, label + 1) in
    let res = car_cdr stack instr f in
    res
  | Mprim { prim = `CDR; args = []; _ } ->
    let f (car, ty) top =
      let res = (name, ty) in
      let node =
        N_SIMPLE (`CDR, [car], [top], [res], label)
        (*N_CDR (top, res, car)*) in
      (res, node, label + 1) in
    car_cdr stack instr f
  | Mprim { prim = `LEFT; args = [right]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack empty")
    | (name_top, left) :: xs ->
      let ty = prim `or_ ~args:[left; right] in
      let res = (name, ty) in
      (*let node = N_LEFT ((name_top, left), res) in*)
      let node = N_SIMPLE (`LEFT, [right], [(name_top, left)], [res], label) in
      Ok (Some (res :: xs), [node], label + 1))
  | Mprim { prim = `RIGHT; args = [left]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack empty")
    | (name_top, right) :: xs ->
      let ty = prim `or_ ~args:[left; right] in
      let res = (name, ty) in
      let node =
        N_SIMPLE (`RIGHT, [left], [(name_top, right)], [res], label)
        (* N_RIGHT ((name_top, right), res)*) in
      Ok (Some (res :: xs), [node], label + 1))
  | Mprim { prim = `IF_LEFT; args = [Mseq instr_left; Mseq instr_right]; _ }
    -> (
    match stack with
    | ((_, Mprim { prim = `or_; args = [left; right]; _ }) as top) :: xs ->
      let pat_left = (name_from "pat_left", left) in
      let pat_right = (name_from "pat_right", right) in
      let label_left = label + 1 in
      let label_right = label_left + 1 in
      let res =
        michelson2node_if (label_right + 1)
          (fun n_then n_else ->
            let left = (pat_left, label_left, n_then) in
            let right = (pat_right, label_right, n_else) in
            N_IF_LEFT (top, label, left, right))
          xs
          (instr_left, pat_left :: xs)
          (instr_right, pat_right :: xs) in
      res
    | _ -> Error (UnstackMicheline "Stack is empty"))
  | Mprim { prim = `GET; args = []; _ } -> (
    match stack with
    | x :: y :: xs ->
      let result = Type_primitive.value_map (snd y) in
      Result.bind result (fun (_, value) ->
          let opt_result = prim `option ~args:[value] in
          let top = (name, opt_result) in
          (*let node = N_GET (x, y, top) in*)
          let node = N_SIMPLE (`GET, [], [x; y], [top], label) in
          Ok (Some (top :: xs), [node], label + 1))
    | _ -> Error (UnstackMicheline "Stack is too short"))
  | Mprim { prim = `GET; args = [Mint n]; _ } ->
    let type_fun = Type_primitive.type_gets n in
    let cons arg top =
      N_SIMPLE (`GET, [Mint n], [arg], [top], label)
      (*N_GETS (n, arg, top)*) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `UPDATE; args = []; _ } -> (
    match stack with
    | x :: xs ->
      let cons arg coll top =
        N_SIMPLE (`UPDATE, [], [x; arg; coll], [top], label)
        (*N_UPDATE (x, arg, coll, top)*) in
      let type_fun = Type_primitive.type_update in
      compile_binary label name xs cons type_fun
    | _ -> Error (UnstackMicheline "Not a good stack for a UPDATE instruction"))
  | Mprim { prim = `UPDATE; args = [Mint n]; _ } ->
    let type_fun = Type_primitive.type_updates n in
    let cons arg1 arg2 top =
      N_SIMPLE (`UPDATE, [Mint n], [arg1; arg2], [top], label)
      (*N_UPDATES (n, arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `FAILWITH; args = []; _ } -> (
    match stack with
    | x :: _ ->
      let node = N_FAILWITH (x, label) in
      Ok (None, [node], label + 1)
    | _ -> Error (UnstackMicheline "Stack is empty"))
  | Mprim { prim = `CAST; args = [_michety]; _ } -> Ok (Some stack, [], label)
  | Mprim { prim = `RENAME; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for RENAME instruction")
    | x :: xs -> Ok (Some (x :: xs), [], label))
  | Mprim { prim = `SLICE; args = []; _ } ->
    Error (UnstackMicheline "SLICE instruction not implemented yet")
  | Mprim { prim = `PACK; args = []; _ } ->
    let type_fun _ = Ok (prim `bytes) in
    let cons arg1 top =
      N_SIMPLE (`PACK, [], [arg1], [top], label)
      (*N_PACK (arg1, top)*) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `UNPACK; args = [mty]; _ } ->
    let type_fun arg =
      match arg with
      | Mprim { prim = `bytes; _ } -> Ok (prim `option ~args:[mty])
      | _ ->
        Error
          (UnstackMicheline "UNPACK instruction is not used on a bytes value")
    in
    let cons arg1 top =
      N_SIMPLE (`UNPACK, [], [arg1], [top], label)
      (*N_UNPACK (arg1, top)*) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `ISNAT; args = []; _ } ->
    let type_fun _ = Type_primitive.make_option (prim `nat) in
    let cons arg1 top =
      N_SIMPLE (`ISNAT, [], [arg1], [top], label)
      (*N_ISNAT (arg1, top)*) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `INT; args = []; _ } ->
    let type_fun _ = Ok (prim `int) in
    let cons arg1 top =
      N_SIMPLE (`INT, [], [arg1], [top], label)
      (*N_INT (arg1, top)*) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `NEG; args = []; _ } ->
    let cons arg1 top =
      N_SIMPLE (`NEG, [], [arg1], [top], label)
      (*N_NEG (arg1, top)*) in
    compile_unary label name stack Type_primitive.type_negativity cons
  | Mprim { prim = `LSL; args = []; _ } ->
    let type_fun _ _ = Ok (prim `nat) in
    let cons arg1 arg2 top =
      N_SIMPLE (`LSL, [], [arg1; arg2], [top], label)
      (*N_LSL (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `LSR; args = []; _ } ->
    let type_fun _ _ = Ok (prim `nat) in
    let cons arg1 arg2 top =
      N_SIMPLE (`LSR, [], [arg1; arg2], [top], label)
      (*N_LSR (arg1, arg2, top)*) in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `OR; args = []; _ } ->
    let cons arg1 arg2 top =
      N_SIMPLE (`OR, [], [arg1; arg2], [top], label)
      (*N_OR (arg1, arg2, top)*) in
    compile_binary label name stack cons Type_primitive.type_or_xor
  | Mprim { prim = `XOR; args = []; _ } ->
    let cons arg1 arg2 top =
      N_SIMPLE (`XOR, [], [arg1; arg2], [top], label)
      (*N_XOR (arg1, arg2, top)*) in
    compile_binary label name stack cons Type_primitive.type_or_xor
  | Mprim { prim = `NOT; args = []; _ } ->
    let cons arg1 top =
      N_SIMPLE (`NOT, [], [arg1], [top], label)
      (*N_NOT (arg1, top)*) in
    compile_unary label name stack Type_primitive.type_not cons
  | Mprim { prim = `NEQ; args = []; _ } ->
    let cons arg1 top =
      N_SIMPLE (`NEQ, [], [arg1], [top], label)
      (*N_NEQ (arg1, top)*) in
    compile_unary label name stack Type_primitive.type_neq cons
  | Mprim { prim = `SELF; args = []; annots } -> (
    let se = Option.get !se_ref in
    let entry = Factori_utils.get_name_annots annots in
    let opt =
      match entry with
      | None -> Factori_utils.get_parameter_from_script_expr se
      | Some e ->
        let entrypoints = Factori_utils.wrap_get_entrypoints_michelson_file se in
        let sanitized_name = Factori_utils.sanitized_of_str e in
        List.find_map
          (fun (key, ty) ->
            if Factori_utils.eq_sanitized sanitized_name key then
              Some ty
            else
              None)
          entrypoints in
    match opt with
    | None ->
      Error
        (UnstackMicheline "Couldn't find the entrypoint for instruction SELF")
    | Some ty ->
      let top = (name, prim `contract ~args:[ty]) in
      let node = N_SELF (entry, top, label) in
      Ok (Some (top :: stack), [node], label + 1))
  | Mprim { prim = `SELF_ADDRESS; args = []; _ } ->
    let top = (name, prim `address) in
    (*let node = N_SELF_ADDRESS top in*)
    let node = N_SIMPLE (`SELF_ADDRESS, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `CONTRACT; args = [ty]; annots } ->
    let type_fun = Type_primitive.type_contract ty in
    let cons arg1 top =
      N_CONTRACT (Factori_utils.get_name_annots annots, arg1, top, label) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `TRANSFER_TOKENS; args = []; annots = _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let type_fun = Type_primitive.type_transfer (snd x) in
      let cons arg1 arg2 top =
        N_SIMPLE (`TRANSFER_TOKENS, [], [x; arg1; arg2], [top], label)
        (* N_TRANSFER_TOKENS (x, arg1, arg2, top)*) in
      compile_binary label name xs cons type_fun)
  | Mprim { prim = `NOW; args = []; _ } ->
    let top = (name, prim `timestamp) in
    (*let node = N_NOW top in*)
    let node = N_SIMPLE (`NOW, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `LEVEL; args = []; _ } ->
    Error (UnstackMicheline "TODO LEVEL")
  | Mprim { prim = `AMOUNT; args = []; _ } ->
    let top = (name, prim `mutez) in
    (*let node = N_AMOUNT top in*)
    let node = N_SIMPLE (`AMOUNT, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `BALANCE; args = []; _ } ->
    let top = (name, prim `mutez) in
    (*let node = N_BALANCE top in*)
    let node = N_SIMPLE (`BALANCE, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `CHECK_SIGNATURE; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let type_fun = Type_primitive.type_check_sig (snd x) in
      let cons arg1 arg2 top =
        N_SIMPLE (`CHECK_SIGNATURE, [], [x; arg1; arg2], [top], label)
        (*N_CHECK_SIGNATURE (x, arg1, arg2, top)*) in
      compile_binary label name xs cons type_fun)
  | Mprim { prim = `SOURCE; args = []; _ } ->
    let ty_top = prim `address in
    let top = (name, ty_top) in
    let node = N_SIMPLE (`SOURCE, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `SENDER; args = []; _ } ->
    let ty_top = prim `address in
    let top = (name, ty_top) in
    let node = N_SIMPLE (`SENDER, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `ADDRESS; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`ADDRESS, [], [arg], [top], label)
      (*N_ADDRESS (arg, top)*) in
    compile_unary label name stack Type_primitive.type_address cons
  | Mprim { prim = `CHAIN_ID; args = []; _ } ->
    let ty = prim `chain_id in
    let top = (name, ty) in
    (*let node = N_CHAIN_ID top in*)
    let node = N_SIMPLE (`CHAIN_ID, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `TOTAL_VOTING_POWER; args = []; _ } ->
    let ty = prim `nat in
    let top = (name, ty) in
    (*let node = N_TOTAL_VOTING_POWER top in*)
    let node = N_SIMPLE (`TOTAL_VOTING_POWER, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `PAIRING_CHECK; args = []; _ } ->
    let type_fun = Type_primitive.type_pairing_check in
    let cons arg top =
      N_SIMPLE (`PAIRING_CHECK, [], [arg], [top], label)
      (*N_PAIRING_CHECK (arg, top)*) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `SET_DELEGATE; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`SET_DELEGATE, [], [arg], [top], label)
      (*N_SET_DELEGATE (arg, top)*) in
    compile_unary label name stack Type_primitive.type_set_delegate cons
  | Mprim { prim = `VOTING_POWER; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`VOTING_POWER, [], [arg], [top], label)
      (*N_VOTING_POWER (arg, top)*) in
    compile_unary label name stack Type_primitive.voting_power cons
  | Mprim { prim = `BLAKE2B as p; args = []; _ }
  | Mprim { prim = `KECCAK as p; args = []; _ }
  | Mprim { prim = `SHA3 as p; args = []; _ }
  | Mprim { prim = `SHA256 as p; args = []; _ }
  | Mprim { prim = `SHA512 as p; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (p, [], [arg], [top], label)
      (*N_SHA3 (arg, top)*) in
    compile_unary label name stack Type_primitive.type_bytes_trans cons
  | Mprim { prim = `HASH_KEY; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`HASH_KEY, [], [arg], [top], label)
      (*N_HASH_KEY (arg, top)*) in
    compile_unary label name stack Type_primitive.type_hash_k cons
  | Mprim { prim = `SAPLING_EMPTY_STATE; args = [Mint n]; _ } ->
    let ty = prim `sapling_state ~args:[Mint n] in
    let top = (name, ty) in
    (*let node = N_SAPLING_EMPTY_STATE (n, top) in*)
    let node = N_SIMPLE (`SAPLING_EMPTY_STATE, [Mint n], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1)
  | Mprim { prim = `VIEW; args = [Mstring str; ty]; _ } ->
    let cons x y top = N_CALL_VIEW (str, x, y, top, label) in
    let type_fun = Type_primitive.type_call_view ty in
    compile_binary label name stack cons type_fun
  | Mprim { prim = `GET_AND_UPDATE; args = []; _ } -> (
    match stack with
    | key :: _ :: coll :: _ ->
      let result = Type_primitive.value_map (snd coll) in
      Result.bind result (fun (_, value) ->
          let opt_result = prim `option ~args:[value] in
          let top = (name, opt_result) in
          let node = N_SIMPLE (`GET, [], [key; coll], [top], label) in
          let cons arg coll top =
            N_SIMPLE (`UPDATE, [], [key; arg; coll], [top], label + 1)
            (*N_UPDATE (key, arg, coll, top)*) in
          let type_fun = Type_primitive.type_update in
          let binary_result =
            compile_binary (label + 1) name (List.tl stack) cons type_fun in
          Result.bind binary_result (fun (stack, node_l, label) ->
              Ok
                ( Option.map (fun stack -> top :: stack) stack,
                  node :: node_l,
                  label )))
    | _ -> Error (UnstackMicheline "Not a good stack for GET_AND_UPDATE"))
  | Mprim { prim = `CREATE_CONTRACT; _ } ->
    Error (UnstackMicheline "CREATE_CONTRACT")
  | Mprim { prim = `IMPLICIT_ACCOUNT; _ } ->
    let type_fun = Type_primitive.implicit_acc in
    let cons arg top =
      N_SIMPLE (`IMPLICIT_ACCOUNT, [], [arg], [top], label)
      (*N_GETS (n, arg, top)*) in
    compile_unary label name stack type_fun cons
  | Mprim { prim = `SAPLING_VERIFY_UPDATE; _ } ->
    Error (UnstackMicheline "SAPLING_VERIFY_UPDATE")
  | Mprim { prim = `TICKET; _ } -> Error (UnstackMicheline "TICKET")
  | Mprim { prim = `READ_TICKET; _ } -> Error (UnstackMicheline "READ_TICKET")
  | Mprim { prim = `SPLIT_TICKET; _ } -> Error (UnstackMicheline "SPLIT_TICKET")
  | Mprim { prim = `JOIN_TICKETS; _ } -> Error (UnstackMicheline "JOIN_TICKETS")
  | Mprim { prim = `OPEN_CHEST; _ } -> Error (UnstackMicheline "OPEN_CHEST")
  | e ->
    Error
      (UnstackMicheline
         (Format.sprintf "Unexpected primitive %s"
            (Unstack_micheline.string_of_micheline e)))

let decompile_view label_start storage name_view param_view return_view code_v =
  let name = name_stack () in
  let ty = prim `pair ~args:[param_view; storage] in
  let top = (name, ty) in
  let stack = [top] in
  let node_l =
    match decompile_instr (label_start + 1) false stack code_v with
    | Error _ -> failwith "Fail in view"
    | e -> e in
  Result.bind node_l (function
    | Some [_], node_l, label_end | None, node_l, label_end ->
      let view =
        {
          view_label = label_start;
          node_name = name_view;
          view_stack = top;
          view_parameter = param_view;
          view_result = return_view;
          node_code_v = node_l;
        } in
      Ok (view, label_end)
    | _ ->
      Error
        (UnstackMicheline
           (Format.sprintf
              "View %s does not return the right number of element on the stack"
              name)))

let decompile_smart se =
  se_ref := Some se ;
  let name = name_stack () in
  let storage = Factori_utils.get_storage_from_script_expr se in
  let parameter = Factori_utils.get_parameter_from_script_expr se in
  let code = Factori_utils.get_code_from_script_expr se in
  let views = Factori_utils.get_views_from_script_expr se in
  match (storage, parameter, code) with
  | Some store, Some param, Some (Mseq code) ->
    let make_view_l views final =
      List.fold_left
        (fun acc elt ->
          Result.bind acc (fun (acc, cpt) ->
              match elt with
              | [Mstring name_view; param_view; return_view; Mseq code_view] ->
                let result =
                  decompile_view cpt store name_view param_view return_view
                    code_view in
                Result.bind result (fun (view, cpt) -> Ok (view :: acc, cpt))
              | _ -> Error (UnstackMicheline "Ill-formed view")))
        (Ok ([], final))
        views in
    let top = (name, prim `pair ~args:[param; store]) in
    let stack = [top] in
    let node_l = decompile_instr 1 false stack code in
    Result.bind node_l (function
      | Some [_], node_l, final | None, node_l, final ->
        let view_l = make_view_l views final in
        Result.bind view_l (fun (view_l, _) ->
            Ok
              {
                begin_label = 0;
                begin_stack = top;
                node_l;
                parameter = param;
                storage = store;
                views_node = view_l;
              })
      | _ -> Error (UnstackMicheline "Wrong final stack for the code section"))
  | _, None, _ -> Error (UnstackMicheline "There is no field parameter")
  | None, _, _ -> Error (UnstackMicheline "There is no field storage")
  | _, _, None -> Error (UnstackMicheline "There is no code section")
  | _ -> Error (GenericError ("decompile_smart", "Unexpected michelson file"))
