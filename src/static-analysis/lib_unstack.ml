open Unstack_micheline
open Unstack_mapper

module NameArg = struct
  type t = name_arg

  let compare (n1, _) (n2, _) = compare n1 n2

  let to_string (n, _) = n
end

module MapName = Map.Make (NameArg)
module NameSet = Set.Make (NameArg)

let change env n =
  match MapName.find_opt n env with
  | None -> n
  | Some e -> e

let change_all env l = List.map (fun n -> change n env) l

let change_mapper = { default_mapper with apply_use_arg = change }

let replace_ident (node_l : name_arg node list) (old : name_arg list)
    (new_ones : name_arg list) : name_arg node list =
  let map =
    List.fold_left2
      (fun acc old young -> MapName.add old young acc)
      MapName.empty old new_ones in
  change_mapper.apply_node_l change_mapper map node_l
