open Unstack_micheline
open Nested_arg
open Factori_errors

(*
module Transmo = Map.Make (struct
  type t = name_arg

  let compare n n2 = compare (fst n) (fst n2)
                   end)*)

module Transmo = Map.Make (String)

let transform env (name, miche) =
  match Transmo.find_opt name env with
  | None ->
    let m = Nested_arg.construct miche in
    let env = Transmo.add name m env in
    (env, m)
  | Some e -> (env, e)

let rec control_structure env node =
  match node with
  | N_IF (arg, label_arg, node_b1, node_b2) ->
    let env, arg = transform env arg in
    let env, node_b1 = propagate_construction env [] node_b1 in
    let env, node_b2 = propagate_construction env [] node_b2 in
    (N_IF (arg, label_arg, node_b1, node_b2), env)
  | N_IF_NONE (arg_opt, lab_arg, node_b1, node_b2, (pat_some, _), label_pat) ->
    let env, arg_opt = transform env arg_opt in
    let env, node_b1 = propagate_construction env [] node_b1 in
    let pat_some_arg =
      match arg_opt with
      | Option (arg, _) -> arg
      | _ -> raise (GenericError ("propagate_opt", "Not an option")) in
    let env = Transmo.add pat_some pat_some_arg env in
    let env, node_b2 = propagate_construction env [] node_b2 in
    let env = Transmo.remove pat_some env in
    ( N_IF_NONE (arg_opt, lab_arg, node_b1, node_b2, pat_some_arg, label_pat),
      env )
  | N_IF_CONS (arg_list, lab_arg, node_b1, node_b2, pat_x, lab_x, pat_xs, lab_xs)
    ->
    let env, arg_list = transform env arg_list in
    let env, node_b1 = propagate_construction env [] node_b1 in
    let pat_env, arg_pat_x = transform env pat_x in
    let pat_env, arg_pat_xs = transform pat_env pat_xs in
    let env, node_b2 = propagate_construction pat_env [] node_b2 in
    ( N_IF_CONS
        ( arg_list,
          lab_arg,
          node_b1,
          node_b2,
          arg_pat_x,
          lab_x,
          arg_pat_xs,
          lab_xs ),
      env )
  | N_IF_LEFT
      ( arg_or,
        lab_arg,
        ((pat_left, _), lab_left, node_b1),
        ((pat_right, _), lab_right, node_b2) ) ->
    let env, arg_or = transform env arg_or in
    let arg_pat_left, arg_pat_right =
      match arg_or with
      | Or (arg_l, arg_r, _) -> (arg_l, arg_r)
      | _ -> raise (GenericError ("propagate_opt", "Not a or")) in
    let env = Transmo.add pat_left arg_pat_left env in
    let env, node_b1 = propagate_construction env [] node_b1 in
    let env = Transmo.remove pat_left env in
    let env = Transmo.add pat_right arg_pat_right env in
    let env, node_b2 = propagate_construction env [] node_b2 in
    ( N_IF_LEFT
        ( arg_or,
          lab_arg,
          (arg_pat_left, lab_left, node_b1),
          (arg_pat_right, lab_right, node_b2) ),
      env )
  | N_ITER
      {
        arg_iter;
        acc_iter;
        pat_acc_iter;
        pat = pat, label_pat;
        body_iter;
        label_iter;
      } ->
    let env, arg_iter = transform env arg_iter in
    let env, acc_iter = List.fold_left_map transform env acc_iter in
    let pat_env, pat_acc_iter = List.fold_left_map transform env pat_acc_iter in
    let pat_env, pat = transform pat_env pat in
    let env, body_iter = propagate_construction pat_env [] body_iter in
    ( N_ITER
        {
          arg_iter;
          acc_iter;
          pat = (pat, label_pat);
          pat_acc_iter;
          body_iter;
          label_iter;
        },
      env )
  | N_MAP (arg, label, pat, node_l, label_map) ->
    let env, arg = transform env arg in
    let pat_env, pat = transform env pat in
    let env, node_l = propagate_construction pat_env [] node_l in
    (N_MAP (arg, label, pat, node_l, label_map), env)
  | N_LOOP_LEFT
      {
        arg_lf;
        pat_lf = pat_lf, lab_lf;
        acc_lf;
        pat_acc_lf;
        node_loop;
        pat_res_lf = pat_res_lf, lab_res;
        label_lf;
      } ->
    let env, arg_lf = transform env arg_lf in
    let env, acc_lf = List.fold_left_map transform env acc_lf in
    let pat_env, pat_lf = transform env pat_lf in
    let pat_env, pat_res_lf = transform pat_env pat_res_lf in
    let pat_env, pat_acc_lf = List.fold_left_map transform pat_env pat_acc_lf in
    let env, node_loop = propagate_construction pat_env [] node_loop in
    ( N_LOOP_LEFT
        {
          arg_lf;
          pat_lf = (pat_lf, lab_lf);
          acc_lf;
          pat_acc_lf;
          node_loop;
          pat_res_lf = (pat_res_lf, lab_res);
          label_lf;
        },
      env )
  | N_FOLD_MAP
      {
        arg_fold_map;
        acc_fold_map;
        pat_acc_fold_map;
        pat_fold_map = pat_fold_map, label_fold_map;
        body_fold_map;
        label_map;
      } ->
    let env, arg_fold_map = transform env arg_fold_map in
    let pat_env, pat_fold_map = transform env pat_fold_map in
    let acc_env, acc_fold_map = List.fold_left_map transform env acc_fold_map in
    let pat_env, pat_acc_fold_map =
      List.fold_left_map transform pat_env pat_acc_fold_map in
    let env, body_fold_map = propagate_construction pat_env [] body_fold_map in
    ( N_FOLD_MAP
        {
          arg_fold_map;
          acc_fold_map;
          pat_acc_fold_map;
          pat_fold_map = (pat_fold_map, label_fold_map);
          body_fold_map;
          label_map;
        },
      Transmo.union (fun _name_arg _left right -> Some right) acc_env env )
  | N_LOOP { arg; body; pat_acc; acc; label_loop; label_pat_l } ->
    let env, arg = transform env arg in
    let env, acc = List.fold_left_map transform env acc in
    let env, pat_acc = List.fold_left_map transform env pat_acc in
    let env, body = propagate_construction env [] body in
    (N_LOOP { body; pat_acc; arg; acc; label_loop; label_pat_l }, env)

and propagate_construction env acc node_l =
  match node_l with
  | [] -> failwith "TODO impossible"
  | [N_END (arg_l, label_end)] ->
    let env, arg_l = List.fold_left_map transform env arg_l in
    let node = N_END (arg_l, label_end) in
    (env, List.rev (node :: acc))
  | [N_FAILWITH (arg, label_fail)] ->
    let env, arg_l = transform env arg in
    let node = N_FAILWITH (arg_l, label_fail) in
    (env, List.rev (node :: acc))
  | [N_NEVER (arg, label_never)] ->
    let env, arg = transform env arg in
    let node = N_NEVER (arg, label_never) in
    (env, List.rev (node :: acc))
  | [FAIL_CONTROL (ctrl, lab_ctrl)] ->
    let node, env = control_structure env ctrl in
    (env, List.rev (FAIL_CONTROL (node, lab_ctrl) :: acc))
  | N_SIMPLE (`CDR, [_], [arg], [res], _) :: next_nodes ->
    let env, arg = transform env arg in
    let new_res =
      match Nested_arg.gets_pair (Z.of_int 2) arg with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`CAR, [_], [arg], [res], _) :: next_nodes ->
    let env, arg = transform env arg in
    let new_res =
      match Nested_arg.gets_pair (Z.of_int 1) arg with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`GET, [Mint n], [arg], [res], _) :: next_nodes ->
    let env, arg = transform env arg in
    let new_res =
      match Nested_arg.gets_pair n arg with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`UPDATE, [Mint n], [arg1; arg2], [res], _) :: next_nodes ->
    let env, arg1 = transform env arg1 in
    let env, arg2 = transform env arg2 in
    let new_res =
      match Nested_arg.updates_pair n arg1 arg2 with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`PAIR, [], arg_l, [res], _) :: next_nodes ->
    let env, arg_l = List.fold_left_map transform env arg_l in
    let arg = Pair (arg_l, []) in
    let env = Transmo.add (fst res) arg env in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`UNPAIR, [], [arg], res_l, _) :: next_nodes ->
    let env, arg = transform env arg in
    let n = Z.of_int (List.length res_l) in
    let new_res_l = Nested_arg.unpair_arg n arg in
    let env =
      List.fold_left2
        (fun env res new_res -> Transmo.add (fst res) new_res env)
        env res_l new_res_l in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`SOME, [], [arg], [res], _) :: next_nodes ->
    let env, arg = transform env arg in
    let env = Transmo.add (fst res) (Option (arg, [])) env in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`LEFT, [right], [arg], [res], _) :: next_nodes ->
    let env, arg = transform env arg in
    let r = Nested_arg.construct right in
    let env = Transmo.add (fst res) (Or (arg, r, [])) env in
    propagate_construction env acc next_nodes
  | N_SIMPLE (`RIGHT, [left], [arg], [res], _) :: next_nodes ->
    let env, arg = transform env arg in
    let l = Nested_arg.construct left in
    let env = Transmo.add (fst res) (Or (l, arg, [])) env in
    propagate_construction env acc next_nodes
  | node :: next_nodes ->
    let new_node, env =
      match node with
      | N_START (name_l, n, lab_start) ->
        let new_n, env = control_structure env n in
        let env, name_l = List.fold_left_map transform env name_l in
        (N_START (name_l, new_n, lab_start), env)
      | N_SIMPLE (prim, args, arg_l, res_l, lab_simple) ->
        let env, arg_l = List.fold_left_map transform env arg_l in
        let env, res_l = List.fold_left_map transform env res_l in
        (N_SIMPLE (prim, args, arg_l, res_l, lab_simple), env)
      | N_CONCAT (arg1, arg2, res, label) ->
        let env, arg1 = transform env arg1 in
        let env, arg2 = transform env arg2 in
        let env, res = transform env res in
        (N_CONCAT (arg1, arg2, res, label), env)
      | N_CONCAT_LIST (arg, res, label) ->
        let env, arg = transform env arg in
        let env, res = transform env res in
        (N_CONCAT_LIST (arg, res, label), env)
      | N_SELF (str_opt, arg, label) ->
        let env, arg = transform env arg in
        (N_SELF (str_opt, arg, label), env)
      | N_CONTRACT (str_opt, arg, res, label) ->
        let env, arg = transform env arg in
        let env, res = transform env res in
        (N_CONTRACT (str_opt, arg, res, label), env)
      | N_CALL_VIEW (str, arg, view, res, label) ->
        let env, arg = transform env arg in
        let env, view = transform env view in
        let env, res = transform env res in
        (N_CALL_VIEW (str, arg, view, res, label), env)
      | N_LAMBDA (pat, lab_pat, node_l, res, lab_in, lab_end) ->
        let env, pat = transform env pat in
        let env, node_l = propagate_construction env [] node_l in
        let env, res = transform env res in
        (N_LAMBDA (pat, lab_pat, node_l, res, lab_in, lab_end), env)
      | N_FAILWITH _ -> failwith "FAILWITH Instruction"
      | N_NEVER _ -> failwith "NEVER instruction"
      | N_CREATE_CONTRACT _ -> failwith "CREATE_CONTRACT Instruction"
      | FAIL_CONTROL (node, label_fail) ->
        let node, env = control_structure env node in
        (FAIL_CONTROL (node, label_fail), env)
      | N_EXEC (arg, lambda, res, label_call, label_ret) ->
        let env, arg = transform env arg in
        let env, lambda = transform env lambda in
        let env, res = transform env res in
        (N_EXEC (arg, lambda, res, label_call, label_ret), env)
      | N_END _ -> failwith "N_END node should be at the end of a node sequence"
    in
    propagate_construction env (new_node :: acc) next_nodes

let transform_view (storage_arg : Nested_arg.t) (view : name_arg v_node) =
  let param = Nested_arg.construct view.view_parameter in
  let view_stack = Pair ([param; storage_arg], []) in
  let env = Transmo.singleton (fst view.view_stack) view_stack in
  let _, node_code_v = propagate_construction env [] view.node_code_v in
  {
    view_label = view.view_label;
    node_name = view.node_name;
    view_stack;
    view_parameter = view.view_parameter;
    view_result = view.view_result;
    node_code_v;
  }

let transform_sc (sc : name_arg sc_node) =
  let env, begin_stack = transform Transmo.empty sc.begin_stack in
  let storage =
    match begin_stack with
    | Pair (_ :: storage, _) -> Nested_arg.pair_simple storage
    | _ -> Error (UnstackMicheline "Error with storage") in
  let result =
    Result.bind storage (fun storage ->
        let _, node_l = propagate_construction env [] sc.node_l in
        let views_node = List.map (transform_view storage) sc.views_node in
        Ok
          {
            begin_label = sc.begin_label;
            begin_stack;
            parameter = sc.parameter;
            storage = sc.storage;
            node_l;
            views_node;
          }) in
  match result with
  | Error exn -> raise exn
  | Ok res -> res
