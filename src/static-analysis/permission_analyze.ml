open Unstack_micheline
open Permission_tool
(*this functions tells if a node list of node has a top level fail *)

module M = Map.Make (Int)

type 'a fail_map = 'a t M.t

let rec will_fail (node_l : 'a node list) : label option =
  match node_l with
  | [] -> None
  | x :: xs -> (
    match x with
    | N_FAILWITH (_, l) | N_NEVER (_, l) | FAIL_CONTROL (_, l) -> Some l
    | _ -> will_fail xs)

let handle_if (node_then : 'a node list) (node_else : 'a node list)
    (perm : permission) (arg : 'a) (lab_arg : label) (acc : 'a fail_map) =
  match will_fail node_then with
  | None -> (
    match will_fail node_else with
    | None -> (acc, node_then @ node_else)
    | Some l ->
      let res = Other_fail (perm, arg, lab_arg) in
      let acc = M.add l res acc in
      (acc, node_then))
  | Some l ->
    let res = Will_fail (perm, arg, lab_arg) in
    let acc = M.add l res acc in
    (acc, node_else)

let handle_control (acc : 'a t M.t) (body_ctrl : 'a node list)
    (perm : permission) (arg : 'a) (lab_arg : label) =
  match will_fail body_ctrl with
  | Some l ->
    let new_elt = Will_fail (perm, arg, lab_arg) in
    let acc = M.add l new_elt acc in
    (acc, [])
  | None -> (acc, body_ctrl)

let permission_check_ctrl (acc : 'a t M.t) (node_l : 'a control_node) =
  match node_l with
  | N_IF (arg, lab_arg, node_then, node_else) ->
    handle_if node_then node_else True arg lab_arg acc
  | N_IF_NONE (arg, lab, node_none, node_some, _pat_some, _lab) ->
    handle_if node_none node_some Is_none arg lab acc
  | N_IF_CONS
      (arg, lab_arg, node_nil, node_cons, _pat_x, _lab_x, _pat_xs, _lab_xs) ->
    handle_if node_nil node_cons Is_cons arg lab_arg acc
  | N_IF_LEFT
      ( arg,
        lab_arg,
        (_pat_l, _lab_pat_l, node_left),
        (_pat_r, _lab_pat_r, node_right) ) ->
    handle_if node_left node_right Is_left arg lab_arg acc
  | N_ITER { arg_iter; body_iter; label_iter; _ } ->
    handle_control acc body_iter Not_empty arg_iter label_iter
  | N_LOOP { arg; body; label_loop; _ } ->
    handle_control acc body True arg label_loop
  | N_LOOP_LEFT { arg_lf; node_loop; label_lf; _ } ->
    handle_control acc node_loop Is_left arg_lf label_lf
  | N_MAP (arg, _lab_arg, _pat, body_map, label_map) ->
    handle_control acc body_map Not_empty arg label_map
  | N_FOLD_MAP { arg_fold_map; body_fold_map; label_map; _ } ->
    handle_control acc body_fold_map Not_empty arg_fold_map label_map

let permission_check (node_l : 'a node list) =
  let rec aux_permission (acc : 'a t M.t) (node_l : 'a node list) =
    match node_l with
    | [] -> acc
    | node :: next_node -> (
      match node with
      | N_LAMBDA (_arg, _label, node_l, top, l_in, l_out) -> (
        match will_fail node_l with
        | None -> aux_permission acc (node_l @ next_node)
        | Some l ->
          let elt_acc = Exec_fail (top, l_in, l_out) in
          let acc = M.add l elt_acc acc in
          aux_permission acc next_node)
      | N_START (_arg_l, ctrl, _lab) ->
        let new_acc, next = permission_check_ctrl acc ctrl in
        aux_permission new_acc (next @ next_node)
      | _ -> aux_permission acc next_node) in
  aux_permission M.empty node_l
