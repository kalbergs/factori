open Unstack_micheline
open Tzfunc.Proto
open Unstack_iterator
(*
open Factori_errors*)

type chain_call = label list

module ChainSet = Set.Make (struct
  type t = chain_call

  let compare = compare
end)

module LabelSet = Set.Make (struct
  type t = label

  let compare = compare
end)

let label_in_node acc node =
  match node with
  | N_SIMPLE (_, _, _, _, label)
  | N_END (_, label)
  | N_CONCAT (_, _, _, label) (* MULTIPLE DEF *)
  | N_CONCAT_LIST (_, _, label)
  | N_SELF (_, _, label)
  | N_CONTRACT (_, _, _, label)
  | N_CREATE_CONTRACT (_, _, label)
  | N_NEVER (_, label)
  | N_FAILWITH (_, label)
  | N_CALL_VIEW (_, _, _, _, label)
  | FAIL_CONTROL (_, label)
  | N_START (_, _, label) -> LabelSet.add label acc
  | N_EXEC (_, _, _, lc, lr) -> LabelSet.add lc (LabelSet.add lr acc)
  | N_LAMBDA (_, label, _, _, lin, lend) ->
    LabelSet.add label (LabelSet.add lin (LabelSet.add lend acc))
(* TODO *)

let label_in_ctrl_node acc ctrl_node =
  match ctrl_node with
  | N_IF (_, label, _, _) -> LabelSet.add label acc
  | N_IF_NONE (_, label, _, _, _, lab_pat)
  | N_LOOP { label_pat_l = lab_pat; label_loop = label; _ }
  | N_LOOP_LEFT
      {
        (* skipping label in pat_res if we use it in info_node change this function too *)
        pat_lf = _, lab_pat;
        label_lf = label;
        _;
      }
  | N_MAP (_, lab_pat, _, _, label)
  | N_ITER { pat = _, lab_pat; label_iter = label; _ }
  | N_FOLD_MAP { pat_fold_map = _, lab_pat; label_map = label; _ } ->
    LabelSet.add label (LabelSet.add lab_pat acc)
  | N_IF_CONS (_, l1, _, _, _, l2, _, l3)
  | N_IF_LEFT (_, l1, (_, l2, _), (_, l3, _)) ->
    LabelSet.add l1 (LabelSet.add l2 (LabelSet.add l3 acc))

let lab_folder =
  let node_fold fold () acc node =
    let acc = label_in_node acc node in
    default_folder.node_fold fold () acc node in
  let control_node_fold fold () acc node =
    let acc = label_in_ctrl_node acc node in
    default_folder.control_node_fold fold () acc node in
  { default_folder with node_fold; control_node_fold }

(* From a type t we should be able to construct a type t from the micheline type *)
module type T = sig
  type t

  val construct : micheline -> t

  val deconstruct : t -> micheline

  val name : t -> string option

  val to_string : t -> string

  val name_string_list : t list -> name_arg list

  val match_or : t -> (t * t) option

  val make_or : t -> t -> t

  val simple : name_arg -> t
end

module FlowSet = Set.Make (struct
  type t = label * label

  let compare = compare
end)

module InterFlow = Set.Make (struct
  type t = label * label * label * label

  let compare = compare
end)

let print_flow f =
  FlowSet.iter (fun (l1, l2) -> Format.eprintf "%d -> %d@." l1 l2) f

module type Analysis = sig
  type arg

  type analysis_tools

  val info_smart : arg sc_node -> analysis_tools

  val block_of_label : label -> analysis_tools -> arg block_unstack

  val print_block : arg block_unstack -> unit

  val list_of_blocks : analysis_tools -> arg block_unstack list

  val get_final : analysis_tools -> arg * label

  val get_init : analysis_tools -> arg * label

  val get_flow : analysis_tools -> FlowSet.t

  val get_inter_flow : analysis_tools -> InterFlow.t

  val view_labels : analysis_tools -> (label * label * arg) list

  val find_labels : label -> analysis_tools -> LabelSet.t

  val find_lambda :
    analysis_tools -> chain_call -> name_arg -> arg complete_lambda list

  val get_original : analysis_tools -> arg sc_node
end

module Aux : Analysis with type arg = Nested_arg.t = struct
  module A = Nested_arg

  type arg = A.t

  module BlockSet = Set.Make (struct
    type t = A.t block_unstack

    let compare = compare
  end)

  module LambdaChain = Map.Make (struct
    type t = name_arg * label list

    let compare = compare
  end)

  module LambdaName = Map.Make (struct
    type t = A.t

    let compare = compare
  end)

  type analysis_tools = {
    initial : label;
    name_init : A.t;
    final : label;
    name_final : A.t;
    blocks : BlockSet.t;
    flow : FlowSet.t;
    inter_flow : InterFlow.t;
    lambda_map : A.t complete_lambda list LambdaName.t;
    view_labels : (label * label * arg) list;
    original : A.t sc_node;
  }

  let build_flow_exec lab_s lab_e (flow, inter_flow) l =
    match l.lambda_code with
    | None -> (flow, inter_flow)
    | Some f ->
      let flow = FlowSet.add (lab_s, f.label_start) flow in
      let flow = FlowSet.add (f.label_end, lab_e) flow in
      let inter_flow =
        InterFlow.add (lab_s, f.label_start, f.label_end, lab_e) inter_flow
      in
      (flow, inter_flow)

  let annon_lambda left base =
    let miche = A.deconstruct left in
    match miche with
    | Mprim { prim = `lambda; _ } -> (
      let l = { lambda_name = left; lambda_code = None; in_between = [] } in
      match base with
      | None -> Some [l]
      | Some e -> Some (l :: e))
    | _ -> base

  let rec info_control_node end_lab result lambda_map label_start block_set flow
      (inter_flow : InterFlow.t) node =
    match node with
    | N_IF (arg_bool, label, node_then, node_else) ->
      let flow = FlowSet.add (label_start, label) flow in
      block_of_if end_lab lambda_map result label arg_bool node_then node_else
        block_set flow inter_flow
    | N_IF_NONE (arg_opt, label, node_none, node_some, _pat, _lab_pat) ->
      let flow = FlowSet.add (label_start, label) flow in
      block_of_if end_lab lambda_map result label arg_opt node_none node_some
        block_set flow inter_flow
    | N_IF_CONS
        (arg_list, label, node_nil, node_cons, _pat_x, _lab_x, _pat_xs, _lab_xs)
      ->
      let flow = FlowSet.add (label_start, label) flow in
      block_of_if end_lab lambda_map result label arg_list node_nil node_cons
        block_set flow inter_flow
    | N_IF_LEFT
        ( arg_or,
          label,
          (_pat_left, _lab_left, node_left),
          (_pat_right, _lab_right, node_right) ) ->
      let flow = FlowSet.add (label_start, label) flow in
      block_of_if end_lab lambda_map result label arg_or node_left node_right
        block_set flow inter_flow
    | N_ITER
        {
          arg_iter;
          acc_iter;
          pat_acc_iter;
          pat = pat, label_pat;
          body_iter;
          label_iter;
        } ->
      let block_start = BIter (pat_acc_iter, acc_iter, arg_iter, label_iter) in
      let block = BlockSet.add block_start block_set in
      let flow = FlowSet.add (label_start, label_iter) flow in
      let block =
        BlockSet.add (BAnnoAssign (pat :: pat_acc_iter, label_pat)) block in
      let flow = FlowSet.add (label_iter, label_pat) flow in
      let lambda_map, final_lab, block, flow, inter_flow =
        info_node lambda_map result label_pat block flow inter_flow body_iter
      in
      (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
      let block = BlockSet.add (BEnd end_lab) block in
      let flow = FlowSet.add (final_lab, end_lab) flow in
      (lambda_map, end_lab, block, flow, inter_flow)
    | N_MAP (arg, label_pat, pat, node, label) -> (
      match result with
      | [result_map] ->
        let ty =
          match
            Type_primitive.type_arg_collection (A.deconstruct result_map)
          with
          | Error e -> raise e
          | Ok res -> res in
        let new_name = A.construct ty in
        let block_start =
          BMap
            {
              top_body = new_name;
              return_coll = result_map;
              arg;
              initial_acc = [];
              result_acc = [];
              pos = label;
            } in
        let block = BlockSet.add block_start block_set in
        let flow = FlowSet.add (label_start, label) flow in
        let block = BlockSet.add (BAnnoAssign ([pat], label_pat)) block in
        let flow = FlowSet.add (label, label_pat) flow in
        let lambda_map, final_lab, block, flow, inter_flow =
          info_node lambda_map [new_name] label_pat block flow inter_flow node
        in
        (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
        let block = BlockSet.add (BEnd end_lab) block in
        let flow = FlowSet.add (final_lab, end_lab) flow in
        (lambda_map, end_lab, block, flow, inter_flow)
      | _ -> failwith "A MAP instruction canno't have a failwith in it's body")
    | N_FOLD_MAP
        {
          arg_fold_map;
          acc_fold_map;
          pat_acc_fold_map;
          pat_fold_map = pat_fold_map, label_pat;
          body_fold_map;
          label_map;
        } -> (
      match result with
      | [] -> failwith "A MAP instruction canno't have a failwith in it's body"
      | new_coll :: acc ->
        let ty =
          match Type_primitive.type_arg_collection (A.deconstruct new_coll) with
          | Error e -> raise e
          | Ok res -> res in
        let new_name = A.construct ty in
        let block_start =
          BMap
            {
              top_body = new_name;
              return_coll = new_coll;
              arg = arg_fold_map;
              initial_acc = acc_fold_map;
              result_acc = acc;
              pos = label_map;
            } in
        let block = BlockSet.add block_start block_set in
        let flow = FlowSet.add (label_start, label_map) flow in
        let block =
          BlockSet.add
            (BAnnoAssign (pat_fold_map :: pat_acc_fold_map, label_pat))
            block in
        let flow = FlowSet.add (label_map, label_pat) flow in
        let lambda_map, final_lab, block, flow, inter_flow =
          info_node lambda_map (new_name :: acc) label_pat block flow inter_flow
            body_fold_map in
        (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
        let block = BlockSet.add (BEnd end_lab) block in
        let flow = FlowSet.add (final_lab, end_lab) flow in
        (lambda_map, end_lab, block, flow, inter_flow))
    | N_LOOP { arg; acc; pat_acc; body; label_loop; label_pat_l = label_pat } ->
      let block_start = BLoop (arg, result, acc, label_loop) in
      let block = BlockSet.add block_start block_set in
      let flow = FlowSet.add (label_start, label_loop) flow in
      let block = BlockSet.add (BPattern (pat_acc, acc, label_pat)) block in
      let flow = FlowSet.add (label_loop, label_pat) flow in
      let lambda_map, final_lab, block, flow, inter_flow =
        info_node lambda_map (arg :: result) label_pat block flow inter_flow
          body in
      (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
      let block = BlockSet.add (BEnd end_lab) block in
      let flow = FlowSet.add (final_lab, end_lab) flow in
      (lambda_map, end_lab, block, flow, inter_flow)
    | N_LOOP_LEFT
        {
          arg_lf;
          pat_lf = pat_lf, label_pat;
          acc_lf;
          pat_acc_lf;
          node_loop;
          pat_res_lf = _res, _lab;
          label_lf;
        } ->
      let l, r =
        match A.unconstruct_or arg_lf with
        | Some res -> res
        | None -> failwith "Arg of Loop left is not a Or type" in
      let return_node =
        match result with
        | right_res :: result -> A.make_or pat_lf right_res :: result
        | _ -> result in
      let flow = FlowSet.add (label_start, label_lf) flow in
      let block_set =
        BlockSet.add (BLoop (arg_lf, result, r :: acc_lf, label_lf)) block_set
      in
      let block =
        BlockSet.add
          (BPattern (pat_lf :: pat_acc_lf, l :: acc_lf, label_pat))
          block_set in
      let flow = FlowSet.add (label_lf, label_pat) flow in
      let lambda_map, final_lab, block, flow, inter_flow =
        info_node lambda_map return_node label_pat block flow inter_flow
          node_loop in
      (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
      let block = BlockSet.add (BEnd end_lab) block in
      let flow = FlowSet.add (final_lab, end_lab) flow in
      (lambda_map, end_lab, block, flow, inter_flow)

  and info_node lambda_map name_l previous_stm block_set flow_set
      (inter_flow : InterFlow.t) sub_node_l =
    match sub_node_l with
    | [] -> (lambda_map, previous_stm, block_set, flow_set, inter_flow)
    | [N_END (exp_l, label)] ->
      let all_name_l = A.name_string_list name_l in
      let all_exp_l = A.name_string_list exp_l in
      let all_name_l_t = List.map A.simple all_name_l in
      let all_exp_l_t = List.map A.simple all_exp_l in
      let lambda_map =
        List.fold_left2
          (fun acc left right ->
            match LambdaName.find_opt right lambda_map with
            | None -> LambdaName.update left (annon_lambda left) acc
            | Some e ->
              LambdaName.update left
                (function
                  | None -> Some e
                  | Some arg -> Some (e @ arg))
                acc)
          lambda_map all_name_l_t all_exp_l_t in
      let flow = FlowSet.add (previous_stm, label) flow_set in
      let block =
        BlockSet.add (BReturnAssign (name_l, exp_l, label)) block_set in
      (lambda_map, label, block, flow, inter_flow)
    | [N_FAILWITH (arg, label)] ->
      let flow = FlowSet.add (previous_stm, label) flow_set in
      let block =
        BlockSet.add
          (BAssign (N_SIMPLE (`FAILWITH, [], [arg], [], label), label))
          block_set in
      (lambda_map, label, block, flow, inter_flow)
    | [N_NEVER (arg, label)] ->
      let flow = FlowSet.add (previous_stm, label) flow_set in
      let block =
        BlockSet.add
          (BAssign (N_SIMPLE (`NEVER, [], [arg], [], label), label))
          block_set in
      (lambda_map, label, block, flow, inter_flow)
    | [FAIL_CONTROL (ctrl_node, label_top)] ->
      info_control_node label_top [] lambda_map previous_stm block_set flow_set
        inter_flow ctrl_node
    | x :: xs -> (
      match x with
      | N_LAMBDA (arg, _label_start, node_l, lambda, label_in, label_out) ->
        let lambda_map, block, flow, inter_flow =
          lambda_stacking label_in label_out arg node_l lambda lambda_map
            block_set flow_set inter_flow in
        info_node lambda_map name_l previous_stm block flow inter_flow xs
      | N_EXEC (arg, lambda, res, label_call, label_ret) -> (
        let flow = FlowSet.add (previous_stm, label_call) flow_set in
        let block =
          BlockSet.add (BStartExec (arg, lambda, label_call)) block_set in
        let block = BlockSet.add (BEndExec (res, lambda, label_ret)) block in
        match LambdaName.find_opt lambda lambda_map with
        | None ->
          let flow = FlowSet.add (label_call, label_ret) flow in
          info_node lambda_map name_l label_ret block flow inter_flow xs
        | Some lambdas ->
          let flow, inter_flow =
            List.fold_left
              (build_flow_exec label_call label_ret)
              (flow, inter_flow) lambdas in
          info_node lambda_map name_l label_ret block flow inter_flow xs)
      | N_SIMPLE (`APPLY, [], [arg; lambda], [res], label) ->
        (* Peut etre en faire un block dans l'analyse
                     arg : 'a;
           label_start : label;
           label_end : label;
           res : 'a option;
        *)
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block = BlockSet.add (BAssign (x, label)) block_set in
        let lambda_map =
          match LambdaName.find_opt lambda lambda_map with
          | None ->
            let lambda =
              { lambda_name = lambda; lambda_code = None; in_between = [arg] }
            in
            let lambda_map = LambdaName.add res [lambda] lambda_map in
            lambda_map
          | Some e ->
            let lambda_element =
              List.map
                (fun l ->
                  let in_between = arg :: l.in_between in
                  { l with in_between })
                e in
            let lambda_map = LambdaName.add res lambda_element lambda_map in
            lambda_map in
        info_node lambda_map name_l previous_stm block flow inter_flow xs
      | N_CONCAT (arg1, arg2, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`CONCAT, [], [arg1; arg2], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_CONCAT_LIST (arg, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`CONCAT, [], [arg], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_SELF (_str_opt, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`SELF, [], [], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_CALL_VIEW (_name, arg, addr, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`VIEW, [], [arg; addr], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_SIMPLE (_, _, _, _, label) as simple_node ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_CONTRACT (_, addr, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block =
          BlockSet.add
            (BAssign (N_SIMPLE (`CONTRACT, [], [addr], [res], label), label))
            block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_FAILWITH (arg, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block =
          BlockSet.add
            (BAssign (N_SIMPLE (`FAILWITH, [], [arg], [], label), label))
            block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_NEVER (arg, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block =
          BlockSet.add
            (BAssign (N_SIMPLE (`FAILWITH, [], [arg], [], label), label))
            block_set in
        info_node lambda_map name_l label block flow inter_flow xs
      | N_START (result, ctrl_node, end_label) ->
        let lambda_map, end_lab, block, flow, inter_flow =
          info_control_node end_label result lambda_map previous_stm block_set
            flow_set inter_flow ctrl_node in
        info_node lambda_map name_l end_lab block flow inter_flow xs
      | FAIL_CONTROL (ctrl_node, end_label) ->
        let lambda_map, end_lab, block, flow, inter_flow =
          info_control_node end_label [] lambda_map previous_stm block_set
            flow_set inter_flow ctrl_node in
        info_node lambda_map name_l end_lab block flow inter_flow xs
      | _ -> failwith "TODO")

  and block_of_if final_label lambda_map (result_if : A.t list) (label : label)
      (arg_if : A.t) (node_l : A.t node list) (node_r : A.t node list)
      (block : BlockSet.t) (flow : FlowSet.t) (inter_flow : InterFlow.t) =
    let block = BlockSet.add (BAnnoUse ([arg_if], label)) block in
    let lambda_map, final_left, block, flow, inter_flow =
      info_node lambda_map result_if label block flow inter_flow node_l in
    let lambda_map, final_right, block, flow, inter_flow =
      info_node lambda_map result_if label block flow inter_flow node_r in
    let flow =
      FlowSet.add (final_left, final_label)
        (FlowSet.add (final_right, final_label) flow) in
    let block = BlockSet.add (BEnd final_label) block in
    (lambda_map, final_label, block, flow, inter_flow)

  and lambda_stacking label_start label_end arg node_l lambda acc block flow
      inter_flow =
    let block = BlockSet.add (BIs label_start) block in
    let ret =
      match A.deconstruct lambda with
      | Mprim { prim = `lambda; args = [_; ret]; _ } -> ret
      | _ -> failwith "The result of a lambda instruction is not a lambda" in
    let final_lambda = A.construct ret in
    match List.rev node_l with
    | N_END ([res], _abel) :: _ ->
      let acc, final_label, block, flow, inter_flow =
        info_node acc [final_lambda] label_start block flow inter_flow node_l
      in
      let block = BlockSet.add (BEndLambda label_end) block in
      let flow = FlowSet.add (final_label, label_end) flow in
      let lambda_code =
        Some { arg; label_start; lcode = node_l; label_end; res = Some res }
      in
      let lambda_element =
        { lambda_name = lambda; lambda_code; in_between = [] } in
      let lambda_map =
        LambdaName.update lambda
          (function
            | None -> Some [lambda_element]
            | Some arg -> Some (lambda_element :: arg))
          acc in
      (lambda_map, block, flow, inter_flow)
    | N_FAILWITH _ :: _ | N_NEVER _ :: _ ->
      let acc, final_label, block, flow, inter_flow =
        info_node acc [] label_start block flow inter_flow node_l in
      let lambda_code =
        Some { arg; label_start; lcode = node_l; label_end; res = None } in
      let block = BlockSet.add (BEndLambda label_end) block in
      let flow = FlowSet.add (final_label, label_end) flow in
      let lambda_element =
        { lambda_name = lambda; lambda_code; in_between = [] } in
      let lambda_map =
        LambdaName.update lambda
          (function
            | None -> Some [lambda_element]
            | Some arg -> Some (lambda_element :: arg))
          acc in
      (lambda_map, block, flow, inter_flow)
    | _ -> failwith "Not possible for a lambda to finish"

  let info_view (lambda_map, blocks, flow, inter_flow) view =
    let final_view = A.construct view.view_result in
    let blocks =
      BlockSet.add (BAnnoAssign ([view.view_stack], view.view_label)) blocks
    in
    let lambda_map, final, blocks, flow, inter_flow =
      info_node lambda_map [final_view] view.view_label blocks flow inter_flow
        view.node_code_v in
    ( (lambda_map, blocks, flow, inter_flow),
      (view.view_label, final, final_view) )

  let info_smart (sc : arg sc_node) =
    let acc =
      (LambdaName.empty, BlockSet.empty, FlowSet.empty, InterFlow.empty) in
    let (lambda_map, blocks, flow, inter_flow), view_labels =
      List.fold_left_map info_view acc sc.views_node in
    let block =
      BlockSet.add (BAnnoAssign ([sc.begin_stack], sc.begin_label)) blocks in
    let op = prim `operation in
    let op_l = prim `list ~args:[op] in
    let ty = prim `pair ~args:[op_l; sc.storage] in
    let name_final = A.construct ty in
    let lambda_map, final, blocks, flow, inter_flow =
      info_node lambda_map [name_final] sc.begin_label block flow inter_flow
        sc.node_l in
    {
      final;
      name_init = sc.begin_stack;
      name_final;
      initial = sc.begin_label;
      blocks;
      inter_flow;
      flow;
      lambda_map;
      view_labels;
      original = sc;
    }

  let lab_in_block (lab : label) (b : _ block_unstack) =
    match b with
    | BAssign (_, l)
    | BAnnoUse (_, l)
    | BAnnoAssign (_, l)
    | BReturnAssign (_, _, l)
    | BStartExec (_, _, l)
    | BEndExec (_, _, l)
    | BIter (_, _, _, l)
    | BEnd l
    | BIs l
    | BMap { pos = l; _ }
    | BLoop (_, _, _, l)
    | BPattern (_, _, l)
    | BEndLambda l -> l = lab

  let string_of_prim m = List.assoc m Unstack_micheline.prim_macro

  (* I am assuming that every variable is unique *)
  let print_block b =
    Format.pp_print_newline Format.err_formatter () ;
    match b with
    | BAssign (N_SIMPLE (p, miche_l, args, res, _), l) ->
      let miche_l = List.map string_of_micheline miche_l in
      let args = List.map A.to_string args in
      let res = List.map A.to_string res in
      Format.eprintf "%s = %s %s (%s) [%d]@." (String.concat ", " res)
        (string_of_prim p)
        (String.concat " " miche_l)
        (String.concat " ," args) l
    | BAnnoUse (name_l, l) ->
      let name_l = List.map A.to_string name_l in
      Format.eprintf "USE(%s) [%d]@." (String.concat ", " name_l) l
    | BAnnoAssign (name_l, l) | BPattern (name_l, _, l) ->
      let name_l = List.map A.to_string name_l in
      Format.eprintf "Pattern(%s) [%d]@." (String.concat ", " name_l) l
    | BReturnAssign (name_assign, name_use, l) ->
      let name_assign = List.map A.to_string name_assign in
      let name_use = List.map A.to_string name_use in
      Format.eprintf "%s = %s [%d]@."
        (String.concat " ," name_assign)
        (String.concat " ," name_use)
        l
    | BEnd l -> Format.eprintf "END [%d]@." l
    | BIter (pat_acc, acc, arg, l) ->
      let acc = List.map A.to_string acc in
      let pat_acc = List.map A.to_string pat_acc in
      Format.eprintf "Iter on %s with acc = (%s) and pat_acc = %s [%d]@."
        (A.to_string arg) (String.concat ", " acc)
        (String.concat ", " pat_acc)
        l
    | BStartExec (_arg, _lambda, _label) ->
      Format.eprintf "START [%d]@." _label
      (*Format.pp_print_list ~pp_sep:Format.pp_print_newline
        (fun ppf elt ->
          match elt.lambda_code with
          | None -> Format.fprintf ppf "Noi lambda"
          | Some m ->
            Format.fprintf ppf
              "arg : %s, start lambda : %d, end lambda : %d, res : %a"
              (A.to_string m.arg) m.label_start m.label_end
              (fun ppf -> function
                | None -> Format.fprintf ppf "No result"
                | Some s -> Format.fprintf ppf "%s" (A.to_string s))
              m.res)
        Format.err_formatter lambda*)
    | BEndLambda l -> Format.eprintf "End Lambda [%d]@." l
    | BEndExec _ -> Format.eprintf "End exec@."
    | BAssign _ -> failwith "Not simple"
    | BIs l -> Format.eprintf "Start in lambda [%d]" l
    | _ -> failwith "TODO"

  let block_of_label lab analysis =
    let blocs = BlockSet.filter (lab_in_block lab) analysis.blocks in
    match BlockSet.choose_opt blocs with
    | Some b when BlockSet.cardinal blocs = 1 -> b
    | _ ->
      BlockSet.iter print_block blocs ;
      failwith
        (Format.sprintf "%d block for label %d" (BlockSet.cardinal blocs) lab)

  let list_of_blocks (tool : analysis_tools) : A.t block_unstack list =
    BlockSet.fold (fun elt acc -> elt :: acc) tool.blocks []

  let get_final (tool : analysis_tools) : arg * label =
    (tool.name_final, tool.final)

  let get_init (tool : analysis_tools) : arg * label =
    (tool.name_init, tool.initial)

  let get_flow (tool : analysis_tools) : FlowSet.t = tool.flow

  let get_inter_flow (tool : analysis_tools) : InterFlow.t = tool.inter_flow

  let find_labels (label : label) (tool : analysis_tools) =
    let block = block_of_label label tool in
    match block with
    | BStartExec (_, lambda, _) -> (
      match LambdaName.find_opt lambda tool.lambda_map with
      | None -> LabelSet.empty
      | Some lambdas ->
        List.fold_left
          (fun acc -> function
            | { lambda_code = Some l; _ } ->
              let lab_set = lab_folder.node_seq lab_folder () acc l.lcode in
              LabelSet.add l.label_start (LabelSet.add l.label_end lab_set)
            | _ -> acc)
          LabelSet.empty lambdas)
    | _ -> LabelSet.empty

  let find_lambda (tool : analysis_tools) (chain : chain_call) (name : name_arg)
      : arg complete_lambda list =
    let lambda_chain =
      LambdaName.fold
        (fun arg l acc ->
          let name = Option.get @@ Nested_arg.to_name_arg arg in
          LambdaChain.add (name, []) l acc)
        tool.lambda_map LambdaChain.empty in
    match LambdaChain.find_opt (name, chain) lambda_chain with
    | None ->
      (* it may be a lambda that was top level so it chain is []*)
      Option.value (LambdaChain.find_opt (name, []) lambda_chain) ~default:[]
    | Some l -> l

  let get_original (tool : analysis_tools) : arg sc_node = tool.original

  let view_labels (tool : analysis_tools) : (label * label * arg) list =
    tool.view_labels
end

let print_inter_flow (ppf : Format.formatter) (inter : InterFlow.t) : unit =
  let inter_elems = InterFlow.elements inter in
  Format.pp_print_list ~pp_sep:Format.pp_print_newline
    (fun ppf (lc, li, le, lr) ->
      Format.fprintf ppf "%d -> %d -> ... -> %d -> %d" lc li le lr)
    ppf inter_elems

module SetInt = Set.Make (Int)

let last_entry_point (sc_node : Nested_arg.t sc_node) : SetInt.t =
  let param_storage = sc_node.begin_stack in
  let _parameter = Nested_arg.recup_parameter param_storage in
  SetInt.empty
