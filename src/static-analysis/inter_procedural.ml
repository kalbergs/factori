open Auxilary
open Unstack_micheline

module type MF = sig
  module SA : Analysis

  type t

  val lub : t -> t -> t

  val leq : t -> t -> bool

  val bot : t

  val iota : SA.analysis_tools -> t

  val ext : SA.analysis_tools -> label list

  val ext_view : SA.analysis_tools -> label list

  val flow : SA.analysis_tools -> FlowSet.t

  val f : SA.analysis_tools -> label -> t -> t

  val f_entry_analyze :
    SA.analysis_tools -> chain_call -> label -> label -> label -> t -> t

  val f_return_analyze :
    SA.analysis_tools -> chain_call -> label -> label -> label -> t * t -> t

  val lambda_entrance : label * label * label * label -> label * label

  val lambda_return : label * label * label * label -> label * label

  val iota_view : SA.analysis_tools -> label -> t

  val print : Format.formatter -> t -> unit

  val union : t -> t -> t

  val diff : t -> t -> t

  val gen : t -> label -> SA.analysis_tools -> t

  val kill : label -> SA.analysis_tools -> t
end

module Label = Map.Make (struct
  type t = label

  let compare = compare
end)

module ChainMap = Map.Make (struct
  type t = chain_call

  let compare = compare
end)

module Make (A : MF) = struct
  (* type of analyses *)
  type t = A.t ChainMap.t Label.t

  (*type of transfer functions: given a label and some knowledge
     computes the added knowledge after the label *)
  type transfer = label -> A.t -> A.t

  type ret_transfer = label -> label -> label -> A.t * A.t -> A.t
  (* initialisation of the analysis
     - labels: list of entry labels to analyze
     - ext: extremal labels
     - iota: initial knowledge
     - bot: absence of knowledge

     for each label 'l' in labels associates either iota, either bot depending
     on if 'l' is extremal or not
  *)

  let init (tool : A.SA.analysis_tools) (labels : label list) (ext : label list)
      (iota : A.t) (bot : A.t) : t =
    let aux_extremal acc l =
      let label_set = A.SA.find_labels l tool in
      let acc =
        LabelSet.fold
          (fun l_lambda acc ->
            Label.update l_lambda
              (function
                | None -> Some (ChainMap.singleton [l] bot)
                | Some map -> Some (ChainMap.add [l] bot map))
              acc)
          label_set acc in
      if List.mem l ext then
        Label.add l (ChainMap.singleton [] iota) acc
      else
        Label.add l (ChainMap.singleton [] bot) acc in
    List.fold_left aux_extremal Label.empty labels

  let result (labels : label list) (f : transfer) (analysis : t) : t =
    List.fold_left
      (fun acc l ->
        let label_analysis = Option.get @@ Label.find_opt l analysis in
        let new_analysis =
          ChainMap.map
            (fun param ->
              let apply_f = f l in
              let res = apply_f param in
              res)
            label_analysis in
        Label.add l new_analysis acc)
      Label.empty labels

  let is_entry_lambda (label : label)
      (inter_flow : label * label * label * label) =
    let label_entry = fst @@ A.lambda_entrance inter_flow in
    label = label_entry

  let is_return_lambda (label : label)
      (inter_flow : label * label * label * label) =
    let label_return = fst @@ A.lambda_return inter_flow in
    label = label_return

  let lifted_f_ret (f_ret : ret_transfer) (inter_ret : InterFlow.t)
      (base_knowledge, l') chain =
    let base_knowledge_unlift = base_knowledge chain in
    let analysis =
      InterFlow.fold
        (fun ((lc, li, le, _) as i_flow) acc ->
          let new_knowledge =
            f_ret li le
              (fst @@ A.lambda_return i_flow)
              (base_knowledge_unlift, l' (lc :: chain)) in
          A.lub acc new_knowledge)
        inter_ret base_knowledge_unlift in
    analysis

  let cpt = ref 0

  let inter_entry_f f_entry l1 l2 intra_analysis ((lc, li, le, _) as i_flow)
      inter_acc =
    let entrance, out = A.lambda_entrance i_flow in
    let chain_analysis_l2 = Option.get @@ Label.find_opt l2 intra_analysis in
    let chain_analysis_l1 = Option.get @@ Label.find_opt l1 intra_analysis in
    ChainMap.fold
      (fun chain _ acc ->
        let lift_entry = f_entry chain li le in
        match chain with
        | x :: xs when lc = x ->
          let specific_analysis =
            try ChainMap.find xs chain_analysis_l1 with _ -> failwith "Not"
          in
          let res_l1 = lift_entry entrance specific_analysis in
          Label.update out
            (function
              | None -> Some (ChainMap.singleton chain res_l1)
              | Some map -> Some (ChainMap.add chain res_l1 map))
            acc
        | _ -> acc)
      chain_analysis_l2 inter_acc

  let update_specific_chain l1 l2 f flow chain analysis1 (intra, next) =
    let chain_analysis2 = Option.get @@ Label.find_opt l2 intra in
    let analysis2_opt = ChainMap.find_opt chain chain_analysis2 in
    match analysis2_opt with
    | None -> (intra, next)
    | Some analysis2 ->
      let transf1 = f l1 analysis1 in
      if not (A.leq transf1 analysis2) then
        let chain_analysis2 =
          ChainMap.add chain (A.lub analysis2 transf1) chain_analysis2 in
        let new_analysis = Label.add l2 chain_analysis2 intra in
        let re_analysis = List.filter (fun (l, _) -> l = l2) flow in
        (new_analysis, re_analysis @ next)
      else
        (intra, next)

  let update (tool : A.SA.analysis_tools) (flow : (label * label) list)
      (f : transfer) (analysis : t) : t =
    let f_entry = A.f_entry_analyze tool in
    let f_ret = A.f_return_analyze tool in
    let inter_flow = A.SA.get_inter_flow tool in
    let rec aux_update (intra_analysis : t) = function
      | [] -> intra_analysis
      | (l1, l2) :: xs ->
        (* dans inter se trouve tous les calls qui arrive ici *)
        let inter_entry = InterFlow.filter (is_entry_lambda l1) inter_flow in
        let inter_ret = InterFlow.filter (is_return_lambda l2) inter_flow in
        if not (InterFlow.is_empty inter_entry) then
          let inter_analysis =
            InterFlow.fold
              (inter_entry_f f_entry l1 l2 intra_analysis)
              inter_entry intra_analysis in
          (*let re_analysis = List.filter (fun (l, _) -> l = l2) flow in*)
          aux_update inter_analysis xs
        (* l'analyse de l1 *)
        else if not (InterFlow.is_empty inter_ret) then
          let chain_analysis =
            match Label.find_opt l2 intra_analysis with
            | None ->
              failwith "Could not find the analysis of l2 in the inraknowlege"
            | Some res -> res in
          let base_knowledge chain =
            match ChainMap.find_opt chain chain_analysis with
            | None ->
              failwith "Could not find the analysis in the base knowledge"
            | Some res -> res in
          let l' chain =
            let chain_analysis =
              match Label.find_opt l1 intra_analysis with
              | None ->
                failwith
                  "Could not find the analysis of l1 in the intra knowledge"
              | Some res -> res in
            match ChainMap.find_opt chain chain_analysis with
            | None -> A.bot
            | Some res -> res in
          let chain_inter_analysis =
            ChainMap.fold
              (fun chain _ acc ->
                ChainMap.add chain
                  (lifted_f_ret (f_ret chain) inter_ret (base_knowledge, l')
                     chain)
                  acc)
              chain_analysis ChainMap.empty in
          let inter_analysis =
            Label.add l2 chain_inter_analysis intra_analysis in
          (* let re_analysis = List.filter (fun (l, _) -> l = l2) flow in *)
          aux_update inter_analysis xs
        else
          let intra, next =
            let chain_analysis1 =
              Option.get @@ Label.find_opt l1 intra_analysis in
            ChainMap.fold
              (update_specific_chain l1 l2 f flow)
              chain_analysis1 (intra_analysis, xs) in
          aux_update intra next in
    aux_update analysis flow

  let solveMF ?(print = false) (tool : A.SA.analysis_tools) : t =
    let flow = FlowSet.elements (A.flow tool) in
    let flow = List.sort (fun a b -> compare a b) flow in
    let labels =
      List.fold_left
        (fun acc (l1, l2) -> LabelSet.add l1 (LabelSet.add l2 acc))
        LabelSet.empty flow in
    let all_labels = LabelSet.elements labels in
    let init_analysis = init tool all_labels (A.ext tool) (A.iota tool) A.bot in
    let update_analysis = update tool flow (A.f tool) init_analysis in
    let res = result all_labels (A.f tool) update_analysis in
    if print then
      List.iter
        (fun i ->
          let chain_map = Label.find i res in
          let print_chain ppf chain_map =
            ChainMap.iter
              (fun chain res ->
                let print_chain =
                  Format.pp_print_list
                    ~pp_sep:(fun ppf () -> Format.fprintf ppf ";")
                    (fun ppf elt -> Format.fprintf ppf "%d" elt) in
                Format.fprintf ppf "[%a] -> %a@." print_chain chain A.print res)
              chain_map in
          Format.eprintf "%d -> %a@." i print_chain chain_map)
        all_labels ;
    res
end
