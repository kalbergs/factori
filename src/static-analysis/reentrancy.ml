open Unstack_iterator
open Unstack_micheline

let node_fold folder env acc n =
  match n with
  | N_SIMPLE (`TRANSFER_TOKENS, [], [_; _; _], [_], _) -> acc + 1
  | _ -> default_folder.node_fold folder env acc n

let folder_reentrancy = { default_folder with node_fold }

let reentrancy_detection sc =
  let nb_transfer =
    folder_reentrancy.node_seq folder_reentrancy () 0 sc.node_l in
  match nb_transfer with
  | 0 -> ()
  | n ->
    Format.eprintf
      "Warning : There is %d potentiel reentrancy problem in this smart \
       contract@."
      n
