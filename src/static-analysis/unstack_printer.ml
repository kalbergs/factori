open Unstack_micheline

module type A = sig
  type t

  val to_string : t -> string
end

module Make (A : A) = struct
  let print_elt ppf elt = Format.fprintf ppf "%s" (A.to_string elt)

  let list_of_arg = Format.pp_print_list ~pp_sep:Format.pp_print_space print_elt

  let list_of_micheline =
    Format.pp_print_list ~pp_sep:Format.pp_print_space (fun ppf elt ->
        Format.fprintf ppf "%s" (Unstack_micheline.string_of_micheline elt))

  let list_of_res =
    Format.pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf ",")
      print_elt

  let print_annot ppf = function
    | None -> Format.fprintf ppf ""
    | Some s -> Format.fprintf ppf "%%%s" s

  let rec pp_print_control ppf control_node =
    match control_node with
    | N_IF (arg, label, node_then, node_else) ->
      Format.fprintf ppf "IF %s [%d] THEN@, %a@,%a" (A.to_string arg) label
        pp_print_node_l node_then pp_print_node_l node_else
    | N_IF_NONE (arg, label, node_none, node_some, pat_some, lab_pat) ->
      Format.fprintf ppf "IF_NONE %s [%d]@,|NONE -> %a@,|SOME %s [%d] -> %a"
        (A.to_string arg) label pp_print_node_l node_none (A.to_string pat_some)
        lab_pat pp_print_node_l node_some
    | N_IF_CONS (arg, label, node_nil, node_cons, pat_hd, lab_x, pat_tl, lab_xs)
      ->
      Format.fprintf ppf
        "IF_CONS %s [%d]@,|Cons(%s,%s) [%d,%d] -> %a@,|Nil -> %a"
        (A.to_string arg) label (A.to_string pat_hd) (A.to_string pat_tl) lab_x
        lab_xs pp_print_node_l node_cons pp_print_node_l node_nil
    | N_IF_LEFT (arg, label, (pat_l, lab_l, node_l), (pat_r, lab_r, node_r)) ->
      Format.fprintf ppf
        "IF_LEFT %s [%d]@,|Left %s [%d] -> %a@,|Right %s [%d] -> %a"
        (A.to_string arg) label (A.to_string pat_l) lab_l pp_print_node_l node_l
        (A.to_string pat_r) lab_r pp_print_node_l node_r
    | N_ITER
        {
          arg_iter;
          acc_iter;
          pat_acc_iter;
          pat = pat, lab_pat;
          body_iter;
          label_iter;
        } ->
      Format.fprintf ppf "ITER ON %s [%d] PAT : %s [%d] PAT_ACC : (%a) %a (%a)"
        (A.to_string arg_iter) label_iter (A.to_string pat) lab_pat list_of_res
        pat_acc_iter pp_print_node_l body_iter list_of_res acc_iter
    | N_LOOP { arg; acc; pat_acc; body; label_pat_l; label_loop } ->
      Format.fprintf ppf "LOOP ON %s [%d] PAT_ACC : (%a) [%d] %a (%a)"
        (A.to_string arg) label_loop list_of_res pat_acc label_pat_l
        pp_print_node_l body list_of_res acc
    | N_LOOP_LEFT
        {
          arg_lf;
          pat_lf = pat_lf, lab_pat;
          acc_lf;
          pat_acc_lf;
          node_loop;
          pat_res_lf = pat_res_lf, lab_pat_res;
          label_lf;
        } ->
      let return_pat_res = A.to_string pat_res_lf in
      Format.fprintf ppf
        "LOOP_LEFT ON %s [%d] PAT_ACC : (%a) |Right %s [%d] -> (%s,%a)@,\
         |Left %s [%d] -> %a (%a)" (A.to_string arg_lf) label_lf list_of_res
        pat_acc_lf return_pat_res lab_pat_res return_pat_res list_of_res
        pat_acc_lf (A.to_string pat_lf) lab_pat pp_print_node_l node_loop
        list_of_res acc_lf
    | N_MAP (arg, label_pat, pat, node_l, label) ->
      Format.fprintf ppf "MAP %s [%d] WITH PAT : %s [%d] %a" (A.to_string arg)
        label (A.to_string pat) label_pat pp_print_node_l node_l
    | N_FOLD_MAP
        {
          arg_fold_map;
          acc_fold_map;
          pat_acc_fold_map;
          pat_fold_map = pat_fold_map, lab_pat;
          body_fold_map;
          label_map;
        } ->
      Format.fprintf ppf "MAP %s [%d] PAT : %s [%d] PAT_ACC : (%a) %a (%a)"
        (A.to_string arg_fold_map) label_map (A.to_string pat_fold_map) lab_pat
        list_of_res pat_acc_fold_map pp_print_node_l body_fold_map list_of_res
        acc_fold_map

  and pp_print_node_l ppf node_l =
    let print_list =
      Format.pp_print_list
        ~pp_sep:(fun format () -> Format.fprintf format " ;@,")
        pp_print_node in
    Format.fprintf ppf "@[<v 2>{@,%a@]@,}" print_list node_l

  and pp_print_node ppf node =
    match node with
    | N_SIMPLE (prim, args, arg_l, res_l, label) ->
      let str = List.assoc prim Unstack_micheline.prim_macro in
      Format.fprintf ppf "@[%a = %s %a %a [%d]@]" list_of_res res_l str
        list_of_micheline args list_of_arg arg_l label
    | N_START (arg_l, control_node, start) ->
      Format.fprintf ppf "@[%a [%d] = %a @]" list_of_res arg_l start
        pp_print_control control_node
    | FAIL_CONTROL (control_node, _label) ->
      Format.fprintf ppf "@[%a@]" pp_print_control control_node
    | N_END (return_l, label) ->
      Format.fprintf ppf "@[return %a [%d]@]" list_of_res return_l label
    | N_LAMBDA (pat, lab_pat, node_l, lambda, lab_in, lab_out) ->
      Format.fprintf ppf "@[%s = LAMBDA WITH %s [%d] %a [%d][%d]@]"
        (A.to_string lambda) (A.to_string pat) lab_pat pp_print_node_l node_l
        lab_in lab_out
    | N_CONCAT (str1, str2, res, label) ->
      Format.fprintf ppf "@[%s = CONCAT %s %s [%d]@]" (A.to_string res)
        (A.to_string str1) (A.to_string str2) label
    | N_CONCAT_LIST (str_l, res, label) ->
      Format.fprintf ppf "@[%s = CONCAT %s [%d]@]" (A.to_string res)
        (A.to_string str_l) label
    | N_SELF (str_opt, res, label) ->
      Format.fprintf ppf "@[%s = SELF %a [%d]@]" (A.to_string res) print_annot
        str_opt label
    | N_CONTRACT (str_opt, ctrt, res, label) ->
      Format.fprintf ppf "@[%s = CONTRACT %a %s [%d]@]" (A.to_string res)
        print_annot str_opt (A.to_string ctrt) label
    | N_CREATE_CONTRACT (_miche, _node_l, _) ->
      Format.fprintf ppf "@[CREATE_CONTRACT TODO@]"
    | N_NEVER (n, label) ->
      Format.fprintf ppf "@[NEVER %s [%d]@]" (A.to_string n) label
    | N_FAILWITH (fail, label) ->
      Format.fprintf ppf "@[FAILWITH %s [%d]@]" (A.to_string fail) label
    | N_CALL_VIEW (str, arg, view, res, label) ->
      Format.fprintf ppf "@[%s = CALL_VIEW %s %s %s [%d]@]" (A.to_string res)
        str (A.to_string arg) (A.to_string view) label
    | N_EXEC (arg, lambda, res, label_call, label_ret) ->
      Format.fprintf ppf "@[%s = EXEC %s %s [%d][%d]@]" (A.to_string res)
        (A.to_string arg) (A.to_string lambda) label_call label_ret
end
