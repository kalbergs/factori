open Unstack_micheline

type iterator = {
  node : iterator -> name_arg node -> unit;
  control_node : iterator -> name_arg control_node -> unit;
}

let control_node it control =
  match control with
  | N_ITER { body_iter = node_l; _ }
  | N_MAP (_, _, _, node_l, _)
  | N_LOOP_LEFT { node_loop = node_l; _ }
  | N_FOLD_MAP { body_fold_map = node_l; _ }
  | N_LOOP { body = node_l; _ } -> List.iter (it.node it) node_l
  | N_IF_NONE (_, _, node_b1, node_b2, _, _)
  | N_IF_CONS (_, _, node_b1, node_b2, _, _, _, _)
  | N_IF (_, _, node_b1, node_b2)
  | N_IF_LEFT (_, _, (_, _, node_b1), (_, _, node_b2)) ->
    List.iter (it.node it) node_b1 ;
    List.iter (it.node it) node_b2

let node it n =
  match n with
  | N_LAMBDA (_, _, node_l, _, _, _) | N_CREATE_CONTRACT (_, node_l, _) ->
    List.iter (it.node it) node_l
  | N_START (_, n, _) | FAIL_CONTROL (n, _) -> it.control_node it n
  | N_EXEC _ -> ()
  | N_SIMPLE _ -> ()
  | N_END _ -> ()
  | N_CONCAT _ -> ()
  | N_CONCAT_LIST _ -> ()
  | N_SELF _ -> ()
  | N_CONTRACT _ -> ()
  | N_NEVER _ -> ()
  | N_FAILWITH _ -> ()
  | N_CALL_VIEW _ -> ()

let default_iterator = { node; control_node }

type ('a, 'arg, 'env) folder = {
  node_fold : ('a, 'arg, 'env) folder -> 'env -> 'a -> 'arg node -> 'a;
  node_seq : ('a, 'arg, 'env) folder -> 'env -> 'a -> 'arg node list -> 'a;
  control_node_fold :
    ('a, 'arg, 'env) folder -> 'env -> 'a -> 'arg control_node -> 'a;
}

let node_seq folder env acc nodes =
  List.fold_left (folder.node_fold folder env) acc nodes

let control_node_fold folder env acc control =
  match control with
  | N_ITER { body_iter = node_l; _ }
  | N_MAP (_, _, _, node_l, _)
  | N_LOOP_LEFT { node_loop = node_l; _ }
  | N_FOLD_MAP { body_fold_map = node_l; _ }
  | N_LOOP { body = node_l; _ } -> folder.node_seq folder env acc node_l
  | N_IF_NONE (_, _, node_b1, node_b2, _, _)
  | N_IF_CONS (_, _, node_b1, node_b2, _, _, _, _)
  | N_IF (_, _, node_b1, node_b2)
  | N_IF_LEFT (_, _, (_, _, node_b1), (_, _, node_b2)) ->
    let acc = folder.node_seq folder env acc node_b1 in
    folder.node_seq folder env acc node_b2

let node_fold folder env acc n =
  match n with
  | N_CREATE_CONTRACT (_, node_l, _) | N_LAMBDA (_, _, node_l, _, _, _) ->
    folder.node_seq folder env acc node_l
  | FAIL_CONTROL (n, _) | N_START (_, n, _) ->
    folder.control_node_fold folder env acc n
  | N_EXEC _ -> acc
  | N_SIMPLE _ -> acc
  | N_END _ -> acc
  | N_CONCAT _ -> acc
  | N_CONCAT_LIST _ -> acc
  | N_SELF _ -> acc
  | N_CONTRACT _ -> acc
  | N_NEVER _ -> acc
  | N_FAILWITH _ -> acc
  | N_CALL_VIEW _ -> acc

let default_folder = { node_fold; node_seq; control_node_fold }
(*let storage sc = prim `pair*)
