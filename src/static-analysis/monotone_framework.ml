open Auxilary
open Unstack_micheline

module type MF = sig
  module SA : Analysis

  type t

  val lub : t -> t -> t

  val leq : t -> t -> bool

  val bot : t

  val iota : SA.analysis_tools -> t

  val ext : SA.analysis_tools -> label list

  val flow : SA.analysis_tools -> FlowSet.t

  val f : SA.analysis_tools -> label -> t -> t

  val print : Format.formatter -> t -> unit

  val union : t -> t -> t

  val diff : t -> t -> t

  val gen : label -> SA.analysis_tools -> t

  val kill : label -> SA.analysis_tools -> t
end

module Label = Map.Make (struct
  type t = label

  let compare = compare
end)

module LabelSet = Set.Make (struct
  type t = label

  let compare = compare
end)

module Make (A : MF) = struct
  (* type of analyses *)
  type t = A.t Label.t

  (*type of transfer functions: given a label and some knowledge
     computes the added knowledge after the label *)
  type transfer = label -> A.t -> A.t

  (* initialisation of the analysis
     - labels: list of entry labels to analyze
     - ext: extremal labels
     - iota: initial knowledge
     - bot: absence of knowledge

     for each label 'l' in labels associates either iota, either bot depending
     on if 'l' is extremal or not
  *)

  let init (labels : label list) (ext : label list) (iota : A.t) (bot : A.t) : t
      =
    let aux_extremal acc l =
      if List.mem l ext then
        Label.add l iota acc
      else
        Label.add l bot acc in
    List.fold_left aux_extremal Label.empty labels

  let result (labels : label list) (f : transfer) (analysis : t) : t =
    List.fold_left
      (fun acc l -> Label.add l (f l (Label.find l analysis)) acc)
      Label.empty labels

  let update (flow : (label * label) list) (f : transfer) (analysis : t) : t =
    let rec aux_update intra_analysis = function
      | [] -> intra_analysis
      | (l1, l2) :: xs ->
        let analysis1 = Label.find l1 intra_analysis in
        let analysis2 = Label.find l2 intra_analysis in
        let transf1 = f l1 analysis1 in
        if not (A.leq transf1 analysis2) then (
          Format.eprintf "%d -> %d@." l1 l2 ;
          let new_analysis =
            Label.add l2 (A.lub analysis2 transf1) intra_analysis in
          let re_analysis = List.filter (fun (l, _) -> l = l2) flow in
          aux_update new_analysis (re_analysis @ xs)
        ) else
          aux_update intra_analysis xs in
    aux_update analysis flow

  let solveMF (tool : A.SA.analysis_tools) : t =
    let flow = FlowSet.elements (A.flow tool) in
    let labels =
      List.fold_left
        (fun acc (l1, l2) -> LabelSet.add l1 (LabelSet.add l2 acc))
        LabelSet.empty flow in
    let all_labels = LabelSet.elements labels in
    let init_analysis = init all_labels (A.ext tool) (A.iota tool) A.bot in
    let update_analysis = update flow (A.f tool) init_analysis in
    result all_labels (A.f tool) update_analysis

  let run (sc : A.SA.arg Unstack_micheline.sc_node) =
    let tool = A.SA.info_smart sc in
    let lm_out = solveMF tool in
    lm_out
end
