open Factori_utils

type 'a tupletree =
  | Leaf of 'a
  | T of 'a tupletree list

let tupletree_of_list (l : 'a list) : 'a tupletree =
  let rec aux accu = function
    | 7, [] -> T accu
    | 7, l -> T (accu @ [aux [] (0, l)])
    | _, [] -> T accu
    | k, x :: xs -> aux (accu @ [Leaf x]) (k + 1, xs) in
  aux [] (0, l)

(* We need a way to get around the fact that tuples are limited to 8 elements in C# *)
let custom_tuple_define ppf i =
  let open Format in
  let tt = tupletree_of_list (list_integers i) in
  let f_leaf ppf x = fprintf ppf "T%d" x in
  let rec f_t ppf t =
    match t with
    | Leaf x -> f_leaf ppf x
    | T l -> fprintf ppf "Tuple<%a>" (pp_print_list ~pp_sep:(tag ",") f_t) l
  in
  let rec f_mytuple_items ~initial ppf t =
    match t with
    | Leaf x -> fprintf ppf "item%d" x
    | T l ->
      if initial then
        fprintf ppf "%a"
          (pp_print_list ~pp_sep:(tag ",") (f_mytuple_items ~initial:false))
          l
      else
        fprintf ppf "Tuple.Create(%a)"
          (pp_print_list ~pp_sep:(tag ",") (f_mytuple_items ~initial:false))
          l in
  fprintf ppf
    {|
public class MyTuple<%a> : %a {
public MyTuple(%a) : base(%a) {
}
public MyTuple<%a> Create(%a){
return new MyTuple<%a>(%a);
}
}
|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i) f_t tt
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d item%d" i i))
    (list_integers i)
    (f_mytuple_items ~initial:true)
    tt
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d item%d" i i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "item%d" i))
    (list_integers i)

let tuple_encode_generate ppf i =
  let open Format in
  fprintf ppf
    {|
public static Func<MyTuple<%a>,IMicheline> Tuple%dEncode<%a>(%a){
            return (MyTuple<%a> arg) =>
            {
                return new MichelinePrim {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        %a
                    }

                };
            };
        }
|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "Func<T%d,IMicheline> t%dEncode" i i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "t%dEncode(arg.%a)" i print_item i))
    (list_integers i)

(* This may seem unnecessary for now but tuples are limited to 8 in C#
   and it may be useful to asbtract away from that particular type *)
let ntuple_to_Sntuple ppf n =
  let open Format in
  fprintf ppf
    "public static Func<T1,MyTuple<%a>> Tuple%d_to_Tuple%d<%a>(%a){ return (T1 \
     x) => {return new MyTuple<%a>(x,%a);};}"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers n) (n - 1) n
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "T%d x%d" i i))
    (list_integers ~from:2 n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:1 n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "x%d" i))
    (list_integers ~from:2 n)

let tuple_decode_generate ppf i =
  let open Format in
  fprintf ppf
    {|
            public static Func<IMicheline,MyTuple<%a>> Tuple%dDecode<%a>(%a){
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple%dDecode, failed on {im}");
                List<IMicheline> args;
                switch (im) {
                case MichelinePrim mp:
                    switch(mp.Prim){
                        case PrimType.Pair:
                          args = mp.Args;
                          break;
                        default:
                          throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                    break;
                case MichelineArray ma:
                    args = ma;
                    break;
                default:
                    throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if(args.Count < 2){
                    throw new DecodeError($"In Tuple%dDecode, there are less than two elements in {args}");
                    }
                else{
                            if(args.Count > 2){
                              T1 first = t1Decode(args[0]);
                              List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                              MichelineArray ma = new MichelineArray();
                              foreach (var x in rest)
                              {
                               ma.Add(x);
                              }
                              MyTuple<%a> second = Tuple%dDecode<%a>(%a)(ma);
                              return Tuple%d_to_Tuple%d<%a>(%a)(first);
                            }
                            else{//there are at exactly two elements
                              T1 first = t1Decode(args[0]);
                              MichelineArray ma = new MichelineArray();
                              ma.Append(args[1]);
                              MyTuple<%a> second = Tuple%dDecode<%a>(%a)(ma);
                              return Tuple%d_to_Tuple%d<%a>(%a)(first);
                          }
                          }
                    };
            }

|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "Func<IMicheline,T%d> t%dDecode" i i))
    (list_integers i) i i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:2 i) (i - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:2 i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "t%dDecode" i))
    (list_integers ~from:2 i) (i - 1) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "second.%a" print_item i))
    (list_integers ~from:1 (i - 1))
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:2 i) (i - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:2 i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "t%dDecode" i))
    (list_integers ~from:2 i) (i - 1) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "second.%a" print_item i))
    (list_integers ~from:1 (i - 1))

let tuple_generator_generate ppf i =
  let open Format in
  fprintf ppf
    {|
public static Func<MyTuple<%a>> Tuple%dGenerator<%a>(%a){
            return () =>
            {
                return new MyTuple<%a>(%a);
            };
        }
|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "Func<T%d> t%dGenerator" i i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "t%dGenerator()" i))
    (list_integers i)

(* public static Func<Tuple<T1,T2>> tuple2_generator<T1,T2>(Func<T1> t1_generator,Func<T2> t2_generator){
 *             return () => {return Tuple.Create(t1_generator(),t2_generator());};
 *         } *)

let repeat n x = List.map (fun _ -> x) (list_integers ~from:1 n)

let tuple_boilerplate n =
  let open Format in
  Format.asprintf "\n\n%a\n\n\n%a\n\n\n%a\n\n\n%a\n\n\n%a\n\n\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_encode_generate ppf i))
    (list_integers ~from:3 n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> ntuple_to_Sntuple ppf i))
    (list_integers ~from:2 n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> tuple_decode_generate ppf i))
    (list_integers ~from:3 n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_generator_generate ppf i))
    (list_integers ~from:2 n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> custom_tuple_define ppf i))
    (list_integers ~from:1 n)
