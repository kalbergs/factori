open Format
open Factori_utils
open Types

let auxiliary_functions ppf () =
  fprintf ppf
    {|/*Auxiliary functions*/

public class LocalRandom {
    private static System.Random random = new System.Random();
    public static int randomInt(int low, int high){
      int num = random.Next(0,high);
      return num;
}
}

public static T chooseFrom<T>(List<T> l){
  System.Random random = new System.Random();
  int num = LocalRandom.randomInt(0,l.Count);
  return l[num];
}



|}
(* "\n\
 *  def myrandom (mn : int,mx : int):\n\
 *  \tr = random() * (mx - mn) + mn\n\
 *  \tres = floor(r)\n\
 *  \treturn res\n\n\n\
 *  def chooseFrom(l : List[T]):\n\
 *  \tmylist = l\n\
 *  \tn = len(mylist)\n\
 *  \tindex = myrandom(0,n)\n\
 *  \tchosenFun = mylist[index]\n\
 *  \treturn chosenFun;\n\n\n\n\
 *  def int64_generator():\n\
 *  \treturn myrandom(0,10)\n\n\n\
 *  def make_list(my_generator : Any,k : int):\n\
 *  \tres = list(range(k))\n\
 *  \tfor i in range(k):\n\
 *  \t\tres[i] = my_generator()\n\
 *  \tdef result():\n\
 *  \t\treturn res\n\n\
 *  \treturn result\n\n\n" *)

let all_base =
  [
    Never;
    String;
    Nat;
    Int;
    Byte;
    Address;
    Signature;
    Unit;
    Bool;
    Timestamp;
    Keyhash;
    Key;
    Mutez;
    Operation;
    Chain_id;
    Sapling_state;
    Sapling_transaction_deprecated;
  ]

let all_unary = [Set; List; Option; Ticket]

let all_binary = [Map; BigMap; Lambda]

let generate_all_kinds (* tuple_size_min tuple_size_max *) =
  [AContract]
  @ List.map (fun b -> ABase b) all_base
  @ List.map (fun b -> AUnary b) all_unary
  @ List.map (fun b -> ABinary b) all_binary
(* @ List.map
 *     (fun i -> ATuple i)
 *     (list_integers ~from:tuple_size_min tuple_size_max) *)

let build_generator ppf kind =
  match kind with
  | ABase b -> (
    match b with
    | Bool ->
      fprintf ppf
        "public static Bool BoolGenerator(){\n\
         \tvar myres = chooseFrom(new List<bool>(){true, false});\n\
         \treturn myres;}"
    | Int ->
      fprintf ppf
        "public static Int IntGenerator(){\n\
         \treturn (BigInteger) (LocalRandom.randomInt(0,10));\n\
         }"
    | Mutez ->
      fprintf ppf
        "public static Tez TezGenerator(){\n\
         \treturn new Tez(IntGenerator());\n\
         }"
    | Nat ->
      fprintf ppf
        "public static Nat NatGenerator(){\n\
         \treturn (BigInteger) (LocalRandom.randomInt(0,10));\n\
         }"
    | Timestamp ->
      fprintf ppf
        "public static Timestamp TimestampGenerator(){\n\
         \treturn \"2021-06-03T09:06:14.990-00:00\";}"
    | Never ->
      fprintf ppf
        "public static Never NeverGenerator(){\n\
         \tthrow new Exception(\"Never say never\");\n\
         }"
    | String ->
      fprintf ppf
        {|
class ShakespeareGenerator
{
    private static readonly string[] Quotes = new[] {
        "To be or not to be",
        "All the world's a stage",
        "To thine own self be true",
        "All that glitters is not gold",
        "There is nothing either good or bad",
        "To err is human, to forgive divine",
        "The lady doth protest too much",
        "To be, and not to be",
        "All's well that ends well",
        "A rose by any other name",
        "To sleep, perchance to dream",
        "To be, or not to be: that is the question"
    };

    public static string GetRandomQuote()
    {
        var random = new Random();
        var index = random.Next(Quotes.Length);
        return Quotes[index];
    }
}


public static string StringGenerator(){
	return ShakespeareGenerator.GetRandomQuote();}
|}
    | Byte ->
      fprintf ppf
        "public static Bytes BytesGenerator(){\n\
         \treturn Encoding.ASCII.GetBytes(\"ff\");}"
    | Address ->
      fprintf ppf
        "public static Address AddressGenerator(){\n\
         \treturn \"tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb\";}\n"
    | Unit ->
      fprintf ppf "public static Unit UnitGenerator(){\n\treturn new Unit();}\n"
    | Keyhash ->
      fprintf ppf
        "public static KeyHash KeyHashGenerator(){\n\
         \treturn new KeyHash(AddressGenerator());}\n"
    | Key ->
      fprintf ppf
        "public static Key KeyGenerator(){\n\
         \treturn \"edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav\";}\n"
    | Signature ->
      fprintf ppf
        "public static Signature SignatureGenerator(){\n\
         \treturn \
         \"edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg\";}\n"
    | Operation ->
      fprintf ppf
        "public static Operation OperationGenerator(){\n\
         \treturn \"op92MgaBZvgq2Rc4Aa4oTeZAjuAWHb63J2oEDP3bnFNWnGeja2T\";}"
    | Chain_id ->
      fprintf ppf
        "public static ChainId ChainIdGenerator(){\n\
         \treturn StringGenerator();}"
    | Sapling_state | Sapling_transaction_deprecated ->
      let typename = str_of_base b in
      fprintf ppf
        "/*public static %s %sGenerator(){\n\
        \        return StringGenerator();}*/\n\
        \        "
        (String.capitalize_ascii typename)
        typename)
  | AContract ->
    fprintf ppf
      "public static Contract ContractGenerator(){\nreturn new Contract ();\n}"
  | AUnary u -> (
    match u with
    | List ->
      fprintf ppf
        "public static Func<List<T>> ListGenerator<T>(Func<T> generator){\n\
        \            return () => {var res = new List<T> ();\n\
        \                int size = LocalRandom.randomInt(0,5);\n\
        \                for (int i = 0; i < size; i++)\n\
        \                {\n\
        \                    res.Add(generator());\n\
        \                }\n\
        \                return res;};\n\
        \        }\n"
    | Set ->
      fprintf ppf
        "public static Func<Set<T>> SetGenerator<T>(Func<T> generator){\n\
        \            return () => {var res = new Set<T> ();\n\
        \                int size = LocalRandom.randomInt(0,5);\n\
        \                for (int i = 0; i < size; i++)\n\
        \                {\n\
        \                    res.Add(generator());\n\
        \                }\n\
        \                return res;};\n\
        \        }\n"
    | Option ->
      fprintf ppf
        "public static Func<Option<T>> OptionGenerator<T>(Func<T> generator) \
         where T : notnull{\n\
         \tvar optres = generator();\n\
         \treturn () => {\n\
         \t\treturn chooseFrom(new List<Option<T>> {new Some<T>(optres),new \
         None<T>()});\n\
         };\n\
         }\n"
    | Ticket ->
      fprintf ppf
        "public static Ticket TicketGenerator(){\nreturn new Ticket();\n}")
  | ABinary b -> (
    match b with
    | Map ->
      fprintf ppf
        "public static Func<Map<TKey,TValue>> \
         MapGenerator<TKey,TValue>(Func<TKey> keyGenerator,Func<TValue> \
         valueGenerator) where TKey : notnull where TValue : notnull{\n\
        \            return () => {return new Map<TKey, \
         TValue>(DictionaryGenerator<TKey,TValue>(keyGenerator,valueGenerator)());};\n\
        \        }"
    | BigMap ->
      fprintf ppf
        "public static Func<BigMap<TKey,TValue>> \
         BigMapGenerator<TKey,TValue>(Func<TKey> keyGenerator,Func<TValue> \
         valueGenerator)where TKey : notnull where TValue : notnull{\n\
        \            return () => {return new LiteralBigMap<TKey, \
         TValue>(DictionaryGenerator<TKey,TValue>(keyGenerator,valueGenerator)());};\n\
        \        }\n"
    | Lambda ->
      fprintf ppf
        "public static Lambda LambdaGenerator(){\n\
        \           return new Lambda();\n\
         }")
  | ATuple k ->
    fprintf ppf
      "public static MyTuple<%a> Tuple%dGenerator<%a>(%a){\n\
       \tdef result():\n\
       \t\treturn [%a]\n\
       \treturn result}\n"
      (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "T%d" i))
      (list_integers ~from:1 k) k
      (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "T%d" i))
      (list_integers ~from:1 k)
      (pp_print_list ~pp_sep:(tag ", ") (fun ppf i ->
           fprintf ppf "Func<T%d> f%d" i i))
      (list_integers ~from:1 k)
      (* (pp_print_list ~pp_sep:(tag ",") (fun ppf i -> fprintf ppf "T%d" i))
       * (list_integers ~from:1 k) *)
      (pp_print_list ~pp_sep:(tag ", ") (fun ppf i -> fprintf ppf "f%d()" i))
      (list_integers ~from:1 k)
