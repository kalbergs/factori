open Format
open Csharp_generator_generation

let csharp_types =
  Format.asprintf
    {|
using Netezos.Encoding;
using System.Numerics;
using System.Text;


namespace FactoriTypes
{

    public class EncodeError : Exception {
        public EncodeError(){
        }

        public EncodeError(string msg) : base(msg) {}
    }

    public class DecodeError : Exception {
        public DecodeError(){
        }

        public DecodeError(string msg) : base(msg) {}
    }

    public class HandleErrors{

    public void HandleEncodingException(EncodeError e){
        Console.WriteLine($"Encoding error: {e}");
    }

    public void HandleDecodingException(EncodeError e){
        Console.WriteLine($"Decoding error: {e}");
    }

    }

    public class Utils
    {
      public static byte[] StringToByteArray(string hex) {
          return Enumerable.Range(0, hex.Length)
                           .Where(x => x %% 2 == 0)
                           .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                           .ToArray();
      }
    }

    // Our type for ints is System.Numerics.BigInteger
    // We could replace MichelineInt by IMicheline, but it's better to have the finer type
    public class Types
    {

        public static bool is_left(IMicheline im){
            switch(im){
                case MichelinePrim mp:
                    switch(mp.Prim){
                        case PrimType.Left:
                            return true;
                        case PrimType.Right:
                            return false;
                        default:
                            throw new DecodeError($"There was an error, we are trying to understand whether {im} is Left");
                    }
                default:
                    throw new DecodeError($"There was an error, we are trying to understand whether {im} is Left");
            }
        }

        public static bool is_right(IMicheline im){
            switch(im){
                case MichelinePrim mp:
                    switch(mp.Prim){
                        case PrimType.Left:
                            return false;
                        case PrimType.Right:
                            return true;
                        default:
                            throw new DecodeError($"There was an error, we are trying to understand whether {im} is Right");
                    }
                default:
                    throw new DecodeError($"There was an error, we are trying to understand whether {im} is Right");
            }
        }

        public class Map<TKey, TValue> : Dictionary<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
        {
            public Map() : base() { }
            public Map(Dictionary<TKey,TValue> dict) : base(dict) { }
        }

        public static Func<Dictionary<TKey, TValue>, IMicheline> DictEncode<TKey,TValue>(Func<TKey, IMicheline> keyEncode, Func<TValue, IMicheline> valueEncode)  where TKey: notnull where TValue: notnull
            {
                return (Dictionary<TKey, TValue> x) =>
                    {
                        var res = new MichelineArray(x.Keys.Count());

                        foreach (KeyValuePair<TKey, TValue> kvPair in x)
                        {
                            var elem = new MichelinePrim
                            {
                                Prim = PrimType.Elt,
                                Args = new List<IMicheline>{
                                keyEncode(kvPair.Key),
                                valueEncode(kvPair.Value)
                                }
                            };
                            res.Append(elem);

                        };
                        return res;
                    };

            }

        public static Func<Map<TKey, TValue>, IMicheline> MapEncode<TKey,TValue>(Func<TKey, IMicheline> keyEncode, Func<TValue, IMicheline> valueEncode)  where TKey: notnull where TValue: notnull
            {
                return DictEncode<TKey,TValue>(keyEncode,valueEncode);
            }


public static Func<IMicheline,KeyValuePair<TKey,TValue>> DecodeMapElt<TKey,TValue>(Func<IMicheline,TKey> keyDecode,Func<IMicheline,TValue> valueDecode)   where TKey: notnull where TValue: notnull{
            return (IMicheline im) => {
                switch(im){
                    case MichelinePrim mp:
                        switch(mp.Prim){
                            case PrimType.Elt:
                                if(mp.Args.Count < 2){
                                    throw new DecodeError($"There should be two elements in Elt component {mp} of {im}");
                                }
                                else
                                {
                                    return new KeyValuePair<TKey, TValue>(keyDecode(mp.Args[0]),valueDecode(mp.Args[1]));
                                }
                            default:
                            throw new DecodeError($"Error trying to decode a map element from {im}, {mp} is not an Elt");
                        }
                    default:
                        throw new DecodeError($"Error trying to decode a map element from {im}");

                }
            };
        }

        public static Func<IMicheline,Map<TKey,TValue>> MapDecode<TKey,TValue>(Func<IMicheline,TKey> keyDecode,Func<IMicheline,TValue> valueDecode)   where TKey: notnull where TValue: notnull{
            return (IMicheline im) => {
                var res = new Dictionary<TKey,TValue>();
                switch(im){
                    case MichelineArray ma:
                        foreach (var item in ma)
                        {
                            res.Append(DecodeMapElt<TKey,TValue>(keyDecode,valueDecode)(item));
                        }
                        return (Map<TKey,TValue>) res;
                    default:
                        throw new DecodeError($"Error trying to decode a map from {im}");
                }
            };
        }

        public static Func<IMicheline,List<T>> ListDecode<T>(Func<IMicheline,T> tDecode){
                return (IMicheline im) => {
                    switch(im){
                        case MichelineArray ma:
                            List<T> res = new List<T>();
                            foreach (var item in ma)
                            {
                               res.Append(tDecode(item));
                            }
                            return res;
                        default:
                            throw new DecodeError($"Error decoding {im} as a list");
                    }
                };
        }

        public static Func<IMicheline,Set<T>> SetDecode<T>(Func<IMicheline,T> tDecode){
                return (IMicheline im) => {
                    switch(im){
                        case MichelineArray ma:
                            Set<T> res = new Set<T>();
                            foreach (var item in ma)
                            {
                               res.Append(tDecode(item));
                            }
                            return res;
                        default:
                            throw new DecodeError($"Error decoding {im} as a set");
                    }
                };
        }


        public static Lambda LambdaDecode(IMicheline im){
            switch (im)
            {
                case MichelinePrim mp:
                    switch (mp.Prim)
                    {
                        case PrimType.LAMBDA:
                            return new Lambda();
                        default:
                            throw new DecodeError($"Could not decode lambda {im} (Prim but not LAMBDA)");
                    }
                default:
                    throw new DecodeError($"Could not decode lambda {im} (Not Prim)");
            }
        }

        public static Func<Dictionary<TKey,TValue>> DictionaryGenerator<TKey,TValue>(Func<TKey> keyGenerator,Func<TValue> valueGenerator) where TKey : notnull where TValue : notnull{
            return () => {int size = LocalRandom.randomInt(0,5);
            var dict = new Dictionary<TKey,TValue> ();

            for (int i = 0; i < size; i++){
                var key = keyGenerator();
                if(!dict.ContainsKey(key)){
                  dict.Add(key,valueGenerator());
                }
            }
            return dict;};
        }


        public class BigMap<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
        { }

        public class LiteralBigMap<TKey, TValue> : BigMap<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
        {
            public Dictionary<TKey, TValue> _value;
            public LiteralBigMap(Dictionary<TKey, TValue> dict) { _value = dict; }
            public LiteralBigMap() { _value = new Dictionary<TKey, TValue>(); }
            public static implicit operator LiteralBigMap<TKey,TValue>(Dictionary<TKey,TValue> dict) => new LiteralBigMap<TKey,TValue>(dict);
        }

        public class AbstractBigMap<TKey, TValue> : BigMap<TKey,TValue>
        where TKey : notnull
        where TValue : notnull
        {
            private BigInteger _value;
            public AbstractBigMap(BigInteger n)
            {
                _value = n;
            }
            public static implicit operator BigInteger(AbstractBigMap<TKey, TValue> i) => i._value;
            public static implicit operator AbstractBigMap<TKey,TValue>(BigInteger i) => new AbstractBigMap<TKey,TValue>(i);
        }


        public static Func<BigMap<TKey,TValue>,IMicheline> BigMapEncode<TKey,TValue>(Func<TKey, IMicheline> keyEncode, Func<TValue, IMicheline> valueEncode) where TKey: notnull where TValue: notnull{
            return (BigMap<TKey,TValue> b) => {switch (b)
            {
                case AbstractBigMap<TKey,TValue> abm:
                    return DictEncode<TKey, TValue>(keyEncode, valueEncode)(new Dictionary<TKey, TValue>());
                case LiteralBigMap<TKey,TValue> lbm:
                    return DictEncode<TKey,TValue>(keyEncode,valueEncode)(lbm._value);
                default:
                    throw new EncodeError("Unknown case in big_mapEncode");
            };};
        }

        public static Func<IMicheline,BigMap<TKey,TValue>> BigMapDecode<TKey,TValue>(Func<IMicheline,TKey> keyDecode,Func<IMicheline,TValue> valueDecode)   where TKey: notnull where TValue: notnull{
           return (IMicheline im) => {
                switch(im){
                    case MichelineInt mi:
                        return new AbstractBigMap<TKey,TValue>(mi.Value);
                    case MichelineArray ma:
                        var dict = new Dictionary<TKey,TValue>();
                        foreach (var item in ma)
                        {
                            dict.Append(DecodeMapElt<TKey,TValue>(keyDecode,valueDecode)(item));
                        }
                        return new LiteralBigMap<TKey,TValue>(dict);
                    default:
                    throw new DecodeError($"Could not decode bigmap from {im}");
                    }
           };
        }

        public class Set<T> : List<T>{
            public Set():base() {}
        }

        public class Contract {
            // TODO
        }

        public static IMicheline ContractEncode(Contract c){
            throw new EncodeError("TODO: ContractEncode");
        }

        public static Contract ContractDecode(IMicheline m){
            return new Contract();
        }

        public class Never {
            //TODO
        }

        public class Lambda {
            // TODO
        }


        public static IMicheline LambdaEncode(Lambda l){
            throw new EncodeError("TODO: LambdaEncode");
        }

        public class Ticket {
            // TODO
        }

        public static IMicheline TicketEncode(Ticket t){
            throw new EncodeError("TODO: TicketEncode");
        }

        public static Ticket TicketDecode(IMicheline m){
            return new Ticket();
        }


        public class ChainId{
            private string _value;
            public ChainId(string addr){
                _value = addr;
            }
            public static implicit operator string(ChainId a) => a._value;
            public static implicit operator ChainId(string s) => new ChainId(s);

        }

        public class Unit {
        public Unit() { }

        }

        public class Address{
            private string _value;
            public Address(string addr){
                _value = addr;
            }
            public static implicit operator string(Address a) => a._value;
            public static implicit operator Address(string s) => new Address(s);
        }

        public class Signature{
            private string _value;
            public Signature(string addr){
                _value = addr;
            }
            public static implicit operator string(Signature a) => a._value;
            public static implicit operator Signature(string s) => new Signature(s);
        }

        public class Timestamp{
           private string _value;
           public Timestamp(string b){
             _value = b;
           }
           public static implicit operator string(Timestamp a) => a._value;
           public static implicit operator Timestamp(string b) => new Timestamp(b);
        }

        public class Key{
            private string _value;
            public Key(string addr){
                _value = addr;
            }
            public static implicit operator string(Key a) => a._value;
            public static implicit operator Key(string s) => new Key(s);
        }

        public class KeyHash{
            private string _value;
            public KeyHash(string addr){
                _value = addr;
            }
            public static implicit operator string(KeyHash a) => a._value;
            public static implicit operator KeyHash(string s) => new KeyHash(s);
        }

        public class Operation{
            private string _value;
            public Operation(string addr){
                _value = addr;
            }
            public static implicit operator string(Operation a) => a._value;
            public static implicit operator Operation(string s) => new Operation(s);
        }

        public class Int{
            private BigInteger _value;
            public Int(BigInteger n){
                _value = n;
            }
            public static implicit operator BigInteger(Int i) => i._value;
            public static implicit operator Int(BigInteger bi) => new Int(bi);
        }

        public class Nat{
            private BigInteger _value;
            public Nat(BigInteger n){
                _value = n;
            }
            public static implicit operator BigInteger(Nat i) => i._value;
            public static implicit operator Nat(BigInteger bi) => new Nat(bi);
        }

        public class Tez{
            private BigInteger _value;
            public Tez(BigInteger n){
                _value = n;
            }
            public static implicit operator BigInteger(Tez i) => i._value;
            public static implicit operator Tez(BigInteger bi) => new Tez(bi);
        }

        public static MichelineInt TezEncode(Tez t){
            return new MichelineInt(t);
        }

        public static Tez TezDecode(IMicheline im){
            switch(im){
              case MichelineInt mi:
                return new Tez(mi.Value);
              default:
                throw new DecodeError($"Couldn't decode tez value {im}");
            }
        }

        public class Bytes {
            private byte[] _bytes;

            public Bytes(byte[] b){
                _bytes = b;
            }
        public static implicit operator Bytes(byte[] b) => new Bytes(b);
        public static implicit operator byte[](Bytes b) => b._bytes;


        }

        public class Bool {
            public bool b;

            public Bool(bool b){
                this.b = b;
            }
        public static implicit operator Bool(bool b) => new Bool(b);
        public static implicit operator bool(Bool b) => b.b;
        }

        public static MichelineString SignatureEncode(Signature s){
            return new MichelineString(s);
        }

        public static MichelineString ChainIdEncode(ChainId s){
            return new MichelineString(s);
        }

        public static Signature SignatureDecode(IMicheline im){
            switch(im){
             case MichelineString ms:
               return new Signature(ms.Value);
             default:
               throw new DecodeError($"Couldn't decode signature from {im}");
           }
        }

        public static ChainId ChainIdDecode(IMicheline im){
            switch(im){
             case MichelineString ms:
               return new ChainId(ms.Value);
             default:
               throw new DecodeError($"Couldn't decode chain_id from {im}");
           }
        }


        public static MichelineString TimestampEncode(Timestamp s){
            return new MichelineString(s);
        }

        public static Timestamp TimestampDecode(IMicheline im){
           switch(im){
             case MichelineString ms:
               return new Timestamp(ms.Value);
             default:
               throw new DecodeError($"Couldn't decode timestamp from {im}");
           }
        }


        public static MichelineString KeyEncode(Key s){
            return new MichelineString(s);
        }

        public static MichelineString KeyHashEncode(KeyHash s){
            return new MichelineString(s);
        }

        public static Key KeyDecode(IMicheline im){
            switch(im){
             case MichelineString ms:
               return new Key(ms.Value);
             default:
               throw new DecodeError($"Couldn't decode Key from {im}");
           }
        }

        public static KeyHash KeyHashDecode(IMicheline im){
            switch(im){
             case MichelineString ms:
               return new KeyHash(ms.Value);
             default:
               throw new DecodeError($"Couldn't decode KeyHash from {im}");
           }
        }

        public static MichelineBytes BytesEncode(Bytes bytes){
            return new MichelineBytes(bytes);
        }

        public static Bytes BytesDecode(IMicheline im){
            switch(im){
              case MichelineBytes mb:
                return mb.ToBytes();
              default:
                throw new DecodeError($"Failed to decode bytes from {im}");
            }
        }

        public static MichelineInt IntEncode(Int i)
        {
            var res = new MichelineInt(i);
            return res;
        }

        public static Int IntDecode(IMicheline im)
        {
            switch(im){
              case MichelineInt mi:
                var res = mi.Value;
                return res;
              default:
                throw new DecodeError($"A Micheline Int was expected, got {im}");
            }
        }

        public static MichelineInt NatEncode(Nat n){
            var res = new MichelineInt(n);
            return res;
        }

        public static Nat NatDecode(IMicheline im){
            switch(im){
              case MichelineInt mi:
                var res = mi.Value;
                return res;
              default:
                throw new DecodeError($"A Micheline Int was expected, got {im}");
            }
        }

        public static MichelineString StringEncode(string s)
        {
            return new MichelineString(s);
        }

        public static string StringDecode(IMicheline im)
        {
            switch(im){
              case MichelineString ms:
                var res = ms.Value;
                return res;
              default:
                throw new DecodeError($"A Micheline String was expected, got {im}");
            }
        }

        public static MichelineString AddressEncode(Address s)
        {
            return new MichelineString(s);
        }

        public static Address AddressDecode(IMicheline im)
        {
            switch(im){
              case MichelineString ms:
              return new Address(ms.Value);
              default:
                throw new DecodeError($"A Micheline String (Address) was expected, got {im}");
            }


        }

        public static MichelinePrim BoolEncode(Bool b){
            if(b.b){
                return new MichelinePrim {Prim = PrimType.True};
            }
            else{
                return new MichelinePrim {Prim = PrimType.True};
            }
        }

        public static Bool BoolDecode(IMicheline im){
            switch (im){
            case MichelinePrim m:
              switch (m.Prim)
              {
                  case(PrimType.True):
                  return new Bool(true);
                  case(PrimType.False):
                  return new Bool(false);
                  default:
                  throw new DecodeError($"Couldn't decode boolean{m}");
              }
           default:
             throw new DecodeError($"Failed to decode a boolean out of {im}");
            }
        }

        public static MichelinePrim UnitEncode(Unit u){
             return new MichelinePrim {Prim = PrimType.Unit};
        }

        public static Unit UnitDecode(IMicheline im){
             switch(im){
               case MichelinePrim mp:
                 switch(mp.Prim){
                   case PrimType.Unit:
                     return new Unit();
                   default:
                     throw new DecodeError($"Error decoding unit from {im}");
                 }
                 default:
                     throw new DecodeError($"Error decoding unit from {im}");
             }
        }

        public class Option<T>
            where T : notnull{
        }

        public class Some<T> : Option<T> where T : notnull{
            public T? Value {get; set;}
            public Some(T value){Value = value;}
        }

        public class None<T> : Option<T> where T : notnull{
            // Nothing
        }

        public static Func<Option<T>,MichelinePrim> OptionEncode<T>(Func<T, IMicheline> encode) where T : notnull{
            return (Option<T> v) =>
                {
                    switch (v)
                    {
                        case(Some<T> s):
                        if(s.Value == null){
                            throw new EncodeError($"null value in option {s}");
                        }
                        return new MichelinePrim {Prim = PrimType.Some,
                                                  Args = new List<IMicheline> {encode(s.Value)}};
                        case(None<T> n):
                        return new MichelinePrim {Prim = PrimType.None};
                        default:
                        throw new EncodeError($"Error encoding option {v}");
                    }
                };

        }

        public static Func<IMicheline,Option<T>> OptionDecode<T>(Func<IMicheline,T> decode) where T : notnull{
            return (IMicheline im) => {
                switch (im)
                {
                    case MichelinePrim mp:
                     switch (mp.Prim)
                     {
                        case PrimType.Some:
                        return new Some<T>(decode(mp.Args[0]));
                        case PrimType.None:
                        return new None<T>();
                        default:
                        throw new DecodeError($"Couldn't decode option value {im} looking at {mp}");
                     }
                    default:
                    throw new DecodeError($"Couldn't decode option value {im}");
                }
            };
        }

        public static Func<List<T>,MichelineArray> ListEncode<T>(Func<T,IMicheline> encode){
            return (List<T> l) => {var res = new MichelineArray();
                for (int i = 0; i < l.Count; i++){
                    res.Add(encode(l[i]));
                }
                return res;
                };

            }

        public static Func<Set<T>,MichelineArray> SetEncode<T>(Func<T,IMicheline> encode){
            return (Set<T> l) => {var res = new MichelineArray();
                for (int i = 0; i < l.Count; i++){
                    res.Add(encode(l[i]));
                }
                return res;
                };

            }

        public static MichelinePrim make_right(IMicheline v){
            var res = new MichelinePrim{
                Prim = PrimType.Right,
                Args = new List<IMicheline>{v}
            };
            return res;
        }

                public static MichelinePrim make_left(IMicheline v){
            var res = new MichelinePrim{
                Prim = PrimType.Left,
                Args = new List<IMicheline>{v}
            };
            return res;
        }


        public static Func<MyTuple<T1,T2>,IMicheline> Tuple2Encode<T1,T2>(Func<T1,IMicheline> t1Encode, Func<T2,IMicheline> t2Encode){
            return (MyTuple<T1,T2> arg) =>
            {
                return new MichelinePrim {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1),
                        t2Encode(arg.Item2)
                    }

                };
            };
        }

        public static Func<IMicheline,MyTuple<T1,T2>> Tuple2Decode<T1,T2>(Func<IMicheline,T1> t1Decode, Func<IMicheline,T2> t2Decode){
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple2Decode, failed on {im}");
                switch (im){
                case (MichelinePrim mp):
                    switch(mp.Prim){
                        case(PrimType.Pair):
                          if(mp.Args.Count < 2){
                            throw new DecodeError($"In Tuple2Decode, there are less than two elements in {mp}");
                          }
                          else{
                            T1 first = t1Decode(mp.Args[0]);
                            T2 second = t2Decode(mp.Args[1]);
                            return new MyTuple<T1,T2>(first,second);
                          }                        default:
                            throw error;
                    }
                case (MichelineArray ma):
                        if (ma.Count==2){
                            T1 first = t1Decode(ma[0]);
                            T2 second = t2Decode(ma[1]);
                            return new MyTuple<T1, T2>(first, second);
                        }
                        else{
                            throw new DecodeError($"In Tuple2Decode, there are {ma.Count} elements instead of 2 elements as expected.");
                        }

                default:
                throw error;
            }
            };
        }

        %s
    
        %a
        %a
    }
}|}
    (Csharp_tuple_boilerplate.tuple_boilerplate 11)
    auxiliary_functions ()
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n\n") (fun ppf x ->
         fprintf ppf "%a" build_generator x))
    generate_all_kinds
(* 2 50 *)

(* limited to 7 because of C#. There are two possible solutions (custom Tuple type or nested tuples), will solve later. *)
