open Factori_utils
open Factori_file

let ( // ) = Factori_options.concat

type factori_config = unit [@@deriving encoding]
(* {
 *   ginger : string option
 * } [@@deriving encoding] *)

(* FIXME/TODO should this be a hardcoded type or do we keep it as
   string? *)
type network = string [@@deriving encoding]

type address = string [@@deriving encoding]

type interface_language =
  | OCaml
  | Typescript
  | Python
  | Csharp
(* Not done yet *)
[@@deriving encoding]

type contract_format =
  | Michelson
  | MichelsonJson
  | KT1
[@@deriving show, encoding]

type contract = {
  contract_name : string;
  original_format : contract_format;
  original_kt1 : (address * network) option;
  import_date : string;
  options : string list;
}
[@@deriving encoding]

let str_of_language (i : interface_language) =
  match i with
  | OCaml -> "ocaml"
  | Typescript -> "typescript"
  | Python -> "python"
  | Csharp -> "csharp"

let has_language (i : interface_language) (c : contract) =
  List.mem (str_of_language i) c.options

let get_default_contract_name ~kt1 =
  String.uncapitalize_ascii (String.sub kt1 0 7)

type local_config = { contracts : contract list } [@@deriving encoding]

type version_config = { version : string } [@@deriving encoding]

(* The main directory of the project, either specified from arguments
   or the current dir *)
let get_dir () =
  match !Factori_options.dir with
  | None -> Sys.getcwd ()
  | Some dir ->
    if Filename.is_relative dir then
      Sys.getcwd () // dir
    else
      dir

let get_contracts_dir ~dir = dir // "src" // "contracts"

let get_typenaming_file ~dir ~contract_name =
  get_contracts_dir ~dir // "type_naming"
  // (get_sanitized contract_name ^ ".json")

let get_local_config_name ~dir = Factori_options.concat dir "contracts.json"

let get_version_file_name ~dir = Factori_options.concat dir "project.json"

let get_contract_signature_filename ~contract_name ~dir =
  get_contracts_dir ~dir
  // Format.sprintf "%s_signature.json" (show_sanitized_name contract_name)

type contract_signature = Proto.micheline * Tzfunc.Node.entrypoints

let contract_signature_enc =
  let open Json_encoding in
  obj2
    (req "storage" Proto.micheline_enc.json)
    (req "entrypoints" Tzfunc.Node.entrypoints_enc.json)

let marshal_contract_signature ~dir ~contract_name storage
    (entrypoints : Tzfunc.Node.entrypoints) =
  let filename = get_contract_signature_filename ~contract_name ~dir in
  serialize_file ~filename ~enc:contract_signature_enc (storage, entrypoints)

let unmarshal_contract_signature ~dir ~contract_name =
  let filename = get_contract_signature_filename ~contract_name ~dir in
  deserialize_file ~filename ~enc:contract_signature_enc

(* OCaml file names and directories *)

let get_ocaml_interface_basename ~contract_name =
  show_sanitized_name contract_name ^ "_ocaml_interface"

let get_ocaml_abstract_interface_basename ~contract_name =
  show_sanitized_name contract_name ^ "_abstract_ocaml_interface"

let get_ocaml_crawlori_tables_basename ~contract_name =
  show_sanitized_name contract_name ^ "_tables"

let get_ocaml_crawlori_plugin_basename ~contract_name =
  show_sanitized_name contract_name ^ "_plugin"

let get_ocaml_abstract_interface_filename ~contract_name =
  get_ocaml_abstract_interface_basename ~contract_name ^ ".ml"

let get_ocaml_abstract_interface_mli_filename ~contract_name =
  get_ocaml_abstract_interface_basename ~contract_name ^ ".mli"

let get_ocaml_interface_filename ~contract_name =
  get_ocaml_interface_basename ~contract_name ^ ".ml"

let get_ocaml_interface_mli_filename ~contract_name =
  get_ocaml_interface_basename ~contract_name ^ ".mli"

let get_ocaml_crawlori_tables_filename ~contract_name =
  get_ocaml_crawlori_tables_basename ~contract_name ^ ".ml"

let get_ocaml_crawlori_plugin_filename ~contract_name =
  get_ocaml_crawlori_plugin_basename ~contract_name ^ ".ml"

let get_ocaml_interface_dir ?(relative = false) ~dir () =
  let tail = "src" // "ocaml_sdk" in
  if relative then
    tail
  else
    dir // tail

let get_ocaml_libraries_dir ~dir = dir // "src" // "libraries"

let get_ocaml_factori_abstract_types_path ~dir =
  get_ocaml_libraries_dir ~dir // "factori_abstract_types.ml"

let get_ocaml_factori_types_path ~dir =
  get_ocaml_libraries_dir ~dir // "factori_types.ml"

let get_ocaml_blockchain_path ~dir =
  get_ocaml_libraries_dir ~dir // "blockchain.ml"

let get_ocaml_crawlori_dir ?(relative = false) ~dir () =
  let tail = "src" // "ocaml_crawlori" in
  if relative then
    tail
  else
    dir // tail

(* Python file names and directories *)

let get_python_interface_dir ?(relative = false) ~dir () =
  let tail = "src" // "python_sdk" in
  if relative then
    tail
  else
    dir // tail

let get_python_interface_basename ~contract_name =
  show_sanitized_name contract_name ^ "_python_interface"

let get_python_code_basename ~contract_name () =
  show_sanitized_name contract_name ^ "_code"

let get_python_interface_filename ~contract_name =
  get_python_interface_basename ~contract_name ^ ".py"

let get_python_code_filename ~contract_name =
  get_python_code_basename ~contract_name () ^ ".json"

let get_python_interface_path ?(relative = false) ~dir ~contract_name () =
  get_python_interface_dir ~relative ~dir ()
  // get_python_interface_filename ~contract_name

let get_python_libraries_dir ~dir = get_python_interface_dir ~dir ()

let get_python_factori_types_path ~dir =
  get_python_libraries_dir ~dir // "factori_types.py"

let get_python_code_path ?(relative = false) ~dir ~contract_name () =
  get_python_interface_dir ~relative ~dir ()
  // get_python_code_filename ~contract_name

let get_python_blockchain_path ~dir =
  get_python_libraries_dir ~dir // "blockchain.py"

(* Csharp file names and directories *)

let get_csharp_interface_dir ?(relative = false) ~dir () =
  let tail = "src" // "csharp_sdk" in
  if relative then
    tail
  else
    dir // tail

let get_csharp_interface_basename ~contract_name =
  show_sanitized_name contract_name ^ "_csharp_interface"

let get_csharp_code_basename ~contract_name =
  show_sanitized_name contract_name ^ "_code"

let get_csharp_interface_filename ~contract_name =
  get_csharp_interface_basename ~contract_name ^ ".cs"

let get_csharp_code_filename ~contract_name =
  get_csharp_code_basename ~contract_name ^ ".json"

let get_csharp_interface_path ?(relative = false) ~dir ~contract_name () =
  get_csharp_interface_dir ~relative ~dir ()
  // get_csharp_interface_filename ~contract_name

let get_csharp_code_path ?(relative = false) ~dir ~contract_name () =
  get_csharp_interface_dir ~relative ~dir ()
  // get_csharp_code_filename ~contract_name

let get_csharp_libraries_dir ~dir = get_csharp_interface_dir ~dir ()

let get_csharp_factori_types_path ~dir =
  get_csharp_libraries_dir ~dir // "factori_types.cs"

let get_csharp_project_file_path ~dir =
  get_csharp_libraries_dir ~dir // "project_file.csproj"

let get_csharp_blockchain_path ~dir =
  get_csharp_libraries_dir ~dir // "blockchain.cs"

(* Typescript file names and directories *)

let get_typescript_interface_dir ?(relative = false) ~dir () =
  let tail = "src" // "typescript_sdk" in
  if relative then
    tail
  else
    dir // tail

let get_typescript_interface_dir_src ?(relative = false) ~dir () =
  get_typescript_interface_dir ~relative ~dir () // "src"

let get_typescript_public_dir ?(relative = false) ~dir () =
  get_typescript_interface_dir ~relative ~dir () // "public"

let get_vue_components_dir ?(relative = false) ~dir () =
  get_typescript_interface_dir_src ~relative ~dir () // "components"

let get_typescript_interface_filename ~contract_name =
  show_sanitized_name contract_name ^ "_interface.ts"

let get_dipdup_dir ~dir = dir // "src" // "dipdup"

let get_dipdup_handlers_dir ~dir = get_dipdup_dir ~dir // "handlers"

(* OCaml dirs and names for factori deploy *)

let ocaml_deploy_dir_name = "ocaml_deploy"

let get_ocaml_deploy_dir ~dir = dir // "src" // ocaml_deploy_dir_name

(* OCaml dirs and names for scenarios *)
let get_ocaml_scenarios_filename = "scenario.ml"

let get_ocaml_scenarios_example_filename = "scenario.ml.example"

let get_ocaml_scenarios_dir ?(relative = false) ~dir () =
  let tail = "src" // "ocaml_scenarios" in
  if relative then
    tail
  else
    dir // tail

let get_ocaml_scenarios_path ?(relative = false) ~dir () =
  get_ocaml_scenarios_dir ~relative ~dir () // get_ocaml_scenarios_filename

let get_ocaml_scenarios_example_path ?(relative = false) ~dir () =
  get_ocaml_scenarios_dir ~relative ~dir ()
  // get_ocaml_scenarios_example_filename

let get_ocaml_abstract_interface_path ?(relative = false) ~dir ~contract_name ()
    =
  get_ocaml_interface_dir ~relative ~dir ()
  // get_ocaml_abstract_interface_filename ~contract_name

let get_ocaml_abstract_interface_mli_path ?(relative = false) ~dir
    ~contract_name () =
  get_ocaml_interface_dir ~relative ~dir ()
  // get_ocaml_abstract_interface_mli_filename ~contract_name

let get_ocaml_interface_path ?(relative = false) ~dir ~contract_name () =
  get_ocaml_interface_dir ~relative ~dir ()
  // get_ocaml_interface_filename ~contract_name

let get_ocaml_interface_mli_path ?(relative = false) ~dir ~contract_name () =
  get_ocaml_interface_dir ~relative ~dir ()
  // get_ocaml_interface_mli_filename ~contract_name

let get_ocaml_code_basename ~contract_name () =
  show_sanitized_name contract_name ^ "_code"

let get_ocaml_crawlori_tables_path ?(relative = false) ~dir ~contract_name () =
  get_ocaml_crawlori_dir ~relative ~dir ()
  // get_ocaml_crawlori_tables_filename ~contract_name

let get_ocaml_crawlori_plugin_path ?(relative = false) ~dir ~contract_name () =
  get_ocaml_crawlori_dir ~relative ~dir ()
  // get_ocaml_crawlori_plugin_filename ~contract_name

let get_ocaml_crawlori_bm_utils_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "bm_utils.ml"

let get_ocaml_crawlori_info_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "info.ml"

let get_ocaml_crawlori_register_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "register.ml"

let get_ocaml_crawlori_common_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "common.ml"

let get_ocaml_crawlori_converters_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "converters.sexp"

let get_ocaml_crawlori_crawler_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "crawler.ml"

let get_ocaml_crawlori_static_tables_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "static_tables.ml"

let get_ocaml_crawlori_contracts_table_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "contracts_table.ml"

let get_ocaml_crawlori_contracts_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "contracts.ml"

let get_ocaml_crawlori_update_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "update.ml"

let get_ocaml_crawlori_config_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "config.json"

let get_ocaml_crawlori_readme_path ?(relative = false) ~dir () =
  get_ocaml_crawlori_dir ~relative ~dir () // "README"

let get_dipdup_models_path ~dir () = get_dipdup_dir ~dir // "models.py"

let get_dipdup_configuration_path ~dir () = get_dipdup_dir ~dir // "dipdup.yml"

let get_dipdup_storage_handler_path ~dir ~contract_name =
  let filename = Format.sprintf "on_%s_new_storage.py" contract_name in
  get_dipdup_handlers_dir ~dir // filename

let get_dipdup_ep_handler_path ~dir ~table_name =
  let filename = Format.sprintf "on_%s.py" table_name in
  get_dipdup_handlers_dir ~dir // filename

let get_dipdup_bm_handler_path ~dir ~contract_name ~bm_name =
  let filename = Format.sprintf "on_%s_%s_updates.py" contract_name bm_name in
  get_dipdup_handlers_dir ~dir // filename

let get_ocaml_code_filename_mli ~contract_name () =
  get_ocaml_code_basename ~contract_name () ^ ".mli"

let get_ocaml_code_filename ~contract_name =
  get_ocaml_code_basename ~contract_name () ^ ".ml"

let get_ocaml_code_path ?(relative = false) ~dir ~contract_name () =
  get_ocaml_interface_dir ~relative ~dir ()
  // get_ocaml_code_filename ~contract_name

let get_ocaml_code_mli_path ?(relative = false) ~dir ~contract_name () =
  get_ocaml_interface_dir ~relative ~dir ()
  // get_ocaml_code_filename_mli ~contract_name ()

let get_typescript_code_basename ~contract_name =
  (* let contract_name = sanitize_basename contract_name in *)
  Format.sprintf "%s_code" (show_sanitized_name contract_name)

let get_typescript_code_filename ~contract_name =
  Format.sprintf "%s.json" (get_typescript_code_basename ~contract_name)

let get_typescript_code_path ?(relative = false) ~dir ~contract_name () =
  get_typescript_interface_dir_src ~relative ~dir ()
  // get_typescript_code_filename ~contract_name

let get_typescript_interface_path ?(relative = false) ~dir ~contract_name () =
  get_typescript_interface_dir_src ~relative ~dir ()
  // get_typescript_interface_filename ~contract_name

let get_typescript_scenario_dir ?(relative = false) ~dir () =
  get_typescript_interface_dir_src ~relative ~dir ()

let get_typescript_scenario_basename ~contract_name =
  let contract_name = sanitize_basename contract_name in
  Format.sprintf "scenario_%s" (show_sanitized_name contract_name)

let get_typescript_scenario_example_filename ~contract_name =
  get_typescript_scenario_basename ~contract_name ^ ".ts.example"

let get_typescript_scenario_example_path ?(relative = false) ~dir ~contract_name
    () =
  get_typescript_scenario_dir ~relative ~dir ()
  // get_typescript_scenario_example_filename ~contract_name

let get_typescript_functolib_path ?(relative = false) ~dir () =
  get_typescript_interface_dir_src ~relative ~dir () // "functolib.ts"

let get_typescript_tsconfig_path ?(relative = false) ~dir () =
  get_typescript_interface_dir ~relative ~dir () // "tsconfig.json"

let get_typescript_package_dot_json_path ?(relative = false) ~dir () =
  get_typescript_interface_dir ~relative ~dir () // "package.json"

let get_typescript_webpack_config_js_path ?(relative = false) ~dir () =
  get_typescript_interface_dir ~relative ~dir () // "webpack.config.js"

let get_typescript_index_html_path ?(relative = false) ~dir () =
  get_typescript_public_dir ~relative ~dir () // "index.html"

let get_typescript_index_ts_path ?(relative = false) ~dir () =
  get_typescript_interface_dir_src ~relative ~dir () // "index.ts"

let get_typescript_app_vue_path ?(relative = false) ~dir () =
  get_typescript_interface_dir_src ~relative ~dir () // "App.vue"

let get_typescript_storage_vue_path ?(relative = false) ~dir () =
  get_vue_components_dir ~relative ~dir () // "Storage.vue"

let get_typescript_vue_shim_ts_path ?(relative = false) ~dir () =
  get_typescript_interface_dir_src ~relative ~dir () // "vue-shim.d.ts"

let get_contracts ~dir =
  let local_config_file = get_local_config_name ~dir in
  let lc =
    Factori_utils.deserialize_file ~filename:local_config_file
      ~enc:local_config_enc in
  lc.contracts

let get_contract_signature ~dir ~contract_name =
  let filename =
    get_contract_signature_filename
      ~contract_name:(sanitized_of_str contract_name)
      ~dir in
  let signature = deserialize_file ~filename ~enc:contract_signature_enc in
  signature

let get_all_contract_signatures ~dir =
  let contracts = get_contracts ~dir in
  let contract_names = List.map (fun c -> c.contract_name) contracts in
  let signatures : (contract_signature * string) list =
    List.map
      (fun n -> (get_contract_signature ~dir ~contract_name:n, n))
      contract_names in
  signatures

let find_same_signature_opt ~contract_name ~dir (signature : contract_signature)
    =
  let signatures =
    List.filter (fun (_, name) -> name <> contract_name.original)
    @@ get_all_contract_signatures ~dir in
  List.assoc_opt signature signatures

let get_contract_opt name =
  let cl = get_contracts ~dir:(get_dir ()) in
  List.find_opt (fun x -> x.contract_name = show_sanitized_name name) cl

let get_contract name =
  let cl = get_contracts ~dir:(get_dir ()) in
  List.find (fun x -> x.contract_name = show_sanitized_name name) cl

let get_contract_original_kt1 name =
  match get_contract_opt name with
  | None -> None
  | Some c -> c.original_kt1

let contract_exists name =
  match get_contract_opt name with
  | None -> false
  | Some _c -> true

let get_format_of_contract name =
  match get_contract_opt name with
  | None -> None
  | Some c -> Some c.original_format

let options () =
  let options =
    [
      (!Factori_options.crawlori, "crawlori");
      (!Factori_options.dipdup, "dipdup");
      (!Factori_options.ocaml, "ocaml");
      (!Factori_options.typescript, "typescript");
      (!Factori_options.web_mode, "web");
    ] in
  let enabled = List.filter (fun (o, _) -> o) options in
  snd @@ List.split enabled

let current_time () =
  let current_time = Unix.localtime @@ Unix.time () in
  Format.asprintf "%d/%d/%d %d:%d:%d" current_time.tm_mday
    (current_time.tm_mon + 1)
    (1900 + current_time.tm_year)
    current_time.tm_hour current_time.tm_min current_time.tm_sec

let remove_contract_from_config_file ~dir ~name =
  if not @@ contract_exists name then
    failwith "No such contract in config file%!\n"
  else
    let local_config_file = get_local_config_name ~dir in
    let lc =
      deserialize_file ~filename:local_config_file ~enc:local_config_enc in
    let new_lc =
      {
        contracts =
          List.filter
            (fun c -> c.contract_name <> show_sanitized_name name)
            lc.contracts;
      } in
    serialize_file ~filename:local_config_file ~enc:local_config_enc new_lc

let add_contract ?(original_kt1 = None) ~dir ~format ~name () =
  let local_config_file = get_local_config_name ~dir in
  let lc = deserialize_file ~filename:local_config_file ~enc:local_config_enc in
  if contract_exists name then begin
    Console.info "Contract already exist@." ;
    let c = get_contract name in
    if c.original_format = format then begin
      Console.info "Contract has the same format@." ;
      if !Factori_options.overwrite then begin
        Console.info "[--force is enabled] Overwriting the contract.@." ;
        let merged_options =
          List.sort_uniq String.compare (c.options @ options ()) in
        let new_contract = { c with options = merged_options } in
        remove_contract_from_config_file ~dir ~name ;
        let lc =
          deserialize_file ~filename:local_config_file ~enc:local_config_enc
        in
        let new_lc = { contracts = new_contract :: lc.contracts } in
        serialize_file ~filename:local_config_file ~enc:local_config_enc new_lc
      end else begin
        Console.error
          "Enable the option --force if you want to force the import of the \
           same contract@." ;
        exit 1
      end
    end else begin
      Console.error
        "A contract with the same name (%s) but a different original format \
         (%s <> %s) already exists in your project@."
        (show_sanitized_name name)
        (show_contract_format c.original_format)
        (show_contract_format format) ;
      exit 1
    end
  end else
    let contract =
      {
        contract_name = show_sanitized_name name;
        original_format = format;
        original_kt1;
        import_date = current_time ();
        options = options ();
      } in
    let lc = { contracts = contract :: lc.contracts } in
    serialize_file ~filename:local_config_file ~enc:local_config_enc lc

(* Borrowed from blockchain_ml.ml; should probably be moved to its own file TODO  *)

let get_storage ?(network = "mainnet") ?(debug = false) kt1 =
  let open Tzfunc.Rp in
  let>? storage =
    Tzfunc.Node.get_storage ~base:(get_base_of_network network) kt1 in
  if debug then
    Factori_utils.output_verbose ~level:2
    @@ Format.asprintf "Storage: %s\n                    %!"
         (EzEncoding.construct Tzfunc.Proto.script_expr_enc.json
            (Micheline storage)) ;
  Lwt.return_ok @@ storage
