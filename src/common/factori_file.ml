module File = struct
  let exists = Sys.file_exists

  let create_file filename =
    if exists filename then
      Format.printf "Replacing [%s]@." filename
    else
      Format.printf "Generating [%s]@." filename ;
    Format.formatter_of_out_channel @@ open_out filename

  let write ?(sp = 0) oc fmt =
    let spaces = String.make (2 * sp) ' ' in
    Format.fprintf oc "%s" spaces ;
    Format.fprintf oc fmt
end

module Console = struct
  let log fmt = Format.printf fmt

  let red fmt = "\027[31m" ^^ fmt ^^ "\027[0m%!"

  let green fmt = "\027[32m" ^^ fmt ^^ "\027[0m%!"

  let yellow fmt = "\027[33m" ^^ fmt ^^ "\027[0m%!"

  let blue fmt = "\027[34m" ^^ fmt ^^ "\027[0m%!"

  let magenta fmt = "\027[35m" ^^ fmt ^^ "\027[0m%!"

  let error fmt = log (red ("[Error] " ^^ fmt))

  let success fmt = log (green fmt)

  let warn fmt = log (yellow ("[Warning] " ^^ fmt))

  let info fmt = log (blue ("[Information] " ^^ fmt))

  let debug fmt = log (magenta ("[Debug] " ^^ fmt))
end
