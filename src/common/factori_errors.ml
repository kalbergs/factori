open Tzfunc.Proto

exception NoDirectoryProvided

exception NotADirectory of string * string

exception NotAFile of string * string

exception DirectoryNotEmpty of string

exception NotOverwritingFile of string

exception NoKT1Provided

exception TzfuncError of Tzfunc.Rp.error

exception NoMichelsonFileProvided

exception NoScript of string

exception GenericError of string * string

exception UnexpectedMichelson of string

exception ParsingError of string

exception CouldNotDestruct of string

exception CouldNotReadAsJson of string

(* Used in Infer_entrypoints *)
exception Nothandled of string

exception NotAType of Tzfunc.Proto.micheline

exception ElementShouldHaveName of string

exception NoContractNameProvided

exception TooManyLanguages

exception NoLanguageProvided

exception UnknownStorageType of string

exception UnstackMicheline of string

exception WrongNumberOfArg of primitive_or_macro

(* (\* This exception is used as a helper to add exceptions *\)
 * exception Don'tKnowWhatToDoWith of exn *)

let hookable_handle_exception =
  let open Format in
  ref @@ function
  | NoDirectoryProvided -> eprintf "No directory provided.\n%!"
  | NotADirectory (d, msg) -> eprintf "[%s] Not a directory: %s.\n%!" msg d
  | NotAFile (f, msg) -> eprintf "[%s] Not a file: %s.\n%!" msg f
  | DirectoryNotEmpty d ->
    eprintf
      "Directory %s not empty. If you still wish to overwrite its contents, \
       please use option --force.\n\
       %!"
      d
  | NotOverwritingFile file -> eprintf "Not overwriting file %s.\n%!" file
  | NoKT1Provided -> eprintf "No KT1 was provided.\n%!"
  | NoMichelsonFileProvided -> eprintf "No Michelson file was provided.\n%!"
  | NoScript s -> eprintf "No script for %s" s
  | GenericError (prg, err) -> eprintf "[%s] %s\n%!" prg err
  | UnexpectedMichelson mic -> eprintf "Unexpected Michelson: %s" mic
  | TzfuncError err ->
    eprintf "[Tzfunc error]: %s\n" (Tzfunc.Rp.string_of_error err)
  | ParsingError s -> eprintf "Parsing Error: %s" s
  | Nothandled s -> eprintf "Not handled: %s" s
  | NotAType m ->
    eprintf "%s is not a type."
      (EzEncoding.construct Tzfunc.Proto.micheline_enc.json m)
  | ElementShouldHaveName s -> eprintf "%s should have a name." s
  | CouldNotDestruct s -> eprintf "Could not destruct %s." s
  | CouldNotReadAsJson s -> eprintf "Could not read %s as a JSON value." s
  | NoContractNameProvided ->
    eprintf "No contract name was provided, please provide one."
  | TooManyLanguages ->
    eprintf
      "Please select exactly one programming language for this operation.\n%!"
  | NoLanguageProvided ->
    eprintf
      "No programming language was provided. Please provide at least one \
       programming language (e.g. --ocaml, --typescript).\n\
       %!"
  | UnknownStorageType s ->
    eprintf
      "Unknown storage type \"%s\". Please choose either \"random\" or \
       \"blockchain\"\n\
       %!"
      s
  | UnstackMicheline s -> eprintf "Error when unstacking Michelson : %s" s
  | Sys_error s -> eprintf "System error: %s" s
  | e -> raise e

let handle_exception e = !hookable_handle_exception e

let add_exception_handler (f : exn -> unit) =
  let previous_f = !hookable_handle_exception in
  hookable_handle_exception := fun e -> try previous_f e with e -> f e
