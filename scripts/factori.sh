#!/bin/sh

USEROPT=$(id -u):$(id -g)
DOCKER=docker
IMAGE=registry.gitlab.com/functori/dev/factori:latest

realpath_custom() {
  OURPWD=$PWD
  cd "$(dirname "$1")"
  LINK=$(readlink "$(basename "$1")")
  while [ "$LINK" ]; do
    cd "$(dirname "$LINK")"
    LINK=$(readlink "$(basename "$1")")
  done
  REALPATH="$PWD/$(basename "$1")"
  cd "$OURPWD"
  echo "$REALPATH"
}

run_image_without_dir(){
    $DOCKER run --user $USEROPT $IMAGE "$@"
}


run_image_with_dir(){
    dir=$3
    echo "dir:$dir"
    fullpath=$(realpath_custom $dir)
    upper_dir=$(dirname $fullpath)
    subdir=$(basename $dir)
    shift
    shift
    shift
    # echo "subdir:$subdir"
    # echo "fullpath:$fullpath"
    # echo mkdir -p $fullpath
    mkdir -p $fullpath
    if [ $# -eq 0 ]; then
	echo $DOCKER run --user $USEROPT -v $fullpath":/home/functori/tmp" $IMAGE import kt1 tmp
	$DOCKER run --user $USEROPT -v $fullpath":/home/functori/tmp" $IMAGE import kt1 tmp
    exit 1
    fi
    echo $DOCKER run --user $USEROPT -v $fullpath":/home/functori/tmp" $IMAGE import kt1 tmp "$@"
    $DOCKER run --user $USEROPT -v $fullpath":/home/functori/tmp" $IMAGE import kt1 tmp "$@"
}

import_kt1(){
    dir="$3"
    args="$@"
    run_image_with_dir import kt1 $args
}

import_michelson(){
    dir="$1"
    shift
    contract_path="$(dirname $1)"
    # echo "contract path: $contract_path"
    shift
    contract_basename="contracts/$1"
    # echo "contract_basename: $contract_basename"
    shift
    contract_dirname=$(dirname $contract_path)
    # echo "contract_dirname: $contract_dirname"
    if [ $# -eq 0 ]; then
	echo $DOCKER run --user $USEROPT -v $PWD/$relpath":/home/functori/$dir" -v $PWD/$contract_path:"/home/functori/contracts" $IMAGE import michelson $dir $contract_basename
	$DOCKER run --user $USEROPT -v $PWD/$relpath":/home/functori/$dir" -v $PWD/$contract_path:"/home/functori/contracts" $IMAGE import michelson $dir $contract_basename
	exit 1
    fi

    echo $DOCKER run --user $USEROPT -v $PWD/$relpath":/home/functori/$dir" -v $PWD/$contract_path:"/home/functori/contracts" $IMAGE import michelson $dir $contract_basename "$@"
    $DOCKER run --user $USEROPT -v $PWD/$relpath":/home/functori/$dir" -v $PWD/$contract_path:"/home/functori/contracts" $IMAGE import michelson $dir $contract_basename "$@"

}

empty_project(){
    dir="$3"
    args="$@"
    run_image_with_dir empty project $args
}

rename_variables(){
    run_image_without_dir rename variables "$@"
}

remove_contract(){
    run_image_without_dir remove contract "$@"
}

# check if there are any arguments at all

usage(){
    echo "usage"
}

process(){
    case "$1" in
	import)
	    shift
	    case "$1" in
		kt1)
		    shift
		    dir="$1"
		    # shift
		    # relpath=$(realpath_custom --relative-to=${PWD} $dir)
		    args="${@}"
		    import_kt1 "$args"
		    ;;
		michelson)
		    shift
		    dir="$1"
		    shift
		    mich_contract="$1"
		    shift
		    relpath_mich=$(realpath_custom --relative-to=${PWD} $mich_contract)
		    basename_mich=$(basename $relpath_mich)
		    args="${@}"
		    if [ $# -eq 0 ]; then
			echo "no extra arguments"
			import_michelson tmp $relpath_mich $basename_mich
			exit1;
		    fi
		    import_michelson tmp $relpath_mich $basename_mich "$args"
		    ;;
	    esac
	    ;;
	empty)
	    shift
	    case "$1" in
		project)
		    shift
		    dir="$1"
		    shift
		    relpath=$(realpath_custom --relative-to=${PWD} $dir)
		    args="${@}"
		    empty_project $relpath "$args"
		    ;;
		*)
		    echo "The command you are looking for is `factori empty project`"
	    esac
	    ;;
	rename)
	    shift
	    case "$1" in
		variables)
		    shift
		    rename_variables "${@}"
		    ;;
	    esac
	    ;;
	build)
	    shift
	    case $1 in
		interface)
		    shift
		    run_image_without_dir build interface "${@}"
		    ;;
	    esac
	    ;;
	remove)
	    shift
	    case "$1" in
		contract)
		    shift
		    remove_contract "${@}"
		    ;;
	    esac
	    ;;
	help)
	    run_image_without_dir help
	    ;;
	*)
            run_image_without_dir "${@}"
            exit 1
    esac
}

process "$@"
