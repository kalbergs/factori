open Format

let ( // ) = Factori_options.concat

let pwd = Sys.getcwd ()

let bin = pwd // "_bin"

let factori = bin // "factori.asm"

let _ =
  Sys.command
  @@ sprintf
       "%s empty project test --force --crawlori --ocaml --typescript --web \
        --python --csharp"
       factori

(* create a special testing directory inside <pwd>/test *)
let () =
  let tests_path = pwd // "test" // "src" // "tests" in
  let ts_tests_path = pwd // "test" // "src" // "ts" in
  Factori_utils.make_dir tests_path ;
  Factori_utils.make_dir ts_tests_path ;
  Ocaml_dune.add_to_dune_file ~file_must_exist:false ~stanza_must_not_exist:true
    ~path:(tests_path // "dune")
    [
      Executable
        {
          name = "tests";
          modules = ["tests"];
          libraries = ["blockchain"; "utils"; "ez_api.icurl_lwt"];
        };
    ] ;
  Factori_utils.write_file (tests_path // "tests.ml") ""
