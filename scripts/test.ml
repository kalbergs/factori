open Format
open Typescript
open Factori_config
open Factori_utils

let ( // ) = Factori_options.concat

let pwd = Sys.getcwd ()

let bin = pwd // "_bin"

let factori = bin // "factori.asm"

let test_contracts = pwd // "test_contracts"

let ts_tests_path = get_typescript_interface_dir ~dir:(pwd // "test") ()

let () = eprintf "pwd: %s\n" pwd

let michelson_contracts =
  List.filter (fun x -> List.mem (Filename.extension x) [".tz"; ".json"])
  @@ Array.to_list @@ Sys.readdir test_contracts

let typescript_imports =
  [
    Import_value
      {
        imported =
          [
            "TezosToolkit";
            "BigMapAbstraction";
            "MichelsonMap";
            "OriginationOperation";
            "OpKind";
            "createTransferOperation";
            "TransferParams";
            "RPCOperation";
            "createRevealOperation";
          ];
        source = "\"@taquito/taquito\"";
      };
    Import_value
      { imported = ["MichelsonV1Expression"]; source = "\"@taquito/rpc\"" };
    Import_value { imported = ["encodeOpHash"]; source = "\"@taquito/utils\"" };
  ]

let ts_preamble =
  {|

const tezosKit_mainnet = new TezosToolkit('https://tz.functori.com/');

|}

let ts_main_function =
  {
    name = "main";
    async = true;
    export = true;
    body = "";
    (* will be filled out progressively *)
    args = [];
  }

let typescript_test_file =
  {
    imports = typescript_imports;
    preamble = ts_preamble;
    functions = [ts_main_function];
    body =
      "let res = main ()\n\
       console.log(\"Battery of Typescript tests successful.\")\n";
  }

(* import { storage_encode } from "./toto0_interface"; *)

(* let () = List.iter (fun x -> eprintf "%s\n" x) michelson_contracts *)

let kt1_contracts =
  [
    (("KT1CpMKxKFoHvS7zxSd2M6SHU8BD4gBmtw2N", "dogami_NFT_sale"), "mainnet");
    (* First contract is
       doubled on purpose to
       test removal *)
    (("KT1CpMKxKFoHvS7zxSd2M6SHU8BD4gBmtw2N", "dogami_NFT_sale"), "mainnet");
    (("KT1NVvPsNDChrLRH5K2cy6Sc9r1uuUwdiZQd", "dogami_NFT"), "mainnet");
    (("KT1NrAnSjrjp3Bycaw8YKPrDq2aXUTHhq8MF", "hic_et_nunc_DAO"), "mainnet");
    (("KT1KVfQzyjxoKx2WjcVAatknW3YsR37cCA6s", "arago"), "mainnet");
    (("KT1TwzD6zV3WeJ39ukuqxcfK2fJCnhvrdN1X", "smlk"), "mainnet");
    (("KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b", "plenty"), "mainnet");
    (("KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb", "quipuswap"), "mainnet");
    (("KT1Su3fNrnABFSYMtWVDZnMbH3DzJeysAd6t", "fILMCrew_NFT"), "mainnet");
    (("KT1ERjU9W6jzZkq8v6caxu65pQkamv1ptn35", "kT1ERjUtn35"), "mainnet");
    (("KT1Ha4yFVeyzw6KRAdkzq6TxDHB97KG4pZe8", "some_fa12"), "mainnet");
    (("KT1D6XTy8oAHkUWdzuQrzySECCDMnANEchQq", "juster" (* Juster *)), "mainnet");
    (* TODO: fix tuple_decode *)
    (("KT1Lz5S39TMHEA7izhQn8Z1mQoddm6v1jTwH", "youves" (* Youves *)), "mainnet");
    (("KT1Xobej4mc6XgEjDoJoHtTKgbD1ELMvcQuL", "youves_bis"), "mainnet");
    (* Youves too *)
    (("KT1VG2WtYdSWz5E7chTeAdDPZNy2MpP8pTfL", "atomex" (* Atomex *)), "mainnet");
    ( ("KT1Ap287P1NzsnToSJdA4aqSNjPomRaHBZSr", "atomex_tzBTC" (* Atomex tzBTC *)),
      "mainnet" );
    ( ("KT1EpQVwqLGSH7vMCWKJnq6Uxi851sEDbhWL", "atomex_kUSD" (* Atomex kUSD *)),
      "mainnet" );
    (* (("KT1ATZMPus96CFMg2s7mHp33bVkHwwpRDS3R", "some_other_fa2"), "ithacanet"); *)
    (("KT18pVpRXKPY2c4U2yFEGSH3ZnhB2kL8kwXS", "rarible"), "mainnet");
    (("KT1HSYQ9NLTQufuvNUwMhLY7B9TX8LDUfgsr", "plenty-farm-std"), "mainnet");
    (("KT1CSZ4ywhbUZwuXKMw3udoYSbisQv44sxsB", "chain_id_example"), "mainnet");
    (* (("KT1UmxfNXrKJvBDarGMiVA2Rnuaxn2Jb6sZQ", "sapling_example"),"mainnet"); *)
    (("KT19h3TpAWUKtrbdSw2MhQXYj95nnxwwLsWG", "ticket_example"), "mainnet");
    (("KT1HXXYrbhyY76afTGJ75UKts8C6j7P6tqXi", "byte_example"), "mainnet");
    (("KT19SUzs8XMJnVx6XrMrYiLRvFnSspgRf1Vt", "decode_error_example"), "mainnet");
    ( ("KT1EQLxRRhgwPd9GRisfE3rrXD24P12WBLNb", "unbound_storage_error"),
      "mainnet" );
    (("KT1JMi5Jg3GX237rgpcsvCNftHtMJpwxuASD", "escape_bytes_example"), "mainnet");
    ( ("KT1Grni14amJ2K6FkmwGznSXqfjckgUBSJ4A", "underscore_in_constructor_names"),
      "mainnet" );
    (("KT1KD9oNXFJHC8auPit74XwYEY89GxzBcJgj", "set_error"), "mainnet");
    ( ("KT1VdCrmZsQfuYgbQsezAHT1pXvs6zKF8xHB", "endlesswaysfa2_sumtype"),
      "mainnet" );
    ( ("KT1CMuXyLx7MjkYPjue5LRZdfxD5qqsBgnQu", "withdraw_type_confusion"),
      "mainnet" )
    (* (( "KT1UN35RSeoddzxHd6W5Y15qerSLVJDdbBQw",
     *     "dummy_example_with_sumtype_storage"),
     *   "ithacanet" ); *);
  ]

(* #KT1NrAnSjrjp3Bycaw8YKPrDq2aXUTHhq8MF is Hic et Nunc DAO
 * #KT1KVfQzyjxoKx2WjcVAatknW3YsR37cCA6s is Arago
 * #KT1NVvPsNDChrLRH5K2cy6Sc9r1uuUwdiZQd is Dogami NFT
 * #KT1CpMKxKFoHvS7zxSd2M6SHU8BD4gBmtw2N is Dogami NFT sale
 * #KT1TwzD6zV3WeJ39ukuqxcfK2fJCnhvrdN1X is Smartlink
 * #KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b is Plenty
 * #KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb is Quipuswap
 * #KT1ERjU9W6jzZkq8v6caxu65pQkamv1ptn35 is an FA2
 * #KT1Ha4yFVeyzw6KRAdkzq6TxDHB97KG4pZe8 is an FA1.2 *)

let import_kt1s () =
  let tests_path = pwd // "test" // "src" // "tests" in
  Factori_utils.append_to_file ~check_already_present:false
    ~filepath:(tests_path // "tests.ml")
    ~content:
      "let _ = Factori_types.set_debug 0\n\
       let _ = Tzfunc.Node.set_silent true\n\n"
    () ;
  let rec aux ~networks ts_file index = function
    | [] -> add_to_body ~fun_name:"main" "return;" ts_file
    | ((kt1, name), network) :: xs ->
      let dipdup_arg =
        if name = "atomex_tzBTC" || name = "atomex_kUSD" then
          ""
        else
          "--dipdup" in
      let cmd =
        sprintf
          "%s import kt1 test %s --network %s --name %s_%d --crawlori %s --web \
           --ocaml --typescript --python --csharp"
          factori kt1 network name index dipdup_arg in
      let _res1 = Sys.command @@ sprintf "echo %s" cmd in
      let _res2 = Sys.command @@ sprintf "%s" cmd in
      if index = 0 then (
        (* test removal of contract toto0 *)
        let cmd =
          sprintf "%s remove contract test --name dogami_NFT_sale_0" factori
        in
        ignore (Sys.command @@ sprintf "echo %s" cmd) ;
        ignore (Sys.command cmd)
      ) else
        () ;
      (* OCaml *)
      if index > 0 then
        Ocaml_dune.add_library_to_stanza_in_dune_file ~file_must_exist:true
          ~stanza_must_exist:true ~path:(tests_path // "dune")
          ~stanza_name:"tests"
          (sprintf "%s_%d_ocaml_interface"
             (show_sanitized_name (sanitize_basename name))
             index) ;
      let name = sanitize_basename name in
      let storage_download_chunk_ml =
        sprintf
          "let _ = Format.eprintf \"Testing %s@@.\"\n\
           let _ = %s_%d_ocaml_interface.test_storage_download ~kt1:\"%s\" \
           ~base:(EzAPI.BASE (\"%s\")) ()\n\
           %!"
          (show_sanitized_name name)
          (String.capitalize_ascii (show_sanitized_name name))
          index kt1
          (Factori_utils.get_raw_network network) in
      if index > 0 then
        Factori_utils.append_to_file ~check_already_present:true
          ~filepath:(tests_path // "tests.ml")
          ~content:storage_download_chunk_ml () ;
      (* Typescript *)
      let network_url = Factori_utils.get_raw_network network in
      let new_tzkit =
        if not (List.mem network networks) then
          sprintf "const tezosKit_%s = new TezosToolkit('%s');" network
            network_url
        else
          "" in
      (* let name = Factori_config.sanitize_basename name in *)
      let sanitized_name = show_sanitized_name name in
      let storage_download_chunk_ts =
        sprintf
          {|
  %s
  try{const contract_%d = await tezosKit_%s.contract.at("%s")
  console.log("Testing %s")
  let pre_storage_%d : MichelsonV1Expression = await contract_%d.script.storage
  let storage_%d = %s_%d_interface.Storage_type_decode(pre_storage_%d)
  //console.log(storage_%d)
  let storage%d_reencoded = %s_%d_interface.Storage_type_encode(storage_%d)
  //console.log(storage%d_reencoded)
  let storage%d_redecoded = %s_%d_interface.Storage_type_decode(storage%d_reencoded)
  let initial_storage%d_encoded = %s_%d_interface.Storage_type_encode(%s_%d_interface.initial_blockchain_storage)}
  catch (error) {
    console.log("Error while processing %s_%d: "+ error)
    throw "Error in test %s"
  }
 |}
          new_tzkit index network kt1 sanitized_name index index index
          sanitized_name index index index index sanitized_name index index
          index index sanitized_name index index index sanitized_name index
          sanitized_name index sanitized_name index sanitized_name in
      let ts_file =
        add_to_body ~fun_name:"main"
          (sprintf ";\n%s" storage_download_chunk_ts)
          ts_file in
      let ts_file =
        add_import
          (Import_as
             {
               alias =
                 sprintf "%s_%d_interface" (show_sanitized_name name) index;
               source =
                 sprintf "\"./src/%s_%d_interface\"" (show_sanitized_name name)
                   index;
             })
          ts_file in
      (* TODO: use config instead of ad-hoc name *)
      aux ~networks:["mainnet"] ts_file (index + 1) xs in
  let res = aux ~networks:["mainnet"] typescript_test_file 0 kt1_contracts in
  Factori_utils.append_to_file ~check_already_present:false
    ~filepath:(tests_path // "tests.ml")
    ~content:
      "let _ = Factori_types.output_debug ~level:0 \"Battery of OCaml tests \
       successful.\n\
       \"\n\n"
    () ;
  res

let import_michelson_contracts () =
  let tests_path = pwd // "test" // "src" // "tests" in
  let rec aux = function
    | [] -> ()
    | filename :: xs ->
      let basename = Filename.remove_extension filename in
      let cmd =
        sprintf
          "%s import michelson test %s --name contract_%s --ocaml --crawlori \
           --typescript --python --csharp "
          factori
          ("test_contracts" // filename)
          basename in
      let _res1 = Sys.command @@ sprintf "echo %s" cmd in
      let _res2 = Sys.command @@ sprintf "%s" cmd in
      Ocaml_dune.add_library_to_stanza_in_dune_file ~file_must_exist:true
        ~stanza_must_exist:true ~path:(tests_path // "dune")
        ~stanza_name:"tests"
        (sprintf "contract_%s_ocaml_interface" basename) ;
      (* TODO: use config instead of ad-hoc name *)
      aux xs in
  aux michelson_contracts

let typescript_test_file = import_kt1s ()

let () = import_michelson_contracts ()

(* let ts_main_function = {
 *           name = "main";
 *           async = true;
 *           export = true;
 *           body = ts_main_body;
 *           args = ["tk","TezosToolkit"]
 *       } *)

(* let typescript_test_file =
 *   {
 *     typescript_test_file with
 *     functions =
 *       [ts_main_function];
 *   } *)

let typescript_test_file_content =
  asprintf "%a" (print_ts_file ~ts_file:typescript_test_file) ()

let () = write_file (ts_tests_path // "test.ts") typescript_test_file_content
