#!/bin/sh
BIN=_bin
FACTORI=$BIN/factori.asm
#KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC TODO: FIX Lambda
# KT1NVvPsNDChrLRH5K2cy6Sc9r1uuUwdiZQd TODO: FIX set
KT1_CONTRACTS="KT1CpMKxKFoHvS7zxSd2M6SHU8BD4gBmtw2N
	KT1NVvPsNDChrLRH5K2cy6Sc9r1uuUwdiZQd
	KT1NrAnSjrjp3Bycaw8YKPrDq2aXUTHhq8MF
	KT1KVfQzyjxoKx2WjcVAatknW3YsR37cCA6s
	KT1TwzD6zV3WeJ39ukuqxcfK2fJCnhvrdN1X
	KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b
	KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb
	KT1Su3fNrnABFSYMtWVDZnMbH3DzJeysAd6t
	KT1ERjU9W6jzZkq8v6caxu65pQkamv1ptn35
	KT1Ha4yFVeyzw6KRAdkzq6TxDHB97KG4pZe8"

#KT1NrAnSjrjp3Bycaw8YKPrDq2aXUTHhq8MF is Hic et Nunc
#KT1KVfQzyjxoKx2WjcVAatknW3YsR37cCA6s is Arago
#KT1NVvPsNDChrLRH5K2cy6Sc9r1uuUwdiZQd is Dogami NFT
#KT1CpMKxKFoHvS7zxSd2M6SHU8BD4gBmtw2N is Dogami NFT sale
#KT1TwzD6zV3WeJ39ukuqxcfK2fJCnhvrdN1X is Smartlink
#KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b is Plenty
#KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb is Quipuswap
#KT1ERjU9W6jzZkq8v6caxu65pQkamv1ptn35 is an FA2
#KT1Ha4yFVeyzw6KRAdkzq6TxDHB97KG4pZe8 is an FA1.2

MICHELSON_CONTRACTS=$(ls test_contracts/*.json)
echo $MICHELSON_CONTRACTS
# "test_contracts/main_checker.json
# test_contracts/main_segmented_cfmm.json"

INDEX=0
for c in $KT1_CONTRACTS; do
    $FACTORI import_kt1 --kt1 $c toto$INDEX --dir test
    INDEX=$((${INDEX}+1))
done

INDEX=0
for c in $MICHELSON_CONTRACTS; do
    NAME=$(echo "$c" | cut -f 1 -d '.')
    NAME=$(basename $NAME)
    echo $FACTORI import_michelson $c  --name contract_$NAME --dir test
    $FACTORI import_michelson $c --name contract$NAME --dir test
    INDEX=$((${INDEX}+1))
done
