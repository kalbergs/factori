# Local network with Flextesa

You can start a [Flextesa sandbox](https://tezos.gitlab.io/flextesa/)
using Factori. This is very useful because you can make it as fast as
one block per second; moreover, you have full access to an `alice`
account with a large amount of funds (every generated SDK knows how to
use this account). Run

```shell
factori sandbox start
```

to start it, and

```shell
factori sandbox stop
```

to stop it.

There is a `--block-interval` option which will set the number of
seconds between blocks (default is `1`).

You can monitor your Flextesa sandbox visually using [Explorus](explorus.md).
