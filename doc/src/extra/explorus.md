# Explorus Interface

Explorus is a full consensus explorer and light block explorer by
Functori. It is very useful to monitor what is happening on any Tezos
blockchain, and in particular a sandbox blockchain for which you have
no out of the box explorer. Simply open
[https://explorus.functori.com/](https://explorus.functori.com/), go
to the dented wheel on the top right corner, and enter the URL of your
sandbox node. If you are using a default Flextesa setup, this will be
[http://localhost:20000](http://localhost:20000).
