# Python

<img src="../factory_snake.png" alt="factory snake" style="max-width:50%; height:auto;">

Let's generate a Python SDK for
[KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf](https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf)
and interact with it. See section [SDK generation](sdk_generation.md)
for the details about this contract. The Python SDK relies on [the
Pytezos library](https://pytezos.org/) for forging and signing
operations.


## Import a KT1 from the Tezos blockchain

```shell
factori import kt1 my_dir KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf \
    --python \
    --name my_contract \
    --network https://explorus-ghostnet-node.functori.com \
    --force
```
## Compile deps and SDK

You want to install the necessary Python dependencies:

```shell
pip install pytezos mypy pyright black
```
now cleanly format the generated code:

```shell
make -C my_dir format-python
```

You are ready to use the Python SDK!

## Hello world example

First, you may want to have a look at the content of
`my_dir/src/python_sdk/` directory.

```shell
ls my_dir/src/python_sdk/
```

Create a file `my_dir/src/python_sdk/test.py` with the following content:

```python
from importlib.metadata import metadata
import time
import my_contract_python_interface
import blockchain

def main():
    debug = False
    hello_kt1 = "KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf"

    hello_param: my_contract_python_interface.Hello = "Hello from Python"
    ophash = my_contract_python_interface.callHello(
        hello_kt1,
        _from=blockchain.alice,
        param=hello_param,
        networkName="ghostnet",
        debug=debug,
    )
    print("A hello operation is sent to a node. Its hash is: " + ophash)
    print("Waiting for its inclusion in a block ...")
    time.sleep(15)  # we need to wait for one block before calling the contract
    print("Check the status of your operation: https://ghostnet.tzkt.io/" + ophash)

main()
```

To run it, simply run:

```shell
python3 my_dir/src/python_sdk/test.py
```

If everything goes well, you should see your transaction at:
[https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf](https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf)
