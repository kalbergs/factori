# C\#

Let's generate a C# SDK for
[KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf](https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf)
and interact with it. See section [SDK generation](sdk_generation.md)
for the details about this contract. The C# SDK relies on [the Netezos
library](https://netezos.dev/) for forging and signing operations.

## Import a KT1 from the Tezos blockchain

```shell
factori import kt1 my_dir KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf \
    --csharp \
    --name my_contract \
    --network https://explorus-ghostnet-node.functori.com \
    --force
```

## Compile deps and SDK

You need to have `dotnet` and the Netezos package installed.
For resources, look at:

- [Dotnet](https://learn.microsoft.com/en-us/dotnet/core/install/)
- https://netezos.dev/
- https://github.com/baking-bad/netezos.

now initiate the C# project and cleanly format the generated code:

```shell
make -C my_dir csharp-init
make -C my_dir format-csharp
```

You are ready to use the C# SDK!

## Hello world example

First, you may want to have a look at the content of
`my_dir/src/csharp_sdk/` directory.

```shell
ls my_dir/src/csharp_sdk/
```

Edit the file `my_dir/src/csharp_sdk/Program.cs` with the following
content:

```csharp
using static Blockchain.Identities;

async static Task main()
{
    var hello_kt1 = "KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf";

    var ophash = await my_contract.Functions.CallHello(aliceFlextesa, hello_kt1, "Hello from C#", 1000000, 100000, 30000, 1000000, "ghostnet", false);
    Console.WriteLine($"A hello operation is sent to a node. Its hash is: {ophash}");
    Console.WriteLine("Waiting for its inclusion in a block ...");
    await Task.Delay(15000); // wait 15 seconds
    Console.WriteLine($"Check the status of your operation: https://ghostnet.tzkt.io/{ophash}");
}

await main();
```

To run it, simply run:

```shell
dotnet run --project my_dir/src/csharp_sdk/
```

If everything goes well, you should see your transaction at:
[https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf](https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf)
