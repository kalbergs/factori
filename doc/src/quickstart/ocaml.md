# OCaml

<img src="../circle_factory.png" alt="circle factory" style="max-width:50%; height:auto;">

Let's generate a OCaml SDK for
[KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf](https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf)
and interact with it. See section [SDK generation](sdk_generation.md)
for the details about this contract. The OCaml SDK relies on [the
tzfunc library](https://gitlab.com/functori/tzfunc) for forging and
signing operations.


## Import a KT1 from the Tezos blockchain

```shell
factori import kt1 my_dir KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf \
    --ocaml \
    --name my_contract \
    --network https://explorus-ghostnet-node.functori.com \
    --force
```
## Compile deps and SDK

You want to install the necessary OCaml dependencies. You need to be
inside an `opam` switch, or you can create one with `make -C my_dir/
_opam`.

```shell
make -C my_dir deps
```
now cleanly format the generated code:

```shell
make -C my_dir format-ocaml
```

You are ready to use the OCaml SDK!

## Hello world example

First, you may want to have a look at the content of
`my_dir/src/ocaml_sdk/`, `my_dir/src/ocaml_scenarios/` and `my_dir/src/ocaml_sdk/` directories:

```shell
tree my_dir/src/ocaml_sdk/ my_dir/src/ocaml_scenarios/ my_dir/src/ocaml_sdk/
```

Edit the file `my_dir/src/ocaml_scenarios/scenario.ml` with the following content:

```ocaml
module M = My_contract_ocaml_interface
module B = Blockchain

let main () =
  let open Tzfunc.Rp in
  let hello_kt1 = "KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf" in
  let>? oph =
    M.call_hello
      ~from:B.alice_flextesa
      ~kt1:hello_kt1
      ~node:B.ghostnet_node
      "Hello from OCaml"
  in
  Format.eprintf "A hello operation was sent to the node, with hash %s@." oph ;
  Format.eprintf
    "Check the status of your operation: https://ghostnet.tzkt.io/%s@."
    oph ;
  Lwt.return_ok oph

let _ = Tzfunc.Node.set_silent true

let _ = Lwt_main.run @@ main ()
```

To run it, simply run:

```shell
make -C my_dir/ run_scenario_ocaml
```

If everything goes well, you should see your transaction at:
[https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf](https://ghostnet.tzkt.io/KT1MGXm7tBbvoWeZ44s1FxsbxS2wbLGkX1Mf)
