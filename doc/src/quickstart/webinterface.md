# Web Interface

Factori can generate a web interface from a KT1:

```shell
factori import kt1 . KT1UcszCkuL5eMpErhWxpRmuniAecD227Dwp --name my_rps --web
```


To generate the web page, run:

```shell
make ts-deps
make web
```

and click on the link to the local web page.
