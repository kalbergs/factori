# Michelson Linting

<img src="../michalson.png" alt="Michalson" style="max-width:50%; height:auto;">

Factori offers some linting capabilities on Michelson contracts. This
feature is in a beta state, the documentation will soon be updated to
detail how it works.
