# Scenarios

Factori allows you to write scenarios for your smart contracts.

- [Simple scenarios](../scenarios.md)
- [Advanced scenarios](../advanced_scenarios.md)

