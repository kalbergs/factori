# Sandbox

Factori provides a wrapper to quickly launch (and stop) a flextesa
sandbox from Docker. You will need to have Docker installed.

- `sandbox start [OPTION]…`
  - Start a Flextesa sandbox. You will need docker installed as well as proper permissions (see for instance https://docs.docker.com/engine/install/linux-postinstall/)

- `sandbox stop`
  - Stop the Flextesa sandbox started with Factori.
