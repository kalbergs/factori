# Import Michelson

Import a contract using its Michelson compiled form. It can be in
either a Json or normal Michelson format. The options are similar to
`import kt1`:

```shell
factori import michelson [OPTION]… [DIRECTORY] [MICHELSON_FILE]
```

- `--crawlori`
  - If activated, a crawlori plugin that can crawl the smart contract(s) will be generated (needs ocaml option).

- `--csharp`
  - If activated, a C# interface for the smart contract(s) will be generated.

- `--db-name=db-name`
  - This is the name for the psql database (default is project directory basename).

- `-f, --force`
  - If set to true, Factori will overwrite the input folder for commands that write to files. Defaults to false.

- `--field_prefixes=field_prefixes`
  - Determines whether record fields are prefixed by the entrypoint name to disambiguate when several entrypoints have the same parameter names.

- `--library`
  - Determines whether the OCaml code is generated only as a library.

- `--name=CONTRACT_NAME`
  - Provide the contract name for which you would like to perform the operation.

- `--ocaml`
  - If activated, an OCaml interface for the smart contract(s) will be generated.

- `--project_name=PROJECT_NAME`
  - Provide a project name. This will be used e.g. for the opam file name.

- `--python`
  - If activated, a Python interface for the smart contract(s) will be generated.

- `-q, --quiet`
  - Set verbosity level to 0.

- `--typescript`
  - If activated, a Typescript interface for the smart contract(s) will be generated.

- `-v, --verbose`
  - Increase verbosity level.

- `--web`
  - Determines whether the Web page of the smart contract is generated.
