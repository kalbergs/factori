# Empty project

Importing a contract into an empty folder will create a new project.
However, in some cases, you may want to first create an empty project
and e.g. install all dependencies to gain time before you actually
start working with contracts. In this case, the command `factori empty
project`, with similar options as `factori import kt1` or `factori
import michelson` will be useful.

- `--crawlori`
  - If activated, a crawlori plugin that can crawl the smart contract(s) will be generated (needs ocaml option).
- `--csharp`
  - If activated, a C# interface for the smart contract(s) will be generated.
- `--db-name=db-name`
  - This is the name for the psql database (default is project directory basename).
- `-f, --force`
  - If set to true, Factori will overwrite the input folder for commands that write to files. Defaults to false.
- `--library`
  - Determines whether the OCaml code is generated only as a library.
- `--ocaml`
  - If activated, an OCaml interface for the smart contract(s) will be generated.
- `--python`
  - If activated, a Python interface for the smart contract(s) will be generated.
- `-q, --quiet`
  - Set verbosity level to 0.
- `--typescript`
  - If activated, a Typescript interface for the smart contract(s) will be generated.
- `-v, --verbose`
  - Increase verbosity level.
- `--web`
  - Determines whether the Web page of the smart contract is generated.
