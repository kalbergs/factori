# Deploy and Deploy-clean

## Deploy

This command enables you to quickly deploy one of your imported
contracts. If you want to do a controlled, refined deploy, you should
probably not use this command and write a scenario (in one of our 4
supported languages).

- `-f, --force`
  - If set to true, Factori will overwrite the input folder for commands that write to files. Defaults to false.

- `--network=network`
  - Specify on which network (mainnet,ithacanet,etc...) you would like to perform the operation.

- `--ocaml`
  - The contract will be deployed using an OCaml scenario (only use this if you have imported an OCaml interface)

- `-q, --quiet`
  - Set verbosity level to 0

- `--storage=storage`
  - Storage type used for the `factori deploy` command.

- `--typescript`
  - If activated, a Typescript interface for the smart contract(s)
    will be generated (only use this if you have imported a Typescript
    interface; in doubt between OCaml and Typescript, prefer
    Typescript which is simpler to set up).

- `-v, --verbose`
  - Increase verbosity level.

## Deploy-clean

The `factori deploy-clean` command will clean up the files generated
by the deploy command for the purpose of originating your contract. It
can be useful if these files are interfering with your development.
