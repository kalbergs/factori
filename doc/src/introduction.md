# Introduction
## Factori: Automatic Code Generation for Smarter Smart Contract Development

![factori](factori.png)

Welcome to [Factori](https://gitlab.com/functori/dev/factori), a tool
designed around the needs of Tezos smart contract developers based on
our long experience in this domain. Our goal is to automate, as much
as possible, the generation of boilerplate code so that you can focus
on writing, testing and deploying your smart contract. Factori's core
feature is the generation of a Typescript/OCaml SDK from any Michelson
smart contract, whether you wrote it yourself or found it on the Tezos
blockchain.

When you use Factori, you will have a working directory, which we will
sometimes call the Factori project directory. Whenever we give
relative paths (.e.g `src/python_sdk`), the will implicitly always be
given relative to this working directory.

For the installation process, see: [Installation](installation.md).
