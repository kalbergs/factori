# Quickstart

![Modernist factories](modernist_factories.png)

## Generating a SDK

Factori can generate SDKs in several languages:
[Typescript](quickstart/typescript.md), [OCaml](quickstart/ocaml.md),
[C#](quickstart/csharp.md) and [Python](quickstart/python.md).

## Generating a web interface (simple Dapp)

Factori can also generate a [Web
Interface](quickstart/webinterface.md) for viewing and interacting
with your smart contract using any standard wallet.

## Generate a (contract-specific) configuration for a blockchain crawler

Factori can also generate code for the use of blockchain crawlers such
as [DipDup](quickstart/dipdup.md) or
[Crawlori](quickstart/crawlori.md).
