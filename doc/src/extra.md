# Extra

In all likelihood, you won't be using Factori in isolation. In this
section we describe how to use the [Flextesa](extra/flextesa.md) local
network and the [Explorus](extra/explorus.md) blockchain interface in
conjunction with Factori.
