# Features

![Japanese modernist factories](japanese_modernist_factories.png)

In this section, we give an overview of Factori's features. (Note that
the next section gives quick tutorials for try out each feature.) Here
is a list of subsections of the current section:

- [SDK generation](features/sdk_generation.md)
- [Crawler configuration generation](features/crawler_configuration_generation.md)
- [Web App generation](features/web_app_generation.md)
- [Scenarios](features/scenarios.md)
- [Michelson Linting](features/michelson_linting.md)
