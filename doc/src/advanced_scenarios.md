# Advanced scenarios

For advanced uses, Factori comes with a Domain Specific Language (DSL)
for scenarios. These uses are typically:

- needing to compile scenarios down to several languages (e.g., OCaml
  and Typescript);
- needing to execute several scenarios in parallel.

To learn more about this DSL, check out our extensive [blog article on
the
topic](https://functori.com/blog/blog-factori-tutorial-for-scenarios.html).
