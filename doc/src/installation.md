# Installation

Factori could be installed in 2 ways.
- Using a docker image, see [this section](installation/docker.md).
- Directly from source by using opam, see [this section](installation/source.md).
