# From source

To install Factori from source code you need to have opam installed in your machine.
## Opam
Check [this link](https://opam.ocaml.org/doc/Install.html) to install opam.
### (Optional) Opam switch
If you want to have a local opam switch for factori, just run:
```shell
make opam-switch
```
## Dependencies
Once opam installed, install all dependencies by running:
```shell
make build-deps
```
## Build
To build factori binaries, run:
```shell
make
```

## Install
You can install Factori by using:
```shell
make install
```

This will, by default, copy the binary `factori.asm` to `~/.local/bin/factori`.
If you want to customize the installation just edit the `Makefile`.
