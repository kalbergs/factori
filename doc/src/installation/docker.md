# Installation using Docker

## Docker pull
You can the latest version of factori using docker by running:
```shell
docker pull functori/factori:latest
```

If you would like to use a previous version, e.g. `0.3.1`, you can run:
```shell
docker pull functori/factori:0.3.1
```

## Script
To use factori with docker, we advise using
[this script](https://gitlab.com/-/snippets/2345857/raw/main/factori.sh)
as follows :
```shell
wget https://gitlab.com/-/snippets/2345857/raw/main/factori.sh -O factori
chmod +x factori
```
