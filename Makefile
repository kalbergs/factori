BUILD=dune
FACTORI_BUILD=./_build/default/src/factori
BIN=_bin
SCRIPTS=scripts
GIT=$(shell git log --pretty=format:'%H' -n 1)
RELEASE_VERSION=0.5.2.2

.PHONY: test test-run

all: version build copy-binaries

format:
	@dune build @fmt --auto-promote 2> /dev/null || exit 0

install: all
	dune install
	@if [ -d "${HOME}/.local/bin" ]; \
	then \
		cp -f $(BIN)/factori.asm ${HOME}/.local/bin/factori; \
		echo "Factori is installed inside ${HOME}/.local/bin/factori"; \
	else \
		echo "The default directory ${HOME}/.local/bin does not exist"; \
		read -p "Provide a directory where factori will be installed(provide full path): " INSTALL_DIR; \
		cp -f $(BIN)/factori.asm $$INSTALL_DIR/factori; \
	fi \

build:
	$(BUILD) build

opam-switch:
	@opam switch create . ocaml-base-compiler.4.14.1 -y --no-install

build-deps:
	./scripts/install-deps.sh

# build-dev-deps:
# 	@opam install . --deps-only -y

copy-binaries:
	@mkdir -p $(BIN)
	@cp -f $(FACTORI_BUILD)/main.exe $(BIN)/factori.asm

test-build-deps-ocaml-only:
	@make -C test/ deps

test-build-deps:
	@make -C test/ deps ts-deps

test-init:
	@mkdir -p test
	./_build/default/scripts/test_init.exe
	./_build/default/scripts/test.exe

test-python-format:
	@make -C test format-python

test-python-static:
	@make -C test python-static-check

test-python-compile:
	@make -C test python-compile

test-ts:
	@make -C test/ ts
	@node test/src/typescript_sdk/dist/test.js

test-run: version
	@make -C test/
	@dune exec test/src/tests/tests.exe

test-clean:
	-make -C test/ drop
	@rm -rf ./test/*
	@rm -rf ./testweb/*

test-ocaml-only: version test-clean test-init test-build-deps-ocaml-only test-run

test: test-clean test-init test-build-deps test-ts test-run

clean:
	$(BUILD) clean

cleanall: clean
	@rm -f _bin/factori.asm

lint:
	@scripts/lint.sh

version:
	@echo "let version = \"$(RELEASE_VERSION)\"" > src/factori/version.ml
	@echo "let commit = \"$(GIT)\"" >> src/factori/version.ml

documentation-check:
	@mlc doc/factori-docs/

documentation:
	@mdbook build doc --dest-dir factori-docs

documentation-serve:
	@mdbook serve --open doc --dest-dir factori-docs
